package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.adapters.lists.RecruitmentForApprovalAdapter;
import applusvelosi.projects.android.salt2.models.recruitments.Recruitment;
import applusvelosi.projects.android.salt2.views.RecruitmentApprovalDetailActivity;

/**
 * Created by Velosi on 10/9/15.
 */
public class HRForApprovalFragment extends RootFragment implements AdapterView.OnItemClickListener, TextWatcher {
    private static HRForApprovalFragment instance;

    private RelativeLayout actionbarMenuButton, actionbarRefreshButton;

    private List<Recruitment> recruitments,tempRecruitments;
    private ListView lv;
    private RecruitmentForApprovalAdapter adapter;
    private EditText etextSearchByName;

//    private int selectedPos;
    public static HRForApprovalFragment getInstance(){
        if(instance == null)
            instance = new HRForApprovalFragment();
        return instance;
    }

    public static void removeInstance(){
        if(instance != null)
            instance = null;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_menurefresh, null);
        actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
        actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
        ((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("HR for Approval");
        actionbarMenuButton.setOnClickListener(this);
        actionbarRefreshButton.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_listview, null);
        lv = (ListView)v.findViewById(R.id.lists_lv);
        recruitments = new ArrayList<Recruitment>();
        tempRecruitments = new ArrayList<Recruitment>();
        etextSearchByName = (EditText)v.findViewById(R.id.etexts_itemforapproval_name);
        etextSearchByName.setVisibility(View.VISIBLE);
        etextSearchByName.addTextChangedListener(this);

        adapter = new RecruitmentForApprovalAdapter(activity, tempRecruitments);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        syncToServer();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
//        filteredRecruitments();
        syncToServer();
    }

    private void syncToServer(){
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempResult;
                try{
                    tempResult = app.onlineGateway.getRecruitmentsForApproval();
                }catch (Exception e){
                    e.printStackTrace();
                    tempResult = e.getMessage();
                }
                final Object result = tempResult;
                new Handler((Looper.getMainLooper())).post(new Runnable() {
                    @Override
                    public void run() {
                        if(result instanceof  String)
                            activity.finishLoading(result.toString());
                        else {
                            activity.finishLoading();
                            recruitments.clear();
                            recruitments.addAll((ArrayList<Recruitment>)result);
                            filteredRecruitments();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }).start();
    }

    private void filteredRecruitments(){
        tempRecruitments.clear();
        if (etextSearchByName.getText().length() > 0){
            for (Recruitment recruitment : recruitments){
                if (recruitment.getRequesterName().toLowerCase().contains(etextSearchByName.getText().toString().toLowerCase()) || etextSearchByName.getText().length() < 1)
                    tempRecruitments.add(recruitment);
            }
        }else
            tempRecruitments.addAll(recruitments);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if(v == actionbarMenuButton){
            actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
            activity.toggleSidebar(actionbarMenuButton);
        }else if(v == actionbarRefreshButton){
            syncToServer();
        }
    }

    @Override
    public void disableUserInteractionsOnSidebarShown() {
        lv.setEnabled(false);
        etextSearchByName.setEnabled(false);
    }

    @Override
    public void enableUserInteractionsOnSidebarHidden() {
        try{
            lv.setEnabled(true);
        }catch(NullPointerException e){
            System.out.println("Null pointer exception at LeaveListFragmentOLD enableUserInteractionOnSidebarHidden()");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            Intent intent = new Intent(activity, RecruitmentApprovalDetailActivity.class);
            intent.putExtra(RecruitmentApprovalDetailActivity.INTENTKEY_RECRUITMENTID, tempRecruitments.get(position).getRecruitmentRequestID());
            startActivity(intent);
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filteredRecruitments();
    }
}
