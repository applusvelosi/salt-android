package applusvelosi.projects.android.salt2.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;


public class SplashActivity extends Activity {
	public static final String KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN = "pendingrootfragtoopen"; //value for this map should be int
	SaltApplication app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (SaltApplication)getApplication();
		setContentView(R.layout.activity_splash);
	}

	@Override
	protected void onResume() {
		super.onResume();

		DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
		ref.addValueEventListener(new ValueEventListener() {

			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {

				Map<String, String> contents = new HashMap<String, String>();
				for (DataSnapshot data: dataSnapshot.getChildren()) {
					contents.put(data.getKey(), data.getValue().toString());
				}

				String localVersion;
				try {
					localVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
					((TextView)findViewById(R.id.tviews_splash_saltversion)).setText("SALT 2016 " + localVersion);

					String latestVersion = contents.get("androidVersion");
					System.out.println("COMPARE CODE: " + app.versionCompare(localVersion,latestVersion));
					if(app.versionCompare(localVersion,latestVersion) < 0){
						try {
							new AlertDialog.Builder(SplashActivity.this).setTitle("Update required").
									setMessage("New version available. Please update with the latest version")
									.setPositiveButton("Update", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=applusvelosi.projects.android.salt2")));
										}
									})
									.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											finish();
										}
									}).create().show();
						}catch(Exception e){
							e.printStackTrace();
						}
					}else{
						startApp();
					}
				} catch (PackageManager.NameNotFoundException e) {
					e.printStackTrace();
					((TextView)findViewById(R.id.tviews_splash_saltversion)).setText("SALT 2016");
					startApp();
				}
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
				System.out.println("Failed to read value: " + databaseError.getMessage());
			}
		});
	}

//	private int versionCompare(String appVersion, String firebaseVersion) {
//		String[] vals1 = appVersion.split("\\.");
//		String[] vals2 = firebaseVersion.split("\\.");
//		int i = 0;
//		// set index to first non-equal ordinal or length of shortest version string
//		while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
//			i++;
//		}
//		// compare first non-equal ordinal number
//		if (i < vals1.length && i < vals2.length) {
//			int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
//			return Integer.signum(diff);
//		}
//		// the strings are equal or one string is a substring of the other
//		// e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
//		return Integer.signum(vals1.length - vals2.length);
//
//	}

	private void startApp(){
		Tracker t = ((SaltApplication)getApplication()).getTracker(SaltApplication.TrackerName.APP_TRACKER);
		GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName(getString(R.string.app_name));

		// Send a screen view.
		t.send(new HitBuilders.ScreenViewBuilder().build());
		System.out.println("Sent request!");


		new Thread(new Runnable() {
			@Override
			public void run() {
				String tempError = null;
				try {
					app.initializeApp();
				}catch(Exception e){
					tempError = e.getMessage();
				}

				final String error = tempError;
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						if(error == null){
							Intent intent;
							if(((SaltApplication)getApplication()).hasSavedData()){
								intent = new Intent(SplashActivity.this, HomeActivity.class);
								if(getIntent().hasExtra(KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN))
									intent.putExtra(HomeActivity.KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN, getIntent().getExtras().getInt(KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN));
							}else
								intent = new Intent(SplashActivity.this, LoginActivity.class);

							startActivity(intent);
							finish();
						}else
							app.showMessageDialog(SplashActivity.this, error);
					}
				});
			}
		}).start();
	}
}
