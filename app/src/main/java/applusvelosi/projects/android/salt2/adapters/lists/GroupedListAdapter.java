package applusvelosi.projects.android.salt2.adapters.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import applusvelosi.projects.android.salt2.utils.interfaces.GroupedListItemInterface;
import applusvelosi.projects.android.salt2.views.HomeActivity;

public class GroupedListAdapter extends BaseAdapter{
	private List<GroupedListItemInterface> items;
	private HomeActivity activity;
	
	public GroupedListAdapter(HomeActivity activity, List<GroupedListItemInterface> items){
		this.activity = activity;
		this.items = items;
	}
	
	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		return items.get(pos).getTextView(activity);
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int pos) {
		return items.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return 0;
	}
}
