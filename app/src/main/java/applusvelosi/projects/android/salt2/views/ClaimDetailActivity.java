package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.models.claimitems.Project;
import applusvelosi.projects.android.salt2.views.fragments.claims.ClaimHeaderBAFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.ClaimHeaderClaimFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.ClaimHeaderLiquidationFragment;

/**
 * Created by Velosi on 11/18/15.
 */
public class ClaimDetailActivity extends LinearNavFragmentActivity {

    public static final String INTENTKEY_CLAIMHEADER = "claimheaderkey";
    public static final String INTENTKEY_CLAIMFORAPPROVAL = "claimheaderforapprovalkey";
    public static final String INTENTKEY_NEWCLAIMHEADERPOS = "newclaimheaderkey";

    public ClaimHeader claimHeader;
    public int claimHeaderID;
    public List<ClaimItem> claimItems;
    public boolean shouldLoadLineItemOnResume = true;
    public List<Project> projects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra(INTENTKEY_CLAIMHEADER))
            claimHeader = (ClaimHeader) getIntent().getExtras().getSerializable(INTENTKEY_CLAIMHEADER);
        else if (getIntent().hasExtra(INTENTKEY_NEWCLAIMHEADERPOS))
            claimHeader = app.getMyClaims().get(getIntent().getExtras().getInt(INTENTKEY_CLAIMHEADER));
        else if (getIntent().hasExtra(INTENTKEY_CLAIMFORAPPROVAL))
            claimHeader = (ClaimHeader)getIntent().getExtras().getSerializable(INTENTKEY_CLAIMFORAPPROVAL);


        try {
            if (claimHeader != null) {
                claimHeaderID = claimHeader.getClaimID();
                if (claimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS)
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimHeaderClaimFragment()).commit();
                else if (claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES)
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimHeaderBAFragment()).commit();
                else
                    getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimHeaderLiquidationFragment()).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        claimItems = app.offlineGateway.deserializeMyClaimItems(claimHeader.getClaimID());
    }

    public void updateProjectList(List<Project> projects) {
        this.projects = new ArrayList<>();
        this.projects.addAll(projects);
        Collections.sort(projects, new Comparator<Project>() {
            @Override
            public int compare(Project lhs, Project rhs) {
                return lhs.getProjectName().toLowerCase().compareTo(rhs.getProjectName().toLowerCase());
            }
        });
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
}
