package applusvelosi.projects.android.salt2.models.claimheaders;

import org.json.JSONObject;

import java.util.List;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;

public class ClaimPaidByCC extends Claim{

	public ClaimPaidByCC(SaltApplication app, int costCenterID, String costCenterName) throws Exception{
		super(app, costCenterID, costCenterName, true);
	}

	public ClaimPaidByCC(SaltApplication app, int costCenterID, String costCenterName, int hasYesNoDoc, List<Document> docs) throws Exception{
		super(app, hasYesNoDoc, docs, costCenterID, costCenterName, true);
	}

	public ClaimPaidByCC(JSONObject jsonClaimPaidbyCC) throws Exception{
		super(jsonClaimPaidbyCC);
	}

	public ClaimPaidByCC(ClaimHeader claimPaidByCC){
		super(claimPaidByCC);
	}

	public ClaimPaidByCC(){

	}

	public boolean isPaidByCC(){
		return true;
	}

	public float getTotalComputedForDeductionInLC(){
		return totalComputedForDeductionInLC;
	}

}
