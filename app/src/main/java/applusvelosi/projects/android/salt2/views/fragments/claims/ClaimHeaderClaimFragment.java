package applusvelosi.projects.android.salt2.views.fragments.claims;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.claimheaders.Claim;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimNotPaidByCC;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimPaidByCC;

public class ClaimHeaderClaimFragment extends ClaimHeaderFragment {

	@Override
	protected String getActionbarTitle() {
		return "Claim Header";
	}

	@Override
	protected View getInflatedLayout(LayoutInflater inflater) {
		claim = (Claim) claimHeader;
		if (claim.isPaidByCC()) //claimHeader paid by company card
			return inflater.inflate(R.layout.fragment_claimdetails_claimpaidbycc, null);
		else
			return inflater.inflate(R.layout.fragment_claimdetails_claimnotpaidbycc, null);
	}

	@Override
	protected void initAdditionalViewComponents(View view) throws Exception {
		if (claim.isPaidByCC()) { //claimHeader paid by company card
			ClaimPaidByCC claimPaidByCC = (ClaimPaidByCC) claimHeader;
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_id)).setText(String.valueOf(claimHeader.getClaimNumber()));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_staff)).setText(claimHeader.getStaffName());
			tvCostCenter = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_costcenter);
			tvCostCenter.setText(claimHeader.getCostCenterName());
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_approver)).setText(claimHeader.getApproverName());

			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_type)).setText(ClaimHeader.getTypeDescriptionForKey(claimHeader.getTypeID()));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_status)).setText(ClaimHeader.getStatusDescriptionForKey(claimHeader.getStatusID()));

			tvNotes = (TextView) view.findViewById(R.id.tviews_claimforapproval_approvednote);
//			((TextView)view.findViewById(R.id.tviews_claimdetail_claim_total)).setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalAmountInLC())));
			tvTotalAmtPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_total);
			tvTotalAmtPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalAmount())));
			tvAmountApprovedPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_amountApproved);
			tvAmountApprovedPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalComputedApprovedInLC())));
			tvAmountRejectedPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_amountRejected);
			tvAmountRejectedPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalComputedRejectedInLC())));
			tvTotalForDeduction = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_forDeduction);
			tvTotalForDeduction.setText(String.valueOf(claimPaidByCC.getTotalComputedForDeductionInLC()));
			tvTotalForPaymentPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_forPayment);
			tvTotalForPaymentPaidByCC.setText(String.valueOf(claimHeader.getTotalComputedForPaymentInLC()));

			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_datesubmitted)).setText(claimHeader.getDateSubmitted(app));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_onApprover)).setText(claimHeader.getDateApprovedByApprover(app));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_onAccount)).setText(claimHeader.getDateApprovedByAccount(app));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_datepaid)).setText(claimHeader.getDatePaid(app));
		} else {
			ClaimNotPaidByCC claimNotPaidByCC = (ClaimNotPaidByCC) claimHeader;
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_id)).setText(String.valueOf(claimHeader.getClaimNumber()));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_staff)).setText(claimHeader.getStaffName());
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_costcenter)).setText(claimHeader.getCostCenterName());
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_approver)).setText(claimHeader.getApproverName());
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_parentclaim)).setText(claimNotPaidByCC.getParentClaimNumber());

			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_type)).setText(ClaimHeader.getTypeDescriptionForKey(claimHeader.getTypeID()));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_status)).setText(ClaimHeader.getStatusDescriptionForKey(claimHeader.getStatusID()));

			tvNotes = (TextView) view.findViewById(R.id.tviews_claimforapproval_approvednote);
			tvTotalAmtNotPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_total);
			tvTotalAmtNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalAmount())));
			tvAmountApprovedNotPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_amountApproved);
			tvAmountApprovedNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalComputedApprovedInLC())));
			tvAmountRejectedNotPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_amountRejected);
			tvAmountRejectedNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(claimHeader.getTotalComputedRejectedInLC())));
			tvTotalForPaymentNotPaidByCC = (TextView) view.findViewById(R.id.tviews_claimdetail_claim_forPayment);
			tvTotalForPaymentNotPaidByCC.setText(String.valueOf(claimHeader.getTotalComputedForPaymentInLC()));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_datesubmitted)).setText(claimHeader.getDateSubmitted(app));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_onApprover)).setText(claimHeader.getDateApprovedByApprover(app));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_onAccount)).setText(claimHeader.getDateApprovedByAccount(app));
			((TextView) view.findViewById(R.id.tviews_claimdetail_claim_datepaid)).setText(claimHeader.getDatePaid(app));
		}
		attachment = (TextView)view.findViewById(R.id.tviews_claimdetail_claim_attachment);
		containerLineItem = (RelativeLayout) view.findViewById(R.id.containers_claimheader_lineitems);
		containerLineItem.setOnClickListener(this);
		tvLineItemCnt = (TextView) view.findViewById(R.id.tviews_claimdetail_lineitemcnt);

		stringNotes = new StringBuilder();
		if(!claimHeader.getApproversNote().isEmpty()) {
			String tempStrings[] = claimHeader.getApproversNote().split(";");
			for (String str : tempStrings) {
				stringNotes.append(str);
				stringNotes.append(System.getProperty("line.separator"));
			}
		} else {
			stringNotes.append(claimHeader.getApproversNote());
		}

		tvNotes.setText(stringNotes.toString());
		if (claimHeader.getAttachments().size() > 0) {
			attachment.setOnClickListener(this);
			attachment.setText(claimHeader.getAttachments().get(0).getDocName());
			attachment.setTextColor(activity.getResources().getColor(R.color.orange));
		} else {
			attachment.setTextColor(activity.getResources().getColor(R.color.blue));
			if (activity.getIntent().hasExtra(activity.INTENTKEY_CLAIMHEADER)) {
				attachment.setText("ADD ATTACHMENT");
				attachment.setOnClickListener(this);
			}else if (activity.getIntent().hasExtra(activity.INTENTKEY_CLAIMFORAPPROVAL)) {
				attachment.setText("NO ATTACHMENT");
				attachment.setOnClickListener(null);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}
}
