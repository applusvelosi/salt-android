package applusvelosi.projects.android.salt2.utils.threads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.utils.interfaces.LoaderStatusInterface;
import applusvelosi.projects.android.salt2.views.ManageClaimItemActivity;

/**
 * Created by Velosi on 12/22/15.
 */

public class ProjectsLoader implements Runnable{
    SaltApplication app;
    ManageClaimItemActivity activity;
    LoaderStatusInterface inf;

    public ProjectsLoader(Context context, LoaderStatusInterface inf){
        this.inf = inf;
        this.activity = (ManageClaimItemActivity)context;
        app = ((SaltApplication)activity.getApplication());
    }

    @Override
    public void run() {
        Object tempResult;
        try{
            tempResult = app.onlineGateway.getClaimItemProjectsByCostCenter(activity.claimHeader.getCostCenterID());
        }catch(Exception e){
            e.printStackTrace();
            tempResult = e.getMessage();
        }

        final Object result = tempResult;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (result instanceof String)
                    inf.onLoadFailed(result.toString());
                else
                    inf.onLoadSuccess(result);
            }
        });
    }

}
