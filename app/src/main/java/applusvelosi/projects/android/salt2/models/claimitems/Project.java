package applusvelosi.projects.android.salt2.models.claimitems;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Velosi on 11/16/15.
 */
public class Project implements Serializable{

    private boolean isActive;
    private String costCenterCode;
    private int costCenterID;
    private String costCenterName;
    private int officeID;
    private String officeName;
    private String projectCode;
    private int projectID;
    private String projectName;

    public Project(int id, String name){
        this.projectID = id;
        this.projectName = name;
    }

    public Project(JSONObject jsonProject) throws Exception{
        isActive = jsonProject.getBoolean("Active");
        costCenterCode = jsonProject.getString("CostCenterCode");
        costCenterID = jsonProject.getInt("CostCenterID");
        costCenterName = jsonProject.getString("CostCenterName");
        officeID = jsonProject.getInt("OfficeID");
        officeName = jsonProject.getString("OfficeName");
        projectCode = jsonProject.getString("ProjectCode");
        projectID = jsonProject.getInt("ProjectID");
        projectName = jsonProject.getString("ProjectName");
    }

    public boolean isActive(){ return  isActive; }
    public int getCostCenterCode() { return Integer.parseInt(costCenterCode); }
    public int getCostCenterID() { return costCenterID; }
    public String getCostCenterName() { return costCenterName; }
    public int getOfficeID() { return officeID; }
    public String getOfficeName() { return officeName; }
    public int getProjectCode() { return Integer.parseInt(projectCode); }
    public int getProjectId(){ return projectID; }
    public String getProjectName(){ return projectName; }
}
