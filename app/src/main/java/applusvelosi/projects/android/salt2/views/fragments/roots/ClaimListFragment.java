package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.lists.MyClaimsAdapter;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;

public class ClaimListFragment extends RootFragment implements OnItemClickListener{
	private static ClaimListFragment instance;

	//action bar buttons
	private RelativeLayout actionbarMenuButton, actionbarRefreshButton, actionbarNewButton;
	private ListView lv;
	private MyClaimsAdapter adapter;

	public static ClaimListFragment getInstance(){
		if(instance == null)
			instance = new ClaimListFragment();

		return instance;
	}

	public static void removeInstance(){
		if(instance != null)
			instance = null;
	}

	@Override
	protected RelativeLayout setupActionbar() {
        RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_claimlist, null);
		actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
//        actionbarNewButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_newclaim);
		((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("My Claims");
		
		actionbarMenuButton.setOnClickListener(this);
		actionbarRefreshButton.setOnClickListener(this);
//		actionbarNewButton.setOnClickListener(this);

		return actionbarLayout;
	}
	
	@Override
	protected View createView(LayoutInflater li, ViewGroup vg, Bundle b) {
		View view = li.inflate(R.layout.fragment_claimlist, null);
		lv = (ListView)view.findViewById(R.id.lists_myclaims);		
		adapter = new MyClaimsAdapter(activity, app.getMyClaims());
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);

        return view;
	}

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
	public void disableUserInteractionsOnSidebarShown() {
        lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		try{
			lv.setEnabled(true);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void updateList(){
		activity.startLoading();
		new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempResult;
                try{
                    tempResult = app.onlineGateway.getMyClaims(app.getStaff().getStaffID());
                }catch(Exception e){
                    e.printStackTrace();
                    tempResult = e.getMessage();
                }
                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
					if(result instanceof String){
						String errorMessage = (String)result;
						if(errorMessage.contains(SaltApplication.CONNECTION_ERROR)){
							activity.finishLoadingAndShowOutdatedData();
							try{
                                ArrayList<ClaimHeader> tempClaimHeaders = new ArrayList<ClaimHeader>();
                                tempClaimHeaders.clear();
                                for(ClaimHeader myClaims :app.offlineGateway.deserializeMyClaims()){
                                    if(myClaims.getStaffID() == app.getStaff().getStaffID())
                                        tempClaimHeaders.add(myClaims);
                                }
                                Collections.sort(tempClaimHeaders, new Comparator<ClaimHeader>() {
                                    @Override
                                    public int compare(ClaimHeader lhs, ClaimHeader rhs) {
                                        return rhs.getClaimID() - lhs.getClaimID();
                                    }
                                });
                                app.updateMyClaims(tempClaimHeaders);
                                adapter.notifyDataSetChanged();
							}catch(Exception e){
								app.showMessageDialog(activity, e.getMessage());
							}
						}else
							activity.finishLoading(result.toString());
					} else {
						activity.finishLoading();
                        ArrayList<ClaimHeader> tempClaimHeaders = new ArrayList<ClaimHeader>();
                        tempClaimHeaders.clear();
                        for(ClaimHeader myClaims :(ArrayList<ClaimHeader>)result) {
                            if(myClaims.getStaffID() == app.getStaff().getStaffID())
                                tempClaimHeaders.add(myClaims);
                        }

                        Collections.sort(tempClaimHeaders, new Comparator<ClaimHeader>() {
                            @Override
                            public int compare(ClaimHeader lhs, ClaimHeader rhs) {
                                return rhs.getClaimID() - lhs.getClaimID();
                            }
                        });
                        app.updateMyClaims(tempClaimHeaders);
                        adapter.notifyDataSetChanged();
					}
                    }
                });
            }
        }).start();
	}

//	private class FetchAsyncClaimByID extends AsyncTask<Void,Void,String> {
//		private ClaimHeader myClaim;
//		private Object tempResult;
//		public FetchAsyncClaimByID(ClaimHeader claim){
//			myClaim = claim;
//		}
//
//		@Override
//		protected String doInBackground(Void... params) {
//			Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
//			try{
//				tempResult = app.onlineGateway.getMyClaimByID(myClaim.getClaimID());
//
//
//			}catch (Exception e){
//				System.out.println("ERROR FETCHING THE CLAIM");
//				return null;
//			}
//			return null;
//		}
//
//		@Override
//		protected void onProgressUpdate(Void... values) {
//			super.onProgressUpdate(values);
//			activity.startLoading();
//		}
//
//		@Override
//		protected void onPostExecute(String s) {
//			super.onPostExecute(s);
//			activity.finishLoading();
//
//			if (tempResult instanceof String) {
//				if (tempResult.toString().contains(SaltApplication.CONNECTION_ERROR))
//					activity.finishLoading(tempResult.toString());
//			}else {
//				Intent intent = new Intent(activity, ClaimDetailActivity.class);
//				intent.putExtra(ClaimDetailActivity.INTENTKEY_CLAIMHEADER, (ClaimHeader) tempResult);
//				startActivity(intent);
//			}
//		}
//	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		final ClaimHeader claimHeader = app.getMyClaims().get(pos);
		activity.startLoading();
		new Thread(new Runnable() {
			@Override
			public void run() {
				Object tempResult;
				try {
					tempResult = app.onlineGateway.getMyClaimByID(claimHeader.getClaimID());
				} catch (Exception e) {
					tempResult = e.getMessage();
				}
				final Object result = tempResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {

						if (!(result instanceof String)) {
							activity.finishLoading();
							Intent intent = new Intent(activity, ClaimDetailActivity.class);
							intent.putExtra(ClaimDetailActivity.INTENTKEY_CLAIMHEADER, (ClaimHeader)result);
							startActivity(intent);
						} else {
							if (result.toString().contains(SaltApplication.CONNECTION_ERROR))
								activity.finishLoading(result.toString());
						}
					}
				});
			}
		}).start();
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarMenuButton){
			actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
			activity.toggleSidebar(actionbarMenuButton);
//		}else if(v == actionbarNewButton){
//            startActivity(new Intent(activity, NewClaimHeaderActivity.class));
		}else if(v == actionbarRefreshButton){
			updateList();
		}
	}




}
