package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;
import applusvelosi.projects.android.salt2.views.ManageClaimItemActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

public class ClaimItemListFragment extends LinearNavActionbarFragment implements OnItemClickListener, ListAdapterInterface, CompoundButton.OnCheckedChangeListener{
	//action bar buttons
	private RelativeLayout actionbarBackButton, actionbarRefreshButton, actionbarNewButton;
	private TextView actionbarSelectButton, actionbarSelectAllButton, actionbarCancelButton, actionbarRejectButton, actionbarReturnButton, actionbarApproveButton;
	private LinearLayout containerNoSelect, containerSelect;
	private TextView actionbarTitleTextview;

	private ArrayList<Integer> selectedItems;

	private ListView lv;
	private ListAdapter adapter;
	private TextView tvNoItems;
	private ClaimDetailActivity activity;
	private ClaimHeader claimHeader;
	private ClaimItem claimItem;

	private RelativeLayout dialogViewReject, diaglogViewReturn;
	private EditText tviewDialogRejectReason, tviewDialogReturnReason;
	private AlertDialog diaglogReject, diaglogReturn;

//	private Map<Integer,ClaimItem> mapData;
//	private Set<Map<Integer,ClaimItem>> selectedDataItems = new HashSet<Map<Integer, ClaimItem>>();
	@Override
	protected RelativeLayout setupActionbar() {
		activity = (ClaimDetailActivity)getActivity();
		claimHeader = activity.claimHeader;
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_claimitemlist, null);
		actionbarBackButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
		actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarNewButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_add);

		containerNoSelect = (LinearLayout)actionbarLayout.findViewById(R.id.containers_actionbar_claimitems_nonselect);
		containerSelect = (LinearLayout)actionbarLayout.findViewById(R.id.containers_actionbar_claimitems_select);
		actionbarSelectButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_select);
		actionbarSelectAllButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_selectall);
		actionbarCancelButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_cancel);
		actionbarApproveButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_approve);
		actionbarReturnButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_return);
		actionbarRejectButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_reject);
		actionbarTitleTextview = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);

		actionbarBackButton.setOnClickListener(this);
		actionbarRefreshButton.setOnClickListener(this);
		actionbarTitleTextview.setOnClickListener(this);

		actionbarSelectButton.setOnClickListener(this);
		actionbarSelectAllButton.setOnClickListener(this);
		actionbarCancelButton.setOnClickListener(this);
		actionbarRejectButton.setOnClickListener(this);
		actionbarReturnButton.setOnClickListener(this);
		actionbarApproveButton.setOnClickListener(this);

        if(claimHeader.getStatusID() == ClaimHeader.STATUSKEY_OPEN )
            actionbarNewButton.setOnClickListener(this);
        else
            actionbarNewButton.setVisibility(View.GONE);

        if(activity.getIntent().hasExtra(ClaimDetailActivity.INTENTKEY_CLAIMHEADER))
            actionbarSelectButton.setVisibility(View.GONE);
        else if(activity.getIntent().hasExtra(ClaimDetailActivity.INTENTKEY_CLAIMFORAPPROVAL)) {
            actionbarNewButton.setVisibility(View.GONE);
			actionbarReturnButton.setVisibility((!claimHeader.isPaidByCompanyCard())?View.VISIBLE:View.GONE);
//			actionbarReturnButton.setVisibility((((activity.claimHeader.getStatusID()==ClaimHeader.TYPEKEY_CLAIMS && ((Claim)activity.claimHeader).isPaidByCC()) || activity.claimHeader.getStatusID()==ClaimHeader.TYPEKEY_LIQUIDATION) && (activity.claimHeader.getStatusID()==ClaimHeader.STATUSKEY_SUBMITTED || activity.claimHeader.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER) || !claimHeader.isPaidByCompanyCard() || claimHeader.getTypeID() != ClaimHeader.TYPEKEY_ADVANCES)?View.VISIBLE:View.GONE);
        }

        return actionbarLayout;
	}
	
	@Override
	protected View createView(LayoutInflater li, ViewGroup vg, Bundle b) {
		View view = li.inflate(R.layout.fragment_claimitemlist, null);
		lv = (ListView)view.findViewById(R.id.lists_claimItemList);
		selectedItems = new ArrayList<Integer>();
		adapter = new ListAdapter(this);
		tvNoItems = (TextView)view.findViewById(R.id.tviews_listempty);

		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);

		dialogViewReject = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
		tviewDialogRejectReason =  (EditText)dialogViewReject.findViewById(R.id.etexts_dialogs_textinput);
		((TextView)dialogViewReject.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Rejection");
		diaglogReject = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReject)
				.setPositiveButton("Reject", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(tviewDialogRejectReason.getText().length() > 0){
							if(activity.claimHeader.isPaidByCompanyCard())
								updateItemStatus(ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION, ClaimItem.UPDATABLEDATE.DATEREJECTED, tviewDialogRejectReason.getText().toString());
							else {
								if(app.getStaff().getUserPosition()== Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
									updateItemStatus(ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER, ClaimItem.UPDATABLEDATE.DATEREJECTED, tviewDialogRejectReason.getText().toString());
								else
									updateItemStatus(ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER, ClaimItem.UPDATABLEDATE.DATEREJECTED, tviewDialogRejectReason.getText().toString());
							}
						}else
							app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for rejection");
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).create();

		diaglogViewReturn = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
		tviewDialogReturnReason = (EditText)diaglogViewReturn.findViewById(R.id.etexts_dialogs_textinput);
		((TextView) diaglogViewReturn.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Returning");
		diaglogReturn = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(diaglogViewReturn)
				.setPositiveButton("Return", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
							if (tviewDialogReturnReason.getText().length() > 0)
								updateItemStatus(ClaimHeader.STATUSKEY_RETURN, ClaimItem.UPDATABLEDATE.NONE,tviewDialogReturnReason.getText().toString());
							else
								app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for returning");
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).create();

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		actionbarTitleTextview.setText("Claim Items (" + activity.claimItems.size() + ")");
		tvNoItems.setVisibility((activity.claimItems.size() > 0) ? View.GONE : View.VISIBLE);
        if(activity.getIntent().hasExtra(ClaimDetailActivity.INTENTKEY_CLAIMFORAPPROVAL) && Math.abs(claimHeader.getTotalAmountInLC()) <= app.getStaff().getApproverLimit()){
            actionbarSelectButton.setVisibility(View.VISIBLE);
        }else {
            if(activity.getIntent().hasExtra(ClaimDetailActivity.INTENTKEY_CLAIMFORAPPROVAL)) {
                app.showMessageDialog(activity, "Approval limit is lower than the requested amount");
                actionbarSelectButton.setVisibility(View.GONE);
            }
        }
		if(activity.shouldLoadLineItemOnResume)
			getClaimItems();
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarBackButton || v == actionbarTitleTextview){
			linearNavFragmentActivity.onBackPressed();
		}else if(v == actionbarNewButton) {
			if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS || claimHeader.getTypeID() == ClaimHeader.TYPEKEY_LIQUIDATION){ //for Claim and Liquidation
				Intent intent = new Intent(linearNavFragmentActivity, ManageClaimItemActivity.class);
				intent.putExtra(ManageClaimItemActivity.INTENTKEY_CLAIMHEADER, activity.claimHeader);
				startActivity(intent);
			}else{ //for BA
				if(activity.claimItems.size() == 0){
					Intent intent = new Intent(linearNavFragmentActivity, ManageClaimItemActivity.class);
					intent.putExtra(ManageClaimItemActivity.INTENTKEY_CLAIMHEADER, activity.claimHeader);
					startActivity(intent);
				}else{
					app.showMessageDialog(activity, "Claim Item limit for "+ ClaimHeader.TYPEDESC_ADVANCES+" is at most one.");
				}
			}
		}else if(v == actionbarRefreshButton){
			getClaimItems();
		}else if(v == actionbarSelectButton){
            containerSelect.setVisibility(View.VISIBLE);
            containerNoSelect.setVisibility(View.GONE);
			updateControlbar();
            adapter.notifyDataSetChanged();
		}else if(v == actionbarCancelButton){
			selectedItems.clear();
			containerSelect.setVisibility(View.GONE);
			containerNoSelect.setVisibility(View.VISIBLE);
			actionbarBackButton.setVisibility(View.VISIBLE);
			actionbarTitleTextview.setText("Claim Items (" + activity.claimItems.size() + ")");
			adapter.notifyDataSetChanged();
		}else if(v == actionbarSelectAllButton){
            selectedItems.clear();
			for(int i=0; i < activity.claimItems.size(); i++) {
				if (activity.claimItems.get(i).statusID() == ClaimHeader.STATUSKEY_SUBMITTED || activity.claimItems.get(i).statusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER) //items that has submitted status should be included
					selectedItems.add(i);
			}
			adapter.notifyDataSetChanged();
			updateControlbar();
		}else if(v == actionbarApproveButton){
//            if(activity.claimHeader.getTypeID()==ClaimHeader.TYPEKEY_ADVANCES && activity.claimHeader.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER){
//                if(app.getStaff().getUserPosition()== Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
//                    updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYCM, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYDIRECTOR, "Approved");
//                else
//                    app.showMessageDialog(activity, "Unable to process. You are not a country manager");
//            }else
//                updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYAPPROVER,"ds");
			if(app.getStaff().getUserPosition()== Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
				updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYCM, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYDIRECTOR, "Approved");
			else
				updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYAPPROVER,"Approved");
		}else if(v == actionbarRejectButton) {
            tviewDialogRejectReason.setText("");
			diaglogReject.show();
		}else if(v == actionbarReturnButton){
			tviewDialogReturnReason.setText("");
			diaglogReturn.show();
		}
	}

	private void updateItemStatus(final int statusID, final ClaimItem.UPDATABLEDATE updatabledate, final String approverNotes){
		activity.startLoading();
		for(int i=0; i<selectedItems.size(); i++){
			final int pos = i;
			new Thread(new Runnable() {
				@Override
				public void run() {
					final ClaimItem newClaimItem = activity.claimItems.get(selectedItems.get(pos));
					ClaimItem oldClaimItem = new ClaimItem(newClaimItem);
					newClaimItem.updateApproverStatus(statusID,updatabledate, app.onlineGateway.epochizeDate(new Date()), approverNotes, app.getStaff());

					Object tempResult;
					try{
						tempResult = app.onlineGateway.saveClaimLineItem(newClaimItem, 0, null);
					}catch(Exception e){
						e.printStackTrace();
						tempResult = e.getMessage();
					}

					final Object result = tempResult;
					new Handler(Looper.getMainLooper()).post(new Runnable() {
						@Override
						public void run() {
                            activity.finishLoading();
							if(result != null) {
								getClaimItems();
							}else{
								app.showMessageDialog(activity, "Unable to update " + newClaimItem.getItemNumber() + ": " + result);

							}
						}
					});
				}
			}).start();
		}
	}

	private String statusDescForStatusKey(int statusId) {
		switch (statusId) {
			case 1:
				return ClaimHeader.STATUSDESC_OPEN;
			case 2:
				return ClaimHeader.STATUSDESC_SUBMITTED;
			case 3:
				return ClaimHeader.STATUSDESC_APPROVEDBYAPPROVER;
			case 4:
				return ClaimHeader.STATUSDESC_REJECTEDBYAPPROVER;
			case 5:
				return ClaimHeader.STATUSDESC_CANCELLED;
			case 7:
				return ClaimHeader.STATUSDESC_APPROVEDBYCM;
			case 9:
				return ClaimHeader.STATUSDESC_REJECTEDBYCOUNTRYMANAGER;
			case 16:
				return ClaimHeader.STATUSDESC_APPROVEDBYACCOUNTS;
			case 11:
				return ClaimHeader.STATUSDESC_REJECTEDBYACCOUNTS;
			case 25:
				return ClaimHeader.STATUSDESC_REJECTEDFORSALARYDEDUCTION;
			case 17:
				return ClaimHeader.STATUSDESC_RETURN;
			case 6:
				return ClaimHeader.STATUSDESC_PAID;
			case 69:
				return ClaimHeader.STATUSDESC_SENTTOSAP;
			default:
				return "";
		}
	}

	private void getClaimItems(){
		selectedItems.clear();
		updateControlbar();
		actionbarSelectButton.setOnClickListener(null);
        actionbarRefreshButton.setOnClickListener(null);

		actionbarSelectButton.setOnClickListener(ClaimItemListFragment.this);
        actionbarRefreshButton.setOnClickListener(ClaimItemListFragment.this);

		actionbarTitleTextview.setText("Claim Items (" + activity.claimItems.size() + ")");
		tvNoItems.setVisibility((activity.claimItems.size() > 0)?View.GONE:View.VISIBLE);
		adapter.notifyDataSetChanged();

        linearNavFragmentActivity.startLoading();
		new Thread(new Runnable() {
			@Override
			public void run() {
				Object tempResult;
				try{
					tempResult = app.onlineGateway.getClaimItemsWithClaimID(claimHeader.getClaimID());
				}catch(Exception e){
					e.printStackTrace();
					tempResult = e.getMessage();
				}

				final Object result = tempResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {

					@Override
					public void run() {
                        actionbarSelectButton.setOnClickListener(ClaimItemListFragment.this);
                        actionbarRefreshButton.setOnClickListener(ClaimItemListFragment.this);
						activity.claimItems.clear();
						if(result instanceof String){
							if(result.toString().contains(SaltApplication.CONNECTION_ERROR)){
								activity.finishLoading();
								activity.claimItems.addAll(app.offlineGateway.deserializeMyClaimItems(activity.claimHeader.getClaimID()));
							}else
								linearNavFragmentActivity.finishLoading(result.toString());
						}else{
							activity.shouldLoadLineItemOnResume = true;
							linearNavFragmentActivity.finishLoading();
							activity.claimItems.addAll((List<ClaimItem>)result);
//							app.offlineGateway.clearMyClaimItems("CLAIM"+claimHeader.getClaimID());
							app.offlineGateway.serializeMyClaimItems(claimHeader.getClaimID(), activity.claimItems);
						}

						actionbarTitleTextview.setText("Claim Items (" + activity.claimItems.size() + ")");
						tvNoItems.setVisibility((activity.claimItems.size() > 0)?View.GONE:View.VISIBLE);
						adapter.notifyDataSetChanged();
					}
				});
			}
		}).start();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		if (containerSelect.getVisibility() == View.VISIBLE) {
			ClaimItemCellHolder holder = (ClaimItemCellHolder) adapter.getView(pos, view, parent).getTag();
			holder.containerTable.setTag(!Boolean.parseBoolean(holder.containerTable.getTag().toString()));
			adapter.notifyDataSetChanged();
		} else {
//            Intent intent = new Intent(linearNavFragmentActivity, ClaimItemDetailActivity.class);
//            intent.putExtra(ClaimItemDetailActivity.INTENTKEY_CLAIMITEM, activity.claimItems.get(pos));
//            intent.putExtra(ClaimItemDetailActivity.INTENTKEY_CLAIMHEADER, activity.claimHeader);
//            startActivity(intent);
			activity.changePage(ClaimItemCarousel.newInstance(pos));
		}
	}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ClaimItemCellHolder holder;

        if(view == null){
            holder = new ClaimItemCellHolder();
            view = activity.getLayoutInflater().inflate(R.layout.node_claimitem_minimize, null);
            holder.categoryTV = (TextView)view.findViewById(R.id.tviews_cells_claimitem_category);
            holder.receiptIV = (ImageView)view.findViewById(R.id.iviews_cells_claimitem_receipt);
            holder.amountFCTV = (TextView)view.findViewById(R.id.tviews_cells_claimitem_foreign);
            holder.dateExpensedTV = (TextView)view.findViewById(R.id.tviews_cells_claimitem_expensedate);
            holder.statusTV = (TextView)view.findViewById(R.id.tviews_cells_claimitem_status);
            holder.descTV = (TextView)view.findViewById(R.id.tviews_cells_claimitem_description);
			holder.dateExpensedTV.setTypeface(SaltApplication.myFont(activity));
            holder.cboxIsSelected = (CheckBox)view.findViewById(R.id.cboxs_claimitem_isselected);
            holder.cboxIsSelected.setOnCheckedChangeListener(this);

			holder.containerTable = (TableLayout)view.findViewById(R.id.container_claimitem_table);
			holder.tableLC = (TextView)view.findViewById(R.id.tviews_cells_claimitem_table_localamount);
			holder.tableFC = (TextView)view.findViewById(R.id.tviews_cells_claimitem_table_foreignamount);
			holder.tableTax = (TextView)view.findViewById(R.id.tviews_cells_claimitem_table_tax);
			holder.holderForex = (TextView)view.findViewById(R.id.tviews_cells_claimitem_table_forex);
			holder.containerTable.setTag(false);
			view.setTag(holder);
        }else {
			holder = (ClaimItemCellHolder) view.getTag();
		}

//		mapData = new HashMap<Integer, ClaimItem>();
		claimItem = activity.claimItems.get(position);

//		mapData.put(claimItem.getItemID(),claimItem);

//		holder.cboxIsSelected.setTag(mapData); //link object to retrieve it later in onCheckedChanged

        holder.categoryTV.setText(claimItem.getCategoryName());
        holder.amountFCTV.setText(SaltApplication.decimalFormat.format(claimItem.getForeignAmount()) + " " + claimItem.getForeignCurrencyName());
        holder.dateExpensedTV.setText(claimItem.getExpenseDate(app));
        holder.receiptIV.setVisibility((claimItem.hasReceipt()) ? View.VISIBLE : View.GONE);
		if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER)
//			holder.statusTV.setText("Approver (A)");
 			holder.statusTV.setText("Approved by Approver");
		else if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER)
//			holder.statusTV.setText("Approver (R)");
			holder.statusTV.setText("Rejected by Approver");
		else if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYCM)
//			holder.statusTV.setText("CM (A)");
			holder.statusTV.setText("Approved by CM");
		else if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER)
//			holder.statusTV.setText("CM (R)");
			holder.statusTV.setText("Rejected by CM");
		else if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_PAIDUNDERCOMPANYCARD)
//			holder.statusTV.setText("Paid by CC");
			holder.statusTV.setText("Paid by CC");
		else if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
//			holder.statusTV.setText("Salary Deduction (R)");
			holder.statusTV.setText("Salary Deduction");
        else
			holder.statusTV.setText(claimItem.getStatusName());

		holder.statusTV.setText(statusDescForStatusKey(claimItem.getStatusID()));

		if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYACCOUNTS || claimItem.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER || claimItem.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYCM || claimItem.getStatusID()==ClaimHeader.STATUSKEY_PAID || claimItem.getStatusID()==ClaimHeader.STATUSKEY_PAIDUNDERCOMPANYCARD || claimItem.getStatusID()==ClaimHeader.STATUSKEY_LIQUIDATED)
			holder.statusTV.setTextColor(activity.getResources().getColor(R.color.green));
		else if(claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDBYACCOUNTS || claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER || claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION || claimItem.getStatusID()==ClaimHeader.STATUSKEY_RETURN || claimItem.getStatusID()==ClaimHeader.STATUSKEY_CANCELLED || claimItem.getStatusID()==ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
			holder.statusTV.setTextColor(activity.getResources().getColor(R.color.red));
		else
			holder.statusTV.setTextColor(activity.getResources().getColor(R.color.black));

		if(claimItem.getDescription().length() < 1)
			holder.descTV.setVisibility(View.GONE);
		else{
			holder.descTV.setVisibility(View.VISIBLE);
			holder.descTV.setText(claimItem.getDescription());
		}

        if(containerSelect.getVisibility() == View.VISIBLE){
            view.findViewById(R.id.iviews_next).setVisibility(View.GONE);
            holder.cboxIsSelected.setVisibility(View.VISIBLE);
			if(claimItem.getStatusID() != claimHeader.getStatusID()) {
				holder.cboxIsSelected.setEnabled(false);
				holder.cboxIsSelected.setOnCheckedChangeListener(null);
				holder.cboxIsSelected.setChecked(false);
				holder.cboxIsSelected.setOnCheckedChangeListener(this);
			}else {
				holder.cboxIsSelected.setEnabled(true);
				holder.cboxIsSelected.setTag(position);
				holder.cboxIsSelected.setOnCheckedChangeListener(null);
				holder.cboxIsSelected.setChecked(selectedItems.contains(position));
				holder.cboxIsSelected.setOnCheckedChangeListener(this);
			}
        }else{
            view.findViewById(R.id.iviews_next).setVisibility(View.VISIBLE);
            holder.cboxIsSelected.setVisibility(View.GONE);
        }

		if(Boolean.parseBoolean(holder.containerTable.getTag().toString())){
			holder.containerTable.setVisibility(View.VISIBLE);
			holder.holderForex.setText(SaltApplication.decimalFormat.format(claimItem.getTaxAmount()));
			holder.tableTax.setText(SaltApplication.decimalFormat.format(claimItem.getStandardExchangeRate()));
			holder.tableFC.setText(SaltApplication.decimalFormat.format(claimItem.getForeignAmount())+" "+claimItem.getForeignCurrencyName());
			holder.tableLC.setText(SaltApplication.decimalFormat.format(claimItem.getLocalAmount())+" "+claimItem.getLocalCurrencyName());
		}else
			holder.containerTable.setVisibility(View.GONE);

        return view;
    }

    @Override
    public int getCount() {
        return activity.claimItems.size();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		int pos = Integer.parseInt(buttonView.getTag().toString());
			if (!isChecked){
				selectedItems.remove(new Integer(pos));
//				selectedDataItems.remove(mapData);
			}else {
				selectedItems.add(pos);
//				selectedDataItems.add(mapData);
			}
//		}
		updateControlbar();
        adapter.notifyDataSetChanged();
    }
    private class ClaimItemCellHolder {
        public ImageView receiptIV;
        public TextView categoryTV, statusTV, dateExpensedTV, amountFCTV, descTV;
        public CheckBox cboxIsSelected;
		public TextView tableLC, tableTax, holderForex, tableFC;
		public TableLayout containerTable;
    }

    private void updateControlbar() {
        if(selectedItems.size() == 0) {
			actionbarApproveButton.setVisibility(View.GONE);
            actionbarReturnButton.setVisibility(View.GONE);
            actionbarRejectButton.setVisibility(View.GONE);
			actionbarBackButton.setVisibility(View.VISIBLE);
			actionbarTitleTextview.setText("Claim Items (" + activity.claimItems.size() + ")");
		} else {
			actionbarTitleTextview.setText("");
			actionbarBackButton.setVisibility(View.GONE);
			actionbarApproveButton.setVisibility(View.VISIBLE);
//			actionbarReturnButton.setVisibility(((claimHeader.getTypeID()==ClaimHeader.TYPEKEY_CLAIMS && !((Claim)claimHeader).isPaidByCC()) || claimHeader.getTypeID()==ClaimHeader.TYPEKEY_LIQUIDATION || !claimHeader.isPaidByCompanyCard() && activity.claimHeader.getTypeID() != ClaimHeader.TYPEKEY_ADVANCES) ? View.VISIBLE : View.GONE);
			actionbarReturnButton.setVisibility((!claimHeader.isPaidByCompanyCard())?View.VISIBLE:View.GONE);
			actionbarRejectButton.setVisibility(View.VISIBLE);
        }
		actionbarSelectAllButton.setVisibility((selectedItems.size() == activity.claimItems.size()) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activity.shouldLoadLineItemOnResume = false;
    }
}
