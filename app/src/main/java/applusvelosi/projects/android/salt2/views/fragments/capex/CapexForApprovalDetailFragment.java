package applusvelosi.projects.android.salt2.views.fragments.capex;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.capex.CapexHeader;
import applusvelosi.projects.android.salt2.models.capex.CapexLineItem;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.views.CapexApprovalDetailActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 10/13/15.
 */
public class CapexForApprovalDetailFragment extends LinearNavActionbarFragment implements FileManager.AttachmentDownloadListener{
    private CapexApprovalDetailActivity activity;
    private static String KEY = "capexforapprovaldetailfragmentkey";
    //actionbar
    private RelativeLayout actionbarButtonBack;
    private TextView actionbarTitle;
    //controls
    private TextView buttonReturn, buttonApprove, buttonReject;

    private TextView    fieldCapexNumber, fieldInvestmentType, fieldAttachment, fieldStatus, fieldTotal,
                        fieldRequesterName, fieldOffice, fieldDepartment, fieldCostCenter,
                        fieldCountryManagaer, fieldRegionalManager,
                        fieldDateSubmitted, fieldDateProcessedbyCM, fieldDateProcessedByRFM, fieldDateProcessedbyRM, fieldDateProcessedbyCFO, fieldDateProcessedbyCEO, fieldApproverNote;

    private LinearLayout  containersLineItems;
    private ImageView ivLineItemLoader;
    private TextView tvLineItemHeader;

    private RelativeLayout dialogViewReject, dialogViewReturn, dialogViewApprove;
    private EditText etextDialogRejectReason, etextDialogReturnReason, etextDialogApproveReason;
    private AlertDialog diaglogReject, diaglogReturn, dialogApprove;
    private StringBuilder stringNotes, approversNote;
    private int staffID;

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (CapexApprovalDetailActivity)getActivity();
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backonly, null);
        actionbarButtonBack = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        actionbarTitle.setText("CAPEX For Approval");

        actionbarTitle.setOnClickListener(this);
        actionbarButtonBack.setOnClickListener(this);

        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //initialization
        final View view = inflater.inflate(R.layout.fragment_capexforapproval_detail, null);
        buttonApprove = (TextView)view.findViewById(R.id.buttons_capexforapprovaldetail_approve);
        buttonReject = (TextView)view.findViewById(R.id.buttons_capexforapprovaldetail_reject);
        buttonReturn = (TextView)view.findViewById(R.id.buttons_capexforapprovaldetail_return);

        fieldCapexNumber = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_capexnumber);
        fieldInvestmentType = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_investment);
        fieldAttachment = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_attachment);
        fieldStatus = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_status);
        fieldTotal = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_total);
        fieldRequesterName = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_requester);
        fieldOffice = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_office);
        fieldDepartment = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_department);
        fieldCostCenter = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_costcenter);
        fieldCountryManagaer = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_cm);
        fieldRegionalManager = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_rm);
        fieldDateSubmitted = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_datesubmitted);
        fieldDateProcessedbyCM = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_processedbycm);
        fieldDateProcessedByRFM = (TextView) view.findViewById(R.id.tviews_capexforapprovaldetail_processedbyrfm);
        fieldDateProcessedbyRM = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_processedbyrm);
        fieldDateProcessedbyCFO = (TextView) view.findViewById(R.id.tviews_capexforapprovaldetail_processedbycfo);
        fieldDateProcessedbyCEO = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_processedbyceo);
        containersLineItems = (LinearLayout)view.findViewById(R.id.containers_capexforapprovaldetail_lineitems);
        ivLineItemLoader = (ImageView)view.findViewById(R.id.iviews_loader);
        tvLineItemHeader = (TextView)view.findViewById(R.id.tviews_capexforapprovaldetail_lineitemheader);
        fieldApproverNote = (TextView) view.findViewById(R.id.tviews_capexforapprovaldetail_approvednote);

        staffID = app.getStaff().getStaffID();

        dialogViewApprove = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogApproveReason = (EditText)dialogViewApprove.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewApprove.findViewById(R.id.tviews_dialogs_textinput)).setText("Approver's Note");
        etextDialogApproveReason.setHint("Note");
        dialogApprove = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewApprove)
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_SUBMITTED) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                if (activity.capexHeader.getOfficeID() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                            } else {
                                if (activity.capexHeader.getOfficeID() != 41)
                                    approversNote.append("CM: Approved");
                                else
                                    approversNote.append("CFO: Approved");
                            }
                            approversNote.append(";");
                            changeApprovalStatus(CapexHeader.CAPEXHEADERID_APPROVEDBYCM, "DateProcessedByCountryManager", approversNote.toString());

                        } else if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCM && activity.capexHeader.getRfmId() == staffID) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("RFM: " + etextNote);
                            } else {
                                approversNote.append("RFM: Approved");
                            }
                            approversNote.append(";");
                            changeApprovalStatus(CapexHeader.CAPEXHEADERID_APPROVEDBYRFM, "DateProcessedByRFM", approversNote.toString());
                        } else if ((activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCM && activity.capexHeader.getRfmId() == 0) || activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYRFM) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                if (activity.capexHeader.getOfficeID() != 41)
                                    approversNote.append("RM: " + etextNote);
                                else
                                    approversNote.append("CEO: " + etextNote);
                            } else {
                                if (activity.capexHeader.getOfficeID() != 41) {
                                    approversNote.append("RM: Approved");
                                } else {
                                    approversNote.append("CEO: Approved");
                                }
                            }
                            approversNote.append(";");
                            changeApprovalStatus(CapexHeader.CAPEXHEADERID_APPROVEDBYRM, "DateProcessedByRegionalManager", approversNote.toString());
                        }else if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYRM) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("CFO: " + etextNote);
                            } else {
                                approversNote.append("CFO: Approved");
                            }
                            approversNote.append(";");
                            changeApprovalStatus(CapexHeader.CAPEXHEADERID_APPROVEDBYCFO, "DateProcessedByCFO", approversNote.toString());
                        } else if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCFO) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("CEO: " + etextNote);
                            } else {
                                approversNote.append("CEO: Approved");
                            }
                            approversNote.append(";");
                            changeApprovalStatus(CapexHeader.CAPEXHEADERID_APPROVEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialogViewReject = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
        etextDialogRejectReason =  (EditText)dialogViewReject.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReject.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Rejection");
        diaglogReject = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReject)
                .setPositiveButton("Reject", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(etextDialogRejectReason.length() > 0){
                            String etextNote = etextDialogRejectReason.getText().toString();
                            if(activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_SUBMITTED) {
                                if(activity.capexHeader.getOfficeID() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(CapexHeader.CAPEXHEADERID_REJECTEDBYCM, "DateProcessedByCountryManager", approversNote.toString());
                            } else if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCM && activity.capexHeader.getRfmId() == staffID) {
                                approversNote.append("RFM: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(CapexHeader.CAPEXHEADERID_REJECTEDBYRFM, "DateProcessedByRFM", approversNote.toString());
                            } else if ((activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCM && activity.capexHeader.getRfmId() == 0) || activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYRFM) {
                                if(activity.capexHeader.getOfficeID() != 41)
                                    approversNote.append("RM: " + etextNote);
                                else
                                    approversNote.append("CEO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(CapexHeader.CAPEXHEADERID_REJECTEDBYRM, "DateProcessedByRegionalManager", approversNote.toString());
                            }else if(activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYRM) {
                                approversNote.append("CFO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(CapexHeader.CAPEXHEADERID_REJECTEDBYCFO, "DateProcessedByCFO", approversNote.toString());
                            } else if(activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCFO) {
                                approversNote.append("CEO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(CapexHeader.CAPEXHEADERID_REJECTEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                            }
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for rejection");
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialogViewReturn = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
        etextDialogReturnReason = (EditText) dialogViewReturn.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView) dialogViewReturn.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Returning");
        diaglogReturn = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReturn)
                .setPositiveButton("Return", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (etextDialogReturnReason.length() > 0) {
                            String etextNote = etextDialogReturnReason.getText().toString();
                            if(activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_SUBMITTED) {
                                if (activity.capexHeader.getOfficeID() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                            } else if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCM && activity.capexHeader.getRfmId() == staffID) {
                                approversNote.insert(0, "RFM: " + etextNote);
                            }else if ((activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCM && activity.capexHeader.getRfmId() == 0) || activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYRFM) {
                                if(activity.capexHeader.getOfficeID() != 41)
                                    approversNote.append("RM: " + etextNote);
                                else
                                    approversNote.append("CEO: " + etextNote);
                            }else if (activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYRM) {
                                approversNote.insert(0, "CFO: " + etextNote);
                            } else if(activity.capexHeader.getStatusID() == CapexHeader.CAPEXHEADERID_APPROVEDBYCFO) {
                                approversNote.append("CEO: " + etextNote);
                            }
                            approversNote.append(";");
                            changeApprovalStatus(CapexHeader.CAPEXHEADERID_OPEN, "NA", approversNote.toString());
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for returning");
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(activity.capexHeader == null){
            activity.startLoading();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Object tempResult;
                    try{
                        tempResult = app.onlineGateway.getCapexHeaderDetail(activity.capexHeaderID);
                    }catch(Exception e){
                        tempResult = e.getMessage();
                    }

                    final Object result = tempResult;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if(result instanceof String)
                                activity.finishLoading(result.toString());
                            else{
                                try{
                                    activity.capexHeader = new CapexHeader((JSONObject) result);
                                    updateViews();
                                    activity.finishLoading();
                                    ((AnimationDrawable)ivLineItemLoader.getDrawable()).start();
                                    syncLineItems();
                                }catch(Exception e){
                                    e.printStackTrace();
                                    System.out.println("SALTX "+e.getMessage());
                                    ivLineItemLoader.setVisibility(View.GONE);
                                    tvLineItemHeader.setText("Asset Detail Line Items");
                                    activity.finishLoading(e.getMessage());
                                }

                            }
                        }
                    });
                }
            }).start();
        }else {
            try {
                updateViews();

                tvLineItemHeader.setText("Asset Detail Line Items");
                ivLineItemLoader.setVisibility(View.GONE);
                for (int i = 0; i < activity.capexLineItems.size(); i++) {
                    final int pos = i;
                    CapexLineItem item = activity.capexLineItems.get(i);
                    View v = LayoutInflater.from(activity).inflate(R.layout.node_tvwithsepartorabove, null);
                    v.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            linearNavFragmentActivity.changePage(CapexForApprovalLineItemDetailsFragment.newInstance(pos));
                        }
                    });

                    ((TextView) v.findViewById(R.id.tviews_node_tvwithseparator)).setText(item.getCapexNumber());
                    containersLineItems.addView(v);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v == actionbarButtonBack || v == actionbarTitle)
            linearNavFragmentActivity.onBackPressed();
        else if(v == buttonApprove){
            etextDialogApproveReason.setText("");
            dialogApprove.show();
        }else if(v == buttonReject){
            etextDialogRejectReason.setText("");
            diaglogReject.show();
        }else if(v == buttonReturn){
            etextDialogReturnReason.setText("");
            diaglogReturn.show();
        }else if(v == fieldAttachment){
            if(!activity.capexHeader.getAttachedCer().equals(CapexHeader.NOATTACHMENT)) {
                try {
                    Document doc = activity.capexHeader.getDocuments().get(0);
                    int docID = doc.getDocID();
                    int objectTypeID = doc.getObjectTypeID();
                    int refID = doc.getRefID();
                    String filename = doc.getDocName();
                    activity.startLoading();
                    app.fileManager.downloadDocument(app, docID, refID, objectTypeID, filename, this);
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.finishLoading();
                    app.showMessageDialog(activity, e.getMessage());
                }
            }
        }
    }

    private void changeApprovalStatus(final int statusID, final String keyForUpdatableDate, final String approverNote){
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                String tempResult;
                try{
                    tempResult = app.onlineGateway.saveCapex(activity.capexHeader.getJSONFromUpdatingCapex(statusID, keyForUpdatableDate, approverNote ,app), activity.capexHeader.jsonize(app));
                }catch(Exception e){
                    tempResult = e.getMessage();
                }
                final String result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(result.equals("OK")){
                            activity.finishLoading();
                            Toast.makeText(activity, "Updated Successfully!", Toast.LENGTH_SHORT).show();
                            activity.finish();
                        }else
                            activity.finishLoading(result.toString());
                    }
                });
            }
        }).start();
    }

    @Override
    public void onAttachmentDownloadFinish(File downloadedFile) {
        activity.finishLoading();
        app.fileManager.openDocument(linearNavFragmentActivity, downloadedFile);
    }

    @Override
    public void onAttachmentDownloadFailed(String errorMessage) {
        activity.finishLoading();
        app.showMessageDialog(linearNavFragmentActivity, errorMessage);
    }

    private void updateViews()throws Exception {
        fieldCapexNumber.setText(activity.capexHeader.getCapexNumber());
        fieldInvestmentType.setText(activity.capexHeader.getInvestmentTypeName());
        fieldAttachment.setText(activity.capexHeader.getAttachedCer());
        fieldStatus.setText(activity.capexHeader.getStatusName());
        fieldTotal.setText(SaltApplication.decimalFormat.format(activity.capexHeader.getTotalAmountInUSD()));
        fieldRequesterName.setText(activity.capexHeader.getRequesterName());
        fieldOffice.setText(activity.capexHeader.getOfficeName());
        fieldDepartment.setText(activity.capexHeader.getDepartmentName());
        fieldCostCenter.setText(activity.capexHeader.getCostCenterName());
        fieldCountryManagaer.setText(activity.capexHeader.getCMName());
        fieldRegionalManager.setText(activity.capexHeader.getRMname());
        fieldDateSubmitted.setText(activity.capexHeader.getDateSubmitted(app));
        fieldDateProcessedbyCM.setText(activity.capexHeader.getDateProcessedByCM(app));
        fieldDateProcessedByRFM.setText(activity.capexHeader.getDateProcessedByRFM(app));
        fieldDateProcessedbyRM.setText(activity.capexHeader.getDateProcessedBYRM(app));
        fieldDateProcessedbyCFO.setText(activity.capexHeader.getDateProcessedBYCFO(app));
        fieldDateProcessedbyCEO.setText(activity.capexHeader.getDateProcessedBYCEO(app));
        stringNotes = new StringBuilder();
        approversNote = new StringBuilder(activity.capexHeader.getApproverNote());
        if(!activity.capexHeader.getApproverNote().isEmpty()) {
            String tempStrings[] = activity.capexHeader.getApproverNote().split(";");
            for (String str : tempStrings) {
                stringNotes.append(str);
                stringNotes.append(System.getProperty("line.separator"));
            }
        } else {
            stringNotes.append(activity.capexHeader.getApproverNote());
        }
        fieldApproverNote.setText(stringNotes.toString());

        if(!activity.capexHeader.getAttachedCer().equals(CapexHeader.NOATTACHMENT)){
            fieldAttachment.setOnClickListener(CapexForApprovalDetailFragment.this);
            fieldAttachment.setTextColor(linearNavFragmentActivity.getResources().getColor(R.color.orange_velosi));
        }

        buttonApprove.setOnClickListener(CapexForApprovalDetailFragment.this);
        buttonReject.setOnClickListener(CapexForApprovalDetailFragment.this);
        buttonReturn.setOnClickListener(CapexForApprovalDetailFragment.this);
    }

    private void syncLineItems(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempResult;
                try {
                    tempResult = app.onlineGateway.getCapexLineItems(activity.capexHeader.getCapexID());
                }catch(Exception e){
                    tempResult = e.getMessage();
                }
                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        tvLineItemHeader.setText("Asset Detail Line Items");
                        ivLineItemLoader.setVisibility(View.GONE);
                        if(result instanceof String)
                            Toast.makeText(activity, "Unable to load line items", Toast.LENGTH_LONG).show();
                        else{
                            activity.capexLineItems = new ArrayList<CapexLineItem>();
                            activity.capexLineItems.addAll((ArrayList<CapexLineItem>) result);
                            for(int i=0; i<activity.capexLineItems.size(); i++){
                                final int pos = i;
                                CapexLineItem item = activity.capexLineItems.get(i);
                                View v = LayoutInflater.from(activity).inflate(R.layout.node_tvwithsepartorabove, null);
                                v.setOnClickListener(new View.OnClickListener(){

                                    @Override
                                    public void onClick(View v) {
                                        linearNavFragmentActivity.changePage(CapexForApprovalLineItemDetailsFragment.newInstance(pos));
                                    }
                                });

                                ((TextView)v.findViewById(R.id.tviews_node_tvwithseparator)).setText(item.getCapexNumber());
                                containersLineItems.addView(v);
                            }
                        }
                    }
                });
            }
        }).start();
    }
}

