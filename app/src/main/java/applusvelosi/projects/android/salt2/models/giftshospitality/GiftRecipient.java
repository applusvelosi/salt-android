package applusvelosi.projects.android.salt2.models.giftshospitality;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Velosi on 29/03/2017.
 */

public class GiftRecipient implements Serializable {
    private String name, email;
    private int giftsHospitalityId;
    private int recipientId;
    private String position;
    private float amount;

    public GiftRecipient(JSONObject jsonGiftRecipient) throws Exception {
        amount = (float) jsonGiftRecipient.getDouble("Amount");
        email = jsonGiftRecipient.getString("Email");
        giftsHospitalityId = jsonGiftRecipient.getInt("GiftsHospitalityID");
        name = jsonGiftRecipient.getString("Name");
        position = jsonGiftRecipient.getString("Position");
        recipientId = jsonGiftRecipient.getInt("RecipientID");
    }

    public float getGiftRecipientAmount() { return amount; }
    public String getGiftRecipientEmail() { return  email; }
    public int getGiftsHospitalityId() { return  giftsHospitalityId; }
    public String getGiftRecipientName() { return name; }
    public String getGiftRecipientPosition() { return position; }
    public int getGiftRecipientId() { return  recipientId; }

    public JsonObject getJSONObject() {
        JsonObject json = new JsonObject();
        json.addProperty("Amount", amount);
        json.addProperty("Email", email);
        json.addProperty("GiftsHospitalityID", giftsHospitalityId);
        json.addProperty("Name", name);
        json.addProperty("Position", position);
        json.addProperty("RecipientID", recipientId);
        return json;
    }
}
