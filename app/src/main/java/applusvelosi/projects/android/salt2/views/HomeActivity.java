package applusvelosi.projects.android.salt2.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.lists.GroupedSidebarMenuAdapter;
import applusvelosi.projects.android.salt2.models.GroupedListHeader;
import applusvelosi.projects.android.salt2.models.GroupedListHeaderMarginedTop;
import applusvelosi.projects.android.salt2.models.GroupedListSidebarItem;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.utils.interfaces.GroupedListItemInterface;
import applusvelosi.projects.android.salt2.views.fragments.HolidayWeeklyFragment;
import applusvelosi.projects.android.salt2.views.fragments.MainFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.CalendarMyMonthlyFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.CapexesForApprovalFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.ClaimListFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.ClaimforApprovalListFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.ContractsForApprovalListFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.GiftsForApprovalListFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.HRForApprovalFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.HolidaysLocalFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.HolidaysMonthlyFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.HomeFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.LeavesForApprovalFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.MyLeavesFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.RootFragment;

//import applusvelosi.projects.android.salt2.views.fragments.roots.ClaimListFragment;

public class HomeActivity extends FragmentActivity implements AnimationListener, OnItemClickListener{
	public static final String KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN = "pendingrootfragtoopen"; //value for this map should be int
	//constants used for sidebar item labels
	private final String SIDEBARITEM_HOME = "Home";
	private final String SIDEBARITEM_MYLEAVES = "My Leaves";
	private final String SIDEBARITEM_MYCLAIMS = "My Claims";
	private final String SIDEBARITEM_LOGOUT = "Logout";

	private final String SIDEBARITEM_LEAVESFORAPPROVAL = "Leaves";
	private final String SIDEBARITEM_CLAIMSFORAPPROVAL = "Expense";
	private final String SIDEBARITEM_CAPEXESFORAPPROVAL = "Capex";
	private final String SIDEBARITEM_RECRUITMENTSFORAPPROVAL = "HR";
	private final String SIDEBARITEM_GIFTSFORAPPROVAL = "Gifts and Hospitality";
	private final String SIDEBARITEM_CONTRACTSFORAPPROVAL = "Contracts and Tenders";

	private final String SIDEBARITEM_HOLIDAYSMONTHLY = "This Month";
	private final String SIDEBARITEM_HOLIDAYSWEEKLY = "This Week";
	private final String SIDEBARITEM_HOLIDAYSLOCAL = "Local Holidays";
	private final String SIDEBARITEM_MYCALENDAR = "My Calendar";

	private final String SIDEBARITEM_MYACCOUNTHEADER_KEY = "accountheader";
	private final String SIDEBARITEM_HOME_KEY = "home";
	private final String SIDEBARITEM_MYLEAVES_KEY = "leaves";
	private final String SIDEBARITEM_MYCLAIMS_KEY = "myclaims";
	private final String SIDEBARITEM_MYCALENDAR_KEY = "mycalendar";
	private final String SIDEBARITEM_LOGOUT_KEY = "logout";

	public static final String SIDEBARITEM_APPROVALHEADER_KEY = "approvalheader";
	public static final String SIDEBARITEM_LEAVESFORAPPROVAL_KEY = "leaveapproval";
	public static final String SIDEBARITEM_CLAIMSFORAPPROVAL_KEY = "expenseapproval";
	public static final String SIDEBARITEM_CAPEXESFORAPPROVAL_KEY = "capexapproval";
	public static final String SIDEBARITEM_CONTRACTSFORAPPROVAL_KEY = "contractapproval";
	public static final String SIDEBARITEM_RECRUITMENTSFORAPPROVAL_KEY = "hrapproval";
	public static final String SIDEBARITEM_GIFTSFORAPPROVAL_KEY = "giftapproval";

	private final String SIDEBARITEM_HOLIDAYHEADER_KEY = "holidayheader";
	private final String SIDEBARITEM_HOLIDAYSMONTHLY_KEY = "thismonth";
	private final String SIDEBARITEM_HOLIDAYSWEEKLY_KEY = "thisweek";
	private final String SIDEBARITEM_HOLIDAYSLOCAL_KEY = "localholidays";
//	private final String SIDEBARITEM_HOLIDAYFOOTER_KEY = "holidayfooter";

	//sidebar manageability constants
	private float maxSidebarShownWidth = 0;
	private final int TOGGLE_SPEED = 1000;
	private SaltApplication app;

	//views
	private ListView menuList;
	private Map<String, GroupedListItemInterface> mapSidebarItems; //tells the adapter which item is to be displayed as a header or an item in the actionbar items
	private RelativeLayout foreFragment, foreFragmentShadow; //the main display whose display corresponds to the selected item in the sidebar
	private TranslateAnimation animationShowSidebar, animationHideSidebar;
	private MainFragment mainFragment;
	//holders
	private RootFragment toBeShownFragment;
	private GroupedListSidebarItem lastMenuItemClicked;

	//flags for sidebar visibility
	private final String SIDEBAR_HIDDEN = "sidebar_hidden";
	private final String SIDEBAR_SHOWN = "sidebar_shown";

	private View menuButton;
	private int loaderCnt = 0;
	private List<String> mapSidebarKey;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		setContentView(R.layout.activity_home);
		toBeShownFragment = null;
		app = (SaltApplication)getApplication();
		foreFragment = (RelativeLayout)findViewById(R.id.containers_activities_home_fore);
		foreFragmentShadow = (RelativeLayout)findViewById(R.id.containers_activities_home_foreshadow);
		mainFragment = new MainFragment();
		setupSidebar();
		getSupportFragmentManager().beginTransaction().replace(foreFragment.getId(), mainFragment).commit();
		initAnimations();
	}

	//sidebar
	private void setupSidebar(){
		menuList = (ListView)findViewById(R.id.lists_sidebar);

		mapSidebarItems = new HashMap<String, GroupedListItemInterface>();
		mapSidebarItems.put(SIDEBARITEM_MYACCOUNTHEADER_KEY, new GroupedListHeaderMarginedTop("My Account"));
		mapSidebarItems.put(SIDEBARITEM_HOME_KEY, new GroupedListSidebarItem(SIDEBARITEM_HOME, ContextCompat.getDrawable(this, R.drawable.icon_home), ContextCompat.getDrawable(this, R.drawable.icon_home_sel)));
		mapSidebarItems.put(SIDEBARITEM_MYLEAVES_KEY, new GroupedListSidebarItem(SIDEBARITEM_MYLEAVES, ContextCompat.getDrawable(this, R.drawable.icon_leaves), ContextCompat.getDrawable(this, R.drawable.icon_leaves_sel)));
		mapSidebarItems.put(SIDEBARITEM_MYCLAIMS_KEY, new GroupedListSidebarItem(SIDEBARITEM_MYCLAIMS, ContextCompat.getDrawable(this, R.drawable.icon_myclaims), ContextCompat.getDrawable(this, R.drawable.icon_myclaims_sel)));
		mapSidebarItems.put(SIDEBARITEM_MYCALENDAR_KEY, new GroupedListSidebarItem(SIDEBARITEM_MYCALENDAR, ContextCompat.getDrawable(this, R.drawable.icon_mycalendar), ContextCompat.getDrawable(this, R.drawable.icon_mycalendar_sel)));
		mapSidebarItems.put(SIDEBARITEM_LOGOUT_KEY, new GroupedListSidebarItem(SIDEBARITEM_LOGOUT, ContextCompat.getDrawable(this, R.drawable.icon_logout), ContextCompat.getDrawable(this, R.drawable.icon_logout_sel)));
		mapSidebarKey = new ArrayList<String>(Arrays.asList(SIDEBARITEM_MYACCOUNTHEADER_KEY, SIDEBARITEM_HOME_KEY, SIDEBARITEM_MYLEAVES_KEY, SIDEBARITEM_MYCLAIMS_KEY, SIDEBARITEM_MYCALENDAR_KEY,
				SIDEBARITEM_LOGOUT_KEY));
		//avoid npe
		Staff staff = ((SaltApplication)getApplication()).getStaff();
		try {
			staff.isAdmin();
		} catch (Exception e) {
			e.printStackTrace();
			app.setStaff(app.offlineGateway.deserializeStaff());
		}

		if(!staff.isUser()) {
			mapSidebarItems.put(SIDEBARITEM_APPROVALHEADER_KEY, new GroupedListHeader("For Approval"));
			mapSidebarItems.put(SIDEBARITEM_LEAVESFORAPPROVAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_LEAVESFORAPPROVAL, ContextCompat.getDrawable(this, R.drawable.icon_leavesforapproval), ContextCompat.getDrawable(this, R.drawable.icon_leavesforapproval_sel)));
			mapSidebarKey.addAll(Arrays.asList(SIDEBARITEM_APPROVALHEADER_KEY, SIDEBARITEM_LEAVESFORAPPROVAL_KEY));
			if (!staff.isAccount()) {
				mapSidebarItems.put(SIDEBARITEM_CLAIMSFORAPPROVAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_CLAIMSFORAPPROVAL, ContextCompat.getDrawable(this, R.drawable.icon_claimforapproval), ContextCompat.getDrawable(this, R.drawable.icon_claimforapproval_sel)));
				mapSidebarKey.add(SIDEBARITEM_CLAIMSFORAPPROVAL_KEY);
			}
			if (staff.isAdmin() || staff.isCM()) {
				mapSidebarItems.put(SIDEBARITEM_CAPEXESFORAPPROVAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_CAPEXESFORAPPROVAL, ContextCompat.getDrawable(this, R.drawable.icon_capexforapproval), ContextCompat.getDrawable(this, R.drawable.icon_capexforapproval_sel)));
				mapSidebarKey.add(SIDEBARITEM_CAPEXESFORAPPROVAL_KEY);
			}
			mapSidebarItems.put(SIDEBARITEM_CONTRACTSFORAPPROVAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_CONTRACTSFORAPPROVAL, ContextCompat.getDrawable(this, R.drawable.icon_contractsforapproval), ContextCompat.getDrawable(this, R.drawable.icon_contractforapproval_sel)));
			mapSidebarKey.add(SIDEBARITEM_CONTRACTSFORAPPROVAL_KEY);
			if (!staff.isManager()) {
				mapSidebarItems.put(SIDEBARITEM_RECRUITMENTSFORAPPROVAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_RECRUITMENTSFORAPPROVAL, ContextCompat.getDrawable(this, R.drawable.icon_recruitmentforapproval), ContextCompat.getDrawable(this, R.drawable.icon_recruitmentforapproval_sel)));
				mapSidebarItems.put(SIDEBARITEM_GIFTSFORAPPROVAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_GIFTSFORAPPROVAL, ContextCompat.getDrawable(this, R.drawable.icon_giftforapproval), ContextCompat.getDrawable(this, R.drawable.icon_giftforapproval_sel)));
				mapSidebarKey.addAll(Arrays.asList(SIDEBARITEM_RECRUITMENTSFORAPPROVAL_KEY, SIDEBARITEM_GIFTSFORAPPROVAL_KEY));
			}
		}

		mapSidebarItems.put(SIDEBARITEM_HOLIDAYHEADER_KEY, new GroupedListHeader("Holidays"));
		mapSidebarItems.put(SIDEBARITEM_HOLIDAYSWEEKLY_KEY, new GroupedListSidebarItem(SIDEBARITEM_HOLIDAYSWEEKLY, ContextCompat.getDrawable(this, R.drawable.icon_weeklycalendar), ContextCompat.getDrawable(this, R.drawable.icon_weeklycalendar_sel)));
		mapSidebarItems.put(SIDEBARITEM_HOLIDAYSMONTHLY_KEY, new GroupedListSidebarItem(SIDEBARITEM_HOLIDAYSMONTHLY, ContextCompat.getDrawable(this, R.drawable.icon_monthlycalendar), ContextCompat.getDrawable(this, R.drawable.icon_monthlycalendar_sel)));
		mapSidebarItems.put(SIDEBARITEM_HOLIDAYSLOCAL_KEY, new GroupedListSidebarItem(SIDEBARITEM_HOLIDAYSLOCAL, ContextCompat.getDrawable(this, R.drawable.icon_localholidays), ContextCompat.getDrawable(this, R.drawable.icon_localholidays_sel)));
//		mapSidebarItems.put(SIDEBARITEM_HOLIDAYFOOTER_KEY, new GroupedListHeaderMarginedTop(""));
		mapSidebarKey.addAll(Arrays.asList(SIDEBARITEM_HOLIDAYHEADER_KEY, SIDEBARITEM_HOLIDAYSWEEKLY_KEY, SIDEBARITEM_HOLIDAYSMONTHLY_KEY, SIDEBARITEM_HOLIDAYSLOCAL_KEY));

//		menuList.setAdapter(new GroupedListAdapter(this, sidebarItems));
//        menuList.post(new Runnable() {
//			@Override
//			public void run() {
//				selectMenu((getIntent().hasExtra(KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN)) ? getIntent().getExtras().getInt(KEY_AUTONAV_PENDINGROOTFRAGINDEXTOOPEN) : 1);
//			}
//		});

		menuList.setAdapter(new GroupedSidebarMenuAdapter(this, mapSidebarItems, mapSidebarKey));
		menuList.post(new Runnable() {
			@Override
			public void run() {
				selectMenu(SIDEBARITEM_HOME_KEY);
			}
		});
	}

	//needs to call this so that we can have proper position of our forefragment's shadow when sidebar list is shown
	public void updateMaxSidebarShowWidth(float width){
		if(width>maxSidebarShownWidth)
			maxSidebarShownWidth = width;
	}

	private void hideSidebar(){
		foreFragment.startAnimation(animationHideSidebar);
		menuList.setOnItemClickListener(null);
	}

	public void toggleSidebar(View menuButton){
		this.menuButton = menuButton;
		if(foreFragment.getTag().toString().equals(SIDEBAR_SHOWN)){
			hideSidebar();
		}else{
			foreFragment.startAnimation(animationShowSidebar);
			menuList.setOnItemClickListener(this);
		}
	}

	public void changeChildPage(RootFragment fragment) {
		mainFragment.changePage(fragment);
	}

	public void setupActionbar(RelativeLayout actionbar) {
		mainFragment.setupActionbar(actionbar);
	}

	@Override
	public void onAnimationStart(Animation animation) {
		foreFragment.setTag((animation == animationShowSidebar) ? SIDEBAR_SHOWN : SIDEBAR_HIDDEN);
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		foreFragment.setX((animation == animationShowSidebar) ? maxSidebarShownWidth : 0);
		foreFragmentShadow.setX(maxSidebarShownWidth - 10);


		if(animation == animationHideSidebar) {
			mainFragment.changePage(toBeShownFragment);
			getSupportFragmentManager().executePendingTransactions();
		}

		try{
			if (animation == animationShowSidebar)
				mainFragment.getCurrRootFragment().disableUserInteractionsOnSidebarShown();
			else
				mainFragment.getCurrRootFragment().enableUserInteractionsOnSidebarHidden();
		}catch(Exception e){
			e.printStackTrace();
		}

		menuButton.setEnabled(true);
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		String key = mapSidebarKey.get(pos);
		selectMenu(key);

		if(foreFragment.getTag().equals(SIDEBAR_SHOWN))
			hideSidebar();
	}

	private void selectMenu(String key){
		GroupedListItemInterface sidebarItem = mapSidebarItems.get(key);
		if(sidebarItem instanceof GroupedListSidebarItem) { //prevents actions when clicking sidebar headers
			if(lastMenuItemClicked != null)
				lastMenuItemClicked.displayAsNormalItem();

			((GroupedListSidebarItem) sidebarItem).displayAsSelectedItem();
			lastMenuItemClicked = (GroupedListSidebarItem)sidebarItem;

			if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_HOME))
				toBeShownFragment = HomeFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_LOGOUT))
				logout();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_MYLEAVES))
				toBeShownFragment = MyLeavesFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_MYCLAIMS))
				toBeShownFragment = ClaimListFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_HOLIDAYSWEEKLY))
				toBeShownFragment = HolidayWeeklyFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_HOLIDAYSMONTHLY))
				toBeShownFragment = HolidaysMonthlyFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_HOLIDAYSLOCAL))
				toBeShownFragment = HolidaysLocalFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_MYCALENDAR))
				toBeShownFragment = CalendarMyMonthlyFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_LEAVESFORAPPROVAL))
				toBeShownFragment = LeavesForApprovalFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_CLAIMSFORAPPROVAL))
				toBeShownFragment = ClaimforApprovalListFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_CAPEXESFORAPPROVAL))
				toBeShownFragment = CapexesForApprovalFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_RECRUITMENTSFORAPPROVAL))
				toBeShownFragment = HRForApprovalFragment.getInstance();
			else if(((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_GIFTSFORAPPROVAL) )
				toBeShownFragment = GiftsForApprovalListFragment.getInstance();
			else if (((GroupedListSidebarItem) sidebarItem).getLabel().equals(SIDEBARITEM_CONTRACTSFORAPPROVAL) )
				toBeShownFragment = ContractsForApprovalListFragment.getInstance();
		}
	}

	private void logout() {
		HomeFragment.removeInstance();
		MyLeavesFragment.removeInstance();
		HolidaysLocalFragment.removeInstance();
		CalendarMyMonthlyFragment.removeInstance();
		ClaimListFragment.removeInstance();
		ClaimforApprovalListFragment.removeInstance();
		GiftsForApprovalListFragment.removeInstance();
		CapexesForApprovalFragment.removeInstance();
		LeavesForApprovalFragment.removeInstance();
		HRForApprovalFragment.removeInstance();
		ContractsForApprovalListFragment.removeInstance();

		((SaltApplication)getApplication()).offlineGateway.logout();
		startActivity(new Intent(this, LoginActivity.class));
		finish();
	}

	public void linkToLeavesForApproval(String key) {
		selectMenu(key);
		changeChildPage(LeavesForApprovalFragment.getInstance());
	}

	public void linkToClaimsForApproval(String key) {
		selectMenu(key);
		changeChildPage(ClaimforApprovalListFragment.getInstance());
	}

	public void linkToCapexForApproval(String key) {
		selectMenu(key);
		changeChildPage(CapexesForApprovalFragment.getInstance());
	}

	public void linkToRecruitmentsForApproval(String key) {
		selectMenu(key);
		changeChildPage(HRForApprovalFragment.getInstance());
	}

	public void linkToGiftsForApproval(String key) {
		selectMenu(key);
		changeChildPage(GiftsForApprovalListFragment.getInstance());
	}

	public void linkToContractForApproval(String key) {
		selectMenu(key);
		changeChildPage(ContractsForApprovalListFragment.getInstance());
	}

	private void initAnimations(){
		animationShowSidebar = new TranslateAnimation(0, maxSidebarShownWidth, 0, 0);
		animationShowSidebar.setDuration(TOGGLE_SPEED);
		animationShowSidebar.setFillEnabled(true);
		animationShowSidebar.setAnimationListener(this);
		animationHideSidebar = new TranslateAnimation(0, -maxSidebarShownWidth, 0, 0);
		animationHideSidebar.setDuration(TOGGLE_SPEED);
		animationHideSidebar.setFillEnabled(true);
		animationHideSidebar.setAnimationListener(this);
		foreFragment.setTag(SIDEBAR_HIDDEN);
	}

	public void startLoading(){
		if(loaderCnt == 0 ){
			mainFragment.tviewOutdatedData.setVisibility(View.GONE);
			mainFragment.containersLoader.setVisibility(View.VISIBLE);
			mainFragment.containersLoader.startAnimation(app.animationShow);
			mainFragment.ivLoader.setVisibility(View.VISIBLE);
			mainFragment.tviewsLoader.setText("Loading...");
			mainFragment.tviewsLoader.setTextColor(getResources().getColor(R.color.black));
		}

		loaderCnt++;
	}

	//not all data server retrieval data are saved in the offline gateway so there is a separate function for finish loading that only displays the server connection only
	public void finishLoadingAndShowOutdatedData(){
		loaderCnt--;
		if(loaderCnt == 0){
			mainFragment.containersLoader.setVisibility(View.GONE);
			mainFragment.containersLoader.startAnimation(app.animationHide);
		}
		mainFragment.tviewOutdatedData.setVisibility(View.VISIBLE);
	}

	public void finishLoading(){
		loaderCnt--;
		if(loaderCnt == 0){
			mainFragment.containersLoader.setVisibility(View.GONE);
			mainFragment.containersLoader.startAnimation(app.animationHide);
		}
	}

	public void finishLoading(String error){
		loaderCnt--;
		if(loaderCnt == 0) {
			mainFragment.ivLoader.setVisibility(View.GONE);
			if (error.contains("No address associated with hostname"))
				error = "Server Connection Failed";
			mainFragment.tviewsLoader.setText(error);
			mainFragment.tviewsLoader.setTextColor(getResources().getColor(R.color.red));
		}
	}

	@Override
	public void onBackPressed() {
		if(mainFragment.containersLoader.getVisibility() == View.GONE)
			new AlertDialog.Builder(this).setMessage("Are you sure you want to exit SALT?")
					.setPositiveButton("Yes", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							HomeActivity.super.onBackPressed();
						}
					}).setNegativeButton("No", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
					.create().show();
	}

}
