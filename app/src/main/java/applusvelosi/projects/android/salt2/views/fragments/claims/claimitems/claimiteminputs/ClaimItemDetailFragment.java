package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;
/**
 * Created by Velosi on 2/22/16.
 */

public abstract class ClaimItemDetailFragment extends Fragment implements View.OnClickListener, FileManager.AttachmentDownloadListener {

    protected static final String KEY_ITEMPOS = "keyitempos";
    protected abstract View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) ;

    protected SaltApplication app;
    protected ClaimDetailActivity activity;
    protected ClaimItem claimItem;
    protected TextView noteForReceipt;
    protected TextView receiptDoc, hasYesNoDocFlag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (ClaimDetailActivity)getActivity();
        app = (SaltApplication)activity.getApplication();
        claimItem = activity.claimItems.get(getArguments().getInt(KEY_ITEMPOS));

        return createView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == receiptDoc) {
            if (claimItem.getAttachments().size() > 0) {
                try {
                    Document doc = claimItem.getAttachments().get(0);
                    int docID = doc.getDocID();
                    int objectTypeID = doc.getObjectTypeID();
                    int refID = doc.getRefID();
                    String filename = doc.getDocName();
                    activity.startLoading();
                    app.fileManager.downloadDocument(app, docID, refID, objectTypeID, filename, this);
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.finishLoading();
                    app.showMessageDialog(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onAttachmentDownloadFinish(File file) {
        activity.finishLoading();
        app.fileManager.openDocument(activity, file);
    }

    @Override
    public void onAttachmentDownloadFailed(String errorMessage) {
        activity.finishLoading();
        app.showMessageDialog(activity, errorMessage);
    }


}
