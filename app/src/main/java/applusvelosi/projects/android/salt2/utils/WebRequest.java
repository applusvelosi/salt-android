package applusvelosi.projects.android.salt2.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Velosi on 03/11/2016.
 */
public class WebRequest {
    public static final int GET = 1;
    public static final int POST = 2;


    public static String makeWebServiceCall(String urlAddress, int requestMethod, String postBody) {
        System.out.println("URL: " + urlAddress + System.getProperty("line.separator") + "POST DATA: " + postBody);
        String AUTHKEY = "ak0ayMa+apAn6";
        URL url;
        StringBuilder response = new StringBuilder();
        HttpURLConnection conn;
        BufferedWriter writer = null;
        BufferedReader inReader = null;
        int responseCode = 0;
        try {
            url = new URL(urlAddress);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30000);
//            conn.setConnectTimeout(10000);
            conn.setRequestProperty("AuthKey", AUTHKEY);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");

            if (requestMethod == POST) {
//                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
            } else if (requestMethod == GET) {
                conn.setRequestMethod("GET");
            }

            if (postBody != null) { //posting to URL socket
                OutputStream os = conn.getOutputStream();
                writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(postBody);
                writer.flush();
                writer.close();
                os.close();
            }
            String line;
            inReader = new BufferedReader(new InputStreamReader(conn.getInputStream())); //reading from URL socket
            while ((line = inReader.readLine()) != null)
                response.append(line);
            return response.toString();
        }catch (IOException e) {
            return "Received bad response from server.";
        }finally {
            try {
                if (writer != null)
                    writer.close();
                if (inReader != null)
                    inReader.close();
            }catch (IOException e){
                System.out.println(e.getMessage());
            }

        }
    }

}