package applusvelosi.projects.android.salt2.models.claimheaders;

import org.json.JSONObject;

import applusvelosi.projects.android.salt2.SaltApplication;

public class LiquidationOfBA extends ClaimHeader{
		
	public LiquidationOfBA(SaltApplication app, int costCenterID, String costCenterName, int BAID, String BACNumber) throws Exception{
		super(app, costCenterID, costCenterName, ClaimHeader.TYPEKEY_LIQUIDATION, false, BAID, BACNumber);
	}

	public LiquidationOfBA(JSONObject jsonLiquidation) throws Exception{
		super(jsonLiquidation);
	}

	public LiquidationOfBA(ClaimHeader source){
		super(source);
	}

	public LiquidationOfBA(){

	}

	public int getBaIDCharged(){ return baIDCharged; }
	public String getBacNumber(){
		return bacNumber;
	}
	public float getTotalComputedForDeductionInLC(){
		return totalComputedForDeductionInLC;
	}
}
