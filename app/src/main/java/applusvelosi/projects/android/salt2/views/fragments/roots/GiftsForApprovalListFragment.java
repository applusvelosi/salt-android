package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.lists.GiftForApprovalAdapter;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftHospitality;
import applusvelosi.projects.android.salt2.views.GiftApprovalDetailActivity;

/**
 * Created by Velosi on 3/15/16.
 */

public class GiftsForApprovalListFragment extends RootFragment implements TextWatcher, AdapterView.OnItemClickListener {

    private static GiftsForApprovalListFragment instance;

    private RelativeLayout actionbarRefreshButton, actionbarMenuButton;
    private TextView actionbarTitle;
    private List<GiftHospitality> giftsAndHospitalities, tempGifts;
    private ListView lv;
    private EditText editTxtSearchGift;
    private GiftForApprovalAdapter adapter;


    public static GiftsForApprovalListFragment getInstance(){
        if (instance == null)
            instance = new GiftsForApprovalListFragment();
        return  instance;
    }

    public static void removeInstance(){
        if (instance != null)
            instance = null;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_menurefresh, null);
        actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
        actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
        ((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Gifts & Hospitality for Approval");

        actionbarMenuButton.setOnClickListener(this);
        actionbarRefreshButton.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listview, null);
        lv = (ListView)view.findViewById(R.id.lists_lv);
        giftsAndHospitalities = new ArrayList<GiftHospitality>();
        tempGifts = new ArrayList<GiftHospitality>();
        editTxtSearchGift = (EditText)view.findViewById(R.id.etexts_itemforapproval_name);
        editTxtSearchGift.setHint("Search With Name or Office");
        editTxtSearchGift.setVisibility(View.VISIBLE);
        editTxtSearchGift.addTextChangedListener(this);

        adapter = new GiftForApprovalAdapter(activity, tempGifts, app);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        syncToServer();
    }

    private void syncToServer() {
        activity.startLoading();
        new Thread(new Runnable() {

            @Override
            public void run() {
                Object tempGiftForApproval;
                try {
                    tempGiftForApproval = app.onlineGateway.getGiftsAndHospitalitiesForApproval();
                }catch (Exception e){
                    tempGiftForApproval = e.getMessage();
                }

                final Object giftForApprovalResult = tempGiftForApproval;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (giftForApprovalResult instanceof String) {
                            if (((String) giftForApprovalResult).contains(SaltApplication.CONNECTION_ERROR)) {
                                activity.finishLoadingAndShowOutdatedData();
                            } else {
                                activity.finishLoading(giftForApprovalResult.toString());
                            }
                        }else {
                            activity.finishLoading();
                            giftsAndHospitalities.clear();
                            giftsAndHospitalities.addAll((ArrayList<GiftHospitality>)giftForApprovalResult);
                            filterGiftsByNameOrOffice();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        if (v == actionbarMenuButton) {
            actionbarMenuButton.setEnabled(false);
            activity.toggleSidebar(actionbarMenuButton);
        } else if (v == actionbarRefreshButton) {
            syncToServer();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(activity, GiftApprovalDetailActivity.class);
        intent.putExtra(GiftApprovalDetailActivity.INTENTKEY_GIFTHOSPITALITYHEADER, tempGifts.get(position).getGiftHospitalityId());
        startActivity(intent);
    }

    @Override
    public void disableUserInteractionsOnSidebarShown() {
        lv.setEnabled(false);
        editTxtSearchGift.setEnabled(false);
    }

    @Override
    public void enableUserInteractionsOnSidebarHidden() {

        lv.setEnabled(true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filterGiftsByNameOrOffice();
    }

    private void filterGiftsByNameOrOffice() {
        tempGifts.clear();
        if (editTxtSearchGift.getText().length() > 0) {
            for (GiftHospitality gift : giftsAndHospitalities) {
                if (gift.getRequestorName().toLowerCase().contains(editTxtSearchGift.getText().toString().toLowerCase()) || gift.getRequestorOfficeName().toLowerCase().contains(editTxtSearchGift.getText().toString().toLowerCase()) ||
                        editTxtSearchGift.getText().length() < 1)
                    tempGifts.add(gift);
            }
        } else {
            tempGifts.addAll(giftsAndHospitalities);
        }
        adapter.notifyDataSetChanged();
    }

}
