package applusvelosi.projects.android.salt2.models.claimheaders;

import org.json.JSONObject;

import java.util.Date;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.CostCenter;

public class BusinessAdvance extends ClaimHeader{

	public BusinessAdvance(SaltApplication app, int costCenterID, String costCenterName) {
		super(app, costCenterID, costCenterName, ClaimHeader.TYPEKEY_ADVANCES, false, 0, "");
	}

	public void editClaimHeaderBA(CostCenter newCostCenter, SaltApplication app) {
		costCenterID = newCostCenter.getCostCenterID();
		costCenterName = newCostCenter.getCostCenterName();
		dateModified = app.onlineGateway.epochizeDate(new Date());
		modifiedByName = app.getStaff().getFname()+" "+app.getStaff().getLname();
		modifiedByID = app.getStaff().getStaffID();
	}

    public BusinessAdvance(JSONObject jsonBa) throws Exception{
        super(jsonBa);
    }

    public BusinessAdvance(ClaimHeader businessAdvance){
        super(businessAdvance);
    }

	public BusinessAdvance(){

	}

	public int getCmID(){
		return cmID;
	}
	public String getCmName(){
		return cmName;
	}
	public String getCmEmail(){
		return cmEmail;
	}
	public String getDateApprovedByCM(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateApprovedByCM);
		return (tempDate.contains("-Jan-1900"))?"":tempDate;
//		return app.onlineGateway.dJsonizeDate(dateApprovedByCM);
	}

}
