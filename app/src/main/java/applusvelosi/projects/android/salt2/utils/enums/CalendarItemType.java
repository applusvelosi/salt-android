package applusvelosi.projects.android.salt2.utils.enums;

public enum CalendarItemType {
	TYPE_WEEKDAYS,
	TYPE_HEADER,
	TYPE_OUTMONTH,
	TYPE_NOW,
	TYPE_NONWORKINGDAY,
	TYPE_INVALIDLEAVEDAYS,
}
