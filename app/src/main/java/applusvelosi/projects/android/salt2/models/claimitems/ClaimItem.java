package applusvelosi.projects.android.salt2.models.claimitems;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.ClaimItemAttendee;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.Office;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;

public class ClaimItem implements Serializable {
	public static final String KEY_CATEGORYID = "CategoryID";
	public static final String KEY_CATEGORYTYPEID = "CategoryTypeID";
//	private static final String KEY_ATTENDEE_REQUIREMENT = "Attendee";
//	private static final String KEY_SPENDLIMIT = "SpendLimit";

	protected boolean isActive;
    protected float amount, amountLC;
    protected ArrayList<Document> attachments;
    protected ArrayList<ClaimItemAttendee> attendees;
    protected int claimID, claimLineItemID;
    protected String claimLineItemNumber;
    protected int claimStatusID, companyToChargeID;
    protected String companyToChargeName, costCenterName;
    protected int currencyID;
    protected String currencyName, dateApprovedByApprover, dateApprovedByDirector, dateCancelled, dateCreated, dateModified, datePaid, dateRejected, description;
    protected float forex;
    protected String dateExpensed;
    protected boolean isRechargeable, isTaxRate, hasReceipt;
    protected int hasAttachmentFlag;
//    protected String itemCostCenterCode;
//    protected int itemCostCenterId;
//    protected String itemCostCenterName;
    protected int localCurrencyID;
    protected String localCurrencyName;
    protected int mileage;
    protected String mileageFrom;
    protected float mileageRate;
    protected boolean mileageReturn;
    protected String mileageTo;
    protected int mileageType, modifiedBy;
    protected String notes;
    protected String attachmentNote;
    protected int projectCodeID;
    protected String projectName;
    protected int rejectedByID;
    protected String rejectedByName;
    protected int staffID;
    protected String staffName, statusName;
    protected float stdExchangeRate, taxAmount;
    protected String sapTaxCode;
    protected String wbsCode;
    protected Category category;
    protected String serviceOrderCode;

	public ClaimItem(JSONObject jsonClaimItem, JSONObject jsonCategory) throws Exception{
        isActive = jsonClaimItem.getBoolean("Active");
        amount = (float)jsonClaimItem.getDouble("Amount");
        amountLC = (float)jsonClaimItem.getDouble("AmountInLC");
        attachmentNote = jsonClaimItem.getString("AttachmentNote");
        claimID = jsonClaimItem.getInt("ClaimID");
        claimLineItemID = jsonClaimItem.getInt("ClaimLineItemID");
        claimLineItemNumber = jsonClaimItem.getString("ClaimLineItemNumber");
        claimStatusID = jsonClaimItem.getInt("ClaimStatus");
        companyToChargeID = jsonClaimItem.getInt("CompanyToChargeID");
        companyToChargeName = jsonClaimItem.getString("CompanyToChargeName");
        costCenterName = jsonClaimItem.getString("CostCenterName");
        currencyID = jsonClaimItem.getInt("Currency");
        currencyName = jsonClaimItem.getString("CurrencyName");
        dateApprovedByApprover = jsonClaimItem.getString("DateApprovedByApprover");
        dateApprovedByDirector = jsonClaimItem.getString("DateApprovedByDirector");
        dateCreated = jsonClaimItem.getString("DateCreated");
        dateCancelled = jsonClaimItem.getString("DateCancelled");
        dateModified = jsonClaimItem.getString("DateModified");
        datePaid = jsonClaimItem.getString("DatePaid");
        dateRejected = jsonClaimItem.getString("DateRejected");
        description = jsonClaimItem.getString("Description");
        forex = (float)jsonClaimItem.getDouble("ExchangeRate");
        dateExpensed = jsonClaimItem.getString("ExpenseDate");
        isRechargeable = jsonClaimItem.getBoolean("IsRechargable");
        isTaxRate = jsonClaimItem.getBoolean("IsTaxRate");
        hasReceipt = jsonClaimItem.getBoolean("IsWithReceipt");
        hasAttachmentFlag = jsonClaimItem.getInt("HasAttachment");
//        itemCostCenterCode = jsonClaimItem.getString("ItemCostCenterCode");
//        itemCostCenterId = jsonClaimItem.getInt("ItemCostCenterId");
//        itemCostCenterName = jsonClaimItem.getString("ItemCostCenterName");
        localCurrencyID = jsonClaimItem.getInt("LocalCurrency");
        localCurrencyName = jsonClaimItem.getString("LocalCurrencyName");
        mileage = (int)jsonClaimItem.getDouble("Mileage");
        mileageFrom = jsonClaimItem.getString("MileageFrom");
        mileageRate = (float)jsonClaimItem.getDouble("MileageRate");
        mileageReturn = jsonClaimItem.getBoolean("MileageReturn");
        mileageTo = jsonClaimItem.getString("MileageTo");
        mileageType = jsonClaimItem.getInt("MileageType");
        modifiedBy = jsonClaimItem.getInt("ModifiedBy");
        notes = jsonClaimItem.getString("Notes");
        projectCodeID = jsonClaimItem.getInt("ProjectCodeID");
        projectName = jsonClaimItem.getString("ProjectName");
        rejectedByID = jsonClaimItem.getInt("RejectedBy");
        rejectedByName = jsonClaimItem.getString("RejectedByName");
        staffID = jsonClaimItem.getInt("StaffID");
        staffName = jsonClaimItem.getString("StaffName");
        statusName = jsonClaimItem.getString("StatusName");
        stdExchangeRate = (float)jsonClaimItem.getDouble("StdExchangeRate");
        taxAmount = (float)jsonClaimItem.getDouble("TaxAmount");
        sapTaxCode = jsonClaimItem.getString("SapTaxCode");
        serviceOrderCode = jsonClaimItem.getString("ServiceOrderCode");
        wbsCode = jsonClaimItem.getString("WBSCode");
        JSONArray jsonDocs = jsonClaimItem.getJSONArray("Attachment");
        attachments = new ArrayList<Document>();
        for(int i=0; i<jsonDocs.length(); i++)
            attachments.add(new Document(jsonDocs.getJSONObject(i)));

        JSONArray jsonAttendees = jsonClaimItem.getJSONArray("Attendees");
        attendees = new ArrayList<ClaimItemAttendee>();
        for(int i=0; i<jsonAttendees.length(); i++)
            attendees.add(new ClaimItemAttendee(jsonAttendees.getJSONObject(i)));

        category = new Category(jsonCategory);
	}

    //constructor for new claimHeader
    public ClaimItem(ClaimHeader claimHeader){
        isActive = true;
        amount = 0;
        amountLC = 0;
        claimID = claimHeader.getClaimID();
        claimLineItemID = 0;
        claimLineItemNumber = "";
        claimStatusID = ClaimHeader.STATUSKEY_OPEN;
        companyToChargeID = 0;
        companyToChargeName = "";
        costCenterName = claimHeader.getCostCenterName();
        currencyID = 0;
        currencyName = "";
        dateApprovedByApprover = "/Date(-2208988800000+0000)/";
        dateApprovedByDirector = "/Date(-2208988800000+0000)/";
        dateCancelled = "/Date(-2208988800000+0000)/";
        dateCreated = "/Date(-2208988800000+0000)/";
        dateModified = "/Date(-2208988800000+0000)/";
        datePaid = "/Date(-2208988800000+0000)/";
        dateRejected = "/Date(-2208988800000+0000)/";
        description = "";
        forex = 0;
        dateExpensed = "\"/Date(-2208988800000+0000)/\"";
        isRechargeable = false;
        isTaxRate = false;
//        itemCostCenterCode = "";
//        itemCostCenterId = 0;
//        itemCostCenterName = "";
        hasReceipt = false;
        localCurrencyID = 0;
        localCurrencyName = "";
        mileage = 0;
        mileageFrom = "";
        mileageRate = 0;
        mileageTo = "";
        mileageType = 0;
        modifiedBy = 0;
        notes = "";
        projectCodeID = 0;
        projectName = "";
        rejectedByID = 0;
        rejectedByName = "";
        staffID = claimHeader.getStaffID();
        staffName = claimHeader.getStaffName();
        statusName = ClaimHeader.STATUSDESC_OPEN;
        stdExchangeRate = 0;
        taxAmount = 0;
        attachments = new ArrayList<Document>();
        attendees = new ArrayList<ClaimItemAttendee>();
        category = new Category();
        sapTaxCode = "0";
        wbsCode = "";
    }

    public ClaimItem(ClaimItem source){
        isActive = source.isActive;
        amount = source.amount;
        amountLC = source.amountLC;
        claimID = source.claimID;
        claimLineItemID = source.claimLineItemID;
        claimLineItemNumber = source.claimLineItemNumber;
        claimStatusID = source.claimStatusID;
        companyToChargeID = source.companyToChargeID;
        companyToChargeName = source.companyToChargeName;
        costCenterName = source.costCenterName;
        currencyID = source.currencyID;
        currencyName = source.currencyName;
        dateApprovedByApprover = source.dateApprovedByApprover;
        dateApprovedByDirector = source.dateApprovedByDirector;
        dateCancelled = source.dateCancelled;
        dateCreated = source.dateCreated;
        dateModified = source.dateModified;
        datePaid = source.datePaid;
        dateRejected = source.dateRejected;
        description = source.description;
        forex = source.forex;
        dateExpensed = source.dateExpensed;
        isRechargeable = source.isRechargeable;
        isTaxRate = source.isTaxRate;
//        itemCostCenterCode = source.itemCostCenterCode;
//        itemCostCenterId = source.itemCostCenterId;
//        itemCostCenterName = source.itemCostCenterName;
        hasReceipt = source.hasReceipt();
        localCurrencyID = source.localCurrencyID;
        localCurrencyName = source.localCurrencyName;
        mileage = source.mileage;
        mileageFrom = source.mileageFrom;
        mileageRate = source.mileageRate;
        mileageTo = source.mileageTo;
        mileageType = source.mileageType;
        modifiedBy = source.modifiedBy;
        notes = source.notes;
        projectCodeID = source.projectCodeID;
        projectName = source.projectName;
        rejectedByID = source.rejectedByID;
        rejectedByName = source.rejectedByName;
        staffID = source.staffID;
        staffName = source.staffName;
        statusName = source.statusName;
        stdExchangeRate = source.stdExchangeRate;
        taxAmount = source.taxAmount;
        attachments = source.attachments;
        attendees = source.attendees;
        category = source.getCategory();
        sapTaxCode = source.sapTaxCode;
        wbsCode = source.wbsCode;
    }

    public void setClaimID(int claimID){
        this.claimID = claimID;
    }
    public void setClaimLineItemID(int itemID){ this.claimLineItemID = itemID; }
    public void setAmount(float amount){ this.amount = amount; }
    public void setAmountLC(float amountLC){ this.amountLC = amountLC; }
    public void setCategory(Category category){ this.category = category; }
    public void setClaimStatusID(int statusID){
        this.claimStatusID = statusID;
    }
    public void setCompanyToCharge(Office office){ this.companyToChargeID = office.getID(); this.companyToChargeName = office.getName(); }
    public void setCurrency(Currency currency){ this.currencyID = currency.getCurrencyID(); this.currencyName = currency.getCurrencySymbol(); }
    public void setDescription(String description){ this.description = description; }
    public void setForex(float forex){ this.forex = forex; }
    public void setDateExpensed(Date expenseDate, SaltApplication app){ this.dateExpensed = app.onlineGateway.epochizeDate(expenseDate); }
    public void setIsRechargable(boolean isRechargable){ this.isRechargeable = isRechargable; }
    public void setIsTaxRated(boolean isTaxRate){
        this.isTaxRate = isTaxRate;
    }
    public void setHasReceipt(boolean hasReceipt){ this.hasReceipt = hasReceipt; }
    public void setLocalCurrency(Currency localCurrency){
        this.localCurrencyID = localCurrency.getCurrencyID();
        this.localCurrencyName = localCurrency.getCurrencySymbol();
     }
    public void setMileageRate(float mileageRate){
        this.mileageRate = mileageRate;
    }
    public void setNotes(String notes){ this.notes = notes; }
    public void setProject(Project project){ this.projectCodeID = project.getProjectId(); this.projectName = project.getProjectName(); }
    public void setSapTaxCode(String sapTaxCode) { this.sapTaxCode = sapTaxCode; }
    public void setStandardExchangeRate(float standardExchangeRate){ this.stdExchangeRate = standardExchangeRate; }
    public void setTaxAmount(float taxAmount){ this.taxAmount = taxAmount; }
    public void addAttachment(Document document){ this.attachments.add(0, document); }
    public void removeAttachment(Document document){ this.attachments.remove(document); }
    public void removeAllAttachments(){ this.attachments.clear(); }
    public void addAttendee(ClaimItemAttendee attendee){ this.attendees.add(attendee); }
    public void removeAttendee(ClaimItemAttendee attendee){ this.attendees.remove(attendee); }
    public void setDateCreated(String dateCreated){ this.dateCreated = dateCreated; }
    public void setWbsCode(String wbsCode) { this.wbsCode = wbsCode; }

	public static String getEmptyClaimLineItemJSON(SaltApplication app){
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("Active", true);
		map.put("Amount", 0);
		map.put("AmountInLC", 0);
		map.put("Attachment", new JsonArray());
		map.put("Attendees", new JsonArray());
		map.put("CategoryID", 0);
		map.put("CategoryName", "");
		map.put("ClaimID", 0);
		map.put("ClaimLineItemID", 0);
		map.put("ClaimLineItemNumber", "");
		map.put("ClaimStatus", ClaimHeader.STATUSKEY_OPEN);
		map.put("CompanyToChargeID", 0);
		map.put("CompanyToChargeName", "");
		map.put("CostCenterName", "");
		map.put("Currency", 0);
		map.put("CurrencyName", "");
		map.put("DateApprovedByApprover", "/Date(-2208988800000+0000)/");
		map.put("DateApprovedByDirector", "/Date(-2208988800000+0000)/");
		map.put("DateCancelled", "/Date(-2208988800000+0000)/");
		map.put("DateCreated", "/Date(-2208988800000+0000)/");
		map.put("DateModified", "/Date(-2208988800000+0000)/");
		map.put("DatePaid", "/Date(-2208988800000+0000)/");
		map.put("DateRejected", "/Date(-2208988800000+0000)/");
		map.put("Description", "");
		map.put("ExchangeRate", 0);
		map.put("ExpenseDate", "/Date(-2208988800000+0000)/");
		map.put("IsRechargable", false);
		map.put("IsTaxRate", false);
		map.put("IsWithReceipt", false);
		map.put("LocalCurrency", 0);
		map.put("LocalCurrencyName", "");
		map.put("Mileage", 0);
		map.put("MileageFrom", "");
		map.put("MileageRate", 0);
//		map.put("MileageReturn", false);
		map.put("MileageTo", "");
		map.put("MileageType", 0);
		map.put("ModifiedBy", 0);
		map.put("Notes", "");
		map.put("ProjectCodeID", 0);
		map.put("ProjectName", "");
		map.put("RejectedBy", 0);
		map.put("RejectedByName", "");
		map.put("StaffID", 0);
		map.put("StaffName", "");
		map.put("StatusName", ClaimHeader.STATUSDESC_OPEN);
		map.put("StdExchangeRate", 0);
		map.put("TaxAmount", 0);
        map.put("SapTaxCode", "0");
        map.put("WBSCode", "");
        return app.gson.toJson(map, app.types.hashmapOfStringObject);
	}

    public String getAttachmentNote() {
        return attachmentNote;
    }
    public int getHasAttachmentFlag() {
        return hasAttachmentFlag;
    }
    public String getAttachmentName(){
        return (attachments.size()>0)?attachments.get(0).getDocName():"";
    }
	public int getStatusID(){ return claimStatusID; }
	public String getStatusName(){ return statusName; }
	public int getItemID(){ return claimLineItemID; }
	public String getItemNumber(){ return claimLineItemNumber; }
	public int statusID(){ return claimStatusID; }
	public int getProjectCodeID(){ return projectCodeID; }
	public String getProjectName(){ return projectName; }
	public int getCategoryID(){ return category.getCategoryID(); }
    public int getCategoryTypeID(){ return category.getCategoryTypeID(); }
	public String getCategoryName(){ return (category!=null)?category.getName():""; }
	public float getForeignAmount(){ return amount; }
	public float getLocalAmount(){ return amountLC; }
    public float getMileageRate() { return mileageRate; }
	public float getTaxAmount(){ return taxAmount; }
	public float getStandardExchangeRate(){ return stdExchangeRate; }
	public float getForex(){ return forex; }
	public String getExpenseDate(SaltApplication app){ return app.onlineGateway.dJsonizeDate(dateExpensed); }
	public String getDescription(){ return description; }
	public int getForeignCurrencyID(){ return currencyID; }
	public String getForeignCurrencyName(){ return currencyName; }
	public int getLocalCurrencyID(){ return localCurrencyID; }
    public String getLocalCurrencyName(){ return localCurrencyName; }
	public String getNotes(){ return notes; }
	public boolean hasReceipt(){ return hasReceipt; }
	public ArrayList<Document> getAttachments(){
        return attachments;
    }
    public ArrayList<ClaimItemAttendee> getAttendees(){ return attendees; }
	public boolean isTaxApplied(){ return isTaxRate; }
	public boolean isBillable(){ return isRechargeable; }
	public Integer getBillableCompanyID(){ return companyToChargeID; }
	public String getBillableCompanyName(){ return companyToChargeName; }
    public String getWbsCode(){ return wbsCode; }
    public Category getCategory(){ return category; }
    public String getSapTaxCode() { return sapTaxCode; }

	public JsonObject jsonize() {
        JsonObject map = new JsonObject();
		map.addProperty("Active", true);
        map.addProperty("Amount", amount);
        map.addProperty("AmountInLC", amountLC);
        JsonArray jsonAttachments = new JsonArray();
        for(Document attachment :attachments)
            jsonAttachments.add(attachment.getJSONObject());
        map.add("Attachment", jsonAttachments);
        map.addProperty("AttachmentNote", attachmentNote);
        JsonArray jsonAttendees = new JsonArray();
        for(ClaimItemAttendee attendee : attendees)
            jsonAttendees.add(attendee.getJSONObject());
        map.add("Attendees", jsonAttendees);
        map.addProperty("CategoryID", category.getCategoryID());
        map.addProperty("CategoryName", category.getName());
        map.addProperty("ClaimID", claimID);
        map.addProperty("ClaimLineItemID", claimLineItemID);
        map.addProperty("ClaimLineItemNumber", claimLineItemNumber);
        map.addProperty("ClaimStatus", claimStatusID);
        map.addProperty("CompanyToChargeID", companyToChargeID);
        map.addProperty("CompanyToChargeName", companyToChargeName);
        map.addProperty("CostCenterName", costCenterName);
        map.addProperty("Currency", currencyID);
        map.addProperty("CurrencyName", currencyName);
        map.addProperty("DateApprovedByApprover", dateApprovedByApprover);
        map.addProperty("DateApprovedByDirector", dateApprovedByDirector);
        map.addProperty("DateCancelled", dateCancelled);
        map.addProperty("DateCreated", dateCreated);
        map.addProperty("DateModified", dateModified);
        map.addProperty("DatePaid", datePaid);
        map.addProperty("DateRejected", dateRejected);
        map.addProperty("Description", description);
        map.addProperty("ExchangeRate", forex);
        map.addProperty("ExpenseDate", dateExpensed);
        map.addProperty("HasAttachment", hasAttachmentFlag);
        map.addProperty("IsRechargable", isRechargeable);
        map.addProperty("IsTaxRate", isTaxRate);
        map.addProperty("IsWithReceipt", hasReceipt);
//        map.addProperty("ItemCostCenterCode", itemCostCenterCode);
//        map.addProperty("ItemCostCenterId", itemCostCenterId);
//        map.addProperty("ItemCostCenterName", itemCostCenterName);
        map.addProperty("LocalCurrency", localCurrencyID);
        map.addProperty("LocalCurrencyName", localCurrencyName);
        map.addProperty("Mileage", mileage);
        map.addProperty("MileageFrom", mileageFrom);
        map.addProperty("MileageRate", mileageRate);
        map.addProperty("MileageReturn", mileageReturn);
        map.addProperty("MileageTo", mileageTo);
        map.addProperty("MileageType", mileageType);
        map.addProperty("ModifiedBy", modifiedBy);
        map.addProperty("Notes", (notes==null)?"":notes);
        map.addProperty("ProjectCodeID", projectCodeID);
        map.addProperty("ProjectName", projectName);
        map.addProperty("RejectedBy", rejectedByID);
        map.addProperty("RejectedByName", rejectedByName);
        map.addProperty("SapTaxCode", sapTaxCode);
        map.addProperty("ServiceOrderCode", serviceOrderCode);
        map.addProperty("StaffID", staffID);
        map.addProperty("StaffName", staffName);
        map.addProperty("StatusName", statusName);
        map.addProperty("StdExchangeRate", forex);
        map.addProperty("TaxAmount", taxAmount);
        map.addProperty("WBSCode", wbsCode);
        return map;
	}

    public void updateApproverStatus(int statusID, UPDATABLEDATE updatabledate, String latestDate, String approverNote, Staff approver){
        this.claimStatusID = statusID;
        this.statusName = ClaimHeader.getStatusDescriptionForKey(statusID);
        this.modifiedBy = approver.getStaffID();
        this.dateModified = latestDate;
        this.notes = approverNote;

        switch (updatabledate){
            case DATEREOPPENED:

            case DATEAPPROVEDBYAPPROVER:
                dateApprovedByApprover = latestDate;
                break;
            case DATEAPPROVEDBYDIRECTOR:
                dateApprovedByDirector = latestDate;
                break;
            case DATECANCELLED:
                dateCancelled = latestDate;
                break;
            case DATEPAID:
                datePaid = latestDate;
                break;
            case DATEREJECTED: {
                dateRejected = latestDate;
                rejectedByID = approver.getStaffID();
                rejectedByName = approver.getFname()+" "+approver.getLname();
            }
            break;
        }
    }

    public void updateStatus(int statusID, UPDATABLEDATE updatabledate, String latestDate, Staff approver){
        this.claimStatusID = statusID;
        this.statusName = ClaimHeader.getStatusDescriptionForKey(statusID);
        modifiedBy = approver.getStaffID();
        dateModified = latestDate;

        switch (updatabledate){
            case DATEREOPPENED:

            case DATEAPPROVEDBYAPPROVER: dateApprovedByApprover = latestDate;
                                         break;
            case DATEAPPROVEDBYDIRECTOR: dateApprovedByDirector = latestDate;
                                         break;
            case DATECANCELLED: dateCancelled = latestDate;
                                break;
            case DATEPAID: datePaid = latestDate;
                           break;
            case DATEREJECTED: {
                                    dateRejected = latestDate;
                                    rejectedByID = approver.getStaffID();
                                    rejectedByName = approver.getFname()+" "+approver.getLname();
                               }
                               break;
        }
    }

    public enum UPDATABLEDATE{
        DATEREOPPENED,
        DATESUBMITTED,
        DATEAPPROVEDBYAPPROVER,
        DATEAPPROVEDBYCM,
        DATEAPPROVEDBYDIRECTOR,
        DATECANCELLED,
        DATEPAID,
        DATEREJECTED,
        NONE
    }
}
