package applusvelosi.projects.android.salt2.models;

import org.json.JSONObject;

import java.io.Serializable;

public class Currency implements Serializable{
	private int currencyID;
	private String currencyName, currencyThree;

	public Currency(JSONObject jsonCurrency) throws Exception{
		currencyID = jsonCurrency.getInt("CurrencyID");
		currencyName = jsonCurrency.getString("CurrencyName");
		currencyThree = jsonCurrency.getString("CurrencySymbol");
	}

	public Currency(int currencyID, String currencyName, String currencySymbol){
		this.currencyID = currencyID;
		this.currencyName = currencyName;
		this.currencyThree = currencySymbol;
	}

	public int getCurrencyID(){ return currencyID; }
	public String getCurrencyName(){ return currencyName; }
	public String getCurrencySymbol(){ return currencyThree; }

}
