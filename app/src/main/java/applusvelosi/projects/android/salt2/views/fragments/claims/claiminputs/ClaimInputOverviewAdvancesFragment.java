package applusvelosi.projects.android.salt2.views.fragments.claims.claiminputs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.CostCenter;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.claimheaders.BusinessAdvance;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.utils.interfaces.CameraCaptureInterface;
import applusvelosi.projects.android.salt2.utils.interfaces.FileSelectionInterface;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 11/5/15.
 */
public class ClaimInputOverviewAdvancesFragment extends LinearNavActionbarFragment implements CameraCaptureInterface, FileSelectionInterface {
    public static final String KEY_CLAIMPOS = "claimkey";
    private RelativeLayout actionbarButtonBack;
    private TextView actionbarTitle, actionbarButtonSave;

    private TextView tvType, tvStaff, tvApprover, tvAttachment;
    private LinearLayout containerCostCenter;
    private ImageView buttonFile;
    private ArrayList<CostCenter> costCenters;
    private ArrayList<LinearLayout> costCenterNodes;
    private int selectedCostCenterPos = 0;
    private AlertDialog dialogAttachment;
    private String userChoosenTask;
    private Document document;
    private File attachedFile;

    public static ClaimInputOverviewAdvancesFragment newInstance(int pos) {
        ClaimInputOverviewAdvancesFragment frag = new ClaimInputOverviewAdvancesFragment();
        Bundle b = new Bundle();
        b.putInt(KEY_CLAIMPOS, pos);
        frag.setArguments(b);
        return frag;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        RelativeLayout actionbarLayout = (RelativeLayout) linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backdone, null);
        actionbarButtonBack = (RelativeLayout) actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionbarButtonSave = (TextView) actionbarLayout.findViewById(R.id.buttons_actionbar_done);
        actionbarTitle = (TextView) actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        actionbarTitle.setText(((getArguments() == null) ? "New" : "Edit") + " Business Advance");
        actionbarButtonSave.setText("Next");

        actionbarTitle.setOnClickListener(this);
        actionbarButtonBack.setOnClickListener(this);
        actionbarButtonSave.setOnClickListener(this);

        return actionbarLayout;
    }

    @Override
    protected View createView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_claimheaderinputoverview_advances, null);

        tvType = (TextView) v.findViewById(R.id.tviews_claiminputoverview_type);
        tvStaff = (TextView) v.findViewById(R.id.tviews_claiminputoverview_staff);
        tvApprover = (TextView) v.findViewById(R.id.tviews_claiminputoverview_approver);
        tvAttachment = (TextView)v.findViewById(R.id.tviews_claiminputoverview_attachment);
        containerCostCenter = (LinearLayout) v.findViewById(R.id.containers_claiminputoverview_costcenter);
        buttonFile = (ImageView)v.findViewById(R.id.buttons_claimiteminput_attachmentfiles);
        buttonFile.setOnClickListener(this);
        tvType.setText(ClaimHeader.TYPEDESC_ADVANCES);
        tvStaff.setText(app.getStaff().getFname() + " " + app.getStaff().getLname());
        tvApprover.setText(app.getStaff().getExpenseApproverName());
        costCenters = new ArrayList<CostCenter>();
        linearNavFragmentActivity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempResult;
                try {
                    tempResult = app.onlineGateway.getCostCentersByOfficeID();
                } catch (Exception e) {
                    tempResult = e.getMessage();
                }

                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (result instanceof String) {
                            linearNavFragmentActivity.finishLoading(result.toString());
                        } else {
                            linearNavFragmentActivity.finishLoading();
                            costCenters.addAll((ArrayList<CostCenter>) result);
                            costCenterNodes = new ArrayList<LinearLayout>();

                            LinearLayout preSelectedCostCenter = null;
                            for (int i = 0; i < costCenters.size(); i++) {
                                LinearLayout node = (LinearLayout) inflater.inflate(R.layout.node_textonly, null);
                                ((TextView) node.findViewById(R.id.tviews_tviews_textonly)).setText(costCenters.get(i).getCostCenterName());
                                node.setOnClickListener(ClaimInputOverviewAdvancesFragment.this);
                                node.setTag(i);
                                costCenterNodes.add(node);
                                containerCostCenter.addView(node);

                                if (getArguments() != null) {
                                    ClaimHeader ch = app.getMyClaims().get(getArguments().getInt(KEY_CLAIMPOS));
                                    if (ch.getCostCenterID() == costCenters.get(i).getCostCenterID())
                                        preSelectedCostCenter = node;
                                } else {
                                    if (app.getStaff().getCostCenterID() == costCenters.get(i).getCostCenterID())
                                        preSelectedCostCenter = node;
                                }
                            }

                            if (costCenters.size() == 1) { //auto selected if it is the only cost center available
                                ((TextView) (costCenterNodes.get(0).findViewById(R.id.tviews_tviews_textonly))).setTextColor(getResources().getColor(R.color.orange_velosi));
                                costCenterNodes.get(0).setOnClickListener(null);
                            } else {
                                preSelectedCostCenter.performClick();
//                                for(int i=0; i<costCenters.size(); i++)
//                                    if(costCenters.get(i).getCostCenterID() == app.getStaff().getCostCenterID())
//                                        containerCostCenter.getChildAt(i).performClick();
                            }

//                            if(preSelectedCostCenter != null){
//                                preSelectedCostCenter.performClick();
//                            }

                        }
                    }
                });
            }
        }).start();

        final CharSequence[] items = {"Take A Photo", "Choose From Library",
                "Cancel"};

        dialogAttachment = new AlertDialog.Builder(linearNavFragmentActivity).setTitle("Add Attachment!").setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = FileManager.checkPermission(linearNavFragmentActivity);

                if (items[item].equals("Take A Photo")) {
                    userChoosenTask = "Take A Photo";
                    if (result)
                        initCameraIntent();
                } else if (items[item].equals("Choose From Library")) {
                    userChoosenTask = "Choose From Library";
                    if (result)
                        initGalleryImageIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        }).create();

        return v;
    }

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, SaltApplication.RESULT_CAMERA);
    }

    private void initGalleryImageIntent() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*,application/pdf");
            startActivityForResult(Intent.createChooser(intent, "Select File"), SaltApplication.RESULT_BROWSEFILES);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String mimeTypes[] = {"image/*", "application/pdf"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            startActivityForResult(intent, SaltApplication.RESULT_BROWSEFILES_KITKAT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case FileManager.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take A Photo"))
                        initCameraIntent();
                    else if (userChoosenTask.equals("Choose From Library"))
                        initGalleryImageIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            String filePath;
            if (resultCode != Activity.RESULT_OK)
                return;
            if (null == data)
                return;
            switch (requestCode) {
                case SaltApplication.RESULT_CAMERA:
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.getFilePathFromUri_BelowKitKat(linearNavFragmentActivity, data.getData());
                        onCameraCaptureSuccess(data, filePath);
                    } else {
                        onCameraCaptureFailed();
                    }
                    break;
                case SaltApplication.RESULT_BROWSEFILES:
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.uriToFilename(linearNavFragmentActivity, data.getData());
                        onFileSelectionSuccess(filePath);
                    } else {
                        onFileSelectionFailed();
                    }
                    break;
                case SaltApplication.RESULT_BROWSEFILES_KITKAT: //for KitKat version and above
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.uriToFilename(linearNavFragmentActivity, data.getData());
                        onFileSelectionSuccess(filePath);
                    } else {
                        onFileSelectionFailed();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraCaptureSuccess(Intent data, String filePath) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        tvAttachment.setText(destination.getName());
        updateAttachment(destination);
    }

    @Override
    public void onCameraCaptureFailed() {
        updateAttachment(null);
    }

    @Override
    public void onFileSelectionSuccess(String filePath) {
        attachedFile = new File(filePath);
        tvAttachment.setText(attachedFile.getName());
        updateAttachment(attachedFile);
    }

    @Override
    public void onFileSelectionFailed() {
        updateAttachment(null);
    }

    private void updateAttachment(File file) {
        attachedFile = file;
        if (file == null) {
            resetAttachment();
        } else {
            if (file.length() <= 10485760) {
                if (SaltApplication.ACCEPTED_FILETYPES.contains(file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()))) {
                    document = new Document(file, app.dateFormatClaimItemAttachment.format(new Date()), app.getStaff().getStaffID(), true, 1);
                    tvAttachment.setText(file.getName());
                    tvAttachment.setTextColor(getResources().getColor(android.R.color.black));
                } else {
                    resetAttachment();
                    app.showMessageDialog(linearNavFragmentActivity, "Invalid File Type");
                }
            } else {
                resetAttachment();
                app.showMessageDialog(linearNavFragmentActivity, "File must not exceed 10mb");
            }
        }
    }

    private void resetAttachment() {
        tvAttachment.setText("No file selected");
        tvAttachment.setTextColor(Color.parseColor("#909090"));
    }


    @Override
    public void onClick(View v) {
        if (v == actionbarButtonBack || v == actionbarTitle)
            linearNavFragmentActivity.onBackPressed();
        else if (v == actionbarButtonSave) {
            try {
                if (selectedCostCenterPos >= 0) {

                    final String oldClaimHeaderJSON;
                    final BusinessAdvance newClaimHeader;
                    final CostCenter selectedCostCenter = costCenters.get(selectedCostCenterPos);
                    try {
                        if (selectedCostCenter.getNumberOfApprover() > 0) {
                            int hasYesNo = 0;
                            if (document != null)
                                hasYesNo = 1;

                            if (getArguments() != null) { //edited business advanced
                                newClaimHeader = (BusinessAdvance) app.getMyClaims().get(getArguments().getInt(KEY_CLAIMPOS));
                                oldClaimHeaderJSON = newClaimHeader.jsonize().toString();
                                newClaimHeader.editClaimHeaderBA(selectedCostCenter, app);
                            } else { //new busioness advanced
                                oldClaimHeaderJSON = (new BusinessAdvance()).getEmptyJSON();
                                newClaimHeader = new BusinessAdvance(app, selectedCostCenter.getCostCenterID(), selectedCostCenter.getCostCenterName());

                                if (document != null) {
                                    newClaimHeader.addAttachment(document);
                                    newClaimHeader.setHasDocument(true);
                                }else {
                                    newClaimHeader.setHasDocument(false);
                                }
                            }
                            linearNavFragmentActivity.startLoading();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String tempResult = null;
                                    try {
//                                        tempResult = app.onlineGateway.saveClaim(newClaimHeader, oldClaimHeaderJSON, attachedFile);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        tempResult = e.getMessage();
                                    }
                                    final String result = tempResult;
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (result == null) {
                                                linearNavFragmentActivity.finishLoading();
                                                if (getArguments() != null) {
                                                    int claimPos = getArguments().getInt(KEY_CLAIMPOS);
                                                    app.getMyClaims().remove(claimPos);
                                                    app.getMyClaims().add(claimPos, new BusinessAdvance(newClaimHeader));
                                                    app.offlineGateway.serializeMyClaims(app.getMyClaims());
                                                    Toast.makeText(linearNavFragmentActivity, "Business Advance Updated Successfully", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(linearNavFragmentActivity, "Business Advance Created Successfully", Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(getActivity(), ClaimDetailActivity.class);
                                                    intent.putExtra(ClaimDetailActivity.INTENTKEY_CLAIMHEADER, 0);
                                                    linearNavFragmentActivity.startActivity(intent);
                                                    linearNavFragmentActivity.finish();
                                                }
                                            } else {
                                                linearNavFragmentActivity.finishLoading(result);
                                            }
                                        }
                                    });
                                }
                            }).start();
                        } else {
                            app.showMessageDialog(getActivity(), "The cost centre you've selected does not have a primary approver.");

//                        AlertDialog alertDialog = new AlertDialog.Builder(linearNavFragmentActivity).create();
//                        alertDialog.setTitle(null);
//                        alertDialog.setMessage("The cost centre you've selected does not have a primary approver.");
//                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                        alertDialog.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        app.showMessageDialog(linearNavFragmentActivity, e.getMessage());
                    }
                } else {
                    app.showMessageDialog(linearNavFragmentActivity, "Please select a cost center");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            if (v.getTag() != null) { //cost center nodes each has defined tags
                if (containerCostCenter.getChildCount() > 1) { //user has selected a cost center
                    int selectedPos = Integer.parseInt(v.getTag().toString());
                    selectedCostCenterPos = selectedPos;
                    for (int i = 0, maxsize = costCenterNodes.size(); i < maxsize; ) {
                        if (i != selectedPos) {
                            containerCostCenter.removeViewAt(i);
                            maxsize--;
                            selectedPos--;
                        } else {
                            ((TextView) containerCostCenter.getChildAt(i).findViewById(R.id.tviews_tviews_textonly)).setTextColor(getResources().getColor(R.color.orange_velosi));
                            i++;
                        }
                    }
                }else { //user has deselected the selected cost center
                    selectedCostCenterPos = 0;
                    containerCostCenter.removeAllViews();
                    for (int i = 0; i < costCenterNodes.size(); i++) {
                        containerCostCenter.addView(costCenterNodes.get(i));
                        ((TextView) costCenterNodes.get(i).findViewById(R.id.tviews_tviews_textonly)).setTextColor(getResources().getColor(R.color.black));
                    }
                }
            }

        }
    }

}
