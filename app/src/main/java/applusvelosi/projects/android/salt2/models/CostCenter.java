package applusvelosi.projects.android.salt2.models;

/**
 * Created by Velosi on 11/6/15.
 */
public class CostCenter {
    private int costCenterID;
    private String costCenterName;
    private int countApprover;

    public CostCenter(int costCenterID, String costCenterName, int countApprover){
        this.costCenterID = costCenterID;
        this.costCenterName = costCenterName;
        this.countApprover = countApprover;
    }

    public int getCostCenterID(){
        return costCenterID;
    }
    public String getCostCenterName(){
        return costCenterName;
    }
    public int getNumberOfApprover(){
        return countApprover;
    }
}
