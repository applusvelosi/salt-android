package applusvelosi.projects.android.salt2.views.dialogs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;

/**
 * Created by Velosi on 04/08/2016.
 */
public class DialogStaffBusinessAdvanceList implements ListAdapterInterface {

    private List<ClaimHeader> baList;
    private ClaimDetailActivity activity;
    private ListView lv;
    private ListAdapter adapter;
    private AlertDialog dialogPrompt, dialogProceed;

    public DialogStaffBusinessAdvanceList(final ClaimDetailActivity activity, List<ClaimHeader> myBA, AlertDialog dialogProcess){
        this.activity = activity;
        baList = myBA;
        dialogProceed = dialogProcess;
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_staffbusinessadvancelist,null);
        lv = (ListView) view.findViewById(R.id.lv_dialogs_balist);
        adapter = new ListAdapter(this);
        lv.setAdapter(adapter);
        dialogPrompt = new AlertDialog.Builder(activity).setTitle(null).setView(view)
                .setPositiveButton("Process", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogProceed.show();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
    }
    public void show() {
        dialogPrompt.show();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ListHolder holder;

        if (v == null) {
            v = LayoutInflater.from(activity).inflate(R.layout.node_businessadvance_list, null);
            holder = new ListHolder();
            holder.tvNumber = (TextView) v.findViewById(R.id.tview_ba_claimnumber);
            holder.tvAmount = (TextView) v.findViewById(R.id.tview_ba_amount);
            holder.tvStatus = (TextView) v.findViewById(R.id.tview_ba_status);
            holder.tvName = (TextView) v.findViewById(R.id.tviews_ba_staffname);

            v.setTag(holder);
//            ((TextView) v.findViewById(R.id.tview_ba_claimnumber)).setText(baList.get(position).getClaimNumber());
//            ((TextView) v.findViewById(R.id.tview_ba_amount)).setText(String.valueOf(baList.get(position).getTotalAmountInLC()));
//            ((TextView) v.findViewById(R.id.tview_ba_status)).setText(baList.get(position).getStatusName());
//            ((TextView) v.findViewById(R.id.tviews_ba_staffname)).setText(baList.get(position).getStaffName());
        }

        holder = (ListHolder) v.getTag();
        ClaimHeader ba = baList.get(position);
        holder.tvNumber.setText(ba.getClaimNumber());
        holder.tvAmount.setText(String.valueOf(ba.getTotalAmountInLC()));
        holder.tvStatus.setText(ba.getStatusName());
        holder.tvName.setText(ba.getStaffName());

        return v;
    }

    @Override
    public int getCount() {
        return baList.size();
    }

    private class ListHolder {
        private TextView tvNumber, tvAmount, tvStatus, tvName;
    }
}
