package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.salt2.adapters.spinners.SimpleSpinnerAdapter.NodeSize;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.utils.customviews.CustomViewPager;
import applusvelosi.projects.android.salt2.views.NewLeaveRequestActivity;

public class MyLeavesFragment extends RootFragment implements OnItemSelectedListener, ViewPager.OnPageChangeListener{
	private static MyLeavesFragment instance;
	//action bar buttons
	private RelativeLayout actionbarMenuButton, actionbarNewButton, actionbarRefreshButton, actionbarHelpButton;

	private Spinner statusSpinner, typeSpinner;
	private ArrayList<String> types, statuses;
	private ArrayList<Leave> filteredLeaves;
	private HashMap<Integer, Integer> mapPositions;

	private ShowcaseView statusSV, typeSV,  newLeaveSV;

	private CustomViewPager pager;
	private LeaveListTabAdapter adapter;
	private RelativeLayout buttonList, buttonCalendar;
	private ImageView iviewList, iviewCalendar;

    private List<MyLeavesDataUpdateObserver> observers;

	public static MyLeavesFragment getInstance(){
		if(instance == null)
			instance = new MyLeavesFragment();

		return instance;
	}

	public static void removeInstance(){
		if(instance != null)
			instance = null;
	}

	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_myleaves, null);
		actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		actionbarNewButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_newleaverequest);
		actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarHelpButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_help);
		((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("My Leaves");

		actionbarMenuButton.setOnClickListener(this);
		actionbarNewButton.setOnClickListener(this);
		actionbarRefreshButton.setOnClickListener(this);
		actionbarHelpButton.setOnClickListener(this);

		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater li, ViewGroup vg, Bundle b) {
		View view = li.inflate(R.layout.fragment_myleaves, null);
		mapPositions = new HashMap<Integer, Integer>();
		pager = (CustomViewPager)view.findViewById(R.id.pager);
		adapter = new LeaveListTabAdapter(getChildFragmentManager());
		pager.setAdapter(adapter);
		pager.addOnPageChangeListener(this);
		pager.setCurrentItem(0);

		buttonList = (RelativeLayout)view.findViewById(R.id.buttons_myleaves_list);
		buttonCalendar = (RelativeLayout)view.findViewById(R.id.buttons_myleaves_calendar);
		buttonList.setOnClickListener(this);
		buttonCalendar.setOnClickListener(this);
		iviewList = (ImageView)view.findViewById(R.id.iviews_myleave_list);
		iviewCalendar = (ImageView)view.findViewById(R.id.iviews_myleave_calendar);

//		yearSpinner = (Spinner)view.findViewById(R.id.choices_myleaves_search_year);
		statusSpinner = (Spinner)view.findViewById(R.id.choices_myleaves_search_status);
		typeSpinner = (Spinner)view.findViewById(R.id.choices_myleaves_search_type);

		filteredLeaves = new ArrayList<Leave>();
		types = new ArrayList<String>();
		statuses = new ArrayList<String>();

		types.add("All");
		for(int i=0; i<Leave.getTypeDescriptionList().size(); i++)
			types.add(Leave.getTypeDescriptionList().get(i));

		statuses.add("All");
		for(int i=0; i<Leave.getStatusDescriptionList().size(); i++)
			statuses.add(Leave.getStatusDescriptionList().get(i));

//		yearSpinner.setAdapter(new SimpleSpinnerAdapter(activity, app.dropDownYears, NodeSize.SIZE_NORMAL));
		statusSpinner.setAdapter(new SimpleSpinnerAdapter(activity, statuses, NodeSize.SIZE_NORMAL));
		typeSpinner.setAdapter(new SimpleSpinnerAdapter(activity, types, NodeSize.SIZE_NORMAL));

//		yearSpinner.setOnItemSelectedListener(this);
		statusSpinner.setOnItemSelectedListener(this);
		typeSpinner.setOnItemSelectedListener(this);
//		yearSpinner.setSelection(4);

		if(!app.offlineGateway.hasFinishedMyLeaveTutorial())
			showTutorial();

        observers = new ArrayList<MyLeavesDataUpdateObserver>();
        observers.add(MyLeavesListFragment.getInstance());
        observers.add(MyLeavesCalendarFragment.getInstance());

        updateList();

		return view;
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		statusSpinner.setEnabled(false);
		typeSpinner.setEnabled(false);
        buttonList.setEnabled(false);
        buttonCalendar.setEnabled(false);
        pager.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		statusSpinner.setEnabled(true);
		typeSpinner.setEnabled(true);
        buttonList.setEnabled(true);
        buttonCalendar.setEnabled(true);
        pager.setEnabled(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		updateList();
		filterLeaves();
	}

	private void updateList(){
		activity.startLoading();
		new Thread(new Runnable() {

			@Override
			public void run() {
				Object tempMyLeaveResult;

				try{
					tempMyLeaveResult = app.onlineGateway.getMyPendingAndApprovedLeaves();
				}catch(Exception e){
					tempMyLeaveResult = e.getMessage();
				}

				final Object myLeaveResult = tempMyLeaveResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {

					@Override
					public void run() {
						if(myLeaveResult instanceof String){
							String errorMessage = (String)myLeaveResult;
                            if(errorMessage.contains(SaltApplication.CONNECTION_ERROR)){
								activity.finishLoadingAndShowOutdatedData();
								updateListSuccess(app.offlineGateway.deserializeMyLeaves());
							}else
								activity.finishLoading(errorMessage);
						}else{
							activity.finishLoading();
                            updateListSuccess((ArrayList<Leave>)myLeaveResult);
						}
					}
				});
			}
		}).start();
	}

    private void updateListSuccess(ArrayList<Leave> leaves){
        app.updateMyLeaves(leaves);
        statusSpinner.setSelection(1);
        filterLeaves();
    }

//	@Override
//	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
//		Intent intent = new Intent(activity, LeaveDetailActivity.class);
//		intent.putExtra(LeaveDetailActivity.INTENTKEY_LEAVEPOS, mapPositions.get(pos));
//		startActivity(intent);
//	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		filterLeaves();
	}

	private void filterLeaves(){
		mapPositions.clear();
		filteredLeaves.clear();
		ArrayList<Leave>  appLeaves = app.getMyLeaves();
		System.out.println("LEAVE LIST: " + appLeaves.size());
		for(int i=0; i<appLeaves.size(); i++){
			Leave leave = appLeaves.get(i);
//			if(leave.getYear()==Integer.parseInt(yearSpinner.getSelectedItem().toString())){ // filter by year
			   if(statusSpinner.getSelectedItem().toString().equals(leave.getStatusDescription()) || statusSpinner.getSelectedItem().toString().equals("All")) {//filter by status
				   if(typeSpinner.getSelectedItem().toString().equals(leave.getTypeDescription()) || typeSpinner.getSelectedItem().toString().equals("All")) {
					   mapPositions.put(leave.getLeaveID(), i);
					   filteredLeaves.add(leave);
				   }
			   }
//		   }
		}

        for(MyLeavesDataUpdateObserver observer : observers){
            observer.update(filteredLeaves, mapPositions);
        }
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarMenuButton){
			actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
			activity.toggleSidebar(actionbarMenuButton);
		}else if(v == actionbarNewButton){
			startActivity(new Intent(activity, NewLeaveRequestActivity.class));
		}else if(v == actionbarRefreshButton){
			updateList();
		}else if(v == actionbarHelpButton){
			showTutorial();
		}else if(v == buttonList){
			pager.setCurrentItem(0);
            setListSelected();
		}else if(v == buttonCalendar){
			pager.setCurrentItem(1);
            setCalendarSelected();
		}
	}

    private void setListSelected(){
        iviewList.setImageDrawable(getResources().getDrawable(R.drawable.icon_leavelist_list_sel));
        iviewCalendar.setImageDrawable(getResources().getDrawable(R.drawable.icon_leavelist_grid));
    }

    private void setCalendarSelected(){
        iviewList.setImageDrawable(getResources().getDrawable(R.drawable.icon_leavelist_list));
        iviewCalendar.setImageDrawable(getResources().getDrawable(R.drawable.icon_leavelist_grid_sel));
    }

	public void sync(){
		updateList();
	}

	private void showTutorial(){
		typeSV = new ShowcaseView.Builder(activity) //new claim button tutorial
				.setTarget(new ViewTarget(typeSpinner))
				.setContentTitle("Type Filter")
				.setContentText("Clicking this button to filter list according to its leave type")
				.setStyle(R.style.CustomShowcaseTheme2)
				.build();

		statusSV = new ShowcaseView.Builder(activity) //new claim button tutorial
				.setTarget(new ViewTarget(statusSpinner))
				.setContentTitle("Status Filter")
				.setContentText("Clicking this button to filter list according to its leave status.\n\nPlease note that it is \"Submitted\" by default. You can set it later to \"All\" to remove status filtering")
				.setStyle(R.style.CustomShowcaseTheme2)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						typeSpinner.performClick();
						statusSV.hide();
						typeSV.show();
					}
				})
				.build();

		newLeaveSV = new ShowcaseView.Builder(activity) //new leave button tutorial
				.setTarget(new ViewTarget(actionbarNewButton))
				.setContentTitle("New Leave Request")
				.setContentText("Clicking this button will navigate you to file a new leave request")
				.setStyle(R.style.CustomShowcaseTheme2)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						statusSpinner.performClick();
						newLeaveSV.hide();
						statusSV.show();
					}
				})
				.build();

		typeSV.hide();
		statusSV.hide();
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	}

	@Override
	public void onPageSelected(int position) {
        if(position == 0) setListSelected();
        else setCalendarSelected();
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	private class LeaveListTabAdapter extends FragmentPagerAdapter {

		public LeaveListTabAdapter(FragmentManager fragmentManager){
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int position) {
			return (position == 0)?MyLeavesListFragment.getInstance():MyLeavesCalendarFragment.getInstance();
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			if (position >= getCount()) {
				android.support.v4.app.FragmentManager manager = ((Fragment) object).getFragmentManager();
				FragmentTransaction trans = manager.beginTransaction();
				trans.remove((Fragment) object);
				trans.commit();
			}
		}

		@Override
		public int getItemPosition (Object object) {
			return 0;
		}
	}

    interface MyLeavesDataUpdateObserver {
        void update(List<Leave> myLeaves, HashMap<Integer, Integer> mapPositions);
    }
}
