package applusvelosi.projects.android.salt2.views.fragments.claims;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.claimheaders.BusinessAdvance;
import applusvelosi.projects.android.salt2.models.claimheaders.Claim;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.models.claimitems.Project;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.utils.interfaces.LoaderStatusInterface;
import applusvelosi.projects.android.salt2.utils.threads.ClaimProjectLoader;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;
import applusvelosi.projects.android.salt2.views.dialogs.DialogStaffBusinessAdvanceList;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claiminputs.ClaimInputOverviewAdvancesFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claiminputs.ClaimInputOverviewClaimFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claiminputs.ClaimInputOverviewLiquidationFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.ClaimItemListFragment;

public abstract class ClaimHeaderFragment extends LinearNavActionbarFragment implements LoaderStatusInterface, FileManager.AttachmentDownloadListener {
    protected  ClaimHeader claimHeader;
    protected Claim claim;
    protected List<ClaimItem> tempClaimItems;
    protected List<Project> tempProjects;
    //action bar buttons
    protected TextView actionbarEditButton, actionbarSubmitButton, actionbarProcessButton, actionbarCancelButton, textviewActionbarTitle;
    protected RelativeLayout actionbarBackButton;
    protected Animation flashAnimation;
    protected RelativeLayout containerLineItem;
    protected TextView tvLineItemCnt, tvTotalAmtPaidByCC, tvTotalAmtNotPaidByCC, tvAmountApprovedPaidByCC, tvAmountApprovedNotPaidByCC, tvAmountRejectedPaidByCC, tvAmountRejectedNotPaidByCC;
    protected TextView tvTotalForPaymentPaidByCC,tvTotalForPaymentNotPaidByCC, tvTotalForDeduction, tvTotalForDeductionNotPaidByCC,tvCostCenter;
    protected TextView attachment;
    protected ClaimDetailActivity activity;
    private float totalLC, totalPositive, totalNegative, totalRejected, totalApproved, totalForDeduction, totalForPayment;
    private int processedItem, approvedItem, rejectedItem, returnedItem;
    protected TextView tvNotes;
    protected StringBuilder stringNotes, approversNote;
    private RelativeLayout dialogViewProcess;
    private EditText etextDialogProcessReason;
    private AlertDialog dialogProcess;
    private DialogStaffBusinessAdvanceList dialogStaffBA;
    private List<ClaimHeader> myBA, tempClaimHeaders;
    protected abstract View getInflatedLayout(LayoutInflater inflater);
    protected abstract void initAdditionalViewComponents(View v)throws Exception;
    protected abstract String getActionbarTitle();
//    private Document document;
//    private File attachedFile;
    private ClaimHeaderSplittingThread mClaimItemSplitThread;
    private static final int SHOW_LOADING = 0;
    private static final int DISMISS_LOADING = 1;
    private static final int DISMISS_MSG_LOADING = 2;
    private static final String ERROR_MSG = "There was an error with the internet connection. Please refresh the page and try again.";
    private Handler uiMonitorHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case SHOW_LOADING:
                    activity.startLoading();
                    break;
                case DISMISS_LOADING:
                    activity.finishLoading();
                    break;
                case DISMISS_MSG_LOADING:
                    activity.finishLoading(msg.obj.toString());
            }
        }
    };

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (ClaimDetailActivity)linearNavFragmentActivity;
        claimHeader = activity.claimHeader;
        tempProjects = activity.projects;
        flashAnimation = new AlphaAnimation(0.0f, 1.0f);
        flashAnimation.setDuration(500); //You can manage the blinking time with this parameter
        flashAnimation.setRepeatMode(Animation.REVERSE);
        flashAnimation.setRepeatCount(Animation.INFINITE);
        approversNote = new StringBuilder(claimHeader.getApproversNote());
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_claimdetails, null);
        actionbarEditButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_edit);
        actionbarSubmitButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_submit);
        actionbarProcessButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_process);
        actionbarCancelButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_cancel);
        actionbarBackButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        textviewActionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        textviewActionbarTitle.setText(getActionbarTitle());

        if(claimHeader.getStatusID() == ClaimHeader.STATUSKEY_OPEN){
            actionbarEditButton.setOnClickListener(this);
            actionbarCancelButton.setOnClickListener(this);
            actionbarSubmitButton.setOnClickListener(this);
        } else {
            actionbarEditButton.setVisibility(View.GONE);
            actionbarSubmitButton.setVisibility(View.GONE);
            actionbarCancelButton.setVisibility(View.GONE);
            if (activity.getIntent().hasExtra(activity.INTENTKEY_CLAIMHEADER)) {
                if (claimHeader.getStatusID() == ClaimHeader.STATUSKEY_SUBMITTED)
                    actionbarCancelButton.setOnClickListener(this);
                else
                    actionbarCancelButton.setOnClickListener(null);
            } else if (activity.getIntent().hasExtra(activity.INTENTKEY_CLAIMFORAPPROVAL)){
                actionbarProcessButton.setOnClickListener(this);
            }
        }
        new Thread(new ClaimProjectLoader(app,activity,this)).start();
        actionbarBackButton.setOnClickListener(this);
        textviewActionbarTitle.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mClaimItemSplitThread = new ClaimHeaderSplittingThread();
        View v = getInflatedLayout(inflater);
        try {
            initAdditionalViewComponents(v);
        }catch (Exception e){
            e.printStackTrace();
        }

        myBA = new ArrayList<ClaimHeader>();
        tempClaimHeaders = new ArrayList<>();
//        myBA.clear();
        totalLC = 0f; totalPositive = 0f; totalNegative = 0f; totalRejected = 0f; totalApproved = 0f; totalForDeduction = 0f; totalForPayment = 0f;
        dialogViewProcess = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogProcessReason = (EditText) dialogViewProcess.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView) dialogViewProcess.findViewById(R.id.tviews_dialogs_textinput)).setText("Approver's Note");
        etextDialogProcessReason.setHint("Note");
        dialogProcess = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewProcess)
                .setPositiveButton("Process", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String etextNote = etextDialogProcessReason.getText().toString().trim();
                        if (claimHeader.getStatusID() == ClaimHeader.STATUSKEY_SUBMITTED) {
                            if (etextNote.length() > 0)
                                approversNote.append("Approver: " + etextNote);
                            else
                                approversNote.append("Approver: Approved");
                        }else if (claimHeader.getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER){
                            if (etextNote.length() > 0) {
                                if (claimHeader.getOfficeID() != 41)
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                            }else {
                                if (claimHeader.getOfficeID() != 41)
                                    approversNote.append("CM: Approved");
                                else
                                    approversNote.append("CFO: Approved");
                            }
                        }
                        approversNote.append(";");
                        int claimStatusId;
                        String msg;
                        ClaimItem.UPDATABLEDATE dateUpdate;
                        if (approvedItem == activity.claimItems.size())
                            msg = "Approved Successfully!";
                        else if(rejectedItem == activity.claimItems.size())
                            msg = "Rejected Successfully!";
                        else if (returnedItem == activity.claimItems.size())
                            msg = "Returned Successfully";
                        else
                            msg = "Processed Successfully";

                        if (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO) {
                            dateUpdate = ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYCM;
                            claimStatusId = ClaimHeader.STATUSKEY_APPROVEDBYCM;
                        } else {
                            dateUpdate = ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYAPPROVER;
                            claimStatusId = ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER;
                        }
                        updateClaimHeader(claimStatusId, dateUpdate, msg, approversNote.toString());
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        return  v;
    }
    @Override
    public void onLoadSuccess(Object result) {
        activity.finishLoading();
        activity.updateProjectList((List<Project>)result);
    }

    @Override
    public void onLoadFailed(String failureMessage) {
        activity.finishLoading(failureMessage);
    }

    @Override
    public void onAttachmentDownloadFinish(File downloadedFile) {
        activity.finishLoading();
        app.fileManager.openDocument(activity,downloadedFile);
    }

    @Override
    public void onAttachmentDownloadFailed(String errorMessage) {
        activity.finishLoading();
        app.showMessageDialog(activity,errorMessage);
    }

    private void openAttachment() throws Exception {
        if (claimHeader.getAttachments().size() > 0) {
            Document document = claimHeader.getAttachments().get(0);
            int docID = document.getDocID();
            int objectTypeID = document.getObjectTypeID();
            int refID = document.getRefID();
            String filename = document.getDocName();
            app.fileManager.downloadDocument(app, docID,refID,objectTypeID,filename,this);
        }
    }

    private void computeLCSum() {
//        claimHeader = activity.claimHeader;
        int i;
        try {
            if (tempClaimItems.size() > 0) {
                for (i = 0; i < tempClaimItems.size(); ++i) {
                    int itemStatus = tempClaimItems.get(i).getStatusID();

                    if(itemStatus == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || itemStatus == ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER || itemStatus == ClaimHeader.STATUSKEY_REJECTEDBYACCOUNTS)
                        totalRejected += tempClaimItems.get(i).getLocalAmount();

                    if(itemStatus == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER || itemStatus == ClaimHeader.STATUSKEY_APPROVEDBYCM || itemStatus == ClaimHeader.STATUSKEY_APPROVEDBYACCOUNTS || itemStatus == ClaimHeader.STATUSKEY_PAIDUNDERCOMPANYCARD || itemStatus == ClaimHeader.STATUSKEY_SENTTOSAP)
                        totalApproved += tempClaimItems.get(i).getLocalAmount();

                    if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES || claimHeader.getTypeID() == ClaimHeader.TYPEKEY_LIQUIDATION) {
                        if (itemStatus != ClaimHeader.STATUSKEY_RETURN && tempClaimItems.get(i).getLocalAmount() < 0f)
                            totalLC += tempClaimItems.get(i).getLocalAmount();
                    } else {
                        if (itemStatus != ClaimHeader.STATUSKEY_RETURN)
                            totalLC += tempClaimItems.get(i).getLocalAmount();
                    }
                    if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS) {   //for claim computation
                        if(itemStatus == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                            totalForDeduction += tempClaimItems.get(i).getLocalAmount();
                    }
                    float resultNegative = 0f;
                    if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_LIQUIDATION) { //for liquidation computation
                        if (itemStatus != ClaimHeader.STATUSKEY_RETURN && tempClaimItems.get(i).getLocalAmount() > 0f)
                            totalPositive += tempClaimItems.get(i).getLocalAmount();
                        if(itemStatus != ClaimHeader.STATUSKEY_RETURN && tempClaimItems.get(i).getLocalAmount() < 0f)
                            totalNegative += tempClaimItems.get(i).getLocalAmount();

                        resultNegative = totalNegative/-1;
                        if(itemStatus == ClaimHeader.STATUSKEY_OPEN) {
                            if (resultNegative < totalPositive)
                                totalForPayment = totalNegative - totalPositive / -1;
                            else
                                totalForDeduction = ((totalNegative / -1) - totalPositive) / -1;
                        } else {
                            if (resultNegative < totalPositive)
                                totalForPayment = totalNegative - totalPositive / -1;
                            else
                                totalForDeduction = ((totalNegative / -1) - totalPositive) / -1;
                        }
                    }else if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES) //for advanced computation
                        if (resultNegative < totalApproved)
                            totalForPayment = totalApproved - totalLC / -1;
                        else
                            totalForPayment = totalApproved;
                    else //for claim computation
                        totalForPayment = 0f;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();


        processedItem = approvedItem = rejectedItem = returnedItem = 0;
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempResult;
                try{
                    tempResult = app.onlineGateway.getMyClaims(claimHeader.getStaffID());
                }catch(Exception e){
                    e.printStackTrace();
                    tempResult = e.getMessage();
                }
                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (result instanceof String) {
                            if(result.toString().contains("No address associated with hostname"))
                                app.showMessageDialog(activity,"Server Connection Failed");
                        } else {
                            activity.finishLoading();
                            tempClaimHeaders.addAll((ArrayList<ClaimHeader>)result);
                            if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES) {
                                for (ClaimHeader myClaim : tempClaimHeaders) {
                                    if (myClaim instanceof BusinessAdvance) {
                                        if (!claimHeader.getClaimNumber().equals(myClaim.getClaimNumber())) {
                                            if (myClaim.getStatusID() == ClaimHeader.STATUSKEY_SUBMITTED || myClaim.getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER || myClaim.getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYCM || myClaim.getStatusID() == ClaimHeader.STATUSKEY_SENTTOSAP || myClaim.getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYACCOUNTS)
                                                myBA.add(myClaim);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempResult;
                try {
                    tempResult = app.onlineGateway.getClaimItemsWithClaimID(claimHeader.getClaimID());
                } catch (Exception e) {
                    e.printStackTrace();
                    tempResult = e.getMessage();
                }
                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        activity.claimItems.clear();
                        if (result instanceof String) {
                            if (result.toString().contains(SaltApplication.CONNECTION_ERROR)) {
                                activity.finishLoading();
                                activity.claimItems.addAll(app.offlineGateway.deserializeMyClaimItems(activity.claimHeader.getClaimID()));
                            } else
                                activity.finishLoading(result.toString());
                        } else {
                            activity.shouldLoadLineItemOnResume = true;
                            activity.claimItems.addAll((ArrayList<ClaimItem>) result);
                            tempClaimItems = activity.claimItems;
                            computeLCSum();
                            activity.finishLoading();
                            app.offlineGateway.serializeMyClaimItems(claimHeader.getClaimID(), activity.claimItems);

                            tvLineItemCnt.setText(String.valueOf(activity.claimItems.size()));
                            claimHeader = activity.claimHeader;

                            try{
                                if (claimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS) {
                                    if (((Claim) claimHeader).isPaidByCC()) {
                                        tvTotalAmtPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalLC)) + " " + claimHeader.getCurrencySymbol());
                                        tvAmountApprovedPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalApproved)) + " " + claimHeader.getCurrencySymbol());
                                        tvAmountRejectedPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalRejected)) + " " + claimHeader.getCurrencySymbol());
                                        tvTotalForDeduction.setText(String.valueOf(SaltApplication.decimalFormat.format(totalForDeduction)) + " " + claimHeader.getCurrencySymbol());
                                        tvTotalForPaymentPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalForPayment)) + " " + claimHeader.getCurrencySymbol());
                                    } else {
                                        tvTotalAmtNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalLC)) + " " + claimHeader.getCurrencySymbol());
                                        tvAmountApprovedNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalApproved)) + " " + claimHeader.getCurrencySymbol());
                                        tvAmountRejectedNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalRejected)) + " " + claimHeader.getCurrencySymbol());
                                        tvTotalForPaymentNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalForPayment)) + " " + claimHeader.getCurrencySymbol());
                                    }
                                } else if (claimHeader.getTypeID() == ClaimHeader.TYPEKEY_LIQUIDATION || claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES) {
                                    tvTotalAmtNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalLC)) + " " + claimHeader.getCurrencySymbol());
                                    tvAmountApprovedNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalApproved)) + " " + claimHeader.getCurrencySymbol());
                                    tvTotalForDeductionNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalForDeduction)) + " " + claimHeader.getCurrencySymbol());
                                    tvAmountRejectedNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalRejected)) + " " + claimHeader.getCurrencySymbol());
                                    tvTotalForPaymentNotPaidByCC.setText(String.valueOf(SaltApplication.decimalFormat.format(totalForPayment)) + " " + claimHeader.getCurrencySymbol());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();

        selectedItems.clear();

        for (int i=0;i < activity.claimItems.size();i++) {
            if (activity.claimItems.get(i).getStatusID() != claimHeader.getStatusID()) {
                processedItem++;
                selectedItems.add(processedItem);
            }
            if (activity.claimItems.get(i).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER || activity.claimItems.get(i).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYCM)
                approvedItem++;
            else if (activity.claimItems.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || activity.claimItems.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER ||  activity.claimItems.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                rejectedItem++;
            else if (activity.claimItems.get(i).getStatusID() == ClaimHeader.STATUSKEY_RETURN)
                returnedItem++;
        }

        if (app.getStaff().isApprover() && activity.claimItems.size() > 0 && processedItem == activity.claimItems.size()) {
            actionbarProcessButton.setAnimation(flashAnimation);
            actionbarProcessButton.setVisibility(View.VISIBLE);
        } else
            actionbarProcessButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (v == actionbarBackButton || v == textviewActionbarTitle)
            linearNavFragmentActivity.onBackPressed();
        else if (v == actionbarEditButton) {
            try {
                if (activity.claimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS)
                    linearNavFragmentActivity.changePage(ClaimInputOverviewClaimFragment.newInstance(activity.getIntent().getExtras().getInt(ClaimDetailActivity.INTENTKEY_CLAIMHEADER)));
                else if (activity.claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES)
                    linearNavFragmentActivity.changePage(ClaimInputOverviewAdvancesFragment.newInstance(activity.getIntent().getExtras().getInt(ClaimDetailActivity.INTENTKEY_CLAIMHEADER)));
                else
                    linearNavFragmentActivity.changePage(ClaimInputOverviewLiquidationFragment.newInstance(activity.getIntent().getExtras().getInt(ClaimDetailActivity.INTENTKEY_CLAIMHEADER)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (v == containerLineItem) {
            if (app.getStaffOffice().hasUsesSap() && app.getStaffOffice().hasUsesSapService()) {
                if (app.getStaff().getVendorCode() != null || app.getStaffOffice().isInternalOrderRequired() && app.getStaff().getVendorCode() != null && app.getStaff().getInternalCode() != null) {
                    if (activity.projects != null) {
                        if (activity.projects.size() > 0)
                            linearNavFragmentActivity.changePage(new ClaimItemListFragment());
                        else
                            app.showMessageDialog(activity, " Sorry you cannot use this since you do not have WBS code. Kindly contact account manager for assistance.");
                    }
                } else {
                    app.showMessageDialog(activity, "Sorry you cannot use this since you do not have vendor code or internal code. Kindly contact account manager for assistance.");
                }
            } else {
                if (activity.projects != null) {
                    if (!app.getStaffOffice().hasUsesSapService() && activity.projects.size() > 0) {
                        linearNavFragmentActivity.changePage(new ClaimItemListFragment());
                    } else
                        app.showMessageDialog(activity, " Sorry you cannot use this since you do not have WBS code. Kindly contact account manager for assistance.");
                }
            }
        } else if (v == actionbarProcessButton){
            if (myBA.size() > 0) {
                if (dialogStaffBA == null)
                    dialogStaffBA = new DialogStaffBusinessAdvanceList(activity, myBA, dialogProcess);
                dialogStaffBA.show();
            }else
                dialogProcess.show();
        } else if(v == actionbarCancelButton){
            new AlertDialog.Builder(activity).setTitle(null).setMessage("This action will change the status of this claim request")
                    .setNegativeButton("Reopen", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            updateClaimHeader(ClaimHeader.STATUSKEY_OPEN, ClaimItem.UPDATABLEDATE.NONE, "Reopened Successfully!", "cancelled");
                        }
                    })
                    .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            updateClaimHeader(ClaimHeader.STATUSKEY_CANCELLED, ClaimItem.UPDATABLEDATE.DATECANCELLED, "Cancelled Successfully!", "");
                        }
                    }).create().show();
        }else if(v == actionbarSubmitButton) {
            if(activity.claimItems.size() > 0)
                updateClaimHeader(ClaimHeader.STATUSKEY_SUBMITTED, ClaimItem.UPDATABLEDATE.DATESUBMITTED, "Submitted Successfully!", "");
            else
                app.showMessageDialog(activity, "Cannot submit claim header. Please attach at least 1 item");
        } else if(v == attachment){
            activity.startLoading();
            try {
                openAttachment();
            }catch (Exception e){
                e.printStackTrace();
                activity.finishLoading(e.getMessage());
            }
        }
    }

    private void updateClaimHeader(int statusID, ClaimItem.UPDATABLEDATE updatabledate, String successMessage, String approverNote) {
        actionbarEditButton.setEnabled(false);
        actionbarCancelButton.setEnabled(false);
        actionbarSubmitButton.setEnabled(false);
        mClaimItemSplitThread.setStatusID(statusID);
        mClaimItemSplitThread.setUpdatableDate(updatabledate);
        mClaimItemSplitThread.setProcessMessage(successMessage);
        mClaimItemSplitThread.setApproversNote(approverNote);
        if(mClaimItemSplitThread.getState() == Thread.State.NEW)
            mClaimItemSplitThread.start();
    }

    private class ClaimHeaderSplittingThread extends HandlerThread {
        private static final int STATE_A_PROCESS_CLAIM = 1;
        private static final int STATE_B_PROCESS_BA = 2;
        private static final int STATE_C_PROCESS_LIQUIDATION = 3;

        private Handler backgroundMonitorHandler;
        private int statusID;
        private ClaimItem.UPDATABLEDATE updatableDate;
        private String successMessage, approversNote;
        private float usdAmount;
        private int staffCMId;

        public ClaimHeaderSplittingThread() {
            super("ClaimHeaderSplittingThread", Process.THREAD_PRIORITY_BACKGROUND);
            staffCMId = claimHeader.getStaffCMId();
        }

        public void setStatusID(final int statusID) {
            this.statusID = statusID;
        }

        public void setUpdatableDate(ClaimItem.UPDATABLEDATE updatableDate) {
            this.updatableDate = updatableDate;
        }

        public void setProcessMessage(String successMessage) {
            this.successMessage = successMessage;
        }

        public void setApproversNote(String note) {
            this.approversNote = note;
        }

        @Override
        protected void onLooperPrepared() {
            super.onLooperPrepared();

            backgroundMonitorHandler = new Handler(getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    uiMonitorHandler.sendEmptyMessage(SHOW_LOADING);
                    switch (msg.what) {
                        case STATE_A_PROCESS_CLAIM: {
                            backgroundMonitorHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    List<ClaimItem> noReceiptApproveItems = new ArrayList<>();
                                    int splitClaimId, newClaimId, rejectedStatusCtr, returnStatusCtr, approveNoReceipt, approveYesReceipt;
                                    rejectedStatusCtr = returnStatusCtr = approveNoReceipt = approveYesReceipt = 0;
                                    List<ClaimItem> items = activity.claimItems;

                                    if (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO) {
                                        int statusId;
                                        splitClaimId = 0;
                                        for (int i = 0; i < items.size(); i++) {
                                            if (items.get(i).getStatusID() != ClaimHeader.STATUSKEY_RETURN) {
                                                if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYCM) {
                                                    Object result = processClaimStatus(claimHeader.getClaimID(), statusID, 0, approversNote);
                                                    if (result != null)
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, result.toString()));
                                                } else {
                                                    rejectedStatusCtr++;
                                                }
                                            } else {
                                                try {
                                                    ClaimHeader splittedClaimHeader = new ClaimHeader(claimHeader);
                                                    if (splitClaimId != 0) {
                                                        items.get(i).setClaimID(splitClaimId);
                                                        Object resultSplitItem = saveClaimLineItem(items.get(i), 0, null);
                                                        if (resultSplitItem instanceof String)
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                    } else {
                                                        splittedClaimHeader.updateSplittedChildClaim(app, ClaimHeader.STATUSKEY_OPEN, 0, "", claimHeader.getClaimID(), claimHeader.getClaimNumber(), staffCMId, updatableDate, approversNote);
                                                        Object resultSplitClaim = saveClaimHeader(splittedClaimHeader, claimHeader.getClaimID(), null);
                                                        if (!(resultSplitClaim instanceof String)) {
                                                            JSONObject jsonSplitClaim = (JSONObject) resultSplitClaim;
                                                            if (jsonSplitClaim.getInt("ProcessedClaimID") != 0) {
                                                                splitClaimId = jsonSplitClaim.getInt("ProcessedClaimID");
                                                                for (ClaimItem returnItem : items) {
                                                                    if (returnItem.getStatusID() == ClaimHeader.STATUSKEY_RETURN) {
                                                                        returnItem.setClaimID(splitClaimId);
                                                                        returnItem.setClaimStatusID(ClaimHeader.STATUSKEY_OPEN);
                                                                        Object resultSplitItem = saveClaimLineItem(returnItem, 0, null);
                                                                        if (resultSplitItem instanceof String)
                                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                                    }
                                                                }
//                                                                items.get(i).setClaimID(splitClaimId);
//                                                                Object resultReturnItem = saveClaimLineItem(items.get(i), 0, null);
//                                                                if (resultReturnItem instanceof String)
//                                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                            } else if(jsonSplitClaim.getJSONArray("SystemErrors").length() > 0) {
                                                                String errMsg = jsonSplitClaim.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, errMsg));
                                                            } else {
                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                            }
                                                        }
                                                    }
                                                }catch(JSONException e) {
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                }
                                            }
                                        }
                                        statusId = (rejectedStatusCtr == activity.claimItems.size()) ? ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER : statusID;
                                        Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                        if (resultClaim != null)
                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                    } else { // start immediate manager processing
                                        for (int i = 0; i < items.size(); i++ ) {
                                            Object resultForex = obtainClaimItemForex(items.get(i));
                                            if (!(resultForex instanceof String)) {
                                                usdAmount = (float) resultForex * items.get(i).getForeignAmount();
                                                String amountInUSD = SaltApplication.decimalFormat.format(usdAmount);
                                                try {
                                                    if (items.get(i).getStatusID() != ClaimHeader.STATUSKEY_RETURN) {
                                                        if (app.versionCompare(amountInUSD, "10.00") > 0 || app.versionCompare(amountInUSD, "10.00") == 0) { // 10$
                                                            if (items.get(i).getHasAttachmentFlag() == 1) {     // with attachment
                                                                if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER)
                                                                    approveYesReceipt++;
                                                                 else
                                                                    rejectedStatusCtr++;
                                                            } else {
                                                                if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER) { //for splitted without receipt item
                                                                    approveNoReceipt++;
                                                                    noReceiptApproveItems.add(items.get(i));
                                                                } else {
                                                                    rejectedStatusCtr++;
                                                                }
                                                            }
                                                        } else {
                                                            if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                                                                rejectedStatusCtr++;
                                                        }
                                                    } else {
                                                        returnStatusCtr++;
                                                    }
                                                } catch (Exception e) {
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, e.getMessage()));
                                                }
                                            } else {
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultForex.toString()));
                                            }
                                        }

                                        if (approveNoReceipt == activity.claimItems.size()) {
                                            Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusID, staffCMId, approversNote);
                                            if (resultClaim != null)
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                        } else if (returnStatusCtr == activity.claimItems.size()) {
                                            Object resultClaim = processClaimStatus(claimHeader.getClaimID(), ClaimHeader.STATUSKEY_OPEN, 0, approversNote);
                                            if (resultClaim != null)
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                            for (ClaimItem returnItem : activity.claimItems) {
//                                                returnItem.setClaimID(claimHeader.getClaimID());
                                                returnItem.setClaimStatusID(ClaimHeader.STATUSKEY_OPEN);
                                                Object resultSplitItem = saveClaimLineItem(returnItem, 0, null);
                                                if (resultSplitItem instanceof String)
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                            }
                                        } else {
                                            Object resultSplitClaim;
                                            ClaimHeader splittedClaimHeader;
                                            int statusId;
                                            if ( returnStatusCtr != 0 ) {
                                                ClaimHeader newClaimHeader = new ClaimHeader(claimHeader);
                                                newClaimHeader.updateSplittedChildClaim(app, ClaimHeader.STATUSKEY_OPEN, 0, "", claimHeader.getClaimID(), claimHeader.getClaimNumber(), 0, updatableDate, approversNote);
                                                resultSplitClaim = saveClaimHeader(newClaimHeader, claimHeader.getClaimID(), null);
                                                if(!(resultSplitClaim instanceof String)) {
                                                    JSONObject jsonSplitClaim = (JSONObject) resultSplitClaim;
                                                    try {
                                                        if (jsonSplitClaim.getInt("ProcessedClaimID") != 0) {
                                                            newClaimId = jsonSplitClaim.getInt("ProcessedClaimID");
                                                            for (ClaimItem returnItem : items) {
                                                                if (returnItem.getStatusID() == ClaimHeader.STATUSKEY_RETURN) {
                                                                    returnItem.setClaimID(newClaimId);
                                                                    returnItem.setClaimStatusID(ClaimHeader.STATUSKEY_OPEN);
                                                                    Object resultSplitItem = saveClaimLineItem(returnItem, 0, null);
                                                                    if (resultSplitItem instanceof String)
                                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                                }
                                                            }
                                                        } else if (jsonSplitClaim.getJSONArray("SystemErrors").length() > 0) {
                                                            String errMsg = jsonSplitClaim.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, errMsg));
                                                        } else {
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                        }
                                                    } catch (JSONException e) {
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                    }
                                                }
                                                Object result = getClaimLineItems(claimHeader.getClaimID());
                                                rejectedStatusCtr = 0;
                                                if(!(result instanceof String)) {
                                                    List<ClaimItem> parentLineItems = (ArrayList<ClaimItem>)result;
                                                    for (ClaimItem noReturnItem : parentLineItems) {
                                                        if (noReturnItem.getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || noReturnItem.getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                                                            rejectedStatusCtr++;
                                                    }
                                                    if (rejectedStatusCtr == parentLineItems.size()) {
                                                        Object resultClaim = processClaimStatus(claimHeader.getClaimID(), ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER, 0, approversNote);
                                                        if (resultClaim != null)
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                                    } else if (approveNoReceipt == parentLineItems.size() || approveYesReceipt == parentLineItems.size()) {
                                                        Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusID, staffCMId, approversNote);
                                                        if (resultClaim != null)
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                                    } else {  //with approve and reject Items
                                                        if (approveNoReceipt != 0) {
                                                            splittedClaimHeader = new ClaimHeader(claimHeader);
                                                            splittedClaimHeader.updateSplittedChildClaim(app, statusID, 0, "", claimHeader.getClaimID(), claimHeader.getClaimNumber(), staffCMId, updatableDate, approversNote);
                                                            resultSplitClaim = saveClaimHeader(splittedClaimHeader, claimHeader.getClaimID(), null);
                                                            if (!(resultSplitClaim instanceof String)) {
                                                                try {
                                                                    JSONObject jsonSplitClaim = (JSONObject) resultSplitClaim;
                                                                    if (jsonSplitClaim.getInt("ProcessedClaimID") != 0) {
                                                                        splitClaimId = jsonSplitClaim.getInt("ProcessedClaimID");
                                                                        for (ClaimItem splitItem : noReceiptApproveItems) {
                                                                            splitItem.setClaimID(splitClaimId);
                                                                            Object resultSplitItem = saveClaimLineItem(splitItem, 0, null);
                                                                            if (resultSplitItem instanceof String)
                                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                                        }
                                                                    } else if (jsonSplitClaim.getJSONArray("SystemErrors").length() > 0) {
                                                                        String errMsg = jsonSplitClaim.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
                                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, errMsg));
                                                                    } else {
                                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                                    }
                                                                } catch (JSONException e) {
                                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                                }
                                                                statusId = (rejectedStatusCtr == parentLineItems.size()) ? ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER : statusID;
                                                                Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                                                if (resultClaim != null)
                                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                                            }
                                                        } else {
                                                            Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusID, 0, approversNote);
                                                            if (resultClaim != null)
                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                                        }
                                                    }
                                                } else {
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                }
                                            } else { // for approved and rejected only and Yes/No Receipt
                                                if (approveNoReceipt != 0) {
                                                    splittedClaimHeader = new ClaimHeader(claimHeader);
                                                    splittedClaimHeader.updateSplittedChildClaim(app, statusID, 0, "", claimHeader.getClaimID(), claimHeader.getClaimNumber(), staffCMId, updatableDate, approversNote);
                                                    resultSplitClaim = saveClaimHeader(splittedClaimHeader, claimHeader.getClaimID(), null);
                                                    if (!(resultSplitClaim instanceof String)) {
                                                        try {
                                                            JSONObject jsonSplitClaim = (JSONObject) resultSplitClaim;
                                                            if (jsonSplitClaim.getInt("ProcessedClaimID") != 0) {
                                                                splitClaimId = jsonSplitClaim.getInt("ProcessedClaimID");
                                                                for (ClaimItem splitItem : noReceiptApproveItems) {
                                                                    splitItem.setClaimID(splitClaimId);
                                                                    Object resultSplitItem = saveClaimLineItem(splitItem, 0, null);
                                                                    if (resultSplitItem instanceof String)
                                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                                }
                                                            } else if (jsonSplitClaim.getJSONArray("SystemErrors").length() > 0) {
                                                                String errMsg = jsonSplitClaim.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, errMsg));
                                                            } else {
                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                            }
                                                        } catch (JSONException e) {
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                        }

                                                        Object result = getClaimLineItems(claimHeader.getClaimID());
                                                        if(!(result instanceof String)) {
                                                            List<ClaimItem> parentLineItems = (List<ClaimItem>) result;
                                                            statusId = (rejectedStatusCtr == parentLineItems.size()) ? ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER : statusID;
                                                            Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                                            if (resultClaim != null)
                                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));

                                                        }else {
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                        }
                                                    } else {
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitClaim.toString()));
                                                    }
                                                } else {
                                                    statusId = (rejectedStatusCtr == activity.claimItems.size())?ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER:statusID;
                                                    Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                                    if (resultClaim != null)
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                                }
                                            }
                                        }
                                    } // end immediate manager processing
                                    uiMonitorHandler.sendEmptyMessage(DISMISS_LOADING);
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(activity, successMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    linearNavFragmentActivity.finish();
                                }
                            });
                        }
                        break;
                        case STATE_B_PROCESS_BA: {
                            backgroundMonitorHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    for (ClaimItem baItem : activity.claimItems) {
                                        if(baItem.getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || baItem.getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER) {
                                            int rejectedIdKey = (app.getStaff().getUserPosition() != Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() != Staff.USERPOSITION.USERPOSITION_CFO)?ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER:ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER;
                                            Object result = processClaimStatus(claimHeader.getClaimID(), rejectedIdKey, 0, approversNote);
                                            if (result instanceof String)
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                        } else {
                                            int nextApprover = (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)?0:staffCMId;
                                            Object result = processClaimStatus(claimHeader.getClaimID(), statusID, nextApprover, approversNote);
                                            if (result instanceof String)
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                        }
                                    }
                                    uiMonitorHandler.sendEmptyMessage(DISMISS_LOADING);
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(activity, successMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    linearNavFragmentActivity.finish();
                                }
                            });
                        }
                        break;
                        case STATE_C_PROCESS_LIQUIDATION: {
                            backgroundMonitorHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    List<ClaimItem> noReceiptApproveItems = new ArrayList<>();
                                    int splitClaimId, rejectedStatusCtr, returnStatusCtr, approveNoReceipt;
                                    rejectedStatusCtr = returnStatusCtr = approveNoReceipt = 0;
                                    List<ClaimItem> items = activity.claimItems;

                                    if (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO) {
                                        int statusId, i, itemSize;
                                        i = (items.get(0).getCategoryTypeID() == Category.TYPE_BUSINESSADVANCE)?1:0;
                                        for (; i < items.size(); i++) {
                                            if (items.get(i).getStatusID() != ClaimHeader.STATUSKEY_RETURN) {
                                                if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER || items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                                                    rejectedStatusCtr++;
                                            } else {
                                                items.get(i).setClaimID(claimHeader.getClaimID());
                                                Object resultSplitItem = saveClaimLineItem(items.get(i), 0, null);
                                                if (resultSplitItem instanceof String)
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                            }
                                        }
                                        itemSize = (items.get(0).getCategoryTypeID() == Category.TYPE_BUSINESSADVANCE)?items.size()-1:items.size();
                                        statusId = (rejectedStatusCtr == itemSize) ? ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER : statusID;
                                        Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                        if (resultClaim != null)
                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                    } else {               // start immediate manager processing
                                        for (int i = 1; i < items.size(); i++ ) {
                                            Object resultForex = obtainClaimItemForex(items.get(i));
                                            if (!(resultForex instanceof String)) {
                                                usdAmount = (float) resultForex * items.get(i).getForeignAmount();
                                                String amountInUSD = SaltApplication.decimalFormat.format(usdAmount);
                                                try {
                                                    if (items.get(i).getStatusID() != ClaimHeader.STATUSKEY_RETURN) {
                                                        if (app.versionCompare(amountInUSD, "10.00") > 0 || app.versionCompare(amountInUSD, "10.00") == 0) { // 10$
                                                            if (items.get(i).getHasAttachmentFlag() == 1) {     // with attachment
                                                                if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                                                                    rejectedStatusCtr++;
                                                            } else {
                                                                if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER) { //for splitted without receipt item
                                                                    approveNoReceipt++;
                                                                    noReceiptApproveItems.add(items.get(i));
                                                                } else {
                                                                    rejectedStatusCtr++;
                                                                }
                                                            }
                                                        } else {
                                                            if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || items.get(i).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION)
                                                                rejectedStatusCtr++;
                                                        }
                                                    } else {
                                                        returnStatusCtr++;
                                                    }
                                                } catch (Exception e) {
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, e.getMessage()));
                                                }
                                            } else {
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultForex.toString()));
                                            }
                                        }

                                        if (approveNoReceipt == items.size()-1) {
                                            Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusID, staffCMId, approversNote);
                                            if (resultClaim != null)
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                        } else if (returnStatusCtr == items.size()-1) {
                                            Object resultClaim = processClaimStatus(claimHeader.getClaimID(), ClaimHeader.STATUSKEY_OPEN, 0, approversNote);
                                            if (resultClaim != null)
                                                uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                            else {
                                                for (int i = 1; i < items.size(); i++) {
                                                    items.get(i).setClaimStatusID(ClaimHeader.STATUSKEY_OPEN);
                                                    Object resultSplitItem = saveClaimLineItem(items.get(i), 0, null);
                                                    if (resultSplitItem instanceof String)
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                }
                                            }
                                        } else { //with approved/rejected/return line items
                                            Object resultSplitClaim;
                                            ClaimHeader splitClaimHeader;
                                            int statusId = 0;
                                            if ( returnStatusCtr != 0 ) {
                                                Object resultClaim = processClaimStatus(claimHeader.getClaimID(), ClaimHeader.STATUSKEY_OPEN, 0, approversNote);
                                                if (resultClaim != null)
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                                for (int i = 1; i < items.size(); i++) {
                                                    if (items.get(i).getStatusID() == ClaimHeader.STATUSKEY_RETURN) {
                                                        items.get(i).setClaimStatusID(ClaimHeader.STATUSKEY_OPEN);
                                                        Object resultSplitItem = saveClaimLineItem(items.get(i), 0, null);
                                                        if (resultSplitItem instanceof String)
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                    }
                                                }
                                            }
                                            if (approveNoReceipt != 0 ) {
                                                splitClaimHeader = new ClaimHeader(claimHeader);
                                                splitClaimHeader.updateSplittedChildClaim(app, statusID, 0, "", claimHeader.getClaimID(), claimHeader.getClaimNumber(), app.getStaffOffice().getCMID(), updatableDate, approversNote);
                                                resultSplitClaim = saveClaimHeader(splitClaimHeader, claimHeader.getClaimID(), null);
                                                if (!(resultSplitClaim instanceof String)) {
                                                    try {
                                                        JSONObject jsonSplitClaim = (JSONObject) resultSplitClaim;
                                                        if (jsonSplitClaim.getInt("ProcessedClaimID") != 0) {
                                                            splitClaimId = jsonSplitClaim.getInt("ProcessedClaimID");
                                                            for (int i = 0; i < noReceiptApproveItems.size(); i++) {
                                                                noReceiptApproveItems.get(i).setClaimID(splitClaimId);
                                                                Object resultSplitItem = saveClaimLineItem(noReceiptApproveItems.get(i), 0, null);
                                                                if (resultSplitItem instanceof String)
                                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultSplitItem.toString()));
                                                            }
                                                        } else if (jsonSplitClaim.getJSONArray("SystemErrors").length() > 0) {
                                                            String errMsg = jsonSplitClaim.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, errMsg));
                                                        } else {
                                                            uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                        }
                                                    } catch (JSONException e) {
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                    }
                                                }else{
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                }
                                                if(returnStatusCtr == 0) {
                                                    Object result = getClaimLineItems(claimHeader.getClaimID());
                                                    if (!(result instanceof String)) {
                                                        List<ClaimItem> parentLineItems = (List<ClaimItem>)  result;
                                                        statusId = (rejectedStatusCtr == parentLineItems.size()) ? ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER : statusID;
                                                    } else {
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                    }
                                                }else{
                                                    statusId = ClaimHeader.STATUSKEY_OPEN;
                                                }
                                                Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                                if (resultClaim != null)
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                            } else {
                                                if (returnStatusCtr == 0){
                                                    Object result = getClaimLineItems(claimHeader.getClaimID());
                                                    if (!(result instanceof String)) {
                                                        List<ClaimItem> parentLineItems = (List<ClaimItem>) result;
                                                        statusId = (rejectedStatusCtr == parentLineItems.size()) ? ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER : statusID;
                                                    } else {
                                                        uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, ERROR_MSG));
                                                    }
                                                }else{
                                                    statusId = ClaimHeader.STATUSKEY_OPEN;
                                                }
                                                Object resultClaim = processClaimStatus(claimHeader.getClaimID(), statusId, 0, approversNote);
                                                if (resultClaim != null)
                                                    uiMonitorHandler.sendMessage(obtainMessage(DISMISS_MSG_LOADING, resultClaim.toString()));
                                            }

                                        }
                                    } // end immediate manager processing
                                    uiMonitorHandler.sendEmptyMessage(DISMISS_LOADING);
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(activity, successMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    linearNavFragmentActivity.finish();
                                }
                            });
                        }
                        break;
                    }
                }
            };
            processExpenseForApproval();
        }

        public Object getClaimLineItems(int claimId) {
            try {
                return app.onlineGateway.getClaimItemsWithClaimID(claimId);
            }catch (Exception e){
                return ERROR_MSG;
            }
        }

        private Object obtainClaimItemForex(ClaimItem claimItem) {
            return app.onlineGateway.getGoogleForexRate(claimItem.getForeignCurrencyName(), "USD");
        }

        private Object saveClaimHeader(ClaimHeader claimData, int oldClaimID, File file){
            try {
                return app.onlineGateway.saveClaim(claimData, oldClaimID, file);
            }catch (Exception e) {
                return ERROR_MSG;
            }
        }

        private Object processClaimStatus(int claimId, int claimStatusId, int approversId, String note) {
            try{
                return app.onlineGateway.processClaim(claimId, claimStatusId, approversId, note);
            }catch (Exception e){
                return ERROR_MSG;
            }
        }

        private Object saveClaimLineItem(ClaimItem claimItemData, int oldClaimItemID, File file) {
            try {
                return app.onlineGateway.saveClaimLineItem(claimItemData, oldClaimItemID, file);
            }catch (Exception e){
                return ERROR_MSG;
            }
        }

        private void processExpenseForApproval() {
            if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS)
                backgroundMonitorHandler.sendEmptyMessage(STATE_A_PROCESS_CLAIM);
            else if(claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES)
                backgroundMonitorHandler.sendEmptyMessage(STATE_B_PROCESS_BA);
            else
                backgroundMonitorHandler.sendEmptyMessage(STATE_C_PROCESS_LIQUIDATION);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mClaimItemSplitThread.quit();
    }


}
