package applusvelosi.projects.android.salt2.models;

import java.util.ArrayList;
import java.util.Date;

import applusvelosi.projects.android.salt2.utils.enums.CalendarItemType;

public class CalendarItem {
	private CalendarItemType type;
	private String label;
	private ArrayList<CalendarEvent> events;
	private Date date;
	public CalendarItem(CalendarItemType type, String label, ArrayList<CalendarEvent> events, Date date){
		this.events = events;
		this.label = label;
		this.type = type;
		this.date = date;
	}
	
	public CalendarItemType getType(){
		return type;
	}
	
	public String getLabel(){
		return label;
	}
	
	public ArrayList<CalendarEvent> getEvents(){
		return events;
	}

	public Date getDate(){ return date; }
}
