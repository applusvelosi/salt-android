package applusvelosi.projects.android.salt2.views.dialogs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.SapTaxCodeSettings;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.ManageClaimItemActivity;

/**
 * Created by Velosi on 05/07/2016.
 */
public class DialogItemSapTaxCodeList implements ManageClaimItemActivity.ItemStatusReloader, AdapterView.OnItemClickListener, ListAdapterInterface{

    private ListView lv;
    private ListAdapter adapter;
    private TextView tvHeader, tvNotificationMessage;
    private LinearLayout viewContainer;
    private AlertDialog ad;
    private ManageClaimItemActivity activity;
    private DialogClaimItemSapTaxCodeInterface dialogClaimInf;

    public DialogItemSapTaxCodeList(final ManageClaimItemActivity activity, DialogClaimItemSapTaxCodeInterface inf){
        this.activity = activity;
        dialogClaimInf = inf;
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_list, null);
        tvNotificationMessage = (TextView)view.findViewById(R.id.tviews_dialogs_list);
        viewContainer = (LinearLayout)view.findViewById(R.id.containers_dialogs_list);
        tvHeader = (TextView)view.findViewById(R.id.tviews_dialogs_list_header);
        lv = (ListView)view.findViewById(R.id.lists_dialogs_list);
        adapter = new ListAdapter(this);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        tvHeader.setText("Select Tax Rate");
        ad = new AlertDialog.Builder(activity).setTitle(null).setView(view)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
    }

    public void show(){
        ad.show();
    }

    @Override
    public void onSuccess() {
        if(activity.getSapTaxCodes().size() > 0){
            viewContainer.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }else
            tvNotificationMessage.setText("No Items");
    }

    @Override
    public void onFailed(String message) {
        tvNotificationMessage.setText(message);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null)
            v = LayoutInflater.from(activity).inflate(R.layout.node_headeronly,null);

        ((TextView)v.findViewById(R.id.tviews_nodes_headeronly_header)).setText(activity.getSapTaxCodes().get(position).getSapCode()+" - ("+activity.getSapTaxCodes().get(position).getValue()+") - "+activity.getSapTaxCodes().get(position).getDescription());

        return v;
    }

    @Override
    public int getCount() {
        return activity.getSapTaxCodes().size();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ad.dismiss();
        dialogClaimInf.onSapTaxCodeSelected(activity.getSapTaxCodes().get(position));
    }

    public interface DialogClaimItemSapTaxCodeInterface{
        void onSapTaxCodeSelected(SapTaxCodeSettings sapTaxCodeSettings);
    }
}
