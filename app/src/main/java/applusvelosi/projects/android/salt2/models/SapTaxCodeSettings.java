package applusvelosi.projects.android.salt2.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import applusvelosi.projects.android.salt2.SaltApplication;

/**
 * Created by Velosi on 05/07/2016.
 */
public class SapTaxCodeSettings implements Serializable {

    private String dateCreated, dateModified;
    private String jurisdictionCode;
    private String isActive;
    private String sapCode;
    private String value;
    private String description;
    private SaltApplication app;

    public SapTaxCodeSettings(SaltApplication app,JSONObject jsonSapTax) throws JSONException {
        this.app = app;
        this.dateCreated = jsonSapTax.getString("DateCreated");
        this.dateModified = jsonSapTax.getString("DateModified");
        this.description = jsonSapTax.getString("Description");
        this.jurisdictionCode = jsonSapTax.getString("JurisdictionCode");
        this.isActive = jsonSapTax.getString("Active");
        this.value = jsonSapTax.getString("Value");
        this.sapCode = jsonSapTax.getString("SapCode");
    }

    public SapTaxCodeSettings(String isActive, String sapCode, String value, String jurisdictionCode, String description){
        this.isActive = isActive;
        this.description = description;
        this.sapCode = sapCode;
        this.value = value;
        this.jurisdictionCode = jurisdictionCode;
    }

    public String getDateCreated(){
        return app.onlineGateway.dJsonizeDate(dateCreated);
    }

    public String getDateModified(){
        return app.onlineGateway.dJsonizeDate(dateModified);
    }

    public String getJurisdictionCode(){
        return jurisdictionCode;
    }

    public boolean isActive(){
        return Boolean.parseBoolean(isActive);
    }

    public String getSapCode(){
        return sapCode;
    }

    public String getValue(){
        return value;
    }

    public String getDescription() {return  description; }

}
