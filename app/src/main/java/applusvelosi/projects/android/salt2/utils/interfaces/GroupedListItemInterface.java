package applusvelosi.projects.android.salt2.utils.interfaces;

import android.app.Activity;
import android.view.View;

public interface GroupedListItemInterface {
	View getTextView(Activity activity);
}
