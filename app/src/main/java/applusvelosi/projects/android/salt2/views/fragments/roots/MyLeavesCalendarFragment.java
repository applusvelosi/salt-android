package applusvelosi.projects.android.salt2.views.fragments.roots;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.grids.MyCalendarAdapter;
import applusvelosi.projects.android.salt2.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.salt2.models.CalendarEventLeave;
import applusvelosi.projects.android.salt2.models.CalendarItem;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.CalendarInterface;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.LeaveDetailActivity;

/**
 * Created by rickroydaban on 03/05/16.
 */
public class MyLeavesCalendarFragment extends Fragment implements MyLeavesFragment.MyLeavesDataUpdateObserver, CalendarInterface, View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static MyLeavesCalendarFragment instance;

    private SaltApplication app;
    private ImageView buttonPrevMonth, buttonNextMonth;
    private Spinner spinnerMonth, spinnerYear;
    private GridView calendarView;

    //for calendarView comparisons
    private Calendar minCalendar, maxCalendar;

    private MyCalendarAdapter calendarAdapter;
    private ArrayList<Leave> leaves;
    private ArrayList<CountryHoliday> holidays;
    private Map<Integer, Integer> mapLeavePositions;

    private RelativeLayout dialogEventsContainer;
    private ListView dialogEventsLV;
    private AlertDialog dialogEvents;

    public static MyLeavesCalendarFragment getInstance(){
        if(instance == null)
            instance = new MyLeavesCalendarFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_myleaves_calendar, null);
        buttonPrevMonth = (ImageView)v.findViewById(R.id.buttons_calendarview_prevmonth);
        buttonNextMonth = (ImageView)v.findViewById(R.id.buttons_calendarview_nextmonth);
        spinnerMonth = (Spinner) v.findViewById(R.id.choices_calendarview_month);
        spinnerYear = (Spinner ) v.findViewById(R.id.choices_calendarview_year);
        calendarView = (GridView) v.findViewById(R.id.calendarview);

        app = ((SaltApplication)getActivity().getApplication());
        minCalendar = Calendar.getInstance();
        minCalendar.set(Calendar.MONTH, Calendar.JANUARY);
        minCalendar.set(Calendar.YEAR, Integer.parseInt(app.dropDownYears.get(0)));
        maxCalendar = Calendar.getInstance();
        maxCalendar.set(Calendar.MONTH, Calendar.DECEMBER);
        maxCalendar.set(Calendar.YEAR, Integer.parseInt(app.dropDownYears.get(app.dropDownYears.size()-1)));

        mapLeavePositions = new HashMap<Integer, Integer>();
        leaves = new ArrayList<Leave>();
        holidays = new ArrayList<CountryHoliday>();
        spinnerMonth.setAdapter(new SimpleSpinnerAdapter(getActivity(), app.dropDownMonths, SimpleSpinnerAdapter.NodeSize.SIZE_NORMAL));
        spinnerYear.setAdapter(new SimpleSpinnerAdapter(getActivity(), app.dropDownYears, SimpleSpinnerAdapter.NodeSize.SIZE_NORMAL));
        spinnerMonth.setSelection(Calendar.getInstance().get(Calendar.MONTH));
        spinnerYear.setSelection(app.dropDownYears.indexOf(String.valueOf(Calendar.getInstance().get(Calendar.YEAR))));

        calendarAdapter = new MyCalendarAdapter(this, minCalendar, maxCalendar, spinnerMonth, spinnerYear, buttonPrevMonth, buttonNextMonth, leaves, holidays);
        calendarView.setAdapter(calendarAdapter);

        buttonPrevMonth.setOnClickListener(this);
        buttonNextMonth.setOnClickListener(this);
        spinnerMonth.setOnItemSelectedListener(this);
        spinnerYear.setOnItemSelectedListener(this);

        dialogEventsContainer = (RelativeLayout) inflater.inflate(R.layout.dialog_list, null);
        ((TextView) dialogEventsContainer.findViewById(R.id.tviews_dialogs_list_header)).setText("Events");
        dialogEventsLV = (ListView) dialogEventsContainer.findViewById(R.id.lists_dialogs_list);
        dialogEvents = new AlertDialog.Builder(getActivity()).setView(dialogEventsContainer)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        return v;
    }

    @Override
    public void onCalendarItemClicked(final CalendarItem calendarItem) {

        try {
            dialogEventsLV.setAdapter(new ListAdapter(new ListAdapterInterface() {
                @Override
                public View getView(int pos, View recyclableView, ViewGroup parent) {
                    View view = recyclableView;
                    LeaveNodeHolder holder;

                    if (view == null) {
                        holder = new LeaveNodeHolder();
                        view = getActivity().getLayoutInflater().inflate(R.layout.node_headerdetailstatus, null);
                        holder.typeTV = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_header);
                        holder.statusTV = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_status);
                        holder.datesTV = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_detail);

                        view.setTag(holder);
                    }

                    holder = (LeaveNodeHolder) view.getTag();
                    Leave leave = ((CalendarEventLeave) calendarItem.getEvents().get(pos)).getLeave();
                    holder.typeTV.setText(leave.getTypeDescription());
                    holder.statusTV.setText(leave.getStatusDescription());
                    if (leave.getStatusID() == Leave.LEAVESTATUSAPPROVEDKEY)
                        holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.green));
                    else if (leave.getStatusID() == Leave.LEAVESTATUSPENDINGID)
                        holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.black));
                    else
                        holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.red));

                    if (leave.getEndDate() != null)
                        holder.datesTV.setText(leave.getStartDate() + " - " + leave.getEndDate());
                    else
                        holder.datesTV.setText(leave.getStartDate());

                    return view;
                }

                @Override
                public int getCount() {
                    return calendarItem.getEvents().size();
                }


            }));
            dialogEventsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), LeaveDetailActivity.class);
                    intent.putExtra(LeaveDetailActivity.INTENTKEY_LEAVEPOS, mapLeavePositions.get(((CalendarEventLeave) calendarItem.getEvents().get(position)).getLeave().getLeaveID()));
                    startActivity(intent);
                }
            });

            dialogEvents.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public void onClick(View v) {
        if(v == buttonPrevMonth){
            calendarAdapter.prevMonth();
        }else if(v == buttonNextMonth){
            calendarAdapter.nextMonth();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        Calendar headerSelectionCalendar = Calendar.getInstance();
        if(parent.getTag()==null || Integer.parseInt(parent.getTag().toString())!=pos){ //selected manually
            parent.setTag(null);

            if(parent == spinnerMonth) headerSelectionCalendar.set(Calendar.MONTH, spinnerMonth.getSelectedItemPosition());
            if(parent == spinnerYear) headerSelectionCalendar.set(Calendar.YEAR, Integer.parseInt(spinnerYear.getSelectedItem().toString()));
        }

        //avoid showing of calendars beyond limit and decrement or increment header calendar according to the pressed button
        buttonPrevMonth.setVisibility((headerSelectionCalendar.equals(minCalendar))?View.INVISIBLE:View.VISIBLE);
        buttonNextMonth.setVisibility((headerSelectionCalendar.equals(maxCalendar))?View.INVISIBLE:View.VISIBLE);

        calendarAdapter.updateItems();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void update(List<Leave> myLeaves, HashMap<Integer, Integer> mapPositions) {
        if(leaves != null){
            this.mapLeavePositions.clear();
            this.mapLeavePositions.putAll(mapPositions);
            leaves.clear();
            leaves.addAll(myLeaves);
            calendarAdapter.updateItems();
        }
    }


    private class LeaveNodeHolder{
        public TextView typeTV, statusTV, datesTV;
    }

}
