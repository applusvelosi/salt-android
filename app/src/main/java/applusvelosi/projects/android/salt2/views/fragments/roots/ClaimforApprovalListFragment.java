package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.adapters.lists.MyClaimsAdapter;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;

public class ClaimforApprovalListFragment extends RootFragment implements OnItemClickListener, OnItemLongClickListener, TextWatcher{
	//action bar buttons
	RelativeLayout actionbarSearchButton, actionbarRefreshButton, actionbarMenuButton;

	private ListView lv;
	private MyClaimsAdapter adapter;
	private List<ClaimHeader> claimHeaders;
	private List<ClaimHeader> tempClaimsByNameForApproval;
	private static ClaimforApprovalListFragment instance;
	private EditText etextSearchByName;

	public static ClaimforApprovalListFragment getInstance(){
		if(instance == null)
			instance = new ClaimforApprovalListFragment();
		return instance;
	}

	public static void removeInstance(){
		if(instance != null)
			instance = null;
	}

	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_menurefresh, null);
		actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Claims for Approval");

		actionbarMenuButton.setOnClickListener(this);
		actionbarRefreshButton.setOnClickListener(this);
		return actionbarLayout;
	}

	@Override
	protected View createView(LayoutInflater li, ViewGroup vg, Bundle b) {
		View view = li.inflate(R.layout.fragment_claimforapprovallist, null);
		lv = (ListView)view.findViewById(R.id.lists_claimsforapproval);
		claimHeaders = new ArrayList<>();
		tempClaimsByNameForApproval = new ArrayList<>();
		etextSearchByName = (EditText)view.findViewById(R.id.etexts_leaveforapproval_name);
		etextSearchByName.setHint("Search With Name");
		etextSearchByName.addTextChangedListener(this);
		adapter = new MyClaimsAdapter(activity, tempClaimsByNameForApproval);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		updateList();
	}


	private void updateList() {
		activity.startLoading();
			new Thread( new Runnable() {
				@Override
				public void run() {
					Object tempResult;
					try {
						tempResult = app.onlineGateway.getClaimsForApproval();
					} catch(Exception e) {
						e.printStackTrace();
						tempResult = e.getMessage();
					}

					final Object result = tempResult;
					new Handler(Looper.getMainLooper()).post(new Runnable() {
						@Override
						public void run() {
							if(!(result instanceof String)) {
								claimHeaders.clear();
								activity.finishLoading();
								claimHeaders.addAll((ArrayList)result);
								filteredClaimsByName();
								try {
									Collections.sort(claimHeaders, new Comparator<ClaimHeader>() {
										@Override
										public int compare(ClaimHeader lhs, ClaimHeader rhs) {
											try {
												return app.dateFormatDefault.parse(rhs.getDateModified(app)).compareTo(app.dateFormatDefault.parse(lhs.getDateModified(app)));
											} catch (Exception e) {
												e.printStackTrace();
												return -1;
											}
										}
									});
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								activity.finishLoading(result.toString());
							}
						}
					});
				}
		}).start();
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		final ClaimHeader claimForApproval = tempClaimsByNameForApproval.get(pos);
		activity.startLoading();
//		mClaimThread.setClaimByID(claimForApproval.getClaimID());
		new Thread(new Runnable() {
			@Override
			public void run() {
				Object tempResult;
				try {
					tempResult = app.onlineGateway.getMyClaimByID(claimForApproval.getClaimID());
				} catch (Exception e) {
					tempResult = e.getMessage();
				}
				final Object result = tempResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						if (!(result instanceof String)) {
							activity.finishLoading();
							Intent intent = new Intent(activity, ClaimDetailActivity.class);
							intent.putExtra(ClaimDetailActivity.INTENTKEY_CLAIMFORAPPROVAL, (ClaimHeader) result);
							startActivity(intent);
						} else {
							activity.finishLoading(result.toString());
						}
					}
				});
			}
		}).start();
	}


	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
		return true;
	}

	@Override
	public void onClick(View v) {
		if(v == actionbarMenuButton){
			actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
			activity.toggleSidebar(actionbarMenuButton);
		}else if(v == actionbarRefreshButton){
			updateList();
		}else if(v == actionbarSearchButton){
			//TODO
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		lv.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		lv.setEnabled(true);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		filteredClaimsByName();
	}

	private void filteredClaimsByName() {
		tempClaimsByNameForApproval.clear();
		if (etextSearchByName.getText().length() > 0) {
			for (ClaimHeader claim : claimHeaders) {
				if (claim.getStaffName().toLowerCase().contains(etextSearchByName.getText().toString().toLowerCase()) || etextSearchByName.getText().length() < 1)
					tempClaimsByNameForApproval.add(claim);
			}
		} else {
			tempClaimsByNameForApproval.addAll(claimHeaders);
		}
		adapter.notifyDataSetChanged();
	}
}
