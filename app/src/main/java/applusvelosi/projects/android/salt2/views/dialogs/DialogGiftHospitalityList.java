package applusvelosi.projects.android.salt2.views.dialogs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftHospitality;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftRecipient;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.GiftApprovalDetailActivity;

/**
 * Created by Velosi on 20/04/2017.
 */

public class DialogGiftHospitalityList implements ListAdapterInterface {

    private List<GiftRecipient> recipientList;
    private GiftApprovalDetailActivity activity;
    private ListView lv;
    private ListAdapter adapter;
    private AlertDialog dialogPrompt;
    private GiftHospitality giftHospitalityHeader;

    public DialogGiftHospitalityList(GiftHospitality giftHeader, final GiftApprovalDetailActivity activity, List<GiftRecipient> recipients) {
        this.activity = activity;
        recipientList = recipients;
        giftHospitalityHeader = giftHeader;
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_giftandhospitalityrecipientlist, null);
        lv = (ListView) view.findViewById(R.id.lv_dialogs_giftrecipient);
        adapter = new ListAdapter(this);
        lv.setAdapter(adapter);
        dialogPrompt = new AlertDialog.Builder(activity).setTitle(null).setView(view).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }

    public void show() {
        dialogPrompt.show();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ListHolder holder;
        if (v == null) {
            v = LayoutInflater.from(activity).inflate(R.layout.node_gifthospitalityrecipient_list, null);
            holder = new ListHolder();
            holder.tvName = (TextView) v.findViewById(R.id.tviews_giftrecipient_staffname);
            holder.tvPosition = (TextView)v.findViewById(R.id.tviews_giftrecipient_position);
            holder.tvAmount = (TextView)v.findViewById(R.id.tviews_giftrecipient_amount);
            v.setTag(holder);
        }
        holder = (ListHolder) v.getTag();
        GiftRecipient recipient = recipientList.get(position);
        holder.tvName.setText(recipient.getGiftRecipientName());
        holder.tvPosition.setText(recipient.getGiftRecipientPosition());
        holder.tvAmount.setText(giftHospitalityHeader.getCurrencySymbol()+" "+SaltApplication.decimalFormat.format(recipient.getGiftRecipientAmount()));

        return v;
    }

    @Override
    public int getCount() {
        return recipientList.size();
    }

    private class ListHolder {
        private TextView tvAmount, tvName, tvPosition;
    }
}
