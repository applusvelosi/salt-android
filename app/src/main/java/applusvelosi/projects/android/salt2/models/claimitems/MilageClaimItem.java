package applusvelosi.projects.android.salt2.models.claimitems;

import org.json.JSONObject;

public class MilageClaimItem extends ClaimItem{
	public static final String MILEAGETYPE_VAL_KILOMETER = "KM";
	public static final String MILEAGETYPE_VAL_MILE = "MI";
	public static final int MILEAGETYPE_KEY_KILOMETER = 1;
	public static final int MILEAGETYPE_KEY_MILE = 2;
		

	public MilageClaimItem(ClaimItem claimItem, int mileageTypeID, float mileageRate, int mileage, String mileageFrom, String mileageTo){
		super(claimItem);
		this.mileageType = mileageTypeID;
		this.mileageRate = mileageRate;
		this.mileage = mileage;
		this.mileageFrom = mileageFrom;
		this.mileageTo = mileageTo;
//		this.mileageReturn = isMileageReturn;
	}

	public MilageClaimItem(JSONObject jsonClaimItem, JSONObject jsonObject) throws Exception{
		super(jsonClaimItem, jsonObject);
	}

	public MilageClaimItem(ClaimItem claimItem){
		super(claimItem);
	}

	public int getMilageTypeID(){
		return mileageType;
	}
	public float getMilageRate(){
		return mileageRate;
	}
	public int getMileage(){
		return mileage;
	}
	public String getMileageFrom(){
		return mileageFrom;
	}
	public String getMileageTo(){
		return mileageTo;
	}
//	public boolean isMileageReturn(){
//		return mileageReturn;
//	}

}
