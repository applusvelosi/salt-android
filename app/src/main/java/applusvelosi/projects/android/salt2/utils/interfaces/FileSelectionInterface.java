package applusvelosi.projects.android.salt2.utils.interfaces;

/**
 * Created by Velosi on 11/17/15.
 */
public interface FileSelectionInterface {
    void onFileSelectionSuccess(String filePath);
    void onFileSelectionFailed();
}


