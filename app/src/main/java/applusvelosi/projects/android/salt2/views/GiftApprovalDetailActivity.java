package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftHospitality;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftRecipient;
import applusvelosi.projects.android.salt2.views.fragments.gifthospitality.GiftApprovalDetailFragment;

/**
 * Created by Velosi on 05/04/2017.
 */

public class GiftApprovalDetailActivity extends LinearNavFragmentActivity {

    public static final String INTENTKEY_GIFTHOSPITALITYHEADER = "giftheaderkey";
    public int giftHeaderID;
    public GiftHospitality giftHeader;
    public List<GiftRecipient> giftRecipients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        giftHeaderID = getIntent().getExtras().getInt(INTENTKEY_GIFTHOSPITALITYHEADER);
        giftRecipients = new ArrayList<GiftRecipient>();
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new GiftApprovalDetailFragment()).commit();
    }
}
