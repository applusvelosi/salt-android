package applusvelosi.projects.android.salt2.views.fragments.leaves;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.grids.MyCalendarAdapter;
import applusvelosi.projects.android.salt2.adapters.grids.MySickLeaveAdapter;
import applusvelosi.projects.android.salt2.adapters.grids.MyVacationLeaveAdapter;
import applusvelosi.projects.android.salt2.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.salt2.models.CalendarEvent;
import applusvelosi.projects.android.salt2.models.CalendarEventLeave;
import applusvelosi.projects.android.salt2.models.CalendarItem;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Dashboard;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.utils.enums.CalendarItemType;
import applusvelosi.projects.android.salt2.utils.interfaces.CalendarInterface;
import applusvelosi.projects.android.salt2.utils.interfaces.CameraCaptureInterface;
import applusvelosi.projects.android.salt2.utils.interfaces.FileSelectionInterface;
import applusvelosi.projects.android.salt2.views.NewLeaveRequestActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 10/27/15.
 */
public class LeaveInputDates extends LinearNavActionbarFragment implements AdapterView.OnItemSelectedListener, CalendarInterface, CameraCaptureInterface, FileSelectionInterface {
    private static final String KEY = "key";

    //actionbar weeks
    private TextView actionbarTitle, actionbarButtonReset;
    private RelativeLayout actionbarButtonBack;
    private ImageView buttonPrevMonth, buttonNextMonth;
    private Spinner spinnerMonth, spinnerYear;
    private GridView calendarView;
    private Calendar minCalendar, maxCalendar, leaveEndCal;
    private int leaveTypeID;
    private Date leaveStartDate;
    private float leaveDuration;
    private MyCalendarAdapter calendarAdapter;
    private ArrayList<Leave> leaves;
    private ArrayList<CountryHoliday> holidays;

    private RelativeLayout dialogAttachmentContainer;
    private LinearLayout dialogEventsContainer, dialogEventsOneDay;
    private RadioGroup dialogEventsRadioGroup;
    private RadioButton dialogEventsRadioAM, dialogEventsRadioPM, dialogEventsRadioOneDay;
    private EditText dialogEventsNumDays, dialogEtextsNotes;
    private ImageView diaglogButtonAttachment;
    private TextView diaglogTviewsAttachment;

    private AlertDialog dialogLeaveDays,dialogApplyLeaves, dialogLeaveAttachment;
    private LinearLayout dialogApplyLeavesContainer;
    private TextView dialogTviewsType, dialogTviewsDates, dialogTviewsDays, dialogTviewsWorkingDays, dialogTviewsHolidays, dialogTviewsLeaveCredits, dialogTviewsNumOfDays;
    private NewLeaveRequestActivity activity;

    private float remLeaveBal, workingDays;
    private int applicableHoliday, leaveEventCtr;
    private File attachedFile;
    private String userChoosenTask;

    public static LeaveInputDates newInstance(int leaveTypeID){
        LeaveInputDates frag = new LeaveInputDates();
        Bundle b = new Bundle();
        b.putInt(KEY, leaveTypeID);
        frag.setArguments(b);

        return frag;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (NewLeaveRequestActivity)getActivity();
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backedit, null);
        actionbarButtonBack = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionbarButtonReset = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_edit);
        actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
//        actionbarButtonReset.setText("Reset");
        actionbarButtonReset.setVisibility(View.GONE);

        actionbarTitle.setOnClickListener(this);
        actionbarButtonBack.setOnClickListener(this);
//        actionbarButtonReset.setOnClickLis÷tener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_newleaveinput_calendar, null);
        buttonPrevMonth = (ImageView) view.findViewById(R.id.buttons_calendarview_prevmonth);
        buttonNextMonth = (ImageView) view.findViewById(R.id.buttons_calendarview_nextmonth);
        spinnerMonth = (Spinner) view.findViewById(R.id.choices_calendarview_month);
        spinnerYear = (Spinner) view.findViewById(R.id.choices_calendarview_year);
        calendarView = (GridView) view.findViewById(R.id.calendarview);

        minCalendar = Calendar.getInstance();
        maxCalendar = Calendar.getInstance();
        leaveTypeID = getArguments().getInt(KEY);

        maxCalendar.set(Calendar.YEAR, Integer.parseInt(app.dropDownYears.get(app.dropDownYears.size()-1)));
        maxCalendar.set(Calendar.MONTH, Calendar.DECEMBER);

        if(leaveTypeID == Leave.LEAVETYPESICKKEY || leaveTypeID == Leave.LEAVETYPEBEREAVEMENTKEY || leaveTypeID == Leave.LEAVETYPEMATERNITYKEY || leaveTypeID == Leave.LEAVETYPEDOCTORKEY
                || leaveTypeID == Leave.LEAVETYPEHOSPITALIZATIONKEY){
            maxCalendar.set(Calendar.DAY_OF_MONTH, 31);
            minCalendar.set(Calendar.MONTH, minCalendar.get(Calendar.MONTH)-3);
        }else {
            minCalendar.set(Calendar.MONTH, minCalendar.get(Calendar.MONTH));
            minCalendar.set(Calendar.DAY_OF_MONTH, minCalendar.get(Calendar.DAY_OF_MONTH));
            minCalendar.set(Calendar.YEAR,minCalendar.get(Calendar.YEAR));
        }
        leaves = new ArrayList<Leave>();
        holidays = new ArrayList<CountryHoliday>();
        spinnerMonth.setAdapter(new SimpleSpinnerAdapter(getActivity(), app.dropDownMonths, SimpleSpinnerAdapter.NodeSize.SIZE_NORMAL));
        spinnerYear.setAdapter(new SimpleSpinnerAdapter(getActivity(), app.dropDownYears, SimpleSpinnerAdapter.NodeSize.SIZE_NORMAL));
        spinnerMonth.setSelection(Calendar.getInstance().get(Calendar.MONTH));
        spinnerYear.setSelection(app.dropDownYears.indexOf(String.valueOf(Calendar.getInstance().get(Calendar.YEAR))));
        reloadAppHolidays();
        if(leaveTypeID == Leave.LEAVETYPESICKKEY || leaveTypeID == Leave.LEAVETYPEBEREAVEMENTKEY || leaveTypeID == Leave.LEAVETYPEMATERNITYKEY || leaveTypeID == Leave.LEAVETYPEDOCTORKEY
                || leaveTypeID == Leave.LEAVETYPEHOSPITALIZATIONKEY)
            calendarAdapter = new MySickLeaveAdapter(this, minCalendar, maxCalendar, spinnerMonth, spinnerYear, buttonPrevMonth, buttonNextMonth, leaves, holidays);
        else {
            calendarAdapter = new MyVacationLeaveAdapter(this, minCalendar, maxCalendar, spinnerMonth, spinnerYear, buttonPrevMonth, buttonNextMonth, leaves, holidays);
        }
        calendarView.setAdapter(calendarAdapter);

        switch(leaveTypeID){
            case 1:
                actionbarTitle.setText("Select Dates for " + Leave.LEAVETYPEBIRTHDAYDESC);
                break;
            case 2:
                actionbarTitle.setText("Select Dates for " + Leave.LEAVETYPEVACATIONDESC);
                break;
            case 3:
                actionbarTitle.setText("Select Dates for " + Leave.LEAVETYPESICKDESC);
                break;
            case 4:
                actionbarTitle.setText("Select Dates for " + Leave.LEAVETYPEUNPAIDDESC);
                break;
            case 5:
                actionbarTitle.setText("Select Dates for "+ Leave.LEAVETYPEBEREAVEMENTDESC);
                break;
            case 6:
                actionbarTitle.setText("Select Dates for "+ Leave.LEAVETYPEMATERNITYDESC);
                break;
            case 7:
                actionbarTitle.setText("Select Dates for "+ Leave.LEAVETYPEDOCTORDESC);
                break;
            case 8:
                actionbarTitle.setText("Select Dates for "+ Leave.LEAVETYPEHOSPITALIZATIONDESC);
                break;
            case 9:
                actionbarTitle.setText("Select Dates for "+ Leave.LEAVETYPEBUSINESSTRIPDESC);
                break;
        }

        buttonPrevMonth.setOnClickListener(this);
        buttonNextMonth.setOnClickListener(this);
        spinnerMonth.setOnItemSelectedListener(this);
        spinnerYear.setOnItemSelectedListener(this);

        dialogEventsContainer = (LinearLayout) inflater.inflate(R.layout.dialog_leavedays, null);
        dialogEventsRadioGroup = (RadioGroup)dialogEventsContainer.findViewById(R.id.rgroups_dialogs_leavedays);
        dialogEventsRadioAM = (RadioButton)dialogEventsContainer.findViewById(R.id.rbuttons_dialogs_leavedays_am);
        dialogEventsRadioPM = (RadioButton)dialogEventsContainer.findViewById(R.id.rbuttons_dialogs_leavedays_pm);
        dialogEventsRadioOneDay = (RadioButton)dialogEventsContainer.findViewById(R.id.rbuttons_dialogs_leavedays_oneday);

        dialogEventsNumDays = (EditText)dialogEventsContainer.findViewById(R.id.etexts_dialogs_leavedays_numdays);
        dialogEventsOneDay = (LinearLayout)dialogEventsContainer.findViewById(R.id.containers_dialogs_leavedays_numdays);
        dialogAttachmentContainer = (RelativeLayout)dialogEventsContainer.findViewById(R.id.container_leaveinput_attachment);
        diaglogButtonAttachment = (ImageView)dialogEventsContainer.findViewById(R.id.buttons_leaveinput_attachment);
        diaglogTviewsAttachment = (TextView)dialogEventsContainer.findViewById(R.id.tviews_leaveinnput_attachment);

        dialogApplyLeavesContainer = (LinearLayout)inflater.inflate(R.layout.dialog_newleavesummary, null);
        dialogTviewsType = (TextView)dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_type);
        dialogTviewsDates = (TextView)dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_dates);
        dialogTviewsNumOfDays = (TextView)dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_numofdays);
        dialogTviewsDays = (TextView)dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_days);
        dialogTviewsWorkingDays = (TextView)dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_numworkingdays);
        dialogEtextsNotes = (EditText) dialogApplyLeavesContainer.findViewById(R.id.etexts_dialogs_newleavesummary_notes);
        dialogTviewsHolidays = (TextView) dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_numholidays);
        dialogTviewsLeaveCredits = (TextView)dialogApplyLeavesContainer.findViewById(R.id.tviews_dialogs_newleavesummary_numleavecredits);

        final int maxLeaveDays = app.getStaffOffice().getMaxConsecutiveDays();
        dialogEventsNumDays.setHint("You're allowed " + maxLeaveDays + " consecutive day.");
        dialogEventsNumDays.setFilters(new InputFilter[]{new InputFilterMinMax(1, maxLeaveDays)}); //limit the maximum consecutive leave days
        dialogEventsRadioOneDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView == dialogEventsRadioOneDay)
                    dialogEventsOneDay.setVisibility((isChecked)?View.VISIBLE:View.GONE);
            }
        });

        dialogEventsNumDays.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.hashCode() == dialogEventsNumDays.getText().hashCode()) {
                        int leaveCnt = Integer.parseInt(s.toString());
                        if (leaveTypeID == Leave.LEAVETYPESICKKEY || leaveTypeID == Leave.LEAVETYPEBEREAVEMENTKEY || leaveTypeID == Leave.LEAVETYPEMATERNITYKEY || leaveTypeID == Leave.LEAVETYPEDOCTORKEY
                                || leaveTypeID == Leave.LEAVETYPEHOSPITALIZATIONKEY) {
                            if (leaveCnt < 4)
                                dialogAttachmentContainer.setVisibility(View.GONE);
                            else
                                dialogAttachmentContainer.setVisibility(View.VISIBLE);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        dialogLeaveDays = new AlertDialog.Builder(getActivity()).setView(dialogEventsContainer)
                .setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogTviewsType.setText(Leave.getLeaveTypeDescForKey(leaveTypeID));
                        if(dialogEventsRadioGroup.getCheckedRadioButtonId() == dialogEventsRadioAM.getId()) {
                            leaveDuration = (float) 0.1; //duration indication for am
                            dialogTviewsNumOfDays.setText("0.5 Days AM");
                            applyLeaves();
                        }else if (dialogEventsRadioGroup.getCheckedRadioButtonId() == dialogEventsRadioPM.getId()) {
                            leaveDuration = (float) 0.2; //duration indication for pm
                            dialogTviewsNumOfDays.setText("0.5 Days PM");
                            applyLeaves();
                        } else {
                            if (dialogEventsNumDays.getText().length() > 0) {
                                leaveDuration = Float.parseFloat(dialogEventsNumDays.getText().toString());
                                dialogTviewsNumOfDays.setText( (Math.round(leaveDuration) > 1) ? String.valueOf(leaveDuration)+" Days" : String.valueOf(leaveDuration)+" Day");
                                if (leaveTypeID == Leave.LEAVETYPESICKKEY || leaveTypeID == Leave.LEAVETYPEBEREAVEMENTKEY || leaveTypeID == Leave.LEAVETYPEMATERNITYKEY || leaveTypeID == Leave.LEAVETYPEDOCTORKEY
                                        || leaveTypeID == Leave.LEAVETYPEHOSPITALIZATIONKEY) {
                                    int sickLeaveDays = Integer.parseInt(dialogEventsNumDays.getText().toString());
                                    if (sickLeaveDays < 4)
                                        applyLeaves();
                                    else {
                                        if (diaglogTviewsAttachment.getText().length() > 0)
                                            applyLeaves();
                                        else
                                            app.showMessageDialog(linearNavFragmentActivity, "Attachment File is Required!");
                                    }
                                } else
                                    applyLeaves();
                            } else
                                Toast.makeText(linearNavFragmentActivity, "Please input number of days", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        diaglogButtonAttachment.setOnClickListener(this);
        final CharSequence[] items = { "Take A Photo", "Choose From Library",
                "Cancel" };

        dialogLeaveAttachment = new AlertDialog.Builder(linearNavFragmentActivity).setTitle("Add Attachment!").setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= FileManager.checkPermission(linearNavFragmentActivity);

                if (items[item].equals("Take A Photo")) {
                    userChoosenTask ="Take A Photo";
                    if(result)
                        initCameraIntent();
                } else if (items[item].equals("Choose From Library")) {
                    userChoosenTask ="Choose From Library";
                    if(result)
                        initGalleryImageIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        }).create();

        dialogApplyLeaves = new AlertDialog.Builder(getActivity()).setView(dialogApplyLeavesContainer)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if ((remLeaveBal - Math.round(workingDays)) < 0 )
                            app.showMessageDialog(linearNavFragmentActivity, "You do not have sufficient credits left to book the selected leave. Please amend the details you entered by resetting the days entered and try again or you may use unpaid leave type instead.");
                        else {
                            if(leaveEventCtr > 0)
                                app.showMessageDialog(linearNavFragmentActivity, "You have already requested leave during the dates/time specified - please check your calendar or consult your approver.");
                            else
                                (new Thread(new SubmitNewLeaveRequest())).start();
                        }
                    }
                }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        return view;
    } //end of createView()

    private void initCameraIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, SaltApplication.RESULT_CAMERA);
    }

    private void initGalleryImageIntent(){
        if(Build.VERSION.SDK_INT < 19){
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*,application/pdf");
            startActivityForResult(Intent.createChooser(intent,"Select File"), SaltApplication.RESULT_BROWSEFILES);
        }else{
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String mimeTypes[] = {"image/*","application/pdf"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
            startActivityForResult(intent,SaltApplication.RESULT_BROWSEFILES_KITKAT);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case FileManager.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take A Photo"))
                        initCameraIntent();
                    else if(userChoosenTask.equals("Choose From Library"))
                        initGalleryImageIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            String filePath;
            if (resultCode != Activity.RESULT_OK)
                return;
            if (null == data)
                return;
            switch (requestCode) {
                case SaltApplication.RESULT_CAMERA:
                    if (resultCode == activity.RESULT_OK) {
                        filePath = FileManager.getFilePathFromUri_BelowKitKat(linearNavFragmentActivity, data.getData());
                        onCameraCaptureSuccess(data, filePath);
                    }else {
                        attachedFile = null;
                        onCameraCaptureFailed();
                    }
                    break;
                case SaltApplication.RESULT_BROWSEFILES:
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.uriToFilename(linearNavFragmentActivity, data.getData());
                        onFileSelectionSuccess(filePath);
                    }else {
                        attachedFile = null;
                        onFileSelectionFailed();
                    }
                    break;
                case SaltApplication.RESULT_BROWSEFILES_KITKAT: //for KitKat version and above
                    if(resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.uriToFilename(linearNavFragmentActivity, data.getData());
                        onFileSelectionSuccess(filePath);

                    }else {
                        attachedFile = null;
                        onFileSelectionFailed();
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraCaptureSuccess(Intent data, String filePath) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        diaglogTviewsAttachment.setText(filePath);
//        updateAttachment(destination);
    }

    @Override
    public void onCameraCaptureFailed() {
//        updateAttachment(null);
    }

    @Override
    public void onFileSelectionSuccess(String filePath) {
        attachedFile = new File(filePath);
        diaglogTviewsAttachment.setText(attachedFile.getName());
//        updateAttachment();
    }

    @Override
    public void onFileSelectionFailed() {
//        updateAttachment(null);
    }

    private void resetAttachment(){
        diaglogTviewsAttachment.setHint("No file selected.");
        diaglogTviewsAttachment.setHintTextColor(Color.parseColor("#909090"));
    }

    private void applyLeaves() {
        float incrementalDays = (dialogTviewsNumOfDays.getText().toString().split(" ")[0]).equals("0.5")?0.5f:1.0f;
        leaveEndCal = Calendar.getInstance();
        leaveEndCal.setTime(leaveStartDate);
        Calendar leaveStartCal = Calendar.getInstance();
        leaveStartCal.setTime(leaveStartDate);
        leaveEndCal.setTime(leaveStartDate);
        int selectedDays = Math.round(Float.parseFloat(dialogTviewsNumOfDays.getText().toString().split(" ")[0]));
        if (selectedDays > 1)
            leaveEndCal.add(Calendar.DAY_OF_MONTH, selectedDays - 1);

        while (leaveStartCal.compareTo(leaveEndCal) <= 0) {
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && app.getStaffOffice().hasSunday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave)
                            leaveEventCtr++;
                        else
                            applicableHoliday++;
                    }
                }else
                    workingDays += incrementalDays;
            }
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY && app.getStaffOffice().hasMonday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave)
                            leaveEventCtr++;
                        else
                            applicableHoliday++;
                    }
                }else
                    workingDays += incrementalDays;
            }
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY && app.getStaffOffice().hasTuesday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave)
                            leaveEventCtr++;
                        else
                            applicableHoliday++;
                    }
                }else
                    workingDays += incrementalDays;
            }
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY && app.getStaffOffice().hasWednesday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave){
                            leaveEventCtr++;
                            incrementalDays++;
                        }else
                            applicableHoliday++;
                    }
                }else
                    workingDays += incrementalDays;
            }
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY && app.getStaffOffice().hasThursday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave)
                            leaveEventCtr++;
                        else
                            applicableHoliday++;
                    }
                }else
                    workingDays += incrementalDays;
            }
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && app.getStaffOffice().hasFriday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
//                    applicableHoliday++;
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave)
                            leaveEventCtr++;

                        else
                            applicableHoliday++;
                    }
                }else
                    workingDays  += incrementalDays;
            }
            if (leaveStartCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && app.getStaffOffice().hasSaturday()) {
                if (calendarAdapter.getPropMapDayEvents().containsKey(app.dateFormatDefault.format(leaveStartCal.getTime()))) {
//                    applicableHoliday++;
                    for (CalendarEvent leaveEvent : calendarAdapter.getPropMapDayEvents().get(app.dateFormatDefault.format(leaveStartCal.getTime()))){
                        if (leaveEvent instanceof CalendarEventLeave)
                            leaveEventCtr++;

                        else
                            applicableHoliday++;
                    }
                }else
                    workingDays += incrementalDays;
            }
            leaveStartCal.set(Calendar.DAY_OF_MONTH, leaveStartCal.get(Calendar.DAY_OF_MONTH) + 1);
        }

        dialogTviewsHolidays.setText(String.valueOf(applicableHoliday));
        dialogTviewsDays.setText(String.valueOf(Float.parseFloat(dialogTviewsNumOfDays.getText().toString().split(" ")[0])));
        dialogTviewsWorkingDays.setText(String.valueOf(workingDays));
        dialogTviewsDates.setText(app.dateFormatCustom.format(leaveStartDate) + " - " + app.dateFormatCustom.format(leaveEndCal.getTime()));
        if (leaveTypeID == Leave.LEAVETYPESICKKEY || leaveTypeID == Leave.LEAVETYPEBEREAVEMENTKEY || leaveTypeID == Leave.LEAVETYPEMATERNITYKEY || leaveTypeID == Leave.LEAVETYPEDOCTORKEY
                || leaveTypeID == Leave.LEAVETYPEHOSPITALIZATIONKEY)
            dialogTviewsLeaveCredits.setText(String.valueOf(app.dashboard.getRemSL()-workingDays));
        else
            dialogTviewsLeaveCredits.setText(String.valueOf(app.dashboard.getRemVL()-workingDays));

        dialogApplyLeaves.show();
    }

    @Override
    public void onClick(View v) {
        if(v == actionbarButtonBack || v == actionbarTitle)
            linearNavFragmentActivity.onBackPressed();
        else if(v == buttonPrevMonth)
            calendarAdapter.prevMonth();
        else if(v == buttonNextMonth)
            calendarAdapter.nextMonth();
        else if(v == diaglogButtonAttachment){
            resetAttachment();
            dialogLeaveAttachment.show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent == calendarView){
            System.out.println("Calendar item selected");
        }else{
            calendarAdapter.updateItems();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCalendarItemClicked(final CalendarItem calendarItem) {
        dialogEventsRadioAM.setEnabled(true);
        dialogEventsRadioPM.setEnabled(true);
        dialogEventsRadioOneDay.setEnabled(true);
        dialogEventsRadioOneDay.setChecked(true);

        float eventDayCtr = 0.0f;
        boolean isHoliday = false;
        boolean isHalfDay = false;
        if (remLeaveBal > 0) {
            if (calendarItem.getEvents() != null && calendarItem.getType() != CalendarItemType.TYPE_NONWORKINGDAY) {
                for (CalendarEvent event : calendarItem.getEvents()) {
                    if (event instanceof CalendarEventLeave) {
                        if (event.getDuration() == CalendarEvent.CalendarEventDuration.AM || event.getDuration() == CalendarEvent.CalendarEventDuration.PM) {
                            isHalfDay = true;
                            if (event.getDuration() == CalendarEvent.CalendarEventDuration.AM)
                                dialogEventsRadioAM.setEnabled(false);
                            if (event.getDuration() == CalendarEvent.CalendarEventDuration.PM)
                                dialogEventsRadioPM.setEnabled(false);
                            eventDayCtr += 0.5f;
                        } else
                            eventDayCtr += 1;
                    } else
                        isHoliday = true;
                }
                if (eventDayCtr >= 1) {
                    Toast.makeText(getActivity(), "Can't add leave for this day anymore", Toast.LENGTH_SHORT).show();
                }else {
                    if (isHoliday || isHalfDay && calendarItem.getType() != CalendarItemType.TYPE_NONWORKINGDAY && calendarItem.getType() != CalendarItemType.TYPE_INVALIDLEAVEDAYS) {
                        if (isHoliday) {
                            new AlertDialog.Builder(getActivity()).setTitle("").setMessage("Date selected is a holiday. Are you sure you want to proceed?")
                                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            pickNumberOfDays(calendarItem.getDate());
                                        }
                                    })
                                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create().show();
                        }else
                            pickNumberOfDays(calendarItem.getDate());
                    } else
                        Toast.makeText(getActivity(), "You cannot file a leave on that selected date.", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (calendarItem.getType() == CalendarItemType.TYPE_NONWORKINGDAY || calendarItem.getType() == CalendarItemType.TYPE_OUTMONTH || calendarItem.getType() == CalendarItemType.TYPE_INVALIDLEAVEDAYS)
                    Toast.makeText(getActivity(), "You cannot file a leave on that selected date.", Toast.LENGTH_SHORT).show();
                else
                    pickNumberOfDays(calendarItem.getDate());
            }
        }else
            app.showMessageDialog(linearNavFragmentActivity, "You have 0 remaining leave.");
    }

    private void pickNumberOfDays(Date startDate){
        applicableHoliday = leaveEventCtr = 0;
        workingDays = 0.0f;
        leaveStartDate = startDate;
        dialogEventsNumDays.setText("");
        dialogTviewsDates.setText("");
        dialogTviewsNumOfDays.setText("");
        dialogTviewsDays.setText("");
        dialogTviewsWorkingDays.setText("");
        dialogEtextsNotes.setText("");
        dialogTviewsHolidays.setText("");
        dialogTviewsLeaveCredits.setText("");
        dialogLeaveDays.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        linearNavFragmentActivity.startLoading();
        new Thread(new ReloadLeaveCredits()).start();
    }

    private class ReloadLeaveCredits implements Runnable {
        @Override
        public void run() {
            app.dashboard = null;
            Object tempResult;
            try {
                app.onlineGateway.updateStaff(app.getStaff().getStaffID(), app.getStaff().getSecurityLevel(), app.getStaff().getOfficeID());
                tempResult = app.onlineGateway.getDashboard();
            } catch (Exception e) {
                e.printStackTrace();
                tempResult = e.getMessage();
            }

            final Object result = tempResult;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (result instanceof String)
                            linearNavFragmentActivity.finishLoading(result.toString());
                        else {
                            linearNavFragmentActivity.finishLoading();
                            app.dashboard = (Dashboard) result;
                        }
                        if (leaveTypeID == Leave.LEAVETYPEVACATIONKEY || leaveTypeID == Leave.LEAVETYPEUNPAIDKEY || leaveTypeID == Leave.LEAVETYPEBUSINESSTRIPKEY)
                            remLeaveBal = app.dashboard.getRemVL();
                        else
                            remLeaveBal = app.dashboard.getRemSL();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    void reloadAppHolidays() {
        linearNavFragmentActivity.startLoading();
        new Thread(new Runnable() {
            Object tempHolidayResult, tempLeaveResult;

            @Override
            public void run() {
                try {
                    tempHolidayResult = app.onlineGateway.getOfficeHolidaysOrErrorMessage(app.getStaff().getOfficeID());
                    tempLeaveResult = app.onlineGateway.getMyPendingAndApprovedLeaves();
                } catch (Exception e) {
                    tempHolidayResult = e.getMessage();
                }

                final Object holidayResult = tempHolidayResult;
                final Object leaveResult = tempLeaveResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (holidayResult instanceof String && !holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR))
                            linearNavFragmentActivity.finishLoading(holidayResult.toString());
                        else if (leaveResult instanceof String && !holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR))
                            linearNavFragmentActivity.finishLoading(leaveResult.toString());
                        else {
                            holidays.clear();
                            leaves.clear();
                            if (leaveResult == null || leaveResult.toString().contains(SaltApplication.CONNECTION_ERROR) || holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR)) {
                                linearNavFragmentActivity.finishLoading(leaveResult.toString());
                            } else {
                                ArrayList<Leave> tempLeaves = (ArrayList<Leave>) leaveResult;
                                ArrayList<CountryHoliday> tempHolidays = (ArrayList<CountryHoliday>) holidayResult;
                                holidays.addAll(tempHolidays);
                                leaves.addAll(tempLeaves);
                                app.offlineGateway.serializeMyLeaves(tempLeaves);
                                app.offlineGateway.serializeMyCalendarHolidays(tempHolidays);
                            }

                            linearNavFragmentActivity.finishLoading();
                            calendarAdapter.updateItems();
                        }
                    }
                });
            }
        }).start();
    }

    private class SubmitNewLeaveRequest implements Runnable{
            private String oldLeaveJSON;
            private Leave newLeave;
            @Override
            public void run() {
                Object tempResult;
                String startDateStr = app.dateFormatDefault.format(leaveStartDate);
                String endDateStr = app.dateFormatDefault.format(leaveEndCal.getTime());
                try {
                    oldLeaveJSON = Leave.createEmptyJSON();
                    newLeave = new Leave(app.getStaff(),
                            (leaveTypeID==Leave.LEAVETYPEVACATIONKEY)?remLeaveBal:app.dashboard.getRemVL(),
                            (leaveTypeID==Leave.LEAVETYPESICKKEY)?remLeaveBal:app.dashboard.getRemSL(),
                            leaveTypeID,
                            Leave.LEAVESTATUSPENDINGID,
                            startDateStr,
                            endDateStr,
                            leaveDuration,
                            Float.parseFloat(dialogTviewsWorkingDays.getText().toString()),
                            dialogEtextsNotes.getText().toString(),
                            app.dateFormatDefault.format(new Date()));

                    tempResult = app.onlineGateway.saveLeave(newLeave.getJSONStringForEditingLeave(), oldLeaveJSON);
                }catch(Exception e){
                    e.printStackTrace();
                    tempResult = e.getMessage();
                }

                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(!(result instanceof String)){
                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    String tempFollowUpLeaveResult;

                                    try{
                                        Object leaveByIDResult = app.onlineGateway.getLeaveByID(Integer.parseInt(result.toString()));
                                        if(leaveByIDResult instanceof String)
                                            tempFollowUpLeaveResult = leaveByIDResult.toString();
                                        else
                                            tempFollowUpLeaveResult = app.onlineGateway.followUpLeave(new Leave((JSONObject) leaveByIDResult, app.onlineGateway).getLeaveID());
                                    }catch(Exception e){
                                        e.printStackTrace();
                                        tempFollowUpLeaveResult = e.getMessage();
                                    }

                                    final String followUpLeaveResult = tempFollowUpLeaveResult;
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (followUpLeaveResult != null)
                                                    app.showMessageDialog(linearNavFragmentActivity, "Failed to send email to approver(s): " + followUpLeaveResult);
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }).start();
                            try {
//                                ParsePush parsePush = new ParsePush();
//                                ParseQuery parseQuery = ParseInstallation.getQuery();
//                                parseQuery.whereEqualTo("staffID", app.getStaff().getLeaveApprover1ID());
//                                parsePush.sendMessageInBackground(ParseReceiver.createLeaveApprovalMessage(newLeave, app), parseQuery);
                                Toast.makeText(getActivity(), "Leave Submitted Successfully!", Toast.LENGTH_SHORT).show();
                                linearNavFragmentActivity.finishLoading();
                                linearNavFragmentActivity.finish();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            app.showMessageDialog(getActivity(), result.toString());
                            linearNavFragmentActivity.finishLoading();
                        }
                    }
                });
            }
    }

    private class InputFilterMinMax implements InputFilter{
        private int min, max;

        public InputFilterMinMax(int min, int max){
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max){
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if(isInRange(min,max,input))
                    return null;
            }catch (NumberFormatException nfe){
                nfe.printStackTrace();
            }
            return "";
        }

        private boolean isInRange(int min, int max, int input){
            return max > min ? input >= min && input <= max : input >= max && input <= min;
        }
    }
}
