package applusvelosi.projects.android.salt2.adapters.grids;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.CalendarEvent;
import applusvelosi.projects.android.salt2.models.CalendarEventLeave;
import applusvelosi.projects.android.salt2.models.CalendarItem;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.utils.customviews.SquareGridItemView;
import applusvelosi.projects.android.salt2.utils.enums.CalendarItemType;
import applusvelosi.projects.android.salt2.utils.interfaces.CalendarInterface;

/**
 * Created by Velosi on 6/7/16.
 */
public class MySickLeaveAdapter extends MyCalendarAdapter {

	public MySickLeaveAdapter(CalendarInterface calendarInterface, Calendar minCalendar, Calendar maxCalendar, Spinner spinnerMonth, Spinner spinnerYear, ImageView prevButton, ImageView nextButton, ArrayList<Leave> leaves, ArrayList<CountryHoliday> holidays) {
		super(calendarInterface, minCalendar, maxCalendar, spinnerMonth, spinnerYear, prevButton, nextButton, leaves, holidays);

	}

	@Override
	public void prevMonth() {
		Calendar headerSelectionCalendar = Calendar.getInstance();
		headerSelectionCalendar.set(Calendar.YEAR, Integer.parseInt(spinnerYear.getSelectedItem().toString()));
		headerSelectionCalendar.set(Calendar.MONTH, spinnerMonth.getSelectedItemPosition());
		headerSelectionCalendar.set(Calendar.MONTH, headerSelectionCalendar.get(Calendar.MONTH)-1);
		int monthSelection = headerSelectionCalendar.get(Calendar.MONTH);
		int yearSelection = app.dropDownYears.indexOf(String.valueOf(headerSelectionCalendar.get(Calendar.YEAR)));
		if(monthSelection >= minCalendar.get(Calendar.MONTH) || headerSelectionCalendar.get(Calendar.YEAR) <= maxCalendar.get(Calendar.YEAR)) {
			spinnerMonth.setTag(monthSelection);
			spinnerMonth.setSelection(monthSelection);
			spinnerYear.setTag(yearSelection);
			spinnerYear.setSelection(yearSelection);
		}
	}


	public void nextMonth(){
		Calendar headerSelectionCalendar = Calendar.getInstance();
		headerSelectionCalendar.set(Calendar.YEAR, Integer.parseInt(spinnerYear.getSelectedItem().toString()));
		headerSelectionCalendar.set(Calendar.MONTH, spinnerMonth.getSelectedItemPosition());
		headerSelectionCalendar.set(Calendar.MONTH, headerSelectionCalendar.get(Calendar.MONTH)+1);
		int monthSelection = headerSelectionCalendar.get(Calendar.MONTH);
		int yearSelection = app.dropDownYears.indexOf(String.valueOf(headerSelectionCalendar.get(Calendar.YEAR)));
		if(monthSelection <= maxCalendar.get(Calendar.MONTH) && headerSelectionCalendar.get(Calendar.YEAR) <= maxCalendar.get(Calendar.YEAR)) {
			spinnerMonth.setTag(monthSelection);
			spinnerMonth.setSelection(monthSelection);
			spinnerYear.setTag(yearSelection);
			spinnerYear.setSelection(yearSelection);
		}
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		super.getView(position,convertView,parent);

		CalendarItem calendarItem = allItems.get(position);
		SquareGridItemView row;
		TextView gridcell;

		if(calendarItem.getType() == CalendarItemType.TYPE_HEADER){
			row = (SquareGridItemView) calendarInterface.getActivity().getLayoutInflater().inflate(R.layout.calendarcell_monthly_header, parent, false);
			gridcell = (TextView)row.findViewById(R.id.calendar_day_gridcell);
			gridcell.setTextColor(Color.parseColor("#FFFFFF"));
			gridcell.setBackgroundColor(calendarInterface.getActivity().getResources().getColor(R.color.black));
		}else{
			row = (SquareGridItemView) calendarInterface.getActivity().getLayoutInflater().inflate(R.layout.calendarcell_monthly_item, parent, false);
			gridcell = (TextView)row.findViewById(R.id.calendar_day_gridcell);

			if(calendarItem.getEvents()!=null && calendarItem.getEvents().size()>0){
				LinearLayout indicatorContainer = (LinearLayout)row.findViewById(R.id.container_mycalendar_cells_events);
				for(int i=0; i<calendarItem.getEvents().size(); i++){
					CalendarEvent evt = calendarItem.getEvents().get(i);
					View indicator = calendarInterface.getActivity().getLayoutInflater().inflate(R.layout.indicator_nonworkingday, null);
					switch(evt.getDuration()){
						case AM:
							indicator.findViewById(R.id.cells_mycalendar_nonworkingday_am).setBackgroundColor(calendarItem.getEvents().get(i).getColor());
							if(!evt.shouldFill())
								indicator.findViewById(R.id.cells_mycalendar_nonworkingday_am_sub).setBackgroundColor(Color.WHITE);
							break;
						case PM:
							indicator.findViewById(R.id.cells_mycalendar_nonworkingday_pm).setBackgroundColor(calendarItem.getEvents().get(i).getColor());
							if(!evt.shouldFill())
								indicator.findViewById(R.id.cells_mycalendar_nonworkingday_pm_sub).setBackgroundColor(Color.WHITE);
							break;
						case AllDay:
							if(!evt.shouldFill()){
								((LinearLayout)indicator.findViewById(R.id.cells_mycalendar_nonworkingday_am).getParent()).setBackgroundColor(evt.getColor());
								indicator.findViewById(R.id.cells_mycalendar_nonworkingday_am).setBackgroundColor(Color.WHITE);
								indicator.findViewById(R.id.cells_mycalendar_nonworkingday_pm).setBackgroundColor(Color.WHITE);
							}else{
								indicator.findViewById(R.id.cells_mycalendar_nonworkingday_am).setBackgroundColor(evt.getColor());
								indicator.findViewById(R.id.cells_mycalendar_nonworkingday_pm).setBackgroundColor(evt.getColor());
							}
							break;
					}
					indicatorContainer.addView(indicator);
				}
			}

			if(calendarItem.getType() == CalendarItemType.TYPE_NOW)
				gridcell.setTextColor(calendarInterface.getActivity().getResources().getColor(R.color.orange_velosi));
			else if(calendarItem.getType() == CalendarItemType.TYPE_OUTMONTH || calendarItem.getType() == CalendarItemType.TYPE_NONWORKINGDAY || calendarItem.getType() == CalendarItemType.TYPE_INVALIDLEAVEDAYS)
				gridcell.setTextColor(Color.parseColor("#ABABAB"));

			gridcell.setTag(position);
			gridcell.setOnClickListener(this);
		}

		gridcell.setText(calendarItem.getLabel());
		return row;
	}

	public void updateItems() {
		propMapDayEvents.clear();
		//store holidays in an event hashmap for faster access later
		//update holidays
		for (CountryHoliday holiday : holidays) {
			ArrayList<CalendarEvent> currEvents = (propMapDayEvents.containsKey(holiday.getStringedDate())) ? propMapDayEvents.get(holiday.getStringedDate()) : new ArrayList<CalendarEvent>();
			currEvents.add(new CalendarEvent(holiday.getName(), calendarInterface.getActivity().getResources().getColor(R.color.holiday), CalendarEvent.CalendarEventDuration.AllDay, true, true, false));
			propMapDayEvents.put(holiday.getStringedDate(), currEvents);
		}

		//update leaves
		try {
			for (Leave leave : leaves) {
				if (leave.getStatusID() == Leave.LEAVESTATUSPENDINGID || leave.getStatusID() == Leave.LEAVESTATUSAPPROVEDKEY) {
					Calendar startCalendar = Calendar.getInstance();
					Calendar endCalendar = Calendar.getInstance();
					startCalendar.setTime(app.dateFormatDefault.parse(leave.getStartDate()));
					endCalendar.setTime(app.dateFormatDefault.parse(leave.getEndDate()));

					while (startCalendar.compareTo(endCalendar) <= 0) {
						String currDate = app.dateFormatDefault.format(startCalendar.getTime());
						ArrayList<CalendarEvent> currEvents = (propMapDayEvents.containsKey(currDate)) ? propMapDayEvents.get(currDate) : new ArrayList<CalendarEvent>();
						CalendarEvent.CalendarEventDuration duration;
						if (leave.getDays() == 0.1f)
							duration = CalendarEvent.CalendarEventDuration.AM;
						else if (leave.getDays() == 0.2f)
							duration = CalendarEvent.CalendarEventDuration.PM;
						else duration = CalendarEvent.CalendarEventDuration.AllDay;

						if (leave.getTypeID() == Leave.LEAVETYPEVACATIONKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_vacation), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPESICKKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_sick), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPEBIRTHDAYKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_birthday), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPEUNPAIDKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_unpaid), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPEBEREAVEMENTKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_bereavement), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPEMATERNITYKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_matpat), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPEDOCTORKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_doctor), duration, false, true));
						else if (leave.getTypeID() == Leave.LEAVETYPEHOSPITALIZATIONKEY)
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_hospitalization), duration, false, true));
						else
							currEvents.add(new CalendarEventLeave(leave, calendarInterface.getActivity().getResources().getColor(R.color.leavetype_businesstrip), duration, false, true));

						propMapDayEvents.put(currDate, currEvents);
						startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.get(Calendar.DAY_OF_MONTH) + 1);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			try {
				app.showMessageDialog(calendarInterface.getActivity(), "Unable to update items " + e.getMessage());
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}

		updateDateItem();
	}



	protected void updateDateItem(){

		Calendar headerSelectionCalendar = Calendar.getInstance();

		headerSelectionCalendar.set(Calendar.YEAR, Integer.parseInt(spinnerYear.getSelectedItem().toString()));
		headerSelectionCalendar.set(Calendar.MONTH, spinnerMonth.getSelectedItemPosition());

		//setup to apply appropriate offsets and trailing days to the calendar
		Calendar beginningOfThisMonth = Calendar.getInstance();
		beginningOfThisMonth.setTime(headerSelectionCalendar.getTime());
		int dayofweek = beginningOfThisMonth.get(Calendar.DAY_OF_WEEK);

		dayofweek = (dayofweek == 1)?7:dayofweek-1; //important to include months that start on saturdays
		int offset = (beginningOfThisMonth.get(Calendar.DAY_OF_MONTH)%7) - dayofweek;

		if(offset > 1)
			offset-=7;

		beginningOfThisMonth.set(Calendar.DAY_OF_MONTH, offset);
		boolean trailingDaysAreDaysOfThisMonth = false;

		dateItems.clear();
		Calendar currDateCalendar = Calendar.getInstance();
		System.out.println("CURRENT YEAR: " + headerSelectionCalendar.get(Calendar.YEAR));

		for(int i=0; i<42; i++) {
			int dayOfMonth = beginningOfThisMonth.get(Calendar.DAY_OF_MONTH);

			if (dayOfMonth == 1)
				trailingDaysAreDaysOfThisMonth = !trailingDaysAreDaysOfThisMonth;

			CalendarItemType itemType;
			ArrayList<CalendarEvent> events;


			if (trailingDaysAreDaysOfThisMonth) {
				if (beginningOfThisMonth.get(Calendar.DAY_OF_MONTH) == currDateCalendar.get(Calendar.DAY_OF_MONTH) &&
						beginningOfThisMonth.get(Calendar.MONTH) == currDateCalendar.get(Calendar.MONTH) &&
						beginningOfThisMonth.get(Calendar.YEAR) == currDateCalendar.get(Calendar.YEAR)) {
					itemType = CalendarItemType.TYPE_NOW;
					events = propMapDayEvents.get(app.dateFormatDefault.format(beginningOfThisMonth.getTime()));
				} else {
					itemType = CalendarItemType.TYPE_WEEKDAYS;
					events = propMapDayEvents.get(app.dateFormatDefault.format(beginningOfThisMonth.getTime()));

					if (headerSelectionCalendar.getTime().compareTo(minCalendar.getTime()) < 0)
						itemType = CalendarItemType.TYPE_INVALIDLEAVEDAYS;

					if (!app.getStaff().hasSunday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
					if (!app.getStaff().hasMonday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
					if (!app.getStaff().hasTuesday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
					if (!app.getStaff().hasWednesday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
					if (!app.getStaff().hasThursday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
					if (!app.getStaff().hasFriday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
					if (!app.getStaff().hasSaturday() && beginningOfThisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
						itemType = CalendarItemType.TYPE_NONWORKINGDAY;
				}
			} else {
				itemType = CalendarItemType.TYPE_OUTMONTH;
				events = null;
			}

			dateItems.add(new CalendarItem(itemType, String.valueOf(dayOfMonth), events, beginningOfThisMonth.getTime()));
			beginningOfThisMonth.set(Calendar.DAY_OF_MONTH, beginningOfThisMonth.get(Calendar.DAY_OF_MONTH) + 1);

			allItems.clear();
			allItems.addAll(headerItems);
			allItems.addAll(dateItems);


			notifyDataSetChanged();
//			prevButton.setVisibility((headerSelectionCalendar.get(Calendar.MONTH) > minCalendar.get(Calendar.MONTH) && headerSelectionCalendar.get(Calendar.YEAR) >= minCalendar.get(Calendar.YEAR)) ? View.VISIBLE : View.INVISIBLE);
//			nextButton.setVisibility((headerSelectionCalendar.get(Calendar.MONTH) <= minCalendar.get(Calendar.MONTH) && headerSelectionCalendar.get(Calendar.YEAR) <= maxCalendar.get(Calendar.YEAR)) ? View.VISIBLE : View.INVISIBLE);

			if (headerSelectionCalendar.get(Calendar.MONTH) > minCalendar.get(Calendar.MONTH) && headerSelectionCalendar.get(Calendar.YEAR) >= minCalendar.get(Calendar.YEAR) || headerSelectionCalendar.get(Calendar.YEAR) == maxCalendar.get(Calendar.YEAR))
				prevButton.setVisibility(View.VISIBLE);
			else
				prevButton.setVisibility(View.INVISIBLE);


			if (headerSelectionCalendar.get(Calendar.MONTH) <= maxCalendar.get(Calendar.MONTH) && headerSelectionCalendar.get(Calendar.YEAR) <= maxCalendar.get(Calendar.YEAR) ) {
				nextButton.setVisibility(View.VISIBLE);
				if (headerSelectionCalendar.get(Calendar.MONTH) == maxCalendar.get(Calendar.MONTH) && headerSelectionCalendar.get(Calendar.YEAR) == maxCalendar.get(Calendar.YEAR))
					nextButton.setVisibility(View.INVISIBLE);
			}
		}

	}

}
