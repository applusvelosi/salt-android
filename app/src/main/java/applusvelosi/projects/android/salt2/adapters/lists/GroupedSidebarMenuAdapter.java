package applusvelosi.projects.android.salt2.adapters.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;
import java.util.Map;

import applusvelosi.projects.android.salt2.utils.interfaces.GroupedListItemInterface;
import applusvelosi.projects.android.salt2.views.HomeActivity;

/**
 * Created by Velosi on 26/07/2017.
 */

public class GroupedSidebarMenuAdapter extends BaseAdapter {

    private HomeActivity activity;
    private Map<String, GroupedListItemInterface> hashData;
    private List<String> keys;

    public GroupedSidebarMenuAdapter(HomeActivity activity, Map<String, GroupedListItemInterface> hashData, List<String> hashKeys) {
        this.activity = activity;
        this.hashData = hashData;
        keys = hashKeys;
    }

    @Override
    public int getCount() {
        return keys.size();
    }

    @Override
    public Object getItem(int position) {
        return hashData.get(keys.get(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return hashData.get(keys.get(position)).getTextView(activity);
    }

}
