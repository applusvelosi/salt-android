package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.claimitems.MilageClaimItem;
import applusvelosi.projects.android.salt2.views.dialogs.DialogItemCategories;

public class ClaimItemEditMileageFragment extends ItemInputFragment implements DialogItemCategories.DialogClaimItemCategoryInterface {
	private MilageClaimItem milageClaimItem;

    private TextView tvCategory;
    private TextView tviewMileageType;
    private EditText etextMileageMileage, etextMileageRate, etextMileageFrom, etextMileageTo;
//    private CheckBox cboxIsReturn;
    private Float mileageCost;
    private DialogItemCategories dialogCategories;

	@Override
	protected View getInflatedLayout(LayoutInflater inflater) {
        milageClaimItem = (MilageClaimItem)activity.claimItem;
		return inflater.inflate(R.layout.fragment_claimitem_editmileage, null);
	}

	@Override
	protected void initAdditionalViewComponents(View v) {
		tvCategory = (TextView)v.findViewById(R.id.tviews_claimiteminput_category);
		tvCategory.setOnClickListener(this);
		tvCategory.setText(activity.claimItem.getCategoryName());

        tviewMileageType = (TextView)v.findViewById(R.id.tviews_claimiteminput_mileagetype);
        tviewMileageType.setOnClickListener(this);

        etextMileageMileage = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileagemileage);
        etextMileageRate = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileagerate);
        etextMileageMileage.setText(String.valueOf(milageClaimItem.getMileage()));
        etextMileageRate.setText(String.valueOf(milageClaimItem.getMilageRate()));

        etextMileageRate.addTextChangedListener(this);

        etextMileageFrom = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileagefrom);
        etextMileageTo = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileageto);
//        cboxIsReturn = (CheckBox)v.findViewById(R.id.cboxs_claimiteminput_mileagereturn);

        etextMileageFrom.setText(milageClaimItem.getMileageFrom());
        etextMileageTo.setText(milageClaimItem.getMileageTo());
//        cboxIsReturn.setChecked(milageClaimItem.isMileageReturn());
        etextAmount.setText(String.valueOf(activity.claimItem.getForeignAmount()));
        etextLocalAmount.setText(String.valueOf(activity.claimItem.getLocalAmount()));
        tviewMileageType.setText((milageClaimItem.getMilageTypeID()==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER)?MilageClaimItem.MILEAGETYPE_VAL_KILOMETER:MilageClaimItem.MILEAGETYPE_VAL_MILE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v == tvCategory){
            if(activity.getCategories() == null){
                activity.startLoading();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Object tempResult;
                        try{
                            tempResult = app.onlineGateway.getClaimItemCategoryByOffice(activity.claimHeader.getTypeID());
                        }catch(Exception e){
                            e.printStackTrace();
                            tempResult = e.getMessage();
                        }

                        final Object result = tempResult;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                activity.finishLoading();
                                if(result instanceof String){
                                    Toast.makeText(activity, result.toString(), Toast.LENGTH_SHORT).show();
                                }else{
                                    activity.updateCategoryList(activity, (ArrayList<Category>) result);
                                    if(dialogCategories == null)
                                        dialogCategories = new DialogItemCategories(activity, ClaimItemEditMileageFragment.this);
                                    dialogCategories.show();
                                }
                            }
                        });
                    }
                }).start();
            }else{
                if(dialogCategories == null)
                    dialogCategories = new DialogItemCategories(activity, ClaimItemEditMileageFragment.this);
                dialogCategories.show();
            }
        }else if(v == tviewMileageType) {
            tviewMileageType.setText((tviewMileageType.getText().equals(MilageClaimItem.MILEAGETYPE_VAL_KILOMETER) ? MilageClaimItem.MILEAGETYPE_VAL_MILE : MilageClaimItem.MILEAGETYPE_VAL_KILOMETER));
            etextMileageMileage.setText("");

        }
    }

    @Override
	protected void saveToServer() {
        try {
            if(tviewsDate.length() > 0){
                if(etextAmount.length() > 0){
                    if(etextDesc.length() > 0) {
                        activity.claimItem.setDateCreated(app.onlineGateway.epochizeDate(new Date()));
                        activity.startLoading();
                        actionbarDone.setEnabled(false);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Object tempResult;
                                try {
                                    int mileageTypeID = (tviewMileageType.getText().equals(MilageClaimItem.MILEAGETYPE_VAL_KILOMETER)) ? MilageClaimItem.MILEAGETYPE_KEY_KILOMETER : MilageClaimItem.MILEAGETYPE_KEY_MILE;
                                    float mileageRate = Float.parseFloat(etextMileageRate.getText().toString());
                                    int mileage = Integer.parseInt(etextMileageMileage.getText().toString());
                                    String mileageFrom = etextMileageFrom.getText().toString();
                                    String mileageTo = etextMileageTo.getText().toString();

                                    MilageClaimItem milageClaimItem = new MilageClaimItem(activity.claimItem, mileageTypeID, mileageRate, mileage, mileageFrom, mileageTo);
                                    tempResult = app.onlineGateway.saveClaimLineItem(milageClaimItem, 0, attachedFile);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    tempResult = e.getMessage();
                                }

                                final Object result = tempResult;
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        actionbarDone.setEnabled(true);
                                        if (result instanceof String) {
                                            activity.finishLoading(result.toString());
                                        } else {
                                            activity.finishLoading();
                                            Toast.makeText(activity, "Please refresh in the claim list to reflect the changes", Toast.LENGTH_LONG).show();
                                            activity.finish();
                                        }
                                    }
                                });
                            }
                        }).start();
                    }else app.showMessageDialog(activity, "Description is required");
                }else app.showMessageDialog(activity, "Amount is required");
            }else app.showMessageDialog(activity, "Expense date is required");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SALTX "+e.getMessage());
        }
	}

    @Override
    public void onCategorySelected(Category category) {
        if (category.getCategoryTypeID() == activity.claimItem.getCategoryTypeID()) {
            activity.claimItem.setCategory(category);
            tvCategory.setText(category.getName());
        } else
            app.showMessageDialog(activity, "App does not support editing category with different a type yet");

    }

    @Override
    public void onResume() {
        super.onResume();
        etextMileageMileage.addTextChangedListener(this);
    }

    void getMileageCost(){
        int mileageKeyType = (tviewMileageType.getText().equals(MilageClaimItem.MILEAGETYPE_VAL_KILOMETER))?MilageClaimItem.MILEAGETYPE_KEY_KILOMETER:MilageClaimItem.MILEAGETYPE_KEY_MILE;

        if((mileageKeyType==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER && app.getStaffOffice().getMileageType()==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER) || (mileageKeyType==MilageClaimItem.MILEAGETYPE_KEY_MILE && app.getStaffOffice().getMileageType()==MilageClaimItem.MILEAGETYPE_KEY_MILE))
            mileageCost = Float.parseFloat(etextMileageMileage.getText().toString());
        else if(app.getStaffOffice().getMileageType() == MilageClaimItem.MILEAGETYPE_KEY_MILE && mileageKeyType==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER)
            mileageCost = Float.parseFloat(etextMileageMileage.getText().toString()) / Float.parseFloat("0.62");
        else if(app.getStaffOffice().getMileageType() == MilageClaimItem.MILEAGETYPE_KEY_KILOMETER && mileageKeyType == MilageClaimItem.MILEAGETYPE_KEY_MILE)
            mileageCost = Float.parseFloat(etextMileageMileage.getText().toString()) * Float.parseFloat("0.62");
    }

    public void afterTextChanged(Editable s) {
        super.afterTextChanged(s);
        etextAmount.removeTextChangedListener(this);
        try {
            etextMileageMileage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        etextMileageMileage.setText("");
                    } else {
                        etextMileageMileage.setText(String.valueOf((((MilageClaimItem) activity.claimItem).getMileage())));
                    }
                }
            });

            if(!app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                etextForex.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            System.out.println("please enter a forex");
                        }else {
                            if(etextForex.length() == 0)
                                etextForex.setText(String.valueOf(forex));
                            else
                                etextForex.setHint("0.00");
                        }
                    }
                });
            }

            if (s.hashCode() == etextMileageMileage.getText().hashCode()) {
                int value = Integer.parseInt(etextMileageMileage.getText().toString());
                if (value > 0) {
                    etextMileageMileage.removeTextChangedListener(this);
                    etextMileageMileage.setText(String.valueOf(value));
                    etextMileageMileage.setSelection(etextMileageMileage.length());
                    etextMileageMileage.addTextChangedListener(this);
                }
                etextAmount.addTextChangedListener(this);
//                activity.claimItem.setAmount(value);
            } else if (s.hashCode() == etextForex.getText().hashCode())
                activity.claimItem.setForex(Float.parseFloat(etextForex.getText().toString()));
            else if (s.hashCode() == etextSapTaxValue.getText().hashCode())
                activity.claimItem.setSapTaxCode(tvSapTaxCode.getText().toString());
            else if (s.hashCode() == etextTax.getText().hashCode() && !etextTax.getText().toString().contains("Tax Not Applicable"))
                activity.claimItem.setTaxAmount(Float.parseFloat(etextTax.getText().toString()));


            float amount, totalLC;
            getMileageCost();

            amount = mileageCost * Float.parseFloat(etextMileageRate.getText().toString());
            etextAmount.setText(SaltApplication.decimalFormat.format(amount));
            activity.claimItem.setAmount(amount);

            totalLC = amount * Float.parseFloat(etextForex.getText().toString());
            etextLocalAmount.setText(SaltApplication.decimalFormat.format(totalLC));
            activity.claimItem.setAmountLC(totalLC);

            if (app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                if (isSapTax)
                    totalLC /= (1 + Float.parseFloat(etextSapTaxValue.getText().toString()));
                else if (cboxTaxable.isShown()) {
                    if (cboxTaxable.isChecked())
                        totalLC /= (1 + Float.parseFloat(etextTax.getText().toString()));
                }
            }

            etextBeforeTaxAmt.setText(SaltApplication.decimalFormat.format(totalLC));
            if (s.hashCode() == etextBillNotes.getText().hashCode())
                activity.claimItem.setNotes(etextBillNotes.getText().toString());
            else if (s.hashCode() == etextDesc.getText().hashCode())
                activity.claimItem.setDescription(etextDesc.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            etextAmount.setText("0.00");
            etextLocalAmount.setText("0.00");
        }
    }

}
