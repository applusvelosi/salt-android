package applusvelosi.projects.android.salt2;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.appsee.Appsee;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Dashboard;
import applusvelosi.projects.android.salt2.models.Holiday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.models.Office;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.utils.OfflineGateway;
import applusvelosi.projects.android.salt2.utils.OnlineGateway;
import applusvelosi.projects.android.salt2.utils.TypeHolder;
import applusvelosi.projects.android.salt2.utils.enums.Months;
import applusvelosi.projects.android.salt2.views.fragments.roots.HolidaysLocalFragment;
import applusvelosi.projects.android.salt2.views.fragments.roots.HolidaysMonthlyFragment;
import io.fabric.sdk.android.Fabric;

/* The entrance point of the app
 * after the app has been opened, the system
 * will display the splash activity which will then trigger the initapplication method in this class
 */

public class SaltApplication extends Application {

	private Tracker mTracker;

//	/**
//	 * Gets the default {@link Tracker} for this {@link Application}.
//	 * @return tracker
//	 */
//	synchronized public Tracker getDefaultTracker() {
//		if (mTracker == null) {
//			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//			// To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//			mTracker = analytics.newTracker(R.xml.global_tracker);
//		}
//		return mTracker;
//	}

	// The following line should be changed to include the correct property id.
	private static final String PROPERTY_ID = "UA-68161281-1";

	/**
	 * Enum used to identify the tracker that needs to be used for tracking.
	 *
	 * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
	 * storing them all in Application object helps ensure that they are created only once per
	 * application instance.
	 */
	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER // Tracker used by all the apps from a company. eg: roll-up tracking.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID): analytics.newTracker(R.xml.app_tracker);
			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}
	public static final String STAFF_POS_LM = "Legal Manager";
	public static final String STAFF_POS_APPLUSCSR = "GEC";
	public static final String CONNECTION_ERROR = "No address associated with hostname"; //Constant for generic error messages
	public static final int RESULT_CAMERA = 0; //Constant for CAMERA requests
	public static final int RESULT_BROWSEFILES = 1; //Constant for File browsing requests below KitKat Version
	public static final int RESULT_BROWSEFILES_KITKAT = 2; //Constant for File browsing requests for KitKat and above version
    public static final long ONEDAY = 24*60*60*1000;
	public static final String ACCEPTED_FILETYPES = ".png.jpg.gif.pdf.jpeg";
	public static final DecimalFormat decimalFormat = new DecimalFormat("#,##0.00"); //should use this when displaying floating types as string

	public static final DecimalFormat sapTaxDecimalFormat = new DecimalFormat("0.0000");
	public static final int MAINHRID = 198; //constant for recruitment module
	public FileManager fileManager;

	public OfflineGateway offlineGateway;
	public OnlineGateway onlineGateway;
	
	public Gson gson;
	public TypeHolder types;
	public Calendar calendar;
	public SimpleDateFormat dateTimeFormat, dateFormatDefault, dateFormatCustom, dateFormatClaimItemAttachment, dateFormatByProcessed;

	private ArrayList<CountryHoliday> nationalHolidays;
	private ArrayList<Holiday> localHolidays;
	private ArrayList<Leave> myLeaves;
	private ArrayList<ClaimHeader> myClaimHeaders;
	private boolean hasLoadedMonthlyHolidays, hasLoadedLocalHolidays;
	private ArrayList<Currency> currencies;
	private Staff staff;
	private Office office;
	
	public ArrayList<String> dropDownYears, dropDownMonths;
	public int thisYear;
	public Animation animationShow, animationHide;
	public Dashboard dashboard;
	
	//initializes all objects that are needed by different objects living within the app
	public void initializeApp() throws Exception{
		fileManager = new FileManager();
		onlineGateway = new OnlineGateway(this);
		offlineGateway = new OfflineGateway(this);
		hasLoadedMonthlyHolidays = false;
		hasLoadedLocalHolidays = false;

		dateFormatByProcessed = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
		dateFormatByProcessed.setTimeZone(TimeZone.getTimeZone("BST"));
		dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
		dateFormatDefault = new SimpleDateFormat("dd-MMM-yyyy");
		dateFormatDefault.setTimeZone(TimeZone.getDefault());
		dateFormatCustom = new SimpleDateFormat("dd/MMM/yyyy", Locale.US);
		dateFormatClaimItemAttachment = new SimpleDateFormat("dd-MM-yyyy-HH-msm-ss-S");
		sapTaxDecimalFormat.setMaximumFractionDigits(4);
		calendar = Calendar.getInstance();
		gson = new Gson();
		types = new TypeHolder();
		thisYear = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.ENGLISH).format(new Date()));
				
		dropDownYears = new ArrayList<String>();
		int currYear = calendar.get(Calendar.YEAR);
		for(int i=currYear-4; i<=currYear+1; i++)
			dropDownYears.add(String.valueOf(i));

		dropDownMonths = new ArrayList<String>();
		for(int i=0; i<Months.values().length; i++)
			dropDownMonths.add(Months.values()[i].toString());

		myClaimHeaders = new ArrayList<ClaimHeader>();
		if(offlineGateway.isLoggedIn()){
			staff = offlineGateway.deserializeStaff();
			office = offlineGateway.deserializeStaffOffice();
			myLeaves = offlineGateway.deserializeMyLeaves();
			currencies = offlineGateway.deserializeCurrencies();
            myClaimHeaders = offlineGateway.deserializeMyClaims();
			nationalHolidays = offlineGateway.deserializeNationalHolidays();
			localHolidays = offlineGateway.deserializeLocalHolidays();
			try{
				onlineGateway.updateStaff(staff.getStaffID(), staff.getSecurityLevel(), staff.getOfficeID());
			}catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			myLeaves = new ArrayList<Leave>();
			myClaimHeaders = new ArrayList<ClaimHeader>();
			nationalHolidays = new ArrayList<CountryHoliday>();
			localHolidays = new ArrayList<Holiday>();
		}
		animationShow = new AlphaAnimation(0.0f,1.0f);
		animationShow.setDuration(1000);

		animationHide = new AlphaAnimation(1.0f,0.0f);
		animationHide.setDuration(1000);

	}

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Appsee.start(getString(R.string.com_appsee_apikey));
//        Parse.initialize(this, "Lsnfv65XD5V1RlgoYnWX8DEB06EW9BCTOdwBDWRb", "CiYC18jb5FXSdjJgvJbixG0wqoC252dnR7YAwgBd");
//		ParseInstallation.getCurrentInstallation().saveInBackground();
	}

	public boolean hasLoadedMonthlyHolidays(){
		return hasLoadedMonthlyHolidays;
	}
	
	public boolean hasLoadedLocalHolidays(){
		return hasLoadedLocalHolidays;
	}
	
	public void setMonthlyHolidaysLoaded(HolidaysMonthlyFragment key){
		hasLoadedMonthlyHolidays = true;
	}

//	public static void hideSoftKeyboard(Activity activity) {
//		InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
//	}

	public void setLocalHolidaysLoaded(HolidaysLocalFragment key){
		hasLoadedLocalHolidays = true;
	}

//	private void downCastClaim(ArrayList<ClaimHeader> claimHeaders, ArrayList<HashMap<String, Object>> maps){
//		for(HashMap<String, Object> map :maps){
//			if(Integer.parseInt(map.get(ClaimHeader.KEY_TYPEID).toString()) == ClaimHeader.TYPEKEY_CLAIMS){
//				if(Boolean.parseBoolean(map.get(ClaimPaidByCC.KEY_ISPAIDBYCOMPANYCARD).toString()))
//					claimHeaders.add(new ClaimPaidByCC(map));
//				else
//					claimHeaders.add(new ClaimNotPaidByCC(map));
//			}else if(Integer.parseInt(map.get(ClaimHeader.KEY_TYPEID).toString()) == ClaimHeader.TYPEKEY_ADVANCES){
//				claimHeaders.add(new BusinessAdvance(map));
//			}else if(Integer.parseInt(map.get(ClaimHeader.KEY_TYPEID).toString()) == ClaimHeader.TYPEKEY_LIQUIDATION){
//				claimHeaders.add(new LiquidationOfBA(map));
//			}
//		}
//	}

	public boolean isNetworkAvailable(){
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
		
	public void showMessageDialog(Context context, String message){
		new AlertDialog.Builder(context).setMessage(message)
										.setPositiveButton("Ok", new OnClickListener() {

											@Override
											public void onClick(DialogInterface dialog, int arg1) {
												dialog.dismiss();
											}
										})
										.create().show();
	}
	
	public void showNetworkErrorMessage(Context context, String message){
		new AlertDialog.Builder(context).setMessage(message)
		.setPositiveButton("Ok", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
			}
		}).create().show();
	}
	
	public boolean hasSavedData(){
		return (this.offlineGateway!=null && this.offlineGateway.isLoggedIn())?true:false;
	}

	public int versionCompare(String appVersion, String firebaseVersion) {
		String[] vals1 = appVersion.split("\\.");
		String[] vals2 = firebaseVersion.split("\\.");
		int i = 0;
		// set index to first non-equal ordinal or length of shortest version string
		while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
			i++;
		}
		// compare first non-equal ordinal number
		if (i < vals1.length && i < vals2.length) {
			int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
			return Integer.signum(diff);
		}
		// the strings are equal or one string is a substring of the other
		// e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
		return Integer.signum(vals1.length - vals2.length);

	}
	
	public void setStaffData(OnlineGateway key, Staff staff, Office office){
		this.staff = staff;
		this.office = office;

		if(office.getCMID() == staff.getStaffID())
			staff.setUserPosition((office.isHeadQuarter())?Staff.USERPOSITION.USERPOSITION_CFO:Staff.USERPOSITION.USERPOSITION_CM);
		else if(office.getRMID() == staff.getStaffID())
			staff.setUserPosition((office.isHeadQuarter())?Staff.USERPOSITION.USERPOSITION_CEO:Staff.USERPOSITION.USERPOSITION_RM);
		else if (Staff.LMId == staff.getStaffID())
			staff.setUserPosition(Staff.USERPOSITION.USERPOSITION_LM);
		else
			staff.setUserPosition(Staff.USERPOSITION.USERPOSITION_DEFAULT);

		this.offlineGateway.serializeStaffData(staff, office);

//        ParseObject dataObject = new ParseObject("Usage");
//        dataObject.put("name", staff.getFname()+" "+staff.getLname());
//        dataObject.put("office", office.getName());
//        dataObject.saveInBackground();

//		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
//		installation.put("staffID", staff.getStaffID());
//		installation.put("staffName", staff.getFname()+" "+staff.getLname());
//		installation.saveInBackground();
    }
	
	public void setStaff(Staff staff){ //temporary data for staff that must be only called by loginactivity class
		this.staff = staff;
	}
	
	public Staff getStaff(){
		return staff;
	}
	
	public Office getStaffOffice(){
		return office;
	}
		
	public void updateMyLeaves(ArrayList<Leave> myLeaves){
		this.myLeaves.clear();
		this.myLeaves.addAll(myLeaves);
		offlineGateway.serializeMyLeaves(myLeaves);
	}

	public void updateMyClaims(List<ClaimHeader> myClaimHeaders){
		this.myClaimHeaders.clear();
		this.myClaimHeaders.addAll(myClaimHeaders);
//		offlineGateway.serializeMyClaims(myClaimHeaders);
	}

	public ArrayList<Leave> getMyLeaves(){
		return myLeaves;
	}

	public ArrayList<ClaimHeader> getMyClaims(){
		return myClaimHeaders;
	}
							
	public ArrayList<CountryHoliday> getNationalHolidays(){
		return nationalHolidays;
	}
	
	public ArrayList<Holiday> getLocalHolidays(){
		return localHolidays;
	}
			
	public void updateNationalHolidays(ArrayList<CountryHoliday> nationalHolidays){
		this.nationalHolidays.clear();
		this.nationalHolidays.addAll(nationalHolidays);
		this.offlineGateway.serializeNationHolidays(this, nationalHolidays);
	}
	
	public void updateLocalHolidays(ArrayList<Holiday> localHolidays){
		this.localHolidays.clear();
		this.localHolidays.addAll(localHolidays);
		this.offlineGateway.serializeLocalHolidays(this, localHolidays);
	}
	
	public void setCurrencies(ArrayList<Currency> currencies){
		System.out.println("SALTX currencies set "+currencies.size());
		this.currencies = new ArrayList<Currency>();
		this.currencies.addAll(currencies);
		offlineGateway.serializeCurrencies(currencies);
	}
	
	public ArrayList<Currency> getCurrencies(){
		return currencies;
	}
		
	private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    public static Typeface myFont(Context context) {
    	String name = "fonts/Tahoma.ttf";
        Typeface tf = fontCache.get(name);
        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }
}
