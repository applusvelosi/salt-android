package applusvelosi.projects.android.salt2.models.claimheaders;

import org.json.JSONObject;

import java.util.List;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;

public class ClaimNotPaidByCC extends Claim{
	
	public ClaimNotPaidByCC(SaltApplication app, int costCenterID, String costCenterName) throws Exception{
		super(app, costCenterID, costCenterName, false);
	}

	public ClaimNotPaidByCC(SaltApplication app, int costCenterID, String costCenterName, int hasYesNoDoc, List<Document> docs) throws Exception{
		super(app, hasYesNoDoc, docs, costCenterID, costCenterName, false);
	}

	public ClaimNotPaidByCC(JSONObject jsonClaimNotPaidByCC) throws Exception{
		super(jsonClaimNotPaidByCC);
	}

	public ClaimNotPaidByCC(ClaimHeader claimNotPaidByCC){
		super(claimNotPaidByCC);
	}

	public boolean isPaidByCC() {
		return false;
	}

	public int getParentClaimID() {
		return parentClaimID;
	}

	public String getParentClaimNumber() {
		return parentClaimNumber;
	}

}
