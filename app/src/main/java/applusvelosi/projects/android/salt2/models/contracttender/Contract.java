package applusvelosi.projects.android.salt2.models.contracttender;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;

/**
 * Created by Velosi on 19/05/2017.
 */

public class Contract implements Serializable {

    private String additionalMembership;
    private int agentApproved;
    private String agentCommission;
    private String agentName;
    private boolean isAgentRequired;
    private String approverNote, bankPerformanceGuarantee, billingCurrency;
    private List<Document> businessCaseDocument;
    private String businessOrigDocName;
    private String cmEmail;
    private int cmId;
    private String cmName;
    private int capexRequired, capexRequiredEquiv;
    private String certificatesValid;
    private int cfoId;
    private String cfoName, commercialTermsOther, company, companyCurrency;
    private int competentPersonnel;
    private String competingGrounds, competingAdvantage, competitorOffers, competitors, consequentialLoss;
    private List<Document> contractDocument;
    private int contractId, contractType;
    private String contractingStrategy;
    private int creditRisk;
    private String dateAwardedLost, dateProcessedByCfo, dateProcessedByCm, dateProcessedByEvp, dateProcessedByLm, dateProcessedByRm, dateProcessedByRfm, dateStartDate, dateTenderSubmission;
    private String dealBreakers, endClient;
    private int equipment, equipmentAvailable, equipmentCost;
    private String equipmentRequired, evpEmail;
    private int evpId;
    private String evpName, existingOrNew;
    private int finalStatus;
    private float grossMargin;
    private String hsqe;
    private String importExport;
    private int infrastructure;
    private String insurance, intellectualPropertyRights, invoicing, jobStaff, justification, lmEmail;
    private int lmId;
    private String lmName;
    private int legalOthers;
    private String legalOthers2, limitationLiability, liquidatedDamage, mainOrigDocName, negotiations, notes;
    private int officeId;
    private String operational, otherLocalTaxes, paymentTerms, permanentEstablishment;
    private int paymentBehavior;
    private int personnel;
    private String pricing, pricingAdditional, primaryResponsibility;
    private int probabilityOfAward, projectDuration, projectDurationRange;
    private String projectExcutionLocation, projectManagerCriteria, projectTeamBuildup;
    private int projectValue, projectValueEquiv;
    private int rfmId;
    private String rmEmail;
    private int rmId;
    private String rmName;
    private float rateToEuro;
    private String referenceNumber, requestDate;
    private int requestStatus;
    private String requestStatusName, requestor, rfmEmail, rfmName, riskAssessmentOthers;
    private int serviceLines, services;
    private String specificTraining, staffAware;
    private int staffId, status;
    private String statusName, technicalAnalysisOthers, visa;

    private static final String CONTRACTTENDERTYPE_OPEN = "70-Open";
    private static final String CONTRACTTENDERTYPE_SUBMITTED = "71-Submitted";
    private static final String CONTRACTTENDERTYPE_APPROVEDBYCM = "72-Approved by CM";
    private static final String CONTRACTTENDERTYPE_REJECTEDBYCM = "73-Rejected by CM";
    private static final String CONTRACTTENDERTYPE_APPROVEDBYRM = "74-Approved by RM";
    private static final String CONTRACTTENDERTYPE_REJECTEDBYRM = "75-Rejected by RM";
    private static final String CONTRACTTENDERTYPE_APPROVEDBYLM = "76-Approved by LM";
    private static final String CONTRACTTENDERTYPE_REJECTEDBYLM = "77-Rejected by LM";
    private static final String CONTRACTTENDERTYPE_APPROVEDBYCFO = "78-Approved by CFO";
    private static final String CONTRACTTENDERTYPE_REJECTEDBYCFO = "79-Rejected by CFO";
    private static final String CONTRACTTENDERTYPE_APPROVEDBYEVP = "80-Approved by EVP";
    private static final String CONTRACTTENDERTYPE_REJECTEDBYEVP = "81-Rejected by EVP";
    private static final String CONTRACTTENDERTYPE_APPROVEDBYRFM = "146-Approved by RFM";
    private static final String CONTRACTTENDERTYPE_REJECTEDBYRFM = "147-Rejected by RFM";

    private static Map<Integer, String> contractTypeHash = new HashMap<Integer, String>();
    private static Map<Integer, String> serviceLineHash = new HashMap<Integer, String>();
    private static Map<Integer, String> statusHash = new HashMap<Integer, String>();
    private static Map<Integer, String> durationRangeHash = new HashMap<Integer, String>();
    private static Map<Integer, String> creditRiskHash = new HashMap<Integer, String>();
    private static Map<Integer, String> paymentBehavioeHash = new HashMap<Integer, String>();

    public static int CONTRACTTENDERID_OPEN, CONTRACTTENDERID_SUBMITTED, CONTRACTTENDERID_APPROVEDBYCM, CONTRACTTENDERID_REJECTEDBYCM,
            CONTRACTTENDERID_APPROVEDBYRM, CONTRACTTENDERID_REJECTEDBYRM, CONTRACTTENDERID_APPROVEDBYLM, CONTRACTTENDERID_REJECTEDBYLM,
            CONTRACTTENDERID_APPROVEDBYCFO, CONTRACTTENDERID_REJECTEDBYCFO, CONTRACTTENDERID_APPROVEDBYEVP, CONTRACTTENDERID_REJECTEDBYEVP,
            CONTRACTTENDERID_APPROVEDBYRFM, CONTRACTTENDERID_REJECTEDBYRFM;

    private static String CONTRACTTENDERDESC_OPEN, CONTRACTTENDERDESC_SUBMITTED, CONTRACTTENDERDESC_APPROVEDBYCM, CONTRACTTENDERDESC_REJECTEDBYCM,
            CONTRACTTENDERDESC_APPROVEDBYRM, CONTRACTTENDERDESC_REJECTEDBYRM,
            CONTRACTTENDERDESC_APPROVEDBYEVP, CONTRACTTENDERDESC_REJECTEDBYEVP, CONTRACTTENDERDESC_APPROVEDBYLM, CONTRACTTENDERDESC_REJECTEDBYLM,
            CONTRACTTENDERDESC_APPROVEDBYCFO, CONTRACTTENDERDESC_REJECTEDBYCFO, CONTRACTTENDERDESC_APPROVEDBYRFM, CONTRACTTENDERDESC_REJECTEDBYRFM;

    static {
        initContractType();
        initServiceLines();
        initStatus();
        initDurationRange();
        initCreditRisk();
        initPaymentBehavior();
        CONTRACTTENDERID_OPEN = Integer.parseInt(CONTRACTTENDERTYPE_OPEN.split("-")[0]);
        CONTRACTTENDERID_SUBMITTED = Integer.parseInt(CONTRACTTENDERTYPE_SUBMITTED.split("-")[0]);
        CONTRACTTENDERID_APPROVEDBYCM = Integer.parseInt(CONTRACTTENDERTYPE_APPROVEDBYCM.split("-")[0]);
        CONTRACTTENDERID_REJECTEDBYCM = Integer.parseInt(CONTRACTTENDERTYPE_REJECTEDBYCM.split("-")[0]);
        CONTRACTTENDERID_APPROVEDBYRM = Integer.parseInt(CONTRACTTENDERTYPE_APPROVEDBYRM.split("-")[0]);
        CONTRACTTENDERID_REJECTEDBYRM = Integer.parseInt(CONTRACTTENDERTYPE_REJECTEDBYRM.split("-")[0]);
        CONTRACTTENDERID_APPROVEDBYLM = Integer.parseInt(CONTRACTTENDERTYPE_APPROVEDBYLM.split("-")[0]);
        CONTRACTTENDERID_REJECTEDBYLM = Integer.parseInt(CONTRACTTENDERTYPE_REJECTEDBYLM.split("-")[0]);
        CONTRACTTENDERID_APPROVEDBYCFO = Integer.parseInt(CONTRACTTENDERTYPE_APPROVEDBYCFO.split("-")[0]);
        CONTRACTTENDERID_REJECTEDBYCFO = Integer.parseInt(CONTRACTTENDERTYPE_REJECTEDBYCFO.split("-")[0]);
        CONTRACTTENDERID_APPROVEDBYEVP = Integer.parseInt(CONTRACTTENDERTYPE_APPROVEDBYEVP.split("-")[0]);
        CONTRACTTENDERID_REJECTEDBYEVP = Integer.parseInt(CONTRACTTENDERTYPE_REJECTEDBYEVP.split("-")[0]);
        CONTRACTTENDERID_APPROVEDBYRFM = Integer.parseInt(CONTRACTTENDERTYPE_APPROVEDBYRFM.split("-")[0]);
        CONTRACTTENDERID_REJECTEDBYRFM = Integer.parseInt(CONTRACTTENDERTYPE_REJECTEDBYRFM.split("-")[0]);

        CONTRACTTENDERDESC_OPEN = CONTRACTTENDERTYPE_OPEN.split("-")[1];
        CONTRACTTENDERDESC_SUBMITTED = CONTRACTTENDERTYPE_SUBMITTED.split("-")[1];
        CONTRACTTENDERDESC_APPROVEDBYCM = CONTRACTTENDERTYPE_APPROVEDBYCM.split("-")[1];
        CONTRACTTENDERDESC_REJECTEDBYCM = CONTRACTTENDERTYPE_REJECTEDBYCM.split("-")[1];
        CONTRACTTENDERDESC_APPROVEDBYRM = CONTRACTTENDERTYPE_APPROVEDBYRM.split("-")[1];
        CONTRACTTENDERDESC_REJECTEDBYRM = CONTRACTTENDERTYPE_REJECTEDBYRM.split("-")[1];
        CONTRACTTENDERDESC_APPROVEDBYLM = CONTRACTTENDERTYPE_APPROVEDBYLM.split("-")[1];
        CONTRACTTENDERDESC_REJECTEDBYLM = CONTRACTTENDERTYPE_REJECTEDBYLM.split("-")[1];
        CONTRACTTENDERDESC_APPROVEDBYCFO = CONTRACTTENDERTYPE_APPROVEDBYCFO.split("-")[1];
        CONTRACTTENDERDESC_REJECTEDBYCFO = CONTRACTTENDERTYPE_REJECTEDBYCFO.split("-")[1];
        CONTRACTTENDERDESC_APPROVEDBYEVP = CONTRACTTENDERTYPE_APPROVEDBYEVP.split("-")[1];
        CONTRACTTENDERDESC_REJECTEDBYEVP = CONTRACTTENDERTYPE_REJECTEDBYEVP.split("-")[1];
        CONTRACTTENDERDESC_APPROVEDBYRFM = CONTRACTTENDERTYPE_APPROVEDBYRFM.split("-")[1];
        CONTRACTTENDERDESC_REJECTEDBYRFM = CONTRACTTENDERTYPE_REJECTEDBYRFM.split("-")[1];
    }

    private static void initContractType() {
        contractTypeHash.put(1, "Backlog Fixed Lumpsum (Contracted-committed)");
        contractTypeHash.put(2, "Backlog Project Contract - Single (Contracted-Committed)");
        contractTypeHash.put(3, "Orderbook MSA (Contracted not-committed)");
        contractTypeHash.put(4, "Others");
    }

    private static void initServiceLines() {
        serviceLineHash.put(1, "Asset Integrity & HSE");
        serviceLineHash.put(2, "Certification");
        serviceLineHash.put(3, "Vendor Surveillance");
        serviceLineHash.put(4, "Technical Staffing");
        serviceLineHash.put(5, "Training");
        serviceLineHash.put(6, "Premium Services");
        serviceLineHash.put(7, "NDT & Corrosion Monitoring");
        serviceLineHash.put(8, "Site Inspection");
        serviceLineHash.put(9, "Power");
        serviceLineHash.put(10, "Construction");
        serviceLineHash.put(11, "Others");
    }

    private static void initStatus() {
        statusHash.put(1, "Awaiting award");
        statusHash.put(2, "Awarded");
        statusHash.put(3, "Tender preparation");
    }

    private static void initDurationRange() {
        durationRangeHash.put(1, "Year/s");
        durationRangeHash.put(2, "Month/s");
        durationRangeHash.put(3, "Week/s");
        durationRangeHash.put(4, "Day/s");
    }

    private static void initCreditRisk() {
        creditRiskHash.put(1, "High");
        creditRiskHash.put(2, "Low");
        creditRiskHash.put(3, "Medium");
        creditRiskHash.put(4, "Don't know");
    }

    private static void initPaymentBehavior() {
        paymentBehavioeHash.put(1, "Good");
        paymentBehavioeHash.put(2, "Bad");
        paymentBehavioeHash.put(3, "Don't know");
    }

    public static String getContractType(int key) {
        return contractTypeHash.get(key);
    }

    public static String getServiceLine(int key) {
        return serviceLineHash.get(key);
    }

    public static String getStatus(int key) {
        return statusHash.get(key);
    }

    public static String getDurationRange(int key) {return durationRangeHash.get(key); }

    public static String getCreditRisk(int key) {
        return creditRiskHash.get(key);
    }

    public static String getPaymentBehavior(int key) {
        return paymentBehavioeHash.get(key);
    }

    public Contract(JSONObject jsonContract) throws Exception {
        additionalMembership = jsonContract.getString("AdditionalMembership");
        agentApproved = jsonContract.getInt("AgentApproved");
        agentCommission = jsonContract.getString("AgentCommission");
        agentName = jsonContract.getString("AgentName");
        isAgentRequired = jsonContract.getBoolean("AgentRequired");
        approverNote = jsonContract.getString("ApproverNote");
        bankPerformanceGuarantee = jsonContract.getString("BankPerformanceGuarantee");
        billingCurrency = jsonContract.getString("BillingCurrency");
        JSONArray jsonBusinessDoc = jsonContract.getJSONArray("BusinessCaseDocument");
        businessCaseDocument = new ArrayList<Document>();
        for (int i=0; i < jsonBusinessDoc.length(); i++)
            businessCaseDocument.add(new Document(jsonBusinessDoc.getJSONObject(i)));
        businessOrigDocName = jsonContract.getString("BusinessOrigDocName");
        cmEmail = jsonContract.getString("CMEmail");
        cmId = jsonContract.getInt("CMId");
        cmName = jsonContract.getString("CMName");
        capexRequired = jsonContract.getInt("CapexRequired");
        capexRequiredEquiv = jsonContract.getInt("CapexRequiredEquiv");
        certificatesValid = jsonContract.getString("CertificatesValid");
        cfoId = jsonContract.getInt("CfoID");
        cfoName = jsonContract.getString("CfoName");
        commercialTermsOther = jsonContract.getString("CommercialTermsOther");
        company = jsonContract.getString("Company");
        companyCurrency = jsonContract.getString("CompanyCurrency");
        competentPersonnel = jsonContract.getInt("CompetentPersonnel");
        competingGrounds = jsonContract.getString("CompetingGrounds");
        competingAdvantage = jsonContract.getString("CompetitiveAdvantage");
        competitorOffers = jsonContract.getString("CompetitorOffers");
        competitors = jsonContract.getString("Competitors");
        consequentialLoss = jsonContract.getString("ConsequentialLoss");
        JSONArray jsonContractDoc = jsonContract.getJSONArray("ContractDocument");
        contractDocument = new ArrayList<Document>();
        for (int i = 0; i < jsonContractDoc.length(); i++)
            contractDocument.add(new Document(jsonContractDoc.getJSONObject(i)));
        contractId = jsonContract.getInt("ContractId");
        contractType = jsonContract.getInt("ContractType");
        contractingStrategy = jsonContract.getString("ContractingStrategy");
        creditRisk = jsonContract.getInt("CreditRisk");
        dateAwardedLost = jsonContract.getString("DateAwardedLost");
        dateProcessedByCfo = jsonContract.getString("DateProcessedByCFO");
        dateProcessedByCm = jsonContract.getString("DateProcessedByCM");
        dateProcessedByEvp = jsonContract.getString("DateProcessedByEVP");
        dateProcessedByLm = jsonContract.getString("DateProcessedByLM");
        dateProcessedByRm = jsonContract.getString("DateProcessedByRM");
        dateProcessedByRfm = jsonContract.getString("DateProcessedByRFM");
        dealBreakers = jsonContract.getString("DealBreakers");
        endClient = jsonContract.getString("EndClient");
        equipment = jsonContract.getInt("Equipment");
        equipmentAvailable = jsonContract.getInt("EquipmentAvailable");
        equipmentCost = jsonContract.getInt("EquipmentCost");
        equipmentRequired = jsonContract.getString("EquipmentRequired");
        evpEmail = jsonContract.getString("EvpEmail");
        evpId = jsonContract.getInt("EvpID");
        evpName = jsonContract.getString("EvpName");
        existingOrNew = jsonContract.getString("ExistingOrNew");
        finalStatus = jsonContract.getInt("FinalStatus");
        grossMargin = (float) jsonContract.getDouble("GrossMargin");
        hsqe = jsonContract.getString("Hsqe");
        importExport = jsonContract.getString("ImportExport");
        infrastructure = jsonContract.getInt("Infrastructure");
        insurance = jsonContract.getString("Insurances");
        intellectualPropertyRights = jsonContract.getString("IntellectualPropertyRights");
        invoicing = jsonContract.getString("Invoicing");
        jobStaff = jsonContract.getString("JobStaff");
        justification = jsonContract.getString("Justification");
        lmEmail = jsonContract.getString("LMEmail");
        lmId = jsonContract.getInt("LMId");
        lmName = jsonContract.getString("LMName");
        legalOthers = jsonContract.getInt("LegalOthers");
        legalOthers2 = jsonContract.getString("LegalOthers2");
        limitationLiability = jsonContract.getString("LimitationLiability");
        liquidatedDamage = jsonContract.getString("LiquidatedDamages");
        mainOrigDocName = jsonContract.getString("MainOrigDocName");
        negotiations = jsonContract.getString("Negotiations");
        notes = jsonContract.getString("Notes");
        officeId = jsonContract.getInt("OfficeId");
        operational = jsonContract.getString("Operational");
        otherLocalTaxes = jsonContract.getString("OtherLocalTaxes");
        paymentBehavior = jsonContract.getInt("PaymentBehavior");
        paymentTerms = jsonContract.getString("PaymentTerms");
        permanentEstablishment = jsonContract.getString("PermanentEstablishment");
        personnel = jsonContract.getInt("Personnel");
        pricing = jsonContract.getString("Pricing");
        pricingAdditional = jsonContract.getString("PricingAdditional");
        primaryResponsibility = jsonContract.getString("PrimaryResponsibility");
        probabilityOfAward = jsonContract.getInt("ProbabilityOfAward");
        projectDuration = jsonContract.getInt("ProjectDuration");
        projectDurationRange = jsonContract.getInt("ProjectDurationRange");
        projectExcutionLocation = jsonContract.getString("ProjectExecutionLocation");
        projectManagerCriteria = jsonContract.getString("ProjectManagerCriteria");
        projectTeamBuildup = jsonContract.getString("ProjectTeamBuildUp");
        projectValue = jsonContract.getInt("ProjectValue");
        projectValueEquiv = jsonContract.getInt("ProjectValueEquiv");
        rfmId = jsonContract.getInt("RfmId");
        rmEmail = jsonContract.getString("RMEmail");
        rmId = jsonContract.getInt("RMId");
        rmName = jsonContract.getString("RMName");
        rateToEuro = jsonContract.getInt("RateToEUR");
        referenceNumber = jsonContract.getString("ReferenceNumber");
        requestDate = jsonContract.getString("RequestDate");
        requestStatus = jsonContract.getInt("RequestStatus");
        requestStatusName = jsonContract.getString("RequestStatusName");
        requestor = jsonContract.getString("Requestor");
        rfmEmail = jsonContract.getString("RfmEmail");
        rfmName = jsonContract.getString("RfmName");
        riskAssessmentOthers = jsonContract.getString("RiskAssessmentOthers");
        serviceLines = jsonContract.getInt("ServiceLines");
        services = jsonContract.getInt("Services");
        specificTraining = jsonContract.getString("SpecificTraining");
        staffAware = jsonContract.getString("StaffAware");
        staffId = jsonContract.getInt("StaffId");
        dateStartDate = jsonContract.getString("StartDate");
        status = jsonContract.getInt("Status");
        statusName = jsonContract.getString("StatusName");
        technicalAnalysisOthers = jsonContract.getString("TechnicalAnalysisOthers");
        dateTenderSubmission = jsonContract.getString("TenderSubmission");
        visa = jsonContract.getString("Visa");
    }

    public void setRequestStatusName(String statusName){
        requestStatusName = statusName;
    }

    public String getAdditionalMembership() {
        return additionalMembership;
    }

    public int getAgentApproved() {
        return agentApproved;
    }

    public String getAgentCommission() {
        return agentCommission;
    }

    public String getAgentName() {
        return agentName;
    }

    public boolean isAgentRequired() {
        return isAgentRequired;
    }

    public String getBankPerformanceGuarantee() {
        return bankPerformanceGuarantee;
    }

    public String getBillingCurrency() {
        return billingCurrency;
    }

    public String getDateStartDate(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDate(dateStartDate);
    }

    public String getDateTenderSubmission(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDate(dateTenderSubmission);
    }

    public String getDateSubmitted(SaltApplication app) throws Exception{
        return app.onlineGateway.dJsonizeDate(requestDate);
    }

    public String getDateAwardedLost(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDate(dateAwardedLost);
    }

    public String getDateProcessedByCm(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDateForApproval(dateProcessedByCm);
    }

    public String getDateProcessedByRfm(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDateForApproval(dateProcessedByRfm);
    }

    public String getDateProcessedByRm(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDateForApproval(dateProcessedByRm);
    }

    public String getDateProcessedByLm(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDateForApproval(dateProcessedByLm);
    }

    public String getDateProcessedByCfo(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDateForApproval(dateProcessedByCfo);
    }

    public String getDateProcessedByEvp(SaltApplication app) throws Exception {
        return app.onlineGateway.dJsonizeDateForApproval(dateProcessedByEvp);
    }

    public List<Document> getBusinessCaseDocument() {
        return businessCaseDocument;
    }

    public String getCmName() {
        return cmName;
    }

    public int getCapexRequired() {
        return capexRequired;
    }

    public int getCapexRequiredEquiv() {
        return capexRequiredEquiv;
    }

    public String getCertificatesValid() {
        return certificatesValid;
    }

    public String getCfoName() {
        return cfoName;
    }

    public String getCommercialTermsOther() {
        return commercialTermsOther;
    }

    public String getCompanyCurrency() {
        return companyCurrency;
    }

    public int getCompetentPersonnel() {
        return competentPersonnel;
    }

    public String getCompetingGrounds() {
        return competingGrounds;
    }

    public String getCompetingAdvantage() {
        return competingAdvantage;
    }

    public String getCompetitorOffers() {
        return competitorOffers;
    }

    public String getCompetitors() {
        return competitors;
    }

    public String getConsequentialLoss() {
        return consequentialLoss;
    }

    public List<Document> getContractDocument() {
        return contractDocument;
    }

    public int getContractType() {
        return contractType;
    }

    public String getContractingStrategy() {
        return contractingStrategy;
    }

    public int getCreditRisk() {
        return creditRisk;
    }

    public String getDealBreakers() {
        return dealBreakers;
    }

    public String getEndClient() {
        return endClient;
    }

    public int getEquipment() {
        return equipment;
    }

    public int getEquipmentAvailable() {
        return equipmentAvailable;
    }

    public int getEquipmentCost() {
        return equipmentCost;
    }

    public String getEquipmentRequired() {
        return equipmentRequired;
    }

    public String getEvpName() {
        return evpName;
    }

    public String getExistingOrNew() {
        return existingOrNew;
    }

    public float getGrossMargin() {
        return grossMargin;
    }

    public String getHsqe() {
        return hsqe;
    }

    public String getImportExport() {
        return importExport;
    }

    public int getInfrastructure() {
        return infrastructure;
    }

    public String getInsurance() {
        return insurance;
    }

    public String getIntellectualPropertyRights() {
        return intellectualPropertyRights;
    }

    public String getInvoicing() {
        return invoicing;
    }

    public String getJobStaff() {
        return jobStaff;
    }

    public String getJustification() {
        return justification;
    }

    public int getLegalOthers() {
        return legalOthers;
    }

    public String getLegalOthers2() { return legalOthers2; }

    public String getLmName() {
        return lmName;
    }

    public String getLimitationLiability() {
        return limitationLiability;
    }

    public String getLiquidatedDamage() {
        return liquidatedDamage;
    }

    public String getMainOrigDocName() {
        return mainOrigDocName;
    }

    public String getNegotiations() {
        return negotiations;
    }

    public String getNotes() {
        return notes;
    }

    public String getOperational() {
        return operational;
    }

    public String getOtherLocalTaxes() {
        return otherLocalTaxes;
    }

    public int getPaymentBehavior() {
        return paymentBehavior;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public String getPermanentEstablishment() {
        return permanentEstablishment;
    }

    public int getPersonnel() {
        return personnel;
    }

    public String getPricing() {
        return pricing;
    }

    public String getPricingAdditional() {
        return pricingAdditional;
    }

    public String getPrimaryResponsibility() {
        return primaryResponsibility;
    }

    public int getProbabilityOfAward() {
        return probabilityOfAward;
    }

    public int getProjectDuration() {
        return projectDuration;
    }

    public int getProjectDurationRange() {
        return projectDurationRange;
    }

    public String getProjectExcutionLocation() {
        return projectExcutionLocation;
    }

    public String getProjectManagerCriteria() {
        return projectManagerCriteria;
    }

    public String getProjectTeamBuildup() {
        return projectTeamBuildup;
    }

    public int getProjectValue() {
        return projectValue;
    }

    public int getRfmId() {
        return rfmId;
    }

    public String getRmName() {
        return rmName;
    }

    public float getRateToEuro() {
        return rateToEuro;
    }

    public String getRiskAssessmentOthers() {
        return riskAssessmentOthers;
    }

    public int getServiceLines() {
        return serviceLines;
    }

    public int getServices() {
        return services;
    }

    public String getSpecificTraining() {
        return specificTraining;
    }

    public String getStaffAware() {
        return staffAware;
    }

    public int getStatus() {
        return status;
    }

    public String getTechnicalAnalysisOthers() {
        return technicalAnalysisOthers;
    }

    public String getVisa() {
        return visa;
    }

    public String getApproverNote() {
        return approverNote;
    }

    public int getRequestStatusId() {
        return requestStatus;
    }

    public String getRequestorName() {
        return requestor;
    }

    public String getRfmName() {
        return rfmName;
    }

    public String getOfficeName() {
        return company;
    }

    public String getContractNumber() {
        return referenceNumber;
    }

    public int getContractId() {
        return contractId;
    }

    public int getOfficeId() {
        return officeId;
    }

    public int getProjectValueEquiv() {
        return projectValueEquiv;
    }

    public static String getStatusDescForKey(int key) {
        switch (key) {
            case 71: return CONTRACTTENDERDESC_SUBMITTED;
            case 72: return CONTRACTTENDERDESC_APPROVEDBYCM;
            case 73: return CONTRACTTENDERDESC_REJECTEDBYCM;
            case 74: return CONTRACTTENDERDESC_APPROVEDBYRM;
            case 75: return CONTRACTTENDERDESC_REJECTEDBYRM;
            case 76: return CONTRACTTENDERDESC_APPROVEDBYLM;
            case 77: return CONTRACTTENDERDESC_REJECTEDBYLM;
            case 78: return CONTRACTTENDERDESC_APPROVEDBYCFO;
            case 79: return CONTRACTTENDERDESC_REJECTEDBYCFO;
            case 80: return CONTRACTTENDERDESC_APPROVEDBYEVP;
            case 81: return CONTRACTTENDERDESC_REJECTEDBYEVP;
            case 146: return CONTRACTTENDERDESC_APPROVEDBYRFM;
            case 147: return CONTRACTTENDERDESC_REJECTEDBYRFM;
            default: return CONTRACTTENDERDESC_OPEN;
        }
    }

    public String getJSONFromUpadatingContract(int requestStatusId, String keyForUpdatableDate, String approverNote, SaltApplication app) {
        Map<String, Object> tempMap = new HashMap<String, Object>();
        tempMap.put("AdditionalMembership", additionalMembership);
        tempMap.put("AgentApproved", agentApproved);
        tempMap.put("AgentCommission", agentCommission);
        tempMap.put("AgentName", agentName);
        tempMap.put("AgentRequired", isAgentRequired);
        tempMap.put("ApproverNote", approverNote);
        tempMap.put("BankPerformanceGuarantee", bankPerformanceGuarantee);
        tempMap.put("BillingCurrency", billingCurrency);
        List<JsonObject> businessDoc = new ArrayList<JsonObject>();
        for(Document document : businessCaseDocument)
            businessDoc.add(document.getJSONObject());
        tempMap.put("BusinessCaseDocument", businessDoc);
        tempMap.put("BusinessOrigDocName", businessOrigDocName);
        tempMap.put("CMEmail", cmEmail);
        tempMap.put("CMId", cmId);
        tempMap.put("CMName", cmName);
        tempMap.put("CapexRequired", capexRequired);
        tempMap.put("CapexRequiredEquiv", capexRequiredEquiv);
        tempMap.put("CertificatesValid", certificatesValid);
        tempMap.put("CfoID", cfoId);
        tempMap.put("CfoName", cfoName);
        tempMap.put("CommercialTermsOther", commercialTermsOther);
        tempMap.put("Company", company);
        tempMap.put("CompanyCurrency", companyCurrency);
        tempMap.put("CompetentPersonnel", competentPersonnel);
        tempMap.put("CompetingGrounds", competingGrounds);
        tempMap.put("CompetitiveAdvantage", competingAdvantage);
        tempMap.put("CompetitorOffers", competitorOffers);
        tempMap.put("Competitors", competitors);
        tempMap.put("ConsequentialLoss", consequentialLoss);
        List<JsonObject> contractDoc = new ArrayList<JsonObject>();
        for(Document document : contractDocument)
            contractDoc.add(document.getJSONObject());
        tempMap.put("ContractDocument", contractDoc);
        tempMap.put("ContractId", contractId);
        tempMap.put("ContractType", contractType);
        tempMap.put("ContractingStrategy", contractingStrategy);
        tempMap.put("CreditRisk", creditRisk);
        tempMap.put("DateAwardedLost", dateAwardedLost);
        tempMap.put("DateProcessedByCFO", dateProcessedByCfo);
        tempMap.put("DateProcessedByCM", dateProcessedByCm);
        tempMap.put("DateProcessedByEVP", dateProcessedByEvp);
        tempMap.put("DateProcessedByLM", dateProcessedByLm);
        tempMap.put("DateProcessedByRM", dateProcessedByRm);
        tempMap.put("DateProcessedByRFM", dateProcessedByRfm);
        tempMap.put("DealBreakers", dealBreakers);
        tempMap.put("EndClient", endClient);
        tempMap.put("Equipment", equipment);
        tempMap.put("EquipmentAvailable", equipmentAvailable);
        tempMap.put("EquipmentCost", equipmentCost);
        tempMap.put("EquipmentRequired", equipmentRequired);
        tempMap.put("EvpEmail", evpEmail);
        tempMap.put("EvpID", evpId);
        tempMap.put("EvpName", evpName);
        tempMap.put("ExistingOrNew", existingOrNew);
        tempMap.put("FinalStatus", finalStatus);
        tempMap.put("GrossMargin", grossMargin);
        tempMap.put("Hsqe", hsqe);
        tempMap.put("ImportExport", importExport);
        tempMap.put("Infrastructure", infrastructure);
        tempMap.put("Insurances", insurance);
        tempMap.put("IntellectualPropertyRights", intellectualPropertyRights);
        tempMap.put("Invoicing", invoicing);
        tempMap.put("JobStaff", jobStaff);
        tempMap.put("Justification", justification);
        tempMap.put("LMEmail", lmEmail);
        tempMap.put("LMId", lmId);
        tempMap.put("LMName", lmEmail);
        tempMap.put("LegalOthers", legalOthers);
        tempMap.put("LegalOthers2", legalOthers2);
        tempMap.put("LimitationLiability", limitationLiability);
        tempMap.put("LiquidatedDamages", liquidatedDamage);
        tempMap.put("MainOrigDocName", mainOrigDocName);
        tempMap.put("Negotiations", negotiations);
        tempMap.put("Notes", notes);
        tempMap.put("OfficeId", officeId);
        tempMap.put("Operational", operational);
        tempMap.put("OtherLocalTaxes", otherLocalTaxes);
        tempMap.put("PaymentBehavior", paymentBehavior);
        tempMap.put("PaymentTerms", paymentTerms);
        tempMap.put("PermanentEstablishment", permanentEstablishment);
        tempMap.put("Personnel", personnel);
        tempMap.put("Pricing", pricing);
        tempMap.put("PricingAdditional", pricingAdditional);
        tempMap.put("PrimaryResponsibility", primaryResponsibility);
        tempMap.put("ProbabilityOfAward", probabilityOfAward);
        tempMap.put("ProjectDuration", projectDuration);
        tempMap.put("ProjectDurationRange", projectDurationRange);
        tempMap.put("ProjectExecutionLocation",projectExcutionLocation);
        tempMap.put("ProjectManagerCriteria", projectManagerCriteria);
        tempMap.put("ProjectTeamBuildUp", projectTeamBuildup);
        tempMap.put("ProjectValue", projectValue);
        tempMap.put("ProjectValueEquiv", projectValueEquiv);
        tempMap.put("RMEmail", rmEmail);
        tempMap.put("RMId", rmId);
        tempMap.put("RMName", rmName);
        tempMap.put("RateToEUR", rateToEuro);
        tempMap.put("ReferenceNumber", referenceNumber);
        tempMap.put("RequestDate", requestDate);
        tempMap.put("RequestStatus", requestStatusId);
        tempMap.put("RequestStatusName", requestStatusName);
        tempMap.put("Requestor", requestor);
        tempMap.put("RfmEmail", rfmEmail);
        tempMap.put("RfmName", rfmName);
        tempMap.put("RiskAssessmentOthers", riskAssessmentOthers);
        tempMap.put("ServiceLines", serviceLines);
        tempMap.put("Services", services);
        tempMap.put("SpecificTraining", specificTraining);
        tempMap.put("StaffAware", staffAware);
        tempMap.put("StaffId", staffId);
        tempMap.put("StartDate", dateStartDate);
        tempMap.put("Status", status);
        tempMap.put("StatusName", statusName);
        tempMap.put("TechnicalAnalysisOthers", technicalAnalysisOthers);
        tempMap.put("TenderSubmission", dateTenderSubmission);
        tempMap.put("Visa", visa);
        if (!keyForUpdatableDate.equals("NA"))
            tempMap.put(keyForUpdatableDate, app.onlineGateway.epochizeDate(new Date()));
        return app.gson.toJson(tempMap, app.types.hashmapOfStringObject);
    }

}
