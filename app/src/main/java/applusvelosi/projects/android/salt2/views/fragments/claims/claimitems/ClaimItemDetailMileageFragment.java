package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.ClaimItemAttendee;
import applusvelosi.projects.android.salt2.models.claimitems.MilageClaimItem;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs.ClaimItemDetailFragment;

public class ClaimItemDetailMileageFragment extends ClaimItemDetailFragment {
	private LinearLayout containersAttendees;
	MilageClaimItem claimItemMileage;

    public static ClaimItemDetailMileageFragment newInstance(int pos){
        ClaimItemDetailMileageFragment frag = new ClaimItemDetailMileageFragment();
        Bundle b = new Bundle();
        b.putInt(KEY_ITEMPOS, pos);
        frag.setArguments(b);

        return frag;
    }

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		try {
			claimItemMileage = (MilageClaimItem) claimItem;
		}catch(Exception e){
			claimItemMileage = new MilageClaimItem(claimItem);
		}
		View view = inflater.inflate(R.layout.fragment_claimitem_catmileage_details, null);

		((TextView)view.findViewById(R.id.tviews_claimitemdetail_id)).setText(claimItemMileage.getItemNumber());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_category)).setText(claimItemMileage.getCategoryName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_expenseDate)).setText(claimItemMileage.getExpenseDate(app));
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_amount)).setText(SaltApplication.decimalFormat.format(claimItemMileage.getForeignAmount())+" "+claimItemMileage.getForeignCurrencyName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_amountLocal)).setText(SaltApplication.decimalFormat.format(claimItemMileage.getLocalAmount()) + " " + claimItemMileage.getLocalCurrencyName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_exRate)).setText(String.valueOf(claimItemMileage.getStandardExchangeRate()));
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_desc)).setText(claimItemMileage.getDescription());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_project)).setText((app.getStaffOffice().hasUsesSap() && app.getStaffOffice().hasUsesSapService())?claimItemMileage.getWbsCode():claimItemMileage.getProjectName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_taxRate)).setText(claimItemMileage.isTaxApplied()?String.valueOf(claimItemMileage.getTaxAmount()):"No");
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_mileagefrom)).setText(claimItemMileage.getMileageFrom());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_mileageto)).setText(claimItemMileage.getMileageTo());
//		((CheckBox)view.findViewById(R.id.cboxs_claimitemdetail_ismileagereturn)).setChecked(claimItemMileage.isMileageReturn());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_mileagemileage)).setText(String.valueOf(claimItemMileage.getMileage()));
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_mileagetype)).setText((claimItemMileage.getMilageTypeID()==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER)?MilageClaimItem.MILEAGETYPE_VAL_KILOMETER:MilageClaimItem.MILEAGETYPE_VAL_MILE);
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_mileagerate)).setText(String.valueOf(claimItemMileage.getMilageRate()));
		containersAttendees = (LinearLayout)view.findViewById(R.id.containers_claimitemdetail_attendees);
		if(claimItemMileage.isBillable()){
			((TextView)view.findViewById(R.id.tviews_claimitemdetail_billto)).setText(claimItemMileage.getBillableCompanyName());
			((TextView)view.findViewById(R.id.tviews_claimitemdetail_notesorbillstoclient)).setText(claimItemMileage.getNotes());
		}else{
			((TextView)view.findViewById(R.id.tviews_claimitemdetail_billto)).setText("No");
			view.findViewById(R.id.trs_claimitemdetail_notestoclient).setVisibility(View.GONE);
		}
		hasYesNoDocFlag = (TextView)view.findViewById(R.id.tviews_claimitemdetail_hasAttachmentFlag);
		receiptDoc = (TextView)view.findViewById(R.id.tviews_claimitemdetail_attachment);
		if(claimItemMileage.hasReceipt()){
			receiptDoc.setText(claimItemMileage.getAttachmentName());
			receiptDoc.setTextColor(activity.getResources().getColor(R.color.orange_velosi));
			receiptDoc.setOnClickListener(this);
		}else{
			receiptDoc.setText("No Attachment");
		}
		if(claimItem.getHasAttachmentFlag() == 1)
			receiptDoc.setContentDescription("Yes");
		else if(claimItem.getHasAttachmentFlag() == 2)
			receiptDoc.setContentDescription("No");
		else
			receiptDoc.setContentDescription("N/A");

		for(ClaimItemAttendee attendee :claimItem.getAttendees()){
			LinearLayout v = (LinearLayout)inflater.inflate(R.layout.node_claimitemdetail_attendee, null);
			((TextView)v.findViewById(R.id.tviews_nodes_claimitemdetail_attendee)).setText(attendee.getName());
			((TextView)v.findViewById(R.id.tviews_nodes_claimitemdetail_jobtitle)).setText(attendee.getJobTitle());
			((TextView)v.findViewById(R.id.tviews_nodes_claimitemdetail_notes)).setText(attendee.getNote());
			containersAttendees.addView(v);
		}

		return view;
	}

}
