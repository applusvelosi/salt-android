package applusvelosi.projects.android.salt2.utils.threads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.utils.interfaces.LoaderStatusInterface;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;

/**
 * Created by Velosi on 11/10/2016.
 */
public class ClaimProjectLoader implements Runnable {
    SaltApplication app;
    ClaimDetailActivity activity;
    LoaderStatusInterface projectLoader;

    public ClaimProjectLoader(SaltApplication app, Context context, LoaderStatusInterface inf){
        this.app = app;
        activity = (ClaimDetailActivity)context;
        projectLoader = inf;
    }

    @Override
    public void run() {
        Object tempResult;
        try{
            tempResult = app.onlineGateway.getClaimItemProjectsByCostCenter(activity.claimHeader.getCostCenterID());
        }catch(Exception e){
            e.printStackTrace();
            tempResult = e.getMessage();
        }

        final Object result = tempResult;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (result instanceof String)
                    projectLoader.onLoadFailed(result.toString());
                else
                    projectLoader.onLoadSuccess(result);
            }
        });

    }
}
