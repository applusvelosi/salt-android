package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.SapTaxCodeSettings;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.Project;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.utils.interfaces.CameraCaptureInterface;
import applusvelosi.projects.android.salt2.utils.interfaces.FileSelectionInterface;
import applusvelosi.projects.android.salt2.views.ManageClaimItemActivity;
import applusvelosi.projects.android.salt2.views.dialogs.DialogClaimItemAttendeeList;
import applusvelosi.projects.android.salt2.views.dialogs.DialogItemChargeToList;
import applusvelosi.projects.android.salt2.views.dialogs.DialogItemProjects;
import applusvelosi.projects.android.salt2.views.dialogs.DialogItemSapTaxCodeList;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 12/14/etextBeforeTaxAmt15.
 */
public abstract class ItemInputFragment extends LinearNavActionbarFragment implements DialogItemSapTaxCodeList.DialogClaimItemSapTaxCodeInterface, CompoundButton.OnCheckedChangeListener, TextWatcher, CameraCaptureInterface, FileSelectionInterface, DatePickerDialog.OnDateSetListener, FileManager.AttachmentDownloadListener, DialogItemProjects.DialogClaimItemProjectInterface {
    ManageClaimItemActivity activity;

    protected TextView actionbarTitle, actionbarDone;
    private RelativeLayout actionbarButtonBack;

    protected EditText etextAmount, etextLocalAmount, etextForex, etextTax, etextDesc, etextBeforeTaxAmt;
    protected TextView tviewsDate, tvCurrLocal, tvAttachment, tvBeforeTaxCurr;
    protected CheckBox cboxTaxable, cboxBillable;
    protected ImageView buttonFile, buttonProjects;
    protected TableRow trBillNotes;
    public TextView tvBillTo, tvProject;
    protected TextView etextBillNotes;
    protected RelativeLayout containerAttendees;
    public TextView tvAttendees;

    protected LinearLayout sapTaxContainer;
    protected TextView tvSapTaxCode;
    protected EditText etextSapTaxValue;
    protected TextView tvSapTaxDescription;
    protected boolean isSapTax;


    protected abstract View getInflatedLayout(LayoutInflater inflater);
    protected abstract void initAdditionalViewComponents(View v);
    protected abstract void saveToServer();

    private DatePickerDialog datePicker;
    protected DialogItemProjects dialogProjects;
    private DialogItemChargeToList dialogChargeTo;
    private DialogClaimItemAttendeeList dialogAttendees;
    private DialogItemSapTaxCodeList dialogSapTax;


    protected File attachedFile;
    protected float forex;
    private Calendar currCalendar;
    private AlertDialog dialogAttachment;
    private String userChoosenTask;

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (ManageClaimItemActivity)getActivity();

        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backdone, null);
        actionbarButtonBack = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionbarDone = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_done);
        actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        actionbarTitle.setText(activity.claimItem.getCategoryName());
        actionbarDone.setText("Next");

        actionbarButtonBack.setOnClickListener(this);
        actionbarTitle.setOnClickListener(this);
        actionbarDone.setOnClickListener(this);

        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = getInflatedLayout(inflater);

        tviewsDate = (TextView)v.findViewById(R.id.tviews_claimiteminput_expenseDate);
        etextAmount = (EditText)v.findViewById(R.id.etexts_claimiteminput_fc_amount);
        etextLocalAmount = (EditText)v.findViewById(R.id.etexts_claimiteminput_lc_amount);
        tvCurrLocal = (TextView)v.findViewById(R.id.tviews_claimiteminput_lc_curr);
        tvBeforeTaxCurr = (TextView)v.findViewById(R.id.tviews_claimiteminput_lc_beforetax);
        etextBeforeTaxAmt = (EditText)v.findViewById(R.id.etexts_claimiteminput_lc_beforetaxamount);

        etextForex = (EditText)v.findViewById(R.id.etexts_claimiteminput_forex);
        etextTax = (EditText)v.findViewById(R.id.etexts_claimiteminput_tax);
        cboxTaxable = (CheckBox)v.findViewById(R.id.cboxs_claimiteminput_tax);

        tvAttachment = (TextView)v.findViewById(R.id.tviews_claimiteminput_attachment);
//        buttonCamera = (ImageView)v.findViewById(R.id.buttons_claimiteminput_camera);etextTax
        buttonFile = (ImageView)v.findViewById(R.id.buttons_claimiteminput_files);
        buttonProjects = (ImageView)v.findViewById(R.id.buttons_claimiteminput_project);

        tvProject = (TextView)v.findViewById(R.id.tviews_claimiteminput_project);
        tvBillTo = (TextView)v.findViewById(R.id.tviews_claimiteminput_billTo);
        cboxBillable = (CheckBox)v.findViewById(R.id.cboxs_claimiteminput_billable);
        trBillNotes = (TableRow)v.findViewById(R.id.trs_claimiteminput_clienttobill);
        etextBillNotes = (EditText)v.findViewById(R.id.etexts_claimiteminput_clienttobill);
        containerAttendees =  (RelativeLayout)v.findViewById(R.id.containers_claimiteminput_attendees);
        tvAttendees = (TextView)v.findViewById(R.id.tviews_claimiteminput_attendees);

        sapTaxContainer = (LinearLayout)v.findViewById(R.id.container_claimiteminput_saptax);
        tvSapTaxCode = (TextView)v.findViewById(R.id.tviews_claimiteminput_saptaxcode);
        etextSapTaxValue = (EditText)v.findViewById(R.id.etext_claimiteminput_saptaxvalue);
        tvSapTaxDescription = (TextView)v.findViewById(R.id.tviews_claimiteminput_saptaxdescription);
        etextDesc = (EditText)v.findViewById(R.id.etexts_claimiteminput_desc);

        if(!activity.claimItem.getExpenseDate(app).contains("-1900"))
            tviewsDate.setText(activity.claimItem.getExpenseDate(app));
        tvBillTo.setOnClickListener(this);
        cboxBillable.setOnCheckedChangeListener(this);
        containerAttendees.setOnClickListener(this);
        etextDesc.addTextChangedListener(this);

        etextDesc.setText(activity.claimItem.getDescription());
//        etextAmount.setText(String.valueOf(activity.claimItem.getForeignAmount()));

        tviewsDate.setOnClickListener(this);
        etextAmount.addTextChangedListener(this);
        etextForex.addTextChangedListener(this);
        etextBillNotes.addTextChangedListener(this);
        buttonFile.setOnClickListener(this);

        isSapTax = false;
        if(app.getStaffOffice().hasUsesSap() && app.getStaffOffice().hasUsesSapService()) {
            if (app.getStaff().getVendorCode() != null || app.getStaffOffice().isInternalOrderRequired() && app.getStaff().getVendorCode() != null && app.getStaff().getInternalCode() != null){
                new Thread(new UpdateSapTax()).start();
                isSapTax = true;
                etextTax.setVisibility(View.GONE);
                cboxTaxable.setVisibility(View.GONE);
                sapTaxContainer.setVisibility(View.VISIBLE);
                sapTaxContainer.setOnClickListener(this);
                etextSapTaxValue.addTextChangedListener(this);

            }else{
                app.showMessageDialog(activity,"Sorry you cannot use this since you do not have vendor code or internal code. Kindly contact account manager for assistance.");
            }
            tvProject.setText("");
//            tvProject.setOnClickListener(null);
        }else{
            tvProject.setOnClickListener(this);
            buttonProjects.setBackgroundResource(R.drawable.bg_button_disable);
            dialogProjects = new DialogItemProjects(activity,this);
            if (!app.getStaffOffice().hasUsesSapService()) {
                etextTax.addTextChangedListener(this);
                cboxTaxable.setOnCheckedChangeListener(this);
                if(activity.claimItem.isTaxApplied()){
                    cboxTaxable.setChecked(true);
                    etextTax.setText(String.valueOf(activity.claimItem.getTaxAmount()));
                }
            }
        }

        if (activity.claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES) {
            cboxTaxable.setEnabled(false);
            sapTaxContainer.setEnabled(false);
            tvSapTaxDescription.setHint("N/A");
        }else
            etextAmount.setFilters(new InputFilter[] {new DigitsInputFilter(12,2)});

        if(activity.claimItem.getCategoryTypeID() == Category.TYPE_BUSINESSADVANCE)
            etextAmount.setHint("-0");
        else
            etextAmount.setHint("0.00");


        if(app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
            forex = 1;
            etextForex.setText(String.valueOf("1.00"));
        }else{
            activity.startLoading();
            new ForexGetter().start();
            tvSapTaxDescription.setHint("N/A");
            cboxTaxable.setEnabled(false);
        }

        tvCurrLocal.setText(app.getStaffOffice().getBaseCurrencyThree());
        tvBeforeTaxCurr.setText(app.getStaffOffice().getBaseCurrencyThree());
        activity.claimItem.setLocalCurrency(new Currency(app.getStaffOffice().getBaseCurrencyID(), app.getStaffOffice().getBaseCurrencyName(), app.getStaffOffice().getBaseCurrencyThree()));
        currCalendar = Calendar.getInstance();
        datePicker = new DatePickerDialog(linearNavFragmentActivity, this, currCalendar.get(Calendar.YEAR), currCalendar.get(Calendar.MONTH), currCalendar.get(Calendar.DAY_OF_MONTH));

        dialogChargeTo = new DialogItemChargeToList(this);
        dialogAttendees = new DialogClaimItemAttendeeList(this);
        if(activity.getAttachment() != null)
            updateAttachment(activity.getAttachment());

//        activity.setCameraListener(this);
//        activity.setFileSelectionListener(this);

//        tvProject.setText(activity.claimItem.getProjectName());
        ((TextView)v.findViewById(R.id.tviews_claimiteminput_fc_curr)).setText(activity.claimItem.getForeignCurrencyName());

        if(activity.claimItem.isBillable()){
            cboxBillable.setChecked(true);
            tvBillTo.setText(activity.claimItem.getBillableCompanyName());
            etextBillNotes.setText(activity.claimItem.getNotes());
        }

        initAdditionalViewComponents(v);

        final CharSequence[] items = { "Take A Photo", "Choose From Library",
                "Cancel" };

        dialogAttachment = new AlertDialog.Builder(activity).setTitle("Add Attachment!").setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= FileManager.checkPermission(activity);
                if (items[item].equals("Take A Photo")) {
                    userChoosenTask ="Take A Photo";
                    if(result)
                        initCameraIntent();
                } else if (items[item].equals("Choose From Library")) {
                    userChoosenTask ="Choose From Library";
                    if(result)
                        initGalleryImageIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        }).create();

        return v;
    }

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, SaltApplication.RESULT_CAMERA);
    }

    private void initGalleryImageIntent() {
        if(Build.VERSION.SDK_INT < 19){
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*,application/pdf");
            startActivityForResult(Intent.createChooser(intent,"Select File"), SaltApplication.RESULT_BROWSEFILES);
        }else{
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String mimeTypes[] = {"image/*","application/pdf"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
            startActivityForResult(intent,SaltApplication.RESULT_BROWSEFILES_KITKAT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case FileManager.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take A Photo"))
                        initCameraIntent();
                    else if(userChoosenTask.equals("Choose From Library"))
                        initGalleryImageIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            String filePath;
            if (resultCode != Activity.RESULT_OK)
                return;
            if (null == data)
                return;
            switch (requestCode) {
                case SaltApplication.RESULT_CAMERA:
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.getFilePathFromUri_BelowKitKat(linearNavFragmentActivity, data.getData());
                        System.out.println("CAMERA FILE PATH: " + filePath);
                        onCameraCaptureSuccess(data, filePath);
                    }else {
                        attachedFile = null;
                        onCameraCaptureFailed();
                    }
                    break;
                case SaltApplication.RESULT_BROWSEFILES:
                    if(resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.uriToFilename(linearNavFragmentActivity, data.getData());
                        onFileSelectionSuccess(filePath);
                    }else {
                        attachedFile = null;
                        onFileSelectionFailed();
                    }
                    break;
                case SaltApplication.RESULT_BROWSEFILES_KITKAT: //for KitKat version and above
                    if(resultCode == Activity.RESULT_OK) {
                        filePath = FileManager.uriToFilename(linearNavFragmentActivity, data.getData());
                        onFileSelectionSuccess(filePath);
                    }else {
                        attachedFile = null;
                        onFileSelectionFailed();
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraCaptureSuccess(Intent data, String filePath) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        tvAttachment.setText(destination.getName());
        updateAttachment(destination);
    }

    @Override
    public void onCameraCaptureFailed() {
        updateAttachment(null);
    }

    @Override
    public void onFileSelectionSuccess(String filePath) {
        attachedFile = new File(filePath);
        tvAttachment.setText(attachedFile.getName());
        updateAttachment(attachedFile);
    }

    @Override
    public void onFileSelectionFailed() {
        updateAttachment(null);
    }

    private class UpdateSapTax implements Runnable{
        @Override
        public void run() {
            Object tempResult;
            try {
                tempResult = app.onlineGateway.getAllSapTaxCodeSettingsByOffice();
            } catch (Exception e) {
                e.printStackTrace();
                tempResult = e.getMessage();
            }
            final Object result = tempResult;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (result instanceof String)
                        activity.finishLoading(result.toString());
                    else {
                        activity.updateSapTaxCodeList((List<SapTaxCodeSettings>) result);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (v == actionbarButtonBack || v == actionbarTitle)
            linearNavFragmentActivity.onBackPressed();
        else if(v == sapTaxContainer) {
            try {
                if (app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                    dialogSapTax = new DialogItemSapTaxCodeList(activity, this);
                    dialogSapTax.show();
                } else {
                    sapTaxContainer.setEnabled(false);
                    tvSapTaxDescription.setText("N/A");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (v == tviewsDate)
            datePicker.show();
        else if(v == containerAttendees)
            dialogAttendees.show();
        else if(v == tvBillTo)
            dialogChargeTo.show();
        else if(v == tvProject)
            dialogProjects.show();
        else if(v == buttonFile) {
            resetAttachment();
            dialogAttachment.show();
        }else if(v == tvAttachment) {
            activity.startLoading();
            if(attachedFile == null) {
                try{
                    Document document = activity.claimItem.getAttachments().get(0);
                    app.fileManager.downloadDocument(app, document.getDocID(), document.getRefID(), document.getObjectTypeID(), document.getDocName(), this);
                }catch(Exception e){
                    activity.finishLoading();
                    e.printStackTrace();
                    Toast.makeText(activity, "Cannot download document "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }else if(v == actionbarDone)
            saveToServer();
    }

    @Override
    public void onSapTaxCodeSelected(SapTaxCodeSettings sapTaxCodeSettings) {
        try {
            etextSapTaxValue.setEnabled(false);
            tvSapTaxCode.setText(sapTaxCodeSettings.getSapCode());
            etextSapTaxValue.setText(SaltApplication.sapTaxDecimalFormat.format(Float.parseFloat(sapTaxCodeSettings.getValue()) / 100));
            tvSapTaxDescription.setText(sapTaxCodeSettings.getDescription());
//        activity.claimItem.setSapTaxCode(sapTaxCodeSettings.getSapCode());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        if(v == cboxTaxable){
            if(isChecked){
                activity.claimItem.setIsTaxRated(true);
                etextTax.setEnabled(true);
                etextTax.setText(String.valueOf(app.getStaffOffice().getDefaultTax()/100));
            }else{
                activity.claimItem.setIsTaxRated(false);
                etextTax.setEnabled(false);
                etextTax.setText("(Tax Not Applicable)");
            }
        }else if(v == cboxBillable){
            activity.claimItem.setIsRechargable(isChecked);
            tvBillTo.setEnabled(isChecked);
            if(isChecked){
                trBillNotes.setVisibility(View.VISIBLE);
                tvBillTo.setOnClickListener(this);
            }else{
                trBillNotes.setVisibility(View.GONE);
                tvBillTo.setOnClickListener(null);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TO BE OVERRIDED BY SUBCLASSES
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        activity.claimItem.setDateExpensed(calendar.getTime(), app);
        tviewsDate.setText(app.dateFormatDefault.format(calendar.getTime()));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (s.hashCode() == etextTax.getText().hashCode()) {
            etextTax.setTag(etextTax.getText().toString());
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            if (s.hashCode() == etextTax.getText().hashCode() && !etextTax.getText().toString().contains("Tax Not Applicable")){
                if(Float.parseFloat(etextTax.getText().toString()) >= 1)
                    etextTax.setText(etextTax.getTag().toString());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void updateAttachment(File file){
        attachedFile = file;
        activity.claimItem.getAttachments().clear();
        if(file == null){
            resetAttachment();
        }else{
            if(file.length() <= 10485760){
                if(SaltApplication.ACCEPTED_FILETYPES.contains(file.getName().substring(file.getName().lastIndexOf(".")+1, file.getName().length()))){
                    activity.claimItem.addAttachment(new Document(file, activity.claimHeader, app.onlineGateway.epochizeDate(new Date()), app.dateFormatClaimItemAttachment.format(new Date())));
                    activity.claimItem.setHasReceipt(true);
                    activity.updateAttachmentUri(file);
                    tvAttachment.setText(file.getName());
                    tvAttachment.setTextColor(getResources().getColor(android.R.color.black));
                }else {
                    resetAttachment();
                    app.showMessageDialog(activity, "Invalid File Type");
                }
            }else {
                resetAttachment();
                app.showMessageDialog(activity, "File must not exceed 10mb");
            }
        }
    }

    private void resetAttachment(){
        activity.claimItem.setHasReceipt(false);
        tvAttachment.setText("No Selection");
        tvAttachment.setTextColor(Color.parseColor("#909090"));
    }

    @Override
    public void onProjectSelected(Project project) {
        activity.claimItem.setProject(project);
        tvProject.setText(project.getProjectName());
    }

    public class ForexGetter extends Thread {
        @Override
        public void run() {
            Object tempResult;
//            int remainingQuote;
            try {
//                 remainingQuote = Integer.parseInt(app.onlineGateway.getOandaRemainingQuotes().toString());
//                if (remainingQuote > 0) {
//                    tempResult = app.onlineGateway.getOandaForexRate(activity.claimItem.getForeignCurrencyName(), app.getStaffOffice().getBaseCurrencyThree());
//                }else
                tempResult = app.onlineGateway.getGoogleForexRate(activity.claimItem.getForeignCurrencyName(), app.getStaffOffice().getBaseCurrencyThree());
            } catch (Exception e) {
                e.printStackTrace();
                tempResult = e.getMessage();
            }

            final Object result = tempResult;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    activity.finishLoading();
                    if(result instanceof String){
                        new AlertDialog.Builder(linearNavFragmentActivity).setTitle("").setMessage(result.toString())
                                .setPositiveButton("Reload", new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        activity.startLoading();
                                        new ForexGetter().start();
                                    }
                                }).setNegativeButton("Cancel", new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                linearNavFragmentActivity.finish();
                            }
                        }).create().show();
                    }else{
                        forex = Float.parseFloat(result.toString());
                        etextForex.setText(String.valueOf(forex));
                    }
                }
            });
        }
    }

    @Override
    public void onAttachmentDownloadFinish(File downloadedFile) {
        activity.finishLoading();
        attachedFile = downloadedFile;
        app.fileManager.openDocument(activity, attachedFile);
    }

    @Override
    public void onAttachmentDownloadFailed(String errorMessage) {
        activity.finishLoading();
        Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show();
    }

    protected class DigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DigitsInputFilter(int integerDigit,int decimalDigit) {
            mPattern=Pattern.compile("[0-9]{0," + (integerDigit-1) + "}+((\\.[0-9]{0," + (decimalDigit-1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Matcher matcher=mPattern.matcher(dest);
            if(!matcher.matches())
                return "";
            return null;
        }

    }

}

