package applusvelosi.projects.android.salt2.utils;

import android.net.Uri;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.CostCenter;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Dashboard;
import applusvelosi.projects.android.salt2.models.Holiday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.models.Office;
import applusvelosi.projects.android.salt2.models.SapTaxCodeSettings;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.capex.CapexHeader;
import applusvelosi.projects.android.salt2.models.capex.CapexLineItem;
import applusvelosi.projects.android.salt2.models.capex.CapexLineItemQoutation;
import applusvelosi.projects.android.salt2.models.claimheaders.BusinessAdvance;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimNotPaidByCC;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimPaidByCC;
import applusvelosi.projects.android.salt2.models.claimheaders.LiquidationOfBA;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.models.claimitems.MilageClaimItem;
import applusvelosi.projects.android.salt2.models.claimitems.Project;
import applusvelosi.projects.android.salt2.models.contracttender.Contract;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftHospitality;
import applusvelosi.projects.android.salt2.models.recruitments.Recruitment;

//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.ByteArrayEntity;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.util.EntityUtils;

public class OnlineGateway {
	public final String apiUrl;

//	public static final String rootURL = "https://salt.velosi.com/";		//PROD WEB
//		public static final String rootURL = "https://salttest.velosi.com/";	//TEST WEB
	public static final String rootURL = "http://10.63.201.6:91/";			//DEV'T WEB
	public SaltApplication app;

	private final String SYSTEM_ERRORS = "Server Errors";
	//	private final String SE_MESSAGE = "Message";
	private static final String ERROR_MSG = "There was an error with the internet connection. Please try again or refresh the page.";

	public OnlineGateway(SaltApplication app) {
//		this.apiUrl = "https://saltapi.velosi.com/SALTService.svc/";		//PROD WEB SERVICE
//		this.apiUrl = "https://salttestapi.velosi.com/SALTService.svc/";	//TEST WEB SERVICE
		this.apiUrl = "http://10.63.201.6:90/SaltService.svc/";				//DEV'T WEB SERVICE
		this.app = app;
	}

	private boolean isJSONValid(String jsonStr) {
		try {
			new JSONObject(jsonStr);
		}catch (JSONException e){
			try {
				new JSONArray(jsonStr);
			}catch (JSONException e1){
				return false;
			}
		}
		return true;
	}

//TODO UTILITY METHODS //DEPRECATED
//	private String getHttpGetResult(String url) throws Exception {
//		System.out.println("GET URL: " + url);
//		HttpClient httpclient = new DefaultHttpClient();
//		HttpGet httpGETRequest = new HttpGet(url);
////		httpGETRequest.setHeader("AuthKey", AUTHKEY);
//	    HttpResponse response = httpclient.execute(httpGETRequest);
//	    return EntityUtils.toString(response.getEntity());
//	}

//	private String getHttpPostResult(String url, String postBodyJSON) throws Exception{
//		System.out.println("POST URL: "+ postBodyJSON);
//		HttpClient httpClient = new DefaultHttpClient();
//		HttpPost httpPostRequest = new HttpPost(url);
//		httpPostRequest.setHeader("Content-Type", "application/json");
//		httpPostRequest.setHeader("Accept", "application/json");
////		httpPostRequest.setHeader("AuthKey", AUTHKEY);
//		httpPostRequest.setEntity(new StringEntity(postBodyJSON));
//		HttpResponse response = httpClient.execute(httpPostRequest);
//	    String ret = EntityUtils.toString(response.getEntity());
//		System.out.println("URL: " + ret);
//		return ret;
//	}

	public String getURLEncodedString(String string) throws Exception{
		return URLEncoder.encode(string, "UTF-8");
	}

	public String now() throws Exception{
		return getURLEncodedString(app.dateTimeFormat.format(app.calendar.getTime()));
	}
//	public String getDefaultDate() throws Exception{
//		return getURLEncodedString("01-01-1900");
//	}

	//converts epoch date format returned by web services to the readable default date format
	public String dJsonizeDate(String d) {
		long epoch = Long.valueOf(d.substring(d.indexOf("(")+1, d.indexOf("+")));
		return app.dateFormatDefault.format(new Date(epoch /*+ addableDays*/));
	}

	public String dJsonizeDateForApproval(String d) throws Exception{
		long epoch = Long.valueOf(d.substring(d.indexOf("(")+1, d.indexOf("+")));
		String dateString = app.dateFormatDefault.format(new Date(epoch));
		System.out.println("DATE: "+dateString);
		long jan1 = 946684800000L; //Jan 1, 2000 GMT in epoch time format
		if(app.dateFormatDefault.parse(dateString).after(new Date(jan1))) //Date Before Jan 1, 2000 must not be viewed
			return app.dateFormatByProcessed.format(new Date(epoch));
		return "";
	}

	//epochize date
	public String epochizeDate(Date date) {
		Calendar cal = Calendar.getInstance();
		TimeZone timeZone = TimeZone.getDefault();
		int hoursOffset = timeZone.getOffset(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis())/1000/60/60;
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY,hoursOffset);
		return "/Date("+ cal.getTime().getTime() +"+0000)/";
	}

	//TODO STAFF METHODS
	public String authentication(String username, String password) throws JSONException{
		String url = apiUrl + "AuthenticateLogin?userName=" + username + "&password=" + Uri.encode(password);
		String resultString = WebRequest.makeWebServiceCall(url, WebRequest.POST, null);
		System.out.println("EXCEPTION: "+resultString);
		if (isJSONValid(resultString)) {
			JSONObject jsonString = new JSONObject(resultString).getJSONObject("AuthenticateLoginResult");
			if (jsonString.getJSONArray("SystemErrors").length() > 0) {
				String authError = ((JSONObject) jsonString.getJSONArray("SystemErrors").get(0)).getString("Message");
				String lpadError = authError.split(" ")[0];
				if (lpadError.contentEquals("LDAP") || lpadError.contentEquals("Server"))
					return "Username or password is invalid";
				else
					return jsonString.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			} else
				return jsonString.getInt("StaffID") + "-" + jsonString.getInt("SecurityLevel") + "-" + jsonString.getInt("OfficeID");
		}else{
			return ERROR_MSG;
		}
	}

	public void updateStaff(int staffID, int securityLevel, int officeID) throws Exception {
		String staffUrl = apiUrl+"GetStaffByID?staffID="+staffID+"&requestingPerson="+staffID+"&datetime="+now();
		String resultStaff = WebRequest.makeWebServiceCall(staffUrl, WebRequest.GET, null);
		System.out.println("EXCEPTION: "+resultStaff);
		if (isJSONValid(resultStaff)) {
			JSONObject result = new JSONObject(resultStaff).getJSONObject("GetStaffByIDResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				throw new Exception(result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message"));
			String officeUrl = apiUrl + "GetOfficeByID?officeID=" + officeID + "&requestingPerson=" + staffID + "&datetime=" + now();
			String resultOffice = WebRequest.makeWebServiceCall(officeUrl, WebRequest.GET, null);
			if (isJSONValid(resultOffice)) {
				JSONObject officeResult = new JSONObject(resultOffice).getJSONObject("GetOfficeByIDResult");
				if (officeResult.getJSONArray("SystemErrors").length() > 0)
					throw new Exception(officeResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message"));
				app.setStaffData(this, new Staff(result.getJSONObject("Staff")), new Office(officeResult.getJSONObject("Office"), true));
			} else{
				throw new Exception("Received bad response from server.");
			}
		} else {
			throw new Exception("Received bad response from server.");
		}
	}

	public Object getDashboard() throws Exception{
		int staffID = app.getStaff().getStaffID();
		int staffSecurityLevel = app.getStaff().getSecurityLevel();
		String role;
		if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_ADMIN)
			role = "Admin";
		else if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM)
			role = "CM";
		else if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_RM)
			role = "RM";
		else if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
			role = "CFO";
		else if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CEO)
			role = "CEO";
		else
			role = "";

		String dashboardUrl = apiUrl+"DashboardCount?staffID="+staffID+"&userRole="+role+"&securityLevel="+staffSecurityLevel+"&requestingPerson="+staffID;
		String resultDashboard = WebRequest.makeWebServiceCall(dashboardUrl, WebRequest.GET, null);
		System.out.println("EXCEPTION: "+resultDashboard);
		if(isJSONValid(resultDashboard)) {
			JSONObject result = new JSONObject(resultDashboard).getJSONObject("DashboardCountResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			return new Dashboard(result.getJSONObject("Dashboards"));
		} else {
			return ERROR_MSG;
		}
	}

//TODO LEAVE METHODS
//	public Object getMyLeaves() throws Exception{
//		try{
//			int staffID = app.getStaff().getStaffID();
//			String filter = getURLEncodedString("WHERE year(DateFrom)=Year(getdate()) AND StaffID=" + staffID + " ORDER BY DateSubmitted DESC");
//			JSONObject myLeaveResult = new JSONObject(getHttpGetResult(apiUrl+"GetAllLeave?filter="+filter+"&requestingPerson="+staffID+"&datetime="+now())).getJSONObject("GetAllLeaveResult");
//
//			if(myLeaveResult.getJSONArray("SystemErrors").length() > 0)
//				return myLeaveResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//
//			ArrayList<Leave> myLeaves = new ArrayList<Leave>();
//			JSONArray jsonMyLeaves = myLeaveResult.getJSONArray("Leaves");
//			for(int i=0; i<jsonMyLeaves.length(); i++)
//				myLeaves.add(new Leave(jsonMyLeaves.getJSONObject(i), this));
//
//			System.out.println("NUMBER OF LEAVE LIST: " + myLeaves.size());
//			return myLeaves;
//		}catch(Exception e){
//			return e.getMessage();
//		}
//	}

	public Object getLeaveByID(int leaveID) throws Exception{
		String url = apiUrl+"GetLeaveByID?leaveID="+leaveID+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String resultLeave = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		System.out.println("EXCEPTION: "+resultLeave);
		if (isJSONValid(resultLeave)) {
			JSONObject result = new JSONObject(resultLeave).getJSONObject("GetLeaveByIDResult");
			if (result.getJSONArray(SYSTEM_ERRORS).length() > 0)
				return result.getJSONArray(SYSTEM_ERRORS).getJSONObject(0).getString("Message");
			return result.getJSONObject("Leave");
		}else{
			return ERROR_MSG;
		}
	}

//	public Object getLeaveApprovalByID(int leaveID, int staffID) throws Exception{
//		try{
//			JSONObject result = new JSONObject(webRequest.makeWebServiceCall(apiUrl+"GetLeaveByID?leaveID="+leaveID+"&requestingPerson="+staffID+"&datetime="+now(), WebRequest.GET, null)).getJSONObject("GetLeaveByIDResult");
//			if(result.getJSONArray(SYSTEM_ERRORS).length() > 0)
//				return result.getJSONArray(SYSTEM_ERRORS).getJSONObject(0).getString(SE_MESSAGE);
//
//			return result.getJSONObject("Leave");
//		}catch(Exception e){
//			return e.getMessage();
//		}
//	}

	public Object getMyPendingAndApprovedLeaves() throws Exception{
		int staffID = app.getStaff().getStaffID();
		String filter = getURLEncodedString("WHERE year(DateFrom)=Year(getdate()) AND StaffID=" + staffID + " AND ((LeaveStatus=18)OR(LeaveStatus=20)) ORDER BY DateSubmitted DESC");
		String url = apiUrl+"GetAllLeave?filter="+filter+"&requestingPerson="+staffID+"&datetime="+now();
		String result = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		System.out.println("EXCEPTION: "+result);
		if(isJSONValid(result)) {
			JSONObject myLeaveResult = new JSONObject(result).getJSONObject("GetAllLeaveResult");
			if (myLeaveResult.getJSONArray("SystemErrors").length() > 0)
				return myLeaveResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<Leave> myLeaves = new ArrayList<Leave>();
			JSONArray jsonMyLeaves = myLeaveResult.getJSONArray("Leaves");
			for (int i = 0; i < jsonMyLeaves.length(); i++)
				myLeaves.add(new Leave(jsonMyLeaves.getJSONObject(i), this));

			return myLeaves;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getLeavesForApproval() throws Exception{
		JSONObject leavesForApprovalResult;
		int staffID = app.getStaff().getStaffID();
		String filter = getURLEncodedString("WHERE LeaveApprover1=" + staffID + " ORDER BY DateSubmitted DESC");
		String url = apiUrl+"GetAllLeave?filter="+filter+"&requestingPerson="+staffID+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		System.out.println("EXCEPTION: "+response);
		if(isJSONValid(response)) {
			leavesForApprovalResult = new JSONObject(response).getJSONObject("GetAllLeaveResult");
			if (leavesForApprovalResult.getJSONArray("SystemErrors").length() > 0)
				return leavesForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			ArrayList<Leave> leavesForApproval = new ArrayList<Leave>();
			JSONArray jsonLeaves = leavesForApprovalResult.getJSONArray("Leaves");
			for (int i = 0; i < jsonLeaves.length(); i++)
				leavesForApproval.add(new Leave(jsonLeaves.getJSONObject(i), this));
			return leavesForApproval;
		}else{
			return ERROR_MSG;
		}
	}

	//TODO CLAIMS METHODS
	public Object getMyClaims(int staffId) throws Exception {
		String url = apiUrl+"GetClaimsByStaff?staffID="+staffId+"&requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject staffClaimResult = new JSONObject(response).getJSONObject("GetClaimsByStaffResult");
			if(staffClaimResult.getJSONArray("SystemErrors").length() > 0)
				return staffClaimResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<ClaimHeader> myClaims = new ArrayList<>();
			JSONArray jsonClaims = staffClaimResult.getJSONArray("Claims");
			for(int i=0; i<jsonClaims.length(); i++){
				JSONObject jsonClaim = jsonClaims.getJSONObject(i);
				if(jsonClaim.getInt("ClaimStatus") != ClaimHeader.STATUSKEY_CANCELLED) {
					int type = jsonClaim.getInt(ClaimHeader.KEY_TYPEID);
					if (type == ClaimHeader.TYPEKEY_CLAIMS)
						myClaims.add((jsonClaim.getBoolean("IsPaidByCompanyCC")) ? new ClaimPaidByCC(jsonClaim) : new ClaimNotPaidByCC(jsonClaim));
					else if (type == ClaimHeader.TYPEKEY_ADVANCES)
						myClaims.add(new BusinessAdvance(jsonClaim));
					else if (type == ClaimHeader.TYPEKEY_LIQUIDATION)
						myClaims.add(new LiquidationOfBA(jsonClaim));
					else
						throw new Exception("Invalid Claim Type");
				}
			}
			return myClaims;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getMyClaimByID(int claimID) throws Exception {
		String url = apiUrl + "GetByClaimID?claimID=" + claimID + "&requestingPerson=" + app.getStaff().getStaffID() + "&datetime=" + now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetByClaimIDResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Messagee");
			JSONObject jsonClaim = result.getJSONObject("Claim");
			int claimType = jsonClaim.getInt(ClaimHeader.KEY_TYPEID);
			if (claimType == ClaimHeader.TYPEKEY_CLAIMS)
				return (jsonClaim.getBoolean("IsPaidByCompanyCC")) ? new ClaimPaidByCC(jsonClaim) : new ClaimNotPaidByCC(jsonClaim);
			else if (claimType == ClaimHeader.TYPEKEY_ADVANCES)
				return new BusinessAdvance(jsonClaim);
			else if (claimType == ClaimHeader.TYPEKEY_LIQUIDATION)
				return new LiquidationOfBA(jsonClaim);
			else
				throw new Exception("Invalid Claim Type");
		}else{
			return ERROR_MSG;
		}
	}

//	public Object getMyOutstandingBAClaimsForApproval(int staffID) throws Exception {
//		JSONObject staffClaimResult = new JSONObject(webRequest.makeWebServiceCall(apiUrl + "GetClaimsByStaff?staffID=" + staffID + "&requestingperson=" + staffID + "&datetime=" + now(), WebRequest.GET, null)).getJSONObject("GetClaimsByStaffResult");
//		if (staffClaimResult.getJSONArray("SystemErrors").length() > 0)
//			return staffClaimResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//
//		ArrayList<ClaimHeader> myClaims = new ArrayList<ClaimHeader>();
//		JSONArray jsonClaims = staffClaimResult.getJSONArray("Claims");
//		for (int i = 0; i < jsonClaims.length(); i++) {
//			JSONObject jsonClaim = jsonClaims.getJSONObject(i);
//			if(jsonClaim.getInt("ClaimStatus") != ClaimHeader.STATUSKEY_CANCELLED) {
//				int type = jsonClaim.getInt(ClaimHeader.KEY_TYPEID);
//				if (type == ClaimHeader.TYPEKEY_ADVANCES)
//					myClaims.add(new BusinessAdvance(jsonClaim));
//			}
//		}
//		return myClaims;
//	}

//	public Object getCapexesForApproval() throws Exception {
//		int staffID = app.getStaff().getStaffID();
//		int rfmId = app.getStaffOffice().getRFMId();
//
//		StringBuilder filter = new StringBuilder();
//		if(app.getStaff().isCM()) {
//			if(app.getStaff().isCorporateApprover()) {
//				if(app.getStaff().getUserPosition()==Staff.USERPOSITION.USERPOSITION_CFO || app.getStaff().getUserPosition()==Staff.USERPOSITION.USERPOSITION_CEO) {
//					int statusID = (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO) ? CapexHeader.CAPEXHEADERID_APPROVEDBYRM : CapexHeader.CAPEXHEADERID_APPROVEDBYCFO;
//					filter.append("WHERE ((CountryManager=" + staffID + " AND StatusID=" + CapexHeader.CAPEXHEADERID_SUBMITTED + ")OR(RegionalManager=" + staffID + " AND StatusID=" + CapexHeader.CAPEXHEADERID_APPROVEDBYCM + "))OR(TotalAmountInUSD>=3000 AND StatusID="+statusID+")");
//				} else
//					filter.append("WHERE (CountryManager="+staffID+" AND StatusID="+CapexHeader.CAPEXHEADERID_SUBMITTED+")OR(RegionalManager="+staffID+" AND StatusID="+CapexHeader.CAPEXHEADERID_APPROVEDBYCM+")");
//			}else
//				filter.append("WHERE CountryManager="+staffID+" AND StatusID="+ CapexHeader.CAPEXHEADERID_SUBMITTED);
//		} else
//			filter.append("WHERE CapexID=0");
//		filter.append(" ORDER BY DateCreated DESC");
//
//		String encodedFilter = getURLEncodedString(filter.toString());
//		String url = apiUrl+"GetFilterCapex?filter="+encodedFilter+"&requestingPerson="+staffID+"&datetime="+now();
//		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
//		if(isJSONValid(response)) {
//			JSONObject capexForApprovalResult = new JSONObject(response).getJSONObject("GetFilterCapexResult");
//			if (capexForApprovalResult.getJSONArray("SystemErrors").length() > 0)
//				return capexForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//
//			ArrayList<CapexHeader> capexesForApproval = new ArrayList<CapexHeader>();
//			JSONArray jsonCapexesForApproval = capexForApprovalResult.getJSONArray("Capexes");
//			for (int i = 0; i < jsonCapexesForApproval.length(); i++)
//				capexesForApproval.add(new CapexHeader(jsonCapexesForApproval.getJSONObject(i)));
//			return capexesForApproval;
//		}else{
//			return ERROR_MSG;
//		}
//	}

	public Object getCapexesForApproval() throws Exception {
		int staffID = app.getStaff().getStaffID();

		StringBuilder filter = new StringBuilder();
		filter.append("WHERE (CMId = "+staffID+" AND StatusID = "+ CapexHeader.CAPEXHEADERID_SUBMITTED +
				") OR (RfmId = " + staffID + " AND StatusID = " + CapexHeader.CAPEXHEADERID_APPROVEDBYCM +
				") OR (RMId = " +staffID+ " AND ((StatusID = " +CapexHeader.CAPEXHEADERID_APPROVEDBYCM+ " AND RfmId = 0) OR StatusID = " +CapexHeader.CAPEXHEADERID_APPROVEDBYRFM +
				")) OR (CfoId = " +staffID+" AND TotalAmountInUSD >= 3000 AND StatusID = " +CapexHeader.CAPEXHEADERID_APPROVEDBYRM +
				") OR (EvpId = " +staffID+" AND TotalAmountInUSD >= 3000 AND StatusID = " +CapexHeader.CAPEXHEADERID_APPROVEDBYCFO + ")");
		filter.append(" ORDER BY DateCreated DESC");

		String encodedFilter = getURLEncodedString(filter.toString());
		String url = apiUrl+"GetFilterCapex?filter="+encodedFilter+"&requestingPerson="+staffID+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject capexForApprovalResult = new JSONObject(response).getJSONObject("GetFilterCapexResult");
			if (capexForApprovalResult.getJSONArray("SystemErrors").length() > 0)
				return capexForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<CapexHeader> capexesForApproval = new ArrayList<CapexHeader>();
			JSONArray jsonCapexesForApproval = capexForApprovalResult.getJSONArray("Capexes");
			for (int i = 0; i < jsonCapexesForApproval.length(); i++)
				capexesForApproval.add(new CapexHeader(jsonCapexesForApproval.getJSONObject(i)));
			return capexesForApproval;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getClaimsForApproval() throws Exception {
		int staffID = app.getStaff().getStaffID();
		StringBuilder filter = new StringBuilder();
		List<ClaimHeader> claimsForCM = new ArrayList<ClaimHeader>();
		if(app.getStaff().isApprover()) {
			if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO) {
				filter.append("WHERE (ClaimStatus=" + ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER + " AND ClaimCountryManagerID = "+ staffID + ") OR (ClaimStatus=" + ClaimHeader.STATUSKEY_SUBMITTED + " AND ApproverID=" + staffID + ")");
			} else
				filter.append("WHERE ClaimStatus="+ClaimHeader.STATUSKEY_SUBMITTED+" AND ApproverID="+staffID);
		}
		filter.append(" ORDER BY DateCreated DESC");
		String encodedFilter = getURLEncodedString(filter.toString());

		String url = apiUrl+"GetFilterClaims?filter="+encodedFilter+"&requestingPerson="+staffID;
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject claimsForApprovalResult = new JSONObject(response).getJSONObject("GetFilterClaimsResult");
			if (claimsForApprovalResult.getJSONArray("SystemErrors").length() > 0)
				return claimsForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			List<ClaimHeader> claimsForApprval = new ArrayList<>();
			JSONArray jsonClaimsForApproval = claimsForApprovalResult.getJSONArray("Claims");
			for (int i = 0; i < jsonClaimsForApproval.length(); i++) {
				JSONObject jsonClaim = jsonClaimsForApproval.getJSONObject(i);
				int type = jsonClaim.getInt(ClaimHeader.KEY_TYPEID);
				if (type == ClaimHeader.TYPEKEY_CLAIMS)
					claimsForApprval.add((jsonClaim.getBoolean("IsPaidByCompanyCC")) ? new ClaimPaidByCC(jsonClaim) : new ClaimNotPaidByCC(jsonClaim));
				else if (type == ClaimHeader.TYPEKEY_ADVANCES)
					claimsForApprval.add(new BusinessAdvance(jsonClaim));
				else
					claimsForApprval.add(new LiquidationOfBA(jsonClaim));
			}
			return claimsForApprval;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getRecruitmentsForApproval() throws Exception {
		int staffId = app.getStaff().getStaffID();
		StringBuilder filter = new StringBuilder();

//		if(app.getStaff().isCM()) {
//			if(app.getStaff().isCorporateApprover()){
//				if(app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CEO) {
//					filter.append("WHERE StatusID=" + Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR + "OR((CountryManager=" + staffID + " AND StatusID=" + Recruitment.RECRUITMENT_STATUSID_SUBMITTED + ")OR(RegionalManager=" + staffID + " AND StatusID=" + Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM + "))");
//				}else
//					filter.append("WHERE (CountryManager="+staffID+" AND StatusID="+Recruitment.RECRUITMENT_STATUSID_SUBMITTED+")OR(RegionalManager="+staffID+" AND StatusID="+Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM+")");
//			}else
//				filter.append("WHERE CountryManager="+staffID+" AND StatusID="+Recruitment.RECRUITMENT_STATUSID_SUBMITTED);
//		} else if(app.getStaff().getStaffID() == SaltApplication.MAINHRID) {
//			filter.append("WHERE StatusID="+Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM);
//		} else {
//			filter.append("WHERE RecruitmentRequestID=0");
//		}
//		filter.append(" ORDER BY DateRequested DESC");

		filter.append("WHERE (CountryManager = " +staffId+ " AND StatusID = " +Recruitment.RECRUITMENT_STATUSID_SUBMITTED+
				") OR (RegionalHRManagerId = " +staffId+ " AND StatusID = " +Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM+
				") OR (RegionalManager = " +staffId+ " AND ((StatusID = " +Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM+ " AND RegionalHRManagerId = 0) OR StatusID = " +Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRHM+
				")) OR (MainHRId = " +staffId+ " AND StatusID = " +Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM+
				") OR (EVPId = " +staffId+ " AND StatusID = " +Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR+ ")");
		filter.append(" ORDER BY DateRequested DESC");

		String encodedFilter = getURLEncodedString(filter.toString());
		String url = apiUrl+"GetFilterRecruitment?filter="+encodedFilter+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject recruitmentForApprovalResult = new JSONObject(response).getJSONObject("GetFilterRecruitmentResult");

			if (recruitmentForApprovalResult.getJSONArray("SystemErrors").length() > 0)
				return recruitmentForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<Recruitment> recruitmentsForApproval = new ArrayList<>();
			JSONArray jsonRecruitmentsForApproval = recruitmentForApprovalResult.getJSONArray("RecruitmentRequests");
			for (int i = 0; i < jsonRecruitmentsForApproval.length(); i++) {
				recruitmentsForApproval.add(new Recruitment(jsonRecruitmentsForApproval.getJSONObject(i)));
			}
			return recruitmentsForApproval;
		} else {
			return ERROR_MSG;
		}
	}

	public Object getGiftsAndHospitalitiesForApproval() throws Exception {
		int staffId = app.getStaff().getStaffID();
		StringBuilder filter = new StringBuilder();

		if (app.getStaff().isCM() && app.getStaff().isCorporateApprover()) {
			if ( app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CEO )
				filter.append("WHERE StatusID = " + GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYRM );
			else
				filter.append("WHERE (CountryManager = " + staffId + " AND StatusID = " + GiftHospitality.GIFTHOSPITALITYID_SUBMITTED + ") OR (RegionalManager = " + staffId + " AND StatusID = " + GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCM + ")");
		} else if (app.getStaff().isCM() && !app.getStaff().isCorporateApprover()) {
			filter.append("WHERE (CountryManager = " + staffId + " AND StatusID = " + GiftHospitality.GIFTHOSPITALITYID_SUBMITTED + ")");
		} else if (app.getStaff().getJobTitle().equals(SaltApplication.STAFF_POS_APPLUSCSR)) {
			filter.append("WHERE (StatusID = "+GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCEO+ " AND ( (GiftType = 'Given by Velosi To Third Party' AND AmountInEUR >= 300) OR (GiftType = 'Received by Velosi From Third Party' AND AmountInEUR >= 100) OR GiftType = 'Given by Velosi to Public Officials'))");
		} else {
			filter.append("WHERE GiftsHospitalityID = 0");
		}

		filter.append(" ORDER BY DateCreated DESC");
		String encodedFilter = getURLEncodedString(filter.toString());

		String url = apiUrl+"GetFilterGiftsHospitality?filter="+encodedFilter+"&requestingPerson="+staffId;
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject giftForApprovalResult = new JSONObject(response).getJSONObject("GetFilterGiftsHospitalityResult");
			if (giftForApprovalResult.getJSONArray("SystemErrors").length() > 0)
				return giftForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			List<GiftHospitality> giftsForApproval = new ArrayList<GiftHospitality>();
			JSONArray jsonGiftForApproval = giftForApprovalResult.getJSONArray("GiftsHospitalities");
			for (int i = 0; i < jsonGiftForApproval.length(); i++)
				giftsForApproval.add(new GiftHospitality(jsonGiftForApproval.getJSONObject(i)));
			return giftsForApproval;
		} else {
			return ERROR_MSG;
		}
	}

	public Object getContractsForApproval() throws Exception {
		int staffId = app.getStaff().getStaffID();
		StringBuilder filter = new StringBuilder();

//		if (app.getStaff().isCM() && app.getStaff().isCorporateApprover()) {
//			if (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CEO)
//				filter.append("WHERE (EVPId = " +staffId+" AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYCM+ ") OR (EVPId = " +staffId+" AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYCFO+ ")");
//			else
//				filter.append("WHERE (CMId = " +staffId+ " AND RequestStatus = " + Contract.CONTRACTTENDERID_SUBMITTED+ ") OR (CMId = " +staffId+ " AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYLM+ ")");
//		} else if (app.getStaff().isCM() && !app.getStaff().isCorporateApprover()) {
//			filter.append("WHERE (CMId = " +staffId+ " AND RequestStatus = " + Contract.CONTRACTTENDERID_SUBMITTED+ ") OR (RMId = " +staffId+ " AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYCM+ ")");
//		} else if (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_LM) {
//			filter.append("WHERE RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYRM+" AND ProjectValueEquiv >=" +500);
//		}
//		filter.append("ORDER BY RequestDate DESC");

		filter.append("WHERE (CMId = " +staffId+ " AND RequestStatus = " +Contract.CONTRACTTENDERID_SUBMITTED+
				") OR (RfmId = " +staffId+ "AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYCM+
				") OR (RMId = " +staffId+ " AND ((RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYCM+ " AND RfmId = 0) OR RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYRFM+
				")) OR (LMId = " +staffId+ " AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYRM+ " AND ProjectValueEquiv >= 500)"+
				" OR (CfoId = " +staffId+ " AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYLM+
				") OR (EVPId = " +staffId+ " AND RequestStatus = " +Contract.CONTRACTTENDERID_APPROVEDBYCFO+ ")");
		filter.append(" ORDER BY RequestDate DESC");

		String encodedFilter = getURLEncodedString(filter.toString());
		String url = apiUrl+"GetFilterContract?filter="+encodedFilter+"&requestingPerson="+staffId;
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject contractForApprovalResult = new JSONObject(response).getJSONObject("GetFilterContractResult");
			if (contractForApprovalResult.getJSONArray("SystemErrors").length() > 0)
				return contractForApprovalResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			List<Contract> contractsForApproval = new ArrayList<Contract>();
			JSONArray jsonContractForApproval = contractForApprovalResult.getJSONArray("Contracts");
			for (int i = 0; i < jsonContractForApproval.length(); i++)
				contractsForApproval.add(new Contract(jsonContractForApproval.getJSONObject(i)));
			return contractsForApproval;
		} else {
			return ERROR_MSG;
		}
	}

	public Object getCapexLineItems(int capexID) throws Exception{

		String url = apiUrl+"GetCapexLineItems?CapexID="+capexID+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetCapexLineItemsResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			ArrayList<CapexLineItem> capexLineItems = new ArrayList<CapexLineItem>();
			JSONArray jsonCapexLineItems = result.getJSONArray("CapexLineItems");
			for (int i = 0; i < jsonCapexLineItems.length(); i++)
				capexLineItems.add(new CapexLineItem(jsonCapexLineItems.getJSONObject(i)));
			return capexLineItems;
		}else {
			return ERROR_MSG;
		}
	}

	public Object getCapexLineItemQoutations(int capexLineItemID) throws Exception {
		String url = apiUrl + "GetCapexLinteItemQuotation?capexLineItemID="+capexLineItemID+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetCapexLinteItemQuotationResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<CapexLineItemQoutation> capexLineItemQoutations = new ArrayList<CapexLineItemQoutation>();
			JSONArray jsonCapexLineItemQoutations = result.getJSONArray("CapexLineItemQuotations");
			for (int i = 0; i < jsonCapexLineItemQoutations.length(); i++)
				capexLineItemQoutations.add(new CapexLineItemQoutation(jsonCapexLineItemQoutations.getJSONObject(i)));
			return capexLineItemQoutations;
		}else{
			return ERROR_MSG;
		}

	}

	public Object getRecruitmentDetail(int recruitmentID) throws Exception{
		String url = apiUrl+"GetRecruitmentByID?RecruitmentID="+recruitmentID+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetRecruitmentByIDResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			return result.getJSONArray("RecruitmentRequests").get(0);
		} else {
			return ERROR_MSG;
		}
	}

	public Object getCapexHeaderDetail(int capexHeaderID) throws Exception{
		String url = apiUrl+"GetByCapexID?CapexID="+capexHeaderID+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetByCapexIDResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			return result.getJSONObject("Capex");
		} else {
			return ERROR_MSG;
		}
	}

	public Object getGiftHeaderDetail(int giftHeaderId) throws Exception {
		String url = apiUrl+"GetGiftsHospitalityByID?giftsID="+giftHeaderId+"&requestingPerson="+app.getStaff().getStaffID();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetGiftsHospitalityByIDResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			return result.getJSONObject("GiftHospitality");
		}else {
			return ERROR_MSG;
		}
	}

	public Object getContractDetail(int contractId) throws Exception {
		String url = apiUrl+"GetContractByID?contractID="+contractId+"&requestingPerson="+app.getStaff().getStaffID();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetContractByIDResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			return new Contract(result.getJSONObject("Contracts"));
		}else{
			return ERROR_MSG;
		}
	}

	public Object getClaimItemsWithClaimID(int claimID) throws Exception {
		String url = apiUrl+"GetClaimLineItems?claimID="+claimID+"&requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject claimItemResult = new JSONObject(response).getJSONObject("GetClaimLineItemsResult");
			if (claimItemResult.getJSONArray("SystemErrors").length() > 0)
				return claimItemResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			List<ClaimItem> claimItems = new ArrayList<>();
			JSONArray jsonClaimItems = claimItemResult.getJSONArray("ClaimLineItems");
			HashMap<Integer, JSONObject> catMap = new HashMap<>();
			for (int i = 0; i < jsonClaimItems.length(); i++) {
				JSONObject jsonClaimItem = jsonClaimItems.getJSONObject(i);
				if (!catMap.containsKey(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID))) {
					String urlCategory = apiUrl + "GetByCategoryID?categoryID=" + jsonClaimItem.getString(ClaimItem.KEY_CATEGORYID) + "&requestingperson=" + app.getStaff().getStaffID() + "&datetime=" + now();
					String responseCategory = WebRequest.makeWebServiceCall(urlCategory, WebRequest.GET, null);
					if(isJSONValid(responseCategory)) {
						JSONObject resultCategory = new JSONObject(responseCategory).getJSONObject("GetByCategoryIDResult");
						if (resultCategory.getJSONArray("SystemErrors").length() > 0)
							return resultCategory.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
						catMap.put(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID), resultCategory.getJSONObject("Category"));
					}else{
						return ERROR_MSG;
					}
				}
				if (catMap.get(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID)).getInt("CategoryTypeID") == Category.TYPE_MILEAGE)
					claimItems.add(new MilageClaimItem(jsonClaimItem, catMap.get(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID))));
				else
					claimItems.add(new ClaimItem(jsonClaimItem, catMap.get(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID))));
			}
			return claimItems;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getClaimItemCategoryByOffice(int claimTypeKey) throws Exception{
		String url = apiUrl+"GetCategoryByOffice?officeID="+app.getStaff().getOfficeID()+"&requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetCategoryByOfficeResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			ArrayList<Category> categories = new ArrayList<Category>();
			JSONArray jsonCategories = result.getJSONArray("Categories");
			for (int i = 0; i < jsonCategories.length(); i++) { //exclude of type assets since they are only for capex
				JSONObject jsonCategory = jsonCategories.getJSONObject(i);
				if (jsonCategory.getInt(ClaimItem.KEY_CATEGORYTYPEID) != Category.TYPE_ASSET) {
					if ((claimTypeKey == ClaimHeader.TYPEKEY_ADVANCES && jsonCategory.getInt(ClaimItem.KEY_CATEGORYTYPEID) == Category.TYPE_BUSINESSADVANCE) && !"46000001".equals(jsonCategory.getString("SAPCode")) ||
							(claimTypeKey != ClaimHeader.TYPEKEY_ADVANCES && jsonCategory.getInt(ClaimItem.KEY_CATEGORYTYPEID) != Category.TYPE_BUSINESSADVANCE))
						categories.add(new Category(jsonCategory));
				}
			}
			return categories;
		} else {
			return ERROR_MSG;
		}
	}

	public Object getCostCentersByOfficeID() throws Exception{
		String url = apiUrl+"GetCostCenterByOffice?officeID="+app.getStaff().getOfficeID()+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetCostCenterByOfficeResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<CostCenter> costCenters = new ArrayList<CostCenter>();
			JSONArray jsonCostCenters = result.getJSONArray("CostCenters");
			for (int i = 0; i < jsonCostCenters.length(); i++) {
				JSONObject jsonCostCenter = jsonCostCenters.getJSONObject(i);
//			JSONArray jsonCostCenterApprover = jsonCostCenter.getJSONArray("Approvers");
//			for (int j=0; j<jsonCostCenterApprover.length(); j++)
				costCenters.add(new CostCenter(jsonCostCenter.getInt("CostCenterID"), jsonCostCenter.getString("Description"), jsonCostCenter.getJSONArray("Approvers").length()));
			}
			return costCenters;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getAllSapTaxCodeSettingsByOffice() throws Exception{
		String url = apiUrl + "GetAllSAPTaxCodeSettingsByOffice?filter=" + app.getStaff().getOfficeID() + "&requestingPerson=" + app.getStaff().getStaffID() + "&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetAllSAPTaxCodeSettingsByOfficeResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			List<SapTaxCodeSettings> sapTaxCodes = new ArrayList<>();
			JSONArray jsonTaxCodes = new JSONArray();
			if (result.has("SAPTaxCodeSettings"))
				jsonTaxCodes = result.getJSONArray("SAPTaxCodeSettings");
			for (int i = 0; i < jsonTaxCodes.length(); i++) {
				JSONObject jsonTaxCode = jsonTaxCodes.getJSONObject(i);
				sapTaxCodes.add(new SapTaxCodeSettings(app, jsonTaxCode));
			}
			return sapTaxCodes;
		}else{
			return ERROR_MSG;
		}
	}


	public Object getClaimItemProjectsByCostCenter(int costCenterID) throws Exception{
		String url = apiUrl+"GetProjectByCostCenter?costCenterID="+costCenterID+"&requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetProjectByCostCenterResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			List<Project> projects = new ArrayList<Project>();
			JSONArray jsonProjects = result.getJSONArray("Projects");
			for (int i = 0; i < jsonProjects.length(); i++) { //exclude of type assets since they are only for capex
				JSONObject jsonProject = jsonProjects.getJSONObject(i);
				projects.add(new Project(jsonProject));
			}
			return projects;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getAllOffices() throws Exception{
		String url = apiUrl+"GetAllOffice?requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetAllOfficeResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			ArrayList<Office> offices = new ArrayList<Office>();
			JSONArray jsonOffices = result.getJSONArray("Offices");
			for (int i = 0; i < jsonOffices.length(); i++)
				offices.add(new Office(jsonOffices.getJSONObject(i), false));
			return offices;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getCurrencies() throws Exception {
		int staffID = app.getStaff().getStaffID();
		String url = apiUrl+"GetAllCurrencies?requestingPerson="+staffID+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url,WebRequest.GET,null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetAllCurrenciesResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<Currency> currencies = new ArrayList<>();
			JSONArray jsonCurrencies = result.getJSONArray("Currencies");
			for (int i = 0; i < jsonCurrencies.length(); i++)
				currencies.add(new Currency(jsonCurrencies.getJSONObject(i)));

			return currencies;
		}else{
			return ERROR_MSG;
		}
	}

	public Object getGoogleForexRate(String currFrom, String currTo) {
		String url = "http://www.google.com/finance/converter?a=1&from=" + currFrom + "&to=" + currTo;
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		int parentDivStartPos = response.indexOf("<div id=currency_converter_result>");
		int parentDivEndPos = response.indexOf("<input type=submit value=\"Convert\">");
		if (parentDivStartPos != -1 && parentDivEndPos != -1) {
			String parentDivSubstring = response.substring(parentDivStartPos, parentDivEndPos);
			//check if div has a child, if it does not have then there is something wrong with the parameters
			if (!parentDivSubstring.contains("><input")) {
				//get the span that contains the text of the rate
				int spanStartPos = parentDivSubstring.indexOf("<span class=bld>");
				int spanEndPos = parentDivSubstring.indexOf("</span>");
				if (spanStartPos != -1 && spanEndPos != -1) {
					StringBuilder tempSpanSubstring = new StringBuilder(parentDivSubstring.substring(spanStartPos, spanEndPos));
					return Float.valueOf(tempSpanSubstring.substring(tempSpanSubstring.indexOf(">") + 1, tempSpanSubstring.length()).split(" ")[0]);
				} else
					return "HTML Parsing error. Target source code of container span for rate string might have been changed. Please contact developer";
			} else
				return "Currency Not Found";
		} else
			return "HTML Parsing error. Target source code of container div for rate string might have been changed. Please contact developer";
	}


	public String processClaim(int claimId, int statusId, int approversId, String note) throws JSONException {
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"claimId\":" + claimId);
		sb.append(",\"claimStatusId\":" + statusId);
		sb.append(",\"nextApproverId\":"+ approversId);
		sb.append(",\"requestingPerson\":"+ String.valueOf(app.getStaff().getStaffID()));
		sb.append(",\"noteForReceipt\":\""+note+"\"");
		sb.append("}");

		String responseStr = WebRequest.makeWebServiceCall(apiUrl+"ProcessClaim", WebRequest.POST, sb.toString());
		if(responseStr.equals("\"success\""))
			return null;
		else{
			if (isJSONValid(responseStr)) {
				JSONObject result = new JSONObject(responseStr).getJSONObject("ProcessClaimResult");
				if (result.getJSONArray("SystemErrors").length() > 0)
					return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
				return null;
			}else {
				return ERROR_MSG;
			}
		}
//			ParseObject dataObject = new ParseObject("Requests");
//			dataObject.put("requester", app.getStaff().getFname() + " " + app.getStaff().getLname());
//			dataObject.put("type", "Claim");
//			dataObject.saveInBackground();
	}

	public String saveContract(String newContractJSON, int oldContractId) throws Exception {
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"newContracts\":"+newContractJSON);
		sb.append(",\"oldContractId\":"+oldContractId);
		sb.append(",\"requestingPerson\":"+String.valueOf(app.getStaff().getStaffID()));
		sb.append("}");

		String response = WebRequest.makeWebServiceCall(apiUrl+"SaveContract", WebRequest.POST, sb.toString());
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("SaveContractResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			return "OK";
		}else {
			return ERROR_MSG;
		}
	}

	public String saveGiftHospitality(String newGiftJSON, String oldGiftJSON) throws Exception {
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"newGifts\":"+newGiftJSON);
		sb.append(",\"oldGifts\":"+oldGiftJSON);
		sb.append(",\"requestingPerson\":"+String.valueOf(app.getStaff().getStaffID()));
		sb.append("}");

		String response = WebRequest.makeWebServiceCall(apiUrl+"SaveGiftsHospitality", WebRequest.POST, sb.toString());
		if (isJSONValid(response)){
			JSONObject result = new JSONObject(response).getJSONObject("SaveGiftsHospitalityResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			if (result.getInt("ProcessedGiftsHospitalityID") < 0)
				return "No changes were made";
			return "OK";
		}else{
			return ERROR_MSG;
		}
	}

	public String saveCapex(String newCapexHeaderJSON, String oldCapexHeaderJSON) throws Exception{
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"newCapex\":"+newCapexHeaderJSON);
		sb.append(",\"oldCapex\":"+oldCapexHeaderJSON);
		sb.append(",\"requestingPerson\":"+String.valueOf(app.getStaff().getStaffID()));
		sb.append(",\"datetime\":\""+app.dateTimeFormat.format(app.calendar.getTime())+"\"");
		sb.append("}");
		String response = WebRequest.makeWebServiceCall(apiUrl+"SaveCapex", WebRequest.POST, sb.toString());
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("SaveCapexResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			if (result.getInt("ProcessedCapexID") < 0)
				return "No changes were made";
//			ParseObject dataObject = new ParseObject("Approvals");
//			dataObject.put("approver", app.getStaff().getFname() + " " + app.getStaff().getLname());
//			dataObject.put("type", "Capex");
//			dataObject.saveInBackground();
			return "OK";
		}else{
			return ERROR_MSG;
		}
	}

	public String saveRecruitment(String newRecruitmentJSON, String oldRecruitmentJSON) throws Exception{
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"newRecruitment\":"+newRecruitmentJSON);
		sb.append(",\"oldRecruitment\":"+oldRecruitmentJSON);
		sb.append(",\"requestingPerson\":"+String.valueOf(app.getStaff().getStaffID()));
		sb.append(",\"datetime\":\""+app.dateTimeFormat.format(app.calendar.getTime())+"\"");
		sb.append("}");

		String response = WebRequest.makeWebServiceCall(apiUrl+"SaveRecruitment", WebRequest.POST, sb.toString());
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("SaveRecruitmentResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			if (result.getInt("ProcessedRecruitmentRequestID") < 0)
				return "No changes were made";
//			ParseObject dataObject = new ParseObject("Approvals");
//			dataObject.put("approver", app.getStaff().getFname() + " " + app.getStaff().getLname());
//			dataObject.put("type", "Recruitment");
//			dataObject.saveInBackground();
			return "OK";
		}else{
			return ERROR_MSG;
		}
	}

	public Object saveClaim(ClaimHeader newClaimJson, int oldClaimID, File file) throws Exception{
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"newClaim\":"+newClaimJson.jsonize().toString());
		sb.append(",\"oldClaimId\":"+oldClaimID);
		sb.append(",\"document\":"+((file!=null)?newClaimJson.getAttachments().get(0).getJSONObject().toString():null) );
		sb.append(",\"base64File\":\"");
		if(file != null) {
			String base64Encoded = encodeToBase64(file);
			int cSharpMaxStringLength = 30000;
			for (int ctr = 0; ctr < base64Encoded.length(); ctr += cSharpMaxStringLength) {
				sb.append(base64Encoded.substring(ctr, Math.min(ctr + cSharpMaxStringLength, base64Encoded.length())));
				if (ctr + cSharpMaxStringLength < base64Encoded.length())
					sb.append(",");
			}
		}
		sb.append("\",\"requestingPerson\":"+String.valueOf(app.getStaff().getStaffID()));
		sb.append("}");

		String response = WebRequest.makeWebServiceCall(apiUrl+"SaveClaim", WebRequest.POST, sb.toString());
		if(isJSONValid(response)) {
			return new JSONObject(response).getJSONObject("SaveClaimResult");
//			if (result.getJSONArray("SystemErrors").length() > 0)
//				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//			if (result.getInt("ProcessedClaimID") != 0)
//				return String.valueOf(result.get("ProcessedClaimID"));

//		ParseObject dataObject = new ParseObject("Requests");
//		dataObject.put("requester", app.getStaff().getFname() + " " + app.getStaff().getLname());
//		dataObject.put("type", "Claim");
//		dataObject.saveInBackground();

//		ArrayList<ClaimHeader> claimHeaders = new ArrayList<ClaimHeader>();
//		JSONArray jsonClaims = result.getJSONArray("Claims");
//		for(int i=0; i<jsonClaims.length(); i ++){
//			JSONObject jsonClaim = jsonClaims.getJSONObject(i);
//			if(jsonClaim.getInt("ClaimStatus") != ClaimHeader.STATUSKEY_CANCELLED) {
//				int type = jsonClaim.getInt(ClaimHeader.KEY_TYPEID);
//				if (type == ClaimHeader.TYPEKEY_CLAIMS)
//					claimHeaders.add((jsonClaim.getBoolean("IsPaidByCompanyCC")) ? new ClaimPaidByCC(jsonClaim) : new ClaimNotPaidByCC(jsonClaim));
//				else if (type == ClaimHeader.TYPEKEY_ADVANCES)
//					claimHeaders.add(new BusinessAdvance(jsonClaim));
//				else if (type == ClaimHeader.TYPEKEY_LIQUIDATION)
//					claimHeaders.add(new LiquidationOfBA(jsonClaim));
//				else
//					throw new Exception("Invalid Claim Type");
//			}
//		}
//		Collections.sort(claimHeaders, new Comparator<ClaimHeader>() {
//			@Override
//			public int compare(ClaimHeader lhs, ClaimHeader rhs) {
//				return rhs.getClaimID() - lhs.getClaimID();
//			}
//		});
//		app.updateMyClaims(claimHeaders);
//			return null;
		}else{
			return ERROR_MSG;
		}
	}

//	public String uploadAttachment(File file, Document document) throws Exception{
//		StringBuilder sb = new StringBuilder("{");
////		sb.append("\"document\":"+document.getJSONObject());
//		sb.append("\"document\":"+"{\"Active\":true,\"ContentType\":\"application/pdf\",\"DateCreated\":\"/Date(1422775080000+0000)/\",\"DocID\":867,\"DocName\":\"CapexRequest_29-30-2015-12-30-29-832.pdf\",\"Ext\":\".pdf\",\"FileSize\":852995.0,\"ObjectType\":7,\"OrigDocName\":\"Approved CER.pdf\",\"RefID\":22,\"StaffID\":203}");
////		sb.append("\"document\":{}");
//		sb.append(",\"base64File\": \"");
//		String base64Encoded = encodeToBase64(file);
//		int cSharpMaxStringLength = 30000;
//		for(int ctr=0; ctr<base64Encoded.length(); ctr+=cSharpMaxStringLength) {
//			sb.append(base64Encoded.substring(ctr, Math.min(ctr + cSharpMaxStringLength, base64Encoded.length())));
//			if(ctr+cSharpMaxStringLength < base64Encoded.length())
//				sb.append(",");
//		}
//
//		sb.append("\",\"requestingPerson\":" + String.valueOf(app.getStaff().getStaffID()));
//		sb.append(",\"datetime\":\"" + app.dateTimeFormat.format(app.calendar.getTime()) + "\"");
//		sb.append("}");
//
//		app.fileManager.saveToTextFile(sb.toString());
////		System.out.println("SALTX SENDING "+sb.toString());
//		String result = getHttpPostResult("http://10.63.201.199:90/SaltService.svc/UploadFile", sb.toString());
//		System.out.println("SALTX XXX");
//		System.out.println("SALTX Result " + result);
//		return result;
////        return getHttpPostResult(apiUrl+"UploadFile", sb.toString());
//	}

//	public String uploadToK2(File file) throws Exception{
//		StringBuilder sb = new StringBuilder("{");
//		sb.append("\"filename\": \"test.jpg\"");
//		sb.append(",\"base64File\": \"");
//		String base64Encoded = encodeToBase64(file);
//		int cSharpMaxStringLength = 30000;
//		for(int ctr=0; ctr<base64Encoded.length(); ctr+=cSharpMaxStringLength) {
//			sb.append(base64Encoded.substring(ctr, Math.min(ctr + cSharpMaxStringLength, base64Encoded.length())));
//			if(ctr+cSharpMaxStringLength < base64Encoded.length())
//				sb.append(",");
//		}
//
//		sb.append("\"}");
//
//		app.fileManager.saveToTextFile(sb.toString());
////		System.out.println("SALTX SENDING "+sb.toString());
//		HttpClient httpClient = new DefaultHttpClient();
//		System.out.println("SALTX upload to k2");
//		HttpPost httpPostRequest = new HttpPost("http://10.63.201.22:90/K2WebService.svc/UploadDropsPhotoLarge");
//		httpPostRequest.setHeader("Content-Type", "application/json");
//		httpPostRequest.setHeader("Accept", "application/json");
//		httpPostRequest.setEntity(new StringEntity(sb.toString()));
//		HttpResponse response = httpClient.execute(httpPostRequest);
//		String ret = EntityUtils.toString(response.getEntity());
//		System.out.println("SALTX XXX");
//		System.out.println("SALTX Resul ASDFKLNAt " + ret);
//		return ret;
//        return getHttpPostResult(apiUrl+"UploadFile", sb.toString());
//	}

//	public String test2(File file) throws Exception {
//		StringBuilder sb = new StringBuilder("\"");
//		if(file != null) {
//			String base64Encoded = encodeToBase64(file);
//			int cSharpMaxStringLength = 30000;
//			for (int ctr = 0; ctr < base64Encoded.length(); ctr += cSharpMaxStringLength) {
//				sb.append(base64Encoded.substring(ctr, Math.min(ctr + cSharpMaxStringLength, base64Encoded.length())));
//				if (ctr + cSharpMaxStringLength < base64Encoded.length())
//					sb.append(",");
//			}
//		}
//		sb.append("\"");
//		System.out.println("BASE64: "+ sb.toString());
//		return webRequest.makeWebServiceCall(apiUrl+"Test2", WebRequest.POST, sb.toString());
//	}

	public Object saveClaimLineItem(ClaimItem newClaimItem, int oldClaimItemID, File file) throws Exception {
		StringBuilder sb = new StringBuilder("{");
		sb.append("\"newClaimLineItem\":"+newClaimItem.jsonize().toString());
		sb.append(",\"oldClaimLineItemId\":"+oldClaimItemID);
		sb.append(",\"document\":"+((file!=null)?newClaimItem.getAttachments().get(0).getJSONObject():null));
		sb.append(",\"base64File\":\"\"");

		if(file != null) {
			String base64Encoded = encodeToBase64(file);
			int cSharpMaxStringLength = 30000;
			for (int ctr = 0; ctr < base64Encoded.length(); ctr += cSharpMaxStringLength) {
				sb.append(base64Encoded.substring(ctr, Math.min(ctr + cSharpMaxStringLength, base64Encoded.length())));
				if (ctr + cSharpMaxStringLength < base64Encoded.length())
					sb.append(",");
			}
		}
		sb.append(",\"requestingPerson\":" + String.valueOf(app.getStaff().getStaffID()));
		sb.append("}");

		app.fileManager.saveToTextFile(sb.toString());

		String response = WebRequest.makeWebServiceCall(apiUrl+"SaveClaimLineItem", WebRequest.POST ,sb.toString());

		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("SaveClaimLineItemResult");
			if (result.getJSONArray("SystemErrors").length() > 0) {
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			}
			ArrayList<ClaimItem> claimItems = new ArrayList<>();
			HashMap<Integer, JSONObject> catMap = new HashMap<>();
			JSONArray jsonClaimItems = result.getJSONArray("ClaimLineItems");
			for (int i = 0; i < jsonClaimItems.length(); i++) {
				JSONObject jsonClaimItem = jsonClaimItems.getJSONObject(i);
				if (!catMap.containsKey(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID))) {
					String url = apiUrl + "GetByCategoryID?categoryID=" + jsonClaimItem.getString(ClaimItem.KEY_CATEGORYID) + "&requestingperson=" + app.getStaff().getStaffID() + "&datetime=" + now();
					String responseCategory = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
					if (isJSONValid(responseCategory)) {
						JSONObject resultCategory = new JSONObject(responseCategory).getJSONObject("GetByCategoryIDResult");
						if (resultCategory.getJSONArray("SystemErrors").length() > 0)
							return resultCategory.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
						catMap.put(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID), resultCategory.getJSONObject("Category"));
					} else {
						throw new Exception("Received bad response from server.");
					}
				}
				if (catMap.get(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID)).getInt("CategoryTypeID") == Category.TYPE_MILEAGE)
					claimItems.add(new MilageClaimItem(jsonClaimItem, catMap.get(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID))));
				else
					claimItems.add(new ClaimItem(jsonClaimItem, catMap.get(jsonClaimItem.getInt(ClaimItem.KEY_CATEGORYID))));
			}
			return claimItems;
		} else {
			return ERROR_MSG;
		}
	}


//	public String saveClaimLineItem(String newClaimItemJSON, String oldClaimItemJSON, File file) throws Exception{
//		StringBuilder sb = new StringBuilder("{");
//        sb.append("\"newClaimLineItem\":"+newClaimItemJSON);
//        sb.append(",\"oldClaimLineItem\":"+oldClaimItemJSON);
//		sb.append(",\"base64File\": \"");
//		if(file != null){
//			String base64Encoded = encodeToBase64(file);
//			int cSharpMaxStringLength = 30000;
//			for(int ctr=0; ctr<base64Encoded.length(); ctr+=cSharpMaxStringLength) {
//				sb.append(base64Encoded.substring(ctr, Math.min(ctr + cSharpMaxStringLength, base64Encoded.length())));
//				if(ctr+cSharpMaxStringLength < base64Encoded.length())
//					sb.append(",");
//			}
//		}
//		sb.append("\",\"requestingPerson\":"+String.valueOf(app.getStaff().getStaffID()));
//        sb.append(",\"datetime\":\"" + app.dateTimeFormat.format(app.calendar.getTime()) + "\"");
//        sb.append("}");
//
//		app.fileManager.saveToTextFile(sb.toString());
//
//		JSONObject result = new JSONObject(getHttpPostResult(apiUrl+"SaveClaimLineItem", sb.toString())).getJSONObject("SaveClaimLineItemResult");
////		JSONObject result = new JSONObject(getHttpPostResult("http://10.63.201.199:90/SaltService.svc/SaveClaimLineItem", sb.toString())).getJSONObject("SaveClaimLineItemResult");
//        System.out.println("result " + result);
//        if(result.getJSONArray("SystemErrors").length() > 0)
//            return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//
//        if(result.getInt("ProcessedClaimLineItemID") <= 0)
//            return "No changes were made";
//		else
//			return "Okayzx "+result.getInt("ProcessedClaimLineItemID");
//
//    }

//	public String test(File file) throws Exception{
//		HttpClient httpClient = new DefaultHttpClient();
//		HttpPost httpPostRequest = new HttpPost("http://10.63.201.38:90/SaltService.svc/Test2");
//		httpPostRequest.setHeader("Content-Type", "application/json");
//		httpPostRequest.setHeader("Accept", "application/json");
////        httpPostRequest.setHeader("AuthKey", AUTHKEY);
//		byte [] bytes = encodeToByteArray(file);
//		System.out.println("byte array "+bytes);
//		httpPostRequest.setEntity(new ByteArrayEntity(bytes));
//		HttpResponse response = httpClient.execute(httpPostRequest);
//		String ret = EntityUtils.toString(response.getEntity());
//		System.out.println("Return POST");
//		System.out.println(ret);
//		return ret;
//
//	}

//    public String saveClaimLineItemDocument(JSONArray newDocs, JSONArray oldDocs, int refID) throws Exception{
//        String link = apiUrl+"SaveClaimLineItemDocument?attachments="+getURLEncodedString(newDocs.toString())+"&oldDocuments="+getURLEncodedString(oldDocs.toString())+"&refID="+refID+"&objectType="+ObjectTypes.ClAIMLINEITEM+"&requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
//        System.out.println(link);
//        JSONObject updateDocResult = new JSONObject(link);
//        return updateDocResult.toString();
//    }

	//TODO HOLIDAY METHODS
	public Object getAllHolidayArrayListOrErrorMessage(boolean shouldLoadFlag) throws Exception{
		String url = apiUrl+"GetAllNationalHoliday?requestingperson="+app.getStaff().getOfficeID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if(isJSONValid(response)) {
			JSONObject holidayResult = new JSONObject(response).getJSONObject("GetAllNationalHolidayResult");
			if (holidayResult.getJSONArray("SystemErrors").length() > 0)
				return holidayResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			List<CountryHoliday> nationalHolidays = new ArrayList<CountryHoliday>();
			JSONArray holidayArray = holidayResult.getJSONArray("NationalHoliday");
			for (int i = 0; i < holidayArray.length(); i++) {
				JSONObject jsonHoliday = holidayArray.getJSONObject(i);

				if (jsonHoliday.getBoolean("Active")) {
					String dateStr = dJsonizeDate(jsonHoliday.getString("Date"));
					CountryHoliday holiday = new CountryHoliday(jsonHoliday.getString("Name"), jsonHoliday.getString("Country"), dateStr);
//				String flagUrl = jsonHoliday.getString("ImageURL");
//				if(shouldLoadFlag){ //reuse bitmaps to greatly lessen loading time
//					if(!flagBitmaps.containsKey(flagUrl)){
//						try{
//							flagBitmaps.put(flagUrl, getFlag(flagUrl));
//						}catch(Exception e){
//							System.out.println(flagUrl);
//							flagBitmaps.put(flagUrl, null);
//							e.printStackTrace();
//						}
//					}
//					holiday.setFlag(flagBitmaps.get(flagUrl));
//				}else
//					holiday.setFlag(null);

					JSONArray holidayOffices = jsonHoliday.getJSONArray("OfficesApplied");
					for (int j = 0; j < holidayOffices.length(); j++)
						holiday.addOffice(holidayOffices.getJSONObject(j).getInt("OfficeID"), holidayOffices.getJSONObject(j).getString("OfficeName"));

					nationalHolidays.add(holiday);
				}
			}
			return nationalHolidays;
		}else{
			return ERROR_MSG;
		}

	}

	public Object getOfficeHolidaysOrErrorMessage(int officeID) throws Exception{
		String url = apiUrl+"GetNationalHolidaysByOffice?officeID="+officeID+"&requestingPerson="+app.getStaff().getOfficeID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url,WebRequest.GET,null);
		if(isJSONValid(response)) {
			JSONObject holidayResult = new JSONObject(response).getJSONObject("GetNationalHolidaysByOfficeResult");
			if (holidayResult.getJSONArray("SystemErrors").length() > 0)
				return holidayResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<CountryHoliday> officeHolidays = new ArrayList<CountryHoliday>();
			JSONArray holidayArray = holidayResult.getJSONArray("NationalHolidays");
			for (int i = 0; i < holidayArray.length(); i++) {
				JSONObject jsonHoliday = holidayArray.getJSONObject(i);
				if (jsonHoliday.getBoolean("Active")) {
					JSONArray offices = jsonHoliday.getJSONArray("OfficesApplied");
					for (int officeCTR = 0; officeCTR < offices.length(); officeCTR++) {
						if (offices.getJSONObject(officeCTR).getInt("OfficeID") == app.getStaff().getOfficeID()) {
							String dateStr = dJsonizeDate(jsonHoliday.getString("Date"));
							System.out.println("HOLIDAY NAME & DATE: " + jsonHoliday.getString("Name") + " - " + dateStr);
							officeHolidays.add(new CountryHoliday(jsonHoliday.getString("Name"), jsonHoliday.getString("Country"), dateStr));
							break;
						}
					}
				}
			}
			return officeHolidays;
		}else {
			return ERROR_MSG;
		}
	}

	public Object getLocalHolidayArrayListOrErrorMessage() throws Exception{
		SimpleDateFormat formatDayName = new SimpleDateFormat("EEEE", Locale.UK);
		SimpleDateFormat formatMonthName = new SimpleDateFormat("LLLL",Locale.UK);
		String url = apiUrl+"GetNationalHolidaysByOffice?officeID="+app.getStaff().getOfficeID()+"&requestingperson="+app.getStaff().getStaffID()+"&datetime="+now();
		String resultString = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);

		if(isJSONValid(resultString)) {
			JSONObject holidayResult = new JSONObject(resultString).getJSONObject("GetNationalHolidaysByOfficeResult");
			if (holidayResult.getJSONArray("SystemErrors").length() > 0)
				return holidayResult.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");

			ArrayList<Holiday> localHolidays = new ArrayList<>();
			JSONArray holidayArray = holidayResult.getJSONArray("NationalHolidays");
			for (int i = 0; i < holidayArray.length(); i++) {
				JSONObject jsonHoliday = holidayArray.getJSONObject(i);
				if (jsonHoliday.getBoolean("Active")) {
					JSONArray offices = jsonHoliday.getJSONArray("OfficesApplied");
					for (int officeCTR = 0; officeCTR < offices.length(); officeCTR++) { //not all offices within the nation might not observe some holiday so must check if the office observes the holiday or not
						if (offices.getJSONObject(officeCTR).getInt("OfficeID") == app.getStaff().getOfficeID()) {
							String dateStr = dJsonizeDate(jsonHoliday.getString("Date"));
							if (Integer.parseInt(dateStr.split("-")[2]) == app.thisYear) {
								Date date = app.dateFormatDefault.parse(dateStr);
								localHolidays.add(new Holiday(jsonHoliday.getString("Name"), dateStr, formatDayName.format(date), formatMonthName.format(date)));
							}
							break;
						}
					}
				}
			}
			return localHolidays;
		}else{
			return ERROR_MSG;
		}
	}

	public Object saveLeave(String newLeaveJson, String oldLeaveJson) throws Exception{
		String url = apiUrl+"SaveLeave?newLeave="+newLeaveJson+"&oldLeave="+oldLeaveJson+"&requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.GET, null);
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("SaveLeaveResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//			ParseObject dataObject = new ParseObject("Requests");
//			dataObject.put("requester", app.getStaff().getFname() + " " + app.getStaff().getLname());
//			dataObject.put("type", "Leave");
//			dataObject.saveInBackground();
			System.out.println("SALTX saveleaveresult " + result);
			return result.getInt("ProcessedLeaveID");
		}else{
			return ERROR_MSG;
		}
	}

	public String changeLeaveStatus(int leaveID, int statusID) throws Exception {
		String url = apiUrl + "ApproveLeave?leave=" + leaveID + "&status=" + statusID + "&requestingPerson=" + app.getStaff().getStaffID();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.POST, null);
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("ApproveLeaveResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
//			if (statusID == Leave.LEAVESTATUSAPPROVEDKEY || statusID == Leave.LEAVESTATUSREJECTEDKEY) {
//				ParseObject dataObject = new ParseObject("Approvals");
//				dataObject.put("approver", app.getStaff().getFname() + " " + app.getStaff().getLname());
//				dataObject.put("type", "Leave");
//				dataObject.saveInBackground();
//			}
			return null;
		}else{
			return ERROR_MSG;
		}
	}

	public String followUpLeave(int leaveID) throws Exception{
		String url = apiUrl+"EmailLeave?leave="+leaveID+"&requestingPerson="+app.getStaff().getStaffID();
		String response = WebRequest.makeWebServiceCall(url, WebRequest.POST, null);
		if(isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("EmailLeaveResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			return null;
		}else{
			return ERROR_MSG;
		}
	}

	// Calendar
	public Object getWeeklyHolidays() throws Exception {
		String url = apiUrl+"GetNationalHolidaysForTheWeek?requestingPerson="+app.getStaff().getStaffID()+"&datetime="+now();
		String response = WebRequest.makeWebServiceCall(url,WebRequest.GET,null);
		if (isJSONValid(response)) {
			JSONObject result = new JSONObject(response).getJSONObject("GetNationalHolidaysForTheWeekResult");
			if (result.getJSONArray("SystemErrors").length() > 0)
				return result.getJSONArray("SystemErrors").getJSONObject(0).getString("Message");
			JSONArray jsonHolidays = result.getJSONArray("NationalHoliday");
			ArrayList<CountryHoliday> holidays = new ArrayList<CountryHoliday>();
			for (int i = 0; i < jsonHolidays.length(); i++) {
				JSONObject jsonHoliday = jsonHolidays.getJSONObject(i);
				String dateStr = dJsonizeDate(jsonHoliday.getString("Date"));
				CountryHoliday holiday = new CountryHoliday(jsonHoliday.getString("Name"), jsonHoliday.getString("Country"), dateStr);

				JSONArray jsonOffices = jsonHoliday.getJSONArray("OfficesApplied");
				for (int j = 0; j < jsonOffices.length(); j++)
					holiday.addOffice(jsonOffices.getJSONObject(j).getInt("OfficeID"), jsonOffices.getJSONObject(j).getString("OfficeName"));

				holidays.add(holiday);
			}
			return holidays;
		}else{
			return ERROR_MSG;
		}
	}

	public static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
		HashMap<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key).toString();

			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

//	private static byte[] encodeToByteArray(File fileToEncode) throws Exception{
//		int fileSize = (int) fileToEncode.length();
//		//convert file to byte array
//		BufferedInputStream reader = new BufferedInputStream(new FileInputStream(fileToEncode));
//		byte[] bytes = new byte[fileSize];
//		reader.read(bytes, 0, fileSize);
//		reader.close();
//		//convert byte array to base64 string
//		return bytes;
//	}


	private static String encodeToBase64(File fileToEncode) throws Exception{
		int fileSize = (int) fileToEncode.length();
		//convert file to byte array
		BufferedInputStream reader = new BufferedInputStream(new FileInputStream(fileToEncode));
		byte[] bytes = new byte[fileSize];
		reader.read(bytes, 0, fileSize);
		reader.close();
		//convert byte array to base64 string
		return Base64.encodeToString(bytes, Base64.NO_WRAP);

	}

//	//PRIVATE METHODS
//	public Bitmap getBitmapFromUrl(String src) throws IOException {
//		URL url = new URL(src);
//		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//		connection.setDoInput(true);
//		connection.connect();
//		InputStream input = connection.getInputStream();
//		return BitmapFactory.decodeStream(input);
//	}

}
