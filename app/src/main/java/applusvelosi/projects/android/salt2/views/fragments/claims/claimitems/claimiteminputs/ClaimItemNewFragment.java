package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;

/**
 * Created by Velosi on 10/26/15.
 */
public class ClaimItemNewFragment extends ItemInputFragment {
    @Override
    protected View getInflatedLayout(LayoutInflater inflater) {
        return inflater.inflate(R.layout.fragment_claimitem_newclaim, null);
    }

    @Override
    protected void initAdditionalViewComponents(View v) {
        if(!cboxBillable.isChecked()){
            tvBillTo.setOnClickListener(null);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        super.afterTextChanged(s);
        try {
            etextAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        etextAmount.setHint("");
                    else
                        etextAmount.setHint("0.00");
                }
            });

            if(!app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                etextForex.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            System.out.println("please enter a forex");
                        }else {
                            if(etextForex.length() == 0)
                                etextForex.setText(String.valueOf(forex));
                            else
                                etextForex.setHint("0.00");
                        }
                    }
                });
            }

            if (s.hashCode() == etextAmount.getText().hashCode()) {
                float amt = Float.parseFloat(etextAmount.getText().toString());
                if (activity.claimItem.getCategoryTypeID() == Category.TYPE_BUSINESSADVANCE && amt > 0) {
                    etextAmount.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER);
                    int cashAdvanceAmnt = Integer.parseInt(etextAmount.getText().toString());
                    cashAdvanceAmnt *= -1;
                    etextAmount.removeTextChangedListener(this);
                    etextAmount.setText(String.valueOf(cashAdvanceAmnt));
//                    etextAmount.setSelection(etextAmount.getSelectionEnd());
                    etextAmount.setSelection(etextAmount.length());
                    etextAmount.addTextChangedListener(this);
                }
                activity.claimItem.setAmount(amt);
            }else if (s.hashCode() == etextForex.getText().hashCode())
                activity.claimItem.setForex(Float.parseFloat(etextForex.getText().toString()));
            else if(s.hashCode() == etextSapTaxValue.getText().hashCode())
                activity.claimItem.setSapTaxCode(tvSapTaxCode.getText().toString());
            else if(s.hashCode() == etextTax.getText().hashCode() && !etextTax.getText().toString().contains("Tax Not Applicable"))
                activity.claimItem.setTaxAmount(Float.parseFloat(etextTax.getText().toString()));
            else if (s.hashCode() == tvSapTaxCode.getText().hashCode())
                activity.claimItem.setSapTaxCode(tvSapTaxCode.getText().toString());

//            System.out.println("FOREX: " + Float.parseFloat(etextForex.getText().toString()) + " AmountLC:" + Float.parseFloat(etextAmount.getText().toString()));
            float totalLC = Float.parseFloat(etextAmount.getText().toString()) * (Float.parseFloat(etextForex.getText().toString()));
            System.out.println("TOTAL IN LC: " + totalLC);
            etextLocalAmount.setText(SaltApplication.decimalFormat.format(totalLC));
            activity.claimItem.setAmountLC(totalLC);

            if(app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())){
                if(isSapTax)
                    totalLC /= (1 + Float.parseFloat(etextSapTaxValue.getText().toString()));
                else if(cboxTaxable.isShown()) {
                    if (cboxTaxable.isChecked())
                        totalLC /= (1 + Float.parseFloat(etextTax.getText().toString())) ;
                }
            }

            etextBeforeTaxAmt.setText(SaltApplication.decimalFormat.format(totalLC));
            if(s.hashCode() == etextBillNotes.getText().hashCode())
                activity.claimItem.setNotes(etextBillNotes.getText().toString());
            else if(s.hashCode() == etextDesc.getText().hashCode())
                activity.claimItem.setDescription(etextDesc.getText().toString());
        }catch(Exception e){
            e.printStackTrace();
            etextBeforeTaxAmt.setText("0.00");
            etextLocalAmount.setText("0.00");
        }
    }

    @Override
    protected void saveToServer() {
        try {
            if (!"Entertainment and Gifts".equals(activity.claimItem.getCategory().getName()) || activity.claimItem.getCategory().getName().compareTo("Entertainment and Gifts") == 0 && activity.claimItem.getAttendees().size() > 0) {
                if (tviewsDate.length() > 0) {
                    if (etextAmount.length() > 0) {
                        if (etextDesc.length() > 0) {
                            actionbarDone.setEnabled(false);
                            activity.claimItem.setDateCreated(app.onlineGateway.epochizeDate(new Date()));
                            activity.startLoading();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Object tempResult;
                                    try {
                                        tempResult = app.onlineGateway.saveClaimLineItem(activity.claimItem, activity.claimItem.getItemID(), attachedFile);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        tempResult = e.getMessage();
                                    }
                                    final Object result = tempResult;
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            actionbarDone.setEnabled(true);
                                            if (result instanceof String) {
                                                activity.finishLoading(result.toString());
                                            } else {
                                                activity.finishLoading();
                                                Toast.makeText(activity, "Claim Item Created Successfully", Toast.LENGTH_SHORT).show();
                                                activity.finish();
                                            }
                                        }
                                    });
                                }
                            }).start();
                        } else app.showMessageDialog(activity, "Description is required");
                    } else app.showMessageDialog(activity, "Amount is required");
                } else app.showMessageDialog(activity, "Expense date is required");
            }else app.showMessageDialog(activity, "At least 1 attendee is required");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
