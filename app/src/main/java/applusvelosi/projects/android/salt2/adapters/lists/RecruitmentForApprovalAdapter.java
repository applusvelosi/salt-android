package applusvelosi.projects.android.salt2.adapters.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.recruitments.Recruitment;
import applusvelosi.projects.android.salt2.views.HomeActivity;

/**
 * Created by Velosi on 10/11/15.
 */
public class RecruitmentForApprovalAdapter extends BaseAdapter{
    HomeActivity activity;
    List<Recruitment> recruitments;

    public RecruitmentForApprovalAdapter(HomeActivity activity, List<Recruitment> recruitments){
        this.activity = activity;
        this.recruitments = recruitments;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        View view = convertView;
        RecruitmentNodeHolder holder;

        if (view == null) {
            holder = new RecruitmentNodeHolder();
            view = activity.getLayoutInflater().inflate(R.layout.node_headerdetailstatus, null);
            holder.tvOffice = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_header);
            holder.tvPositionType = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_detail);
            holder.tvStatus = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_status);
            holder.tvName = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_name);
            view.setTag(holder);
        }

        holder = (RecruitmentNodeHolder) view.getTag();
        Recruitment recruitment = recruitments.get(pos);
        String statusName;
        switch (recruitment.getStatusID()) {
            case Recruitment.RECRUITMENT_STATUSID_SUBMITTED:
                statusName = "Submitted";
                break;
            case Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM:
                statusName = "Approved by CM";
                break;
            case Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRHM:
                statusName = "Approved by Regional HR";
                break;
            case Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM:
                statusName = "Approved by RM";
                break;
            case Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR:
                statusName = "Approved by MHR";
                break;
            default:
                statusName = "Open";
                break;
        }

        holder.tvPositionType.setText(recruitment.getRecruitmentNumber());
        holder.tvOffice.setText(recruitment.getRequesterOfficeName());
        holder.tvStatus.setText(statusName);
        holder.tvName.setText(recruitment.getRequesterName());

        return view;
    }

    @Override
    public int getCount() {
        return recruitments.size();
    }

    @Override
    public Object getItem(int position) {
        return recruitments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class RecruitmentNodeHolder{
        public TextView tvOffice, tvStatus, tvPositionType, tvName;
    }
}
