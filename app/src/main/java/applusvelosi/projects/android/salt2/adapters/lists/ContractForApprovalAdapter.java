package applusvelosi.projects.android.salt2.adapters.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.contracttender.Contract;
import applusvelosi.projects.android.salt2.views.HomeActivity;

/**
 * Created by Velosi on 23/05/2017.
 */

public class ContractForApprovalAdapter extends BaseAdapter {
    HomeActivity activity;
    List<Contract> contractsAndTenders;

    public ContractForApprovalAdapter(HomeActivity activity, List<Contract> contracts) {
        this.activity = activity;
        contractsAndTenders = contracts;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ContractHolder holder;
        if (view == null) {
            holder = new ContractHolder();
            view = activity.getLayoutInflater().inflate(R.layout.node_headerdetailstatus, null);
            holder.tvRequestorName = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_name);
            holder.tvOffice = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_header);
            holder.tvReferenceNum =  (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_detail);
            holder.tvStatus = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_status);
            view.setTag(holder);
        }
        holder = (ContractHolder)view.getTag();
        Contract contract = contractsAndTenders.get(position);
        String statusName = Contract.getStatusDescForKey(contract.getRequestStatusId());
        holder.tvRequestorName.setText(contract.getRequestorName());
        holder.tvOffice.setText(contract.getOfficeName());
        holder.tvReferenceNum.setText(contract.getContractNumber());
        holder.tvStatus.setText(statusName);
        return view;
    }

    @Override
    public int getCount() {
        return contractsAndTenders.size();
    }

    @Override
    public Object getItem(int position) {
        return contractsAndTenders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ContractHolder {
        public TextView tvOffice, tvRequestorName, tvReferenceNum, tvStatus;
    }
}
