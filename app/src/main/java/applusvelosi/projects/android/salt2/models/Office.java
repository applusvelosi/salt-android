package applusvelosi.projects.android.salt2.models;

import org.json.JSONObject;

import java.util.HashMap;

public class Office {
	private final HashMap<String, String> map;
//	private static final String MILEAGETYPE_KILOMETER
	
	public Office(JSONObject jsonOffice, boolean isFullDetail) throws Exception{

		HashMap<String, String> temp = new HashMap<String, String>();
		temp.put("OfficeID", jsonOffice.getString("OfficeID"));
		temp.put("OfficeName", jsonOffice.getString("OfficeName"));

		if(isFullDetail){
			temp.put("Active", jsonOffice.getString("Active"));
			temp.put("BereavementLimit", jsonOffice.getString("BereavementLimit"));
			temp.put("BaseCurrency", jsonOffice.getString("BaseCurrency"));
			temp.put("BaseCurrencyName", jsonOffice.getString("BaseCurrencyName"));
			temp.put("CountryManager",jsonOffice.getString("CountryManager"));
			temp.put("DefaultTax", jsonOffice.getString("DefaultTax"));
			temp.put("Friday", jsonOffice.getString("Friday"));
			temp.put("HospitalizationLimit", jsonOffice.getString("HospitalizationLimit"));
			temp.put("HROfficerID", jsonOffice.getString("HROfficerID"));
			temp.put("IsUseBdayLeave", jsonOffice.getString("IsUseBdayLeave"));
			temp.put("IsHeadQuarter", jsonOffice.getString("IsHeadQuarter"));
			temp.put("InternalOrderRequired", jsonOffice.getString("InternalOrderRequired"));
			temp.put("MaternityLimit", jsonOffice.getString("MaternityLimit"));
			temp.put("MaxConsecutiveDays", jsonOffice.getString("MaxConsecutiveDays"));
			temp.put("Mileage", jsonOffice.getString("Mileage"));
			temp.put("MileageCurrency", jsonOffice.getString("MileageCurrency"));
			temp.put("MileageCurrencyName", jsonOffice.getString("MileageCurrencyName"));
			temp.put("MileageType", jsonOffice.getString("MileageType"));
			temp.put("Monday", jsonOffice.getString("Monday"));
			temp.put("PaternityLimit", jsonOffice.getString("PaternityLimit"));
			temp.put("RegionalManager", jsonOffice.getString("RegionalManager"));
			temp.put("RegionalFinanceManager", jsonOffice.getString("RegionalFinanceManager"));
			temp.put("RegionalHRManagerId", jsonOffice.getString("RegionalHRManagerId"));
			temp.put("Saturday", jsonOffice.getString("Saturday"));
			temp.put("SickLeaveAllowance", jsonOffice.getString("SickLeaveAllowance"));
			temp.put("Sunday", jsonOffice.getString("Sunday"));
			temp.put("Thursday", jsonOffice.getString("Thursday"));
			temp.put("TownCity", jsonOffice.getString("TownCity"));
			temp.put("Tuesday", jsonOffice.getString("Tuesday"));
			temp.put("UsesSAP", jsonOffice.getString("UsesSAP"));
			temp.put("UsesSAPService", jsonOffice.getString("UsesSAPService"));
			temp.put("VacationLeaveAllowance", jsonOffice.getString("VacationLeaveAllowance"));
			temp.put("Wednesday", jsonOffice.getString("Wednesday"));
		}
		
		map = temp;
	}
	
	public Office(HashMap<String, String> officeMap){
		this.map = officeMap;
	}
	
	public int getID(){
		return Integer.parseInt(map.get("OfficeID")); 
	}
	
	public String getName(){
		return map.get("OfficeName");
	}
	
	public int getHROfficerID(){
		return Integer.parseInt(map.get("HROfficerID"));
	}
	
	public boolean isActive(){
		return Boolean.parseBoolean(map.get("Active"));
	}
	
	public boolean hasSunday(){
		return Boolean.parseBoolean(map.get("Sunday"));
	}
	
	public boolean hasMonday(){
		return Boolean.parseBoolean(map.get("Monday"));
	}
	
	public boolean hasTuesday(){
		return Boolean.parseBoolean(map.get("Tuesday"));
	}
	
	public boolean hasWednesday(){
		return Boolean.parseBoolean(map.get("Wednesday"));
	}
	
	public boolean hasThursday(){
		return Boolean.parseBoolean(map.get("Thursday"));
	}
	
	public boolean hasFriday(){
		return Boolean.parseBoolean(map.get("Friday"));
	}
	
	public boolean hasSaturday(){
		return Boolean.parseBoolean(map.get("Saturday"));
	}
	
	public boolean hasBirthdayLeave(){
		return Boolean.parseBoolean(map.get("IsUseBdayLeave"));
	}

	public boolean isInternalOrderRequired() {
		return Boolean.parseBoolean(map.get("InternalOrderRequired"));
	}

	public boolean isHeadQuarter(){ return Boolean.parseBoolean(map.get("IsHeadQuarter")); }
	
	public float getMaternityLimit(){
		return Float.parseFloat(map.get("MaternityLimit"));
	}
	
	public float getPaternityLimit(){
		return Float.parseFloat(map.get("PaternityLimit"));
	}
		
	public float getHospitalizationLimit(){
		return Float.parseFloat(map.get("HospitalizationLimit"));
	}

	public String getCityName(){
		return String.valueOf(map.get("TownCity"));
	}
	
	public float getBereavementLimit(){
		return Float.parseFloat(map.get("BereavementLimit"));
	}
	
	public float getSickLimit(){
		return Float.parseFloat(map.get("SickLeaveAllowance"));
	}
	
	public float getVacationLimit(){
		return Float.parseFloat(map.get("VacationLeaveAllowance"));
	}
	
	public int getBaseCurrencyID(){
		return Integer.parseInt(map.get("BaseCurrency").toString());
	}
	
	public String getBaseCurrencyName(){
		String baseName = map.get("BaseCurrencyName").split("\\(")[0];
		return baseName.substring(0, baseName.length() - 1);
	}

	public int getMaxConsecutiveDays() {
		return Integer.parseInt(map.get("MaxConsecutiveDays"));
	}
	
	public String getBaseCurrencyThree(){
		String baseThree = map.get("BaseCurrencyName").split("\\(")[1];
		return baseThree.substring(0, baseThree.length()-1);
	}

	public float getMileageRate(){
		return Float.parseFloat(map.get("Mileage").toString());
	}
	
	public int getMileageCurrencyID(){
		return Integer.parseInt(map.get("MileageCurrency").toString());
	}
	
	public String getMileageCurrencyName(){
		String baseName = map.get("MileageCurrencyName").split("\\(")[0];
		return baseName.substring(0, baseName.length() - 1);
	}

	public String getMileageCurrencyThree(){
		String baseThree = map.get("MileageCurrencyName").split("\\(")[1];
		return baseThree.substring(0, baseThree.length()-1);
	}

	public int getMileageType(){
		return Integer.parseInt(map.get("MileageType").toString());
	}

	public int getCMID(){
		return Integer.parseInt(map.get("CountryManager").toString());
	}

	public int getRMID(){
		return Integer.parseInt(map.get("RegionalManager").toString());
	}

	public int getRFMId() {
		return Integer.parseInt(map.get("RegionalFinanceManager").toString());
	}

	public int getRegionalHRM() {
		return Integer.parseInt(map.get("RegionalHRManagerId").toString());
	}

	public float getDefaultTax(){
		return Float.parseFloat(map.get("DefaultTax").toString());
	}

	public HashMap<String, String> getMap(){
		return map;
	}

	public boolean hasUsesSap() {
		return Boolean.parseBoolean(map.get("UsesSAP"));
	}

	public boolean hasUsesSapService() {
		return Boolean.parseBoolean(map.get("UsesSAPService"));
	}

}
