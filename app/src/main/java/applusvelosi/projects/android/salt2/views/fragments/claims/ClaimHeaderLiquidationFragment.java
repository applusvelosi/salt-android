package applusvelosi.projects.android.salt2.views.fragments.claims;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimheaders.LiquidationOfBA;

public class ClaimHeaderLiquidationFragment extends ClaimHeaderFragment{	
	
	@Override
	protected String getActionbarTitle() {
		return "Liquidation Header";
	}

	@Override
	protected View getInflatedLayout(LayoutInflater inflater) {
		return inflater.inflate(R.layout.fragment_claimdetails_liquidation, null);
	}

	@Override
	protected void initAdditionalViewComponents(View view) throws Exception{
		LiquidationOfBA liquidation = (LiquidationOfBA) claimHeader;
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_id)).setText(String.valueOf(claimHeader.getClaimNumber()));
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_staff)).setText(claimHeader.getStaffName());
		tvCostCenter = (TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_costcenter);
		tvCostCenter.setText(claimHeader.getCostCenterName());
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_approver)).setText(claimHeader.getApproverName());

		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_type)).setText(ClaimHeader.getTypeDescriptionForKey(claimHeader.getTypeID()));
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_status)).setText(ClaimHeader.getStatusDescriptionForKey(claimHeader.getStatusID()));
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_bafreference)).setText(liquidation.getBacNumber());

		tvNotes = (TextView) view.findViewById(R.id.tviews_claimforapproval_approvednote);
		tvTotalAmtNotPaidByCC= (TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_total);
		tvTotalAmtNotPaidByCC.setText(String.valueOf(claimHeader.getTotalComputedInLC()));
		tvAmountApprovedNotPaidByCC= (TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_amountapproved);
		tvAmountApprovedNotPaidByCC.setText(String.valueOf(claimHeader.getTotalComputedApprovedInLC()));
		tvAmountRejectedNotPaidByCC=(TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_amountrejected);
		tvAmountRejectedNotPaidByCC.setText(String.valueOf(claimHeader.getTotalComputedRejectedInLC()));
		tvTotalForDeductionNotPaidByCC=(TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_fordeduction);
		tvTotalForDeductionNotPaidByCC.setText(String.valueOf(liquidation.getTotalComputedForDeductionInLC()));
		tvTotalForPaymentNotPaidByCC=(TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_forpayment);
		tvTotalForPaymentNotPaidByCC.setText(String.valueOf(claimHeader.getTotalComputedForPaymentInLC()));

		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_datesubmitted)).setText(claimHeader.getDateSubmitted(app));
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_onapprover)).setText(claimHeader.getDateApprovedByApprover(app));
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_onaccount)).setText(claimHeader.getDateApprovedByAccount(app));
		((TextView)view.findViewById(R.id.tviews_claimdetail_liquidation_datepaid)).setText(claimHeader.getDatePaid(app));

		containerLineItem = (RelativeLayout)view.findViewById(R.id.containers_claimheader_lineitems);
		containerLineItem.setOnClickListener(this);
		tvLineItemCnt = (TextView)view.findViewById(R.id.tviews_claimdetail_lineitemcnt);

		stringNotes = new StringBuilder();
		if(!claimHeader.getApproversNote().isEmpty()) {
			String tempStrings[] = claimHeader.getApproversNote().split(";");
			for (String str : tempStrings) {
				stringNotes.append(str);
				stringNotes.append(System.getProperty("line.separator"));
			}
		} else {
			stringNotes.append(claimHeader.getApproversNote());
		}

		tvNotes.setText(stringNotes.toString());
		if (claimHeader.getAttachments().size() > 0) {
			attachment.setOnClickListener(this);
			attachment.setText(claimHeader.getAttachments().get(0).getDocName());
			attachment.setTextColor(activity.getResources().getColor(R.color.orange));
		} else {
			attachment.setTextColor(activity.getResources().getColor(R.color.blue));
			if (activity.getIntent().hasExtra(activity.INTENTKEY_CLAIMHEADER)) {
				attachment.setText("ADD ATTACHMENT");
				attachment.setOnClickListener(this);
			}else if (activity.getIntent().hasExtra(activity.INTENTKEY_CLAIMFORAPPROVAL)) {
				attachment.setText("NO ATTACHMENT");
				attachment.setOnClickListener(null);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}
}
