package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;

import java.util.ArrayList;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.capex.CapexHeader;
import applusvelosi.projects.android.salt2.models.capex.CapexLineItem;
import applusvelosi.projects.android.salt2.views.fragments.capex.CapexForApprovalDetailFragment;

/**
 * Created by Velosi on 11/19/15.
 */
public class CapexApprovalDetailActivity extends LinearNavFragmentActivity {
    public static final String INTENTKEY_CAPEXHEADERID = "intentkeycapexheaderid";
    public int capexHeaderID;
    public CapexHeader capexHeader;
    public ArrayList<CapexLineItem> capexLineItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        capexHeaderID = getIntent().getExtras().getInt(INTENTKEY_CAPEXHEADERID);
        capexLineItems = new ArrayList<CapexLineItem>();
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new CapexForApprovalDetailFragment()).commit();
    }
}
