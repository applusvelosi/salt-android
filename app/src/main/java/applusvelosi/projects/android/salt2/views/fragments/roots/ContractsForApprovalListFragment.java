package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.adapters.lists.ContractForApprovalAdapter;
import applusvelosi.projects.android.salt2.models.contracttender.Contract;
import applusvelosi.projects.android.salt2.views.ContractApprovalDetailActivity;

/**
 * Created by Velosi on 23/05/2017.
 */

public class ContractsForApprovalListFragment extends RootFragment implements TextWatcher, AdapterView.OnItemClickListener {
    private static ContractsForApprovalListFragment instance;
    private RelativeLayout actionbarRefreshButton, actionbarMenuButton;
    private List<Contract> contractsAndTerders, tempContract;
    private ListView lv;
    private EditText editTxtSearchContract;
    private ContractForApprovalAdapter adapter;

    public static ContractsForApprovalListFragment getInstance() {
        if (instance == null)
            instance = new ContractsForApprovalListFragment();
        return instance;
    }

    public static void removeInstance() {
        if (instance != null)
            instance = null;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_menurefresh, null);
        actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
        actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
        ((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Contract for Approval");

        actionbarMenuButton.setOnClickListener(this);
        actionbarRefreshButton.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listview, null);
        lv = (ListView)view.findViewById(R.id.lists_lv);
        contractsAndTerders = new ArrayList<>();
        tempContract = new ArrayList<>();
        editTxtSearchContract = (EditText)view.findViewById(R.id.etexts_itemforapproval_name);
        editTxtSearchContract.setVisibility(View.VISIBLE);
        editTxtSearchContract.setHint("Search With Name or Office");
        editTxtSearchContract.addTextChangedListener(this);

        adapter = new ContractForApprovalAdapter(activity, tempContract);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        syncToServer();
    }

    private void syncToServer() {
        activity.startLoading();
        new Thread(new Runnable() {

            @Override
            public void run() {
                Object tempContractResult;
                try {
                    tempContractResult = app.onlineGateway.getContractsForApproval();
                }catch (Exception e){
                    tempContractResult = e.getMessage();
                }
                final Object contractResult = tempContractResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (contractResult instanceof String) {
                            activity.finishLoading(contractResult.toString());
                        }else {
                            activity.finishLoading();
                            contractsAndTerders.clear();
                            contractsAndTerders.addAll((ArrayList<Contract>) contractResult);
                            filteredSearchContract();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }).start();
    }

    private void filteredSearchContract() {
        tempContract.clear();
        if (editTxtSearchContract.getText().length() > 0) {
            for (Contract contract : contractsAndTerders) {
                if (contract.getRequestorName().toLowerCase().contains(editTxtSearchContract.getText().toString().toLowerCase()) || contract.getOfficeName().toLowerCase().contains(editTxtSearchContract.getText().toString().toLowerCase()) || editTxtSearchContract.getText().length() < 1)
                    tempContract.add(contract);
            }
        }else
            tempContract.addAll(contractsAndTerders);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void disableUserInteractionsOnSidebarShown() {
        lv.setEnabled(false);
        editTxtSearchContract.setEnabled(false);
    }

    @Override
    public void enableUserInteractionsOnSidebarHidden() {
        lv.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        if (v == actionbarMenuButton) {
            actionbarMenuButton.setEnabled(false);
            activity.toggleSidebar(actionbarMenuButton);
        } else if (v == actionbarRefreshButton) {
            syncToServer();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filteredSearchContract();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(activity, ContractApprovalDetailActivity.class);
        intent.putExtra(ContractApprovalDetailActivity.INTENTKEY_CONTRACTID, tempContract.get(position).getContractId());
        startActivity(intent);
    }
}
