package applusvelosi.projects.android.salt2.utils.enums;

public enum CalendarEventTypes {
	TYPE_HOLIDAY,
	TYPE_LEAVE
}
