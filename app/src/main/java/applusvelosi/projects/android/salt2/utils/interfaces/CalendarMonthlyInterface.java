package applusvelosi.projects.android.salt2.utils.interfaces;

import android.support.v4.app.FragmentActivity;

import applusvelosi.projects.android.salt2.models.CalendarItem;

public interface CalendarMonthlyInterface {

	FragmentActivity getActivity();
	void onCalendarItemClicked(CalendarItem calendarItem);
}
