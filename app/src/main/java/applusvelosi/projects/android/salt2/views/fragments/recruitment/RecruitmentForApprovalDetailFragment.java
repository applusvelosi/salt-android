package applusvelosi.projects.android.salt2.views.fragments.recruitment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.recruitments.Recruitment;
import applusvelosi.projects.android.salt2.views.RecruitmentApprovalDetailActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 10/12/15.
 */
public class RecruitmentForApprovalDetailFragment extends LinearNavActionbarFragment {
    private RelativeLayout actionbarButtonBack;
    private TextView actionbarTitle;
    private TextView buttonReopen, buttonApprove, buttonReject;
    private TextView    fieldName, fieldEmail, fieldOffice, fieldDepartment, fieldPhone, fieldCM, fieldDateRequested,
            fieldDateProcessedbyCM, fieldDateProcessedbyRHM, fieldDateProcessedbyRM, fieldDateProcessedbyMHR, fieldDateProcessedbyCEO,fieldPosition, fieldPositionFor, fieldReasonForPosition,
            fieldOfficeOfDeployment, fieldDepartmentOfDeployment, fieldTargettedStartDate, fieldJobTitle, fieldPositionCategory,
            fieldRevenue, fieldSalaryRange, fieldBonus, fieldTimeBase, fieldEmployment, fieldHoursPerWeek, tviewReasonFor, fieldSeverancePayment,
            fieldApproverNote;

    private CheckBox    cboxBudgettedCost, cboxSpecificPerson, cboxPositionMayBePermanent;
    private RelativeLayout  containersAttachments;
    private LinearLayout containerBenefits;

    private RecruitmentApprovalDetailActivity activity;
    private EditText etextDialogRejectReason, etextDialogReopenReason, etextDialogApproveReason;
    private AlertDialog dialogReject, dialogReopen, dialogApprove;
    private StringBuilder stringNotes,approversNote;
    private RadioButton radioReplacementYes, radioReplacementNo;
    private int staffId;
    private TableRow tablerowSeveranceDetails;

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (RecruitmentApprovalDetailActivity)getActivity();
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backonly, null);
        actionbarButtonBack = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        actionbarTitle.setText("HR For Approval");

        actionbarButtonBack.setOnClickListener(this);
        actionbarTitle.setOnClickListener(this);

        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rfa_detail, null);
        buttonApprove = (TextView)view.findViewById(R.id.buttons_rfadetail_approve);
        buttonReject = (TextView)view.findViewById(R.id.buttons_rfadetail_reject);
        buttonReopen = (TextView)view.findViewById(R.id.buttons_rfadetail_return);
        fieldName = (TextView)view.findViewById(R.id.tviews_rfadetail_name);
        fieldEmail = (TextView)view.findViewById(R.id.tviews_rfadetail_email);
        fieldOffice = (TextView)view.findViewById(R.id.tviews_rfadetail_office);
        fieldDepartment = (TextView)view.findViewById(R.id.tviews_rfadetail_department);
        fieldPhone = (TextView)view.findViewById(R.id.tviews_rfadetail_phoneNumber);
        fieldCM = (TextView)view.findViewById(R.id.tviews_rfadetail_countryManager);
        fieldDateRequested = (TextView)view.findViewById(R.id.tviews_rfadetail_dateRequested);
        fieldDateProcessedbyCM = (TextView)view.findViewById(R.id.tviews_rfadetail_dateprocessedbycm);
        fieldDateProcessedbyRHM = (TextView)view.findViewById(R.id.tviews_rfadetail_dateprocessedbyrhm);
        fieldDateProcessedbyRM = (TextView)view.findViewById(R.id.tviews_rfadetail_dateprocessedbyrm);
        fieldDateProcessedbyMHR = (TextView) view.findViewById(R.id.tviews_rfadetail_dateprocessedbymhr);
        fieldDateProcessedbyCEO = (TextView) view.findViewById(R.id.tviews_rfadetail_dateprocessedbyceo);
        fieldPosition = (TextView)view.findViewById(R.id.tviews_rfadetail_positionType);
//        fieldPositionFor = (TextView)view.findViewById(R.id.tviews_rfadetail_reasonforreplacement);
        fieldReasonForPosition = (TextView)view.findViewById(R.id.tviews_rfadetail_reasonforreplacement);

        fieldOfficeOfDeployment = (TextView)view.findViewById(R.id.tviews_rfadetail_officeofdeployment);
        fieldDepartmentOfDeployment = (TextView)view.findViewById(R.id.tviews_rfadetail_departmentofdeployment);
        fieldTargettedStartDate = (TextView)view.findViewById(R.id.tviews_rfadetail_targettedstartdate);
        fieldJobTitle = (TextView)view.findViewById(R.id.tviews_rfadetail_jobTitle);
        fieldPositionCategory = (TextView)view.findViewById(R.id.tviews_rfadetail_employeecategory);
        fieldRevenue = (TextView)view.findViewById(R.id.tviews_rfadetail_annualrevenue);
        fieldSalaryRange = (TextView)view.findViewById(R.id.tviews_rfadetail_salaryrange);
        fieldSeverancePayment = (TextView)view.findViewById(R.id.tviews_rfadetail_severancedetails);
        fieldBonus = (TextView)view.findViewById(R.id.tviews_rfadetail_grossbasebunos);
        fieldTimeBase = (TextView)view.findViewById(R.id.tviews_rfadetail_timebase);
        fieldEmployment = (TextView)view.findViewById(R.id.tviews_rfadetail_employmenttype);
        fieldHoursPerWeek = (TextView)view.findViewById(R.id.tviews_rfadetail_hoursperweek);
        radioReplacementYes = (RadioButton)view.findViewById(R.id.rbuttons_rfdetails_yes);
        radioReplacementNo = (RadioButton)view.findViewById(R.id.rbuttons_rfdetails_no);
        tablerowSeveranceDetails = (TableRow)view.findViewById(R.id.tablerow_rfadetail_severancedetailscontainer);
        tviewReasonFor = (TextView)view.findViewById(R.id.tviews_rfadtail_reasonforpositionlabel);
        cboxBudgettedCost = (CheckBox)view.findViewById(R.id.cbox_rfadetail_budgetedcost);
        cboxSpecificPerson = (CheckBox)view.findViewById(R.id.cbox_rfadetail_specificperson);
        cboxPositionMayBePermanent = (CheckBox)view.findViewById(R.id.cbox_rfadetail_positionmaybecomepermanent);
        containersAttachments = (RelativeLayout)view.findViewById(R.id.containers_rfadetail_attachments);
        containerBenefits = (LinearLayout)view.findViewById(R.id.containers_rfadetail_benefits);
        fieldApproverNote = (TextView) view.findViewById(R.id.tviews_recruitmentforapprovaldetail_approvednote);
        staffId = app.getStaff().getStaffID();

        buttonApprove.setOnClickListener(this);
        buttonReject.setOnClickListener(this);
        buttonReopen.setOnClickListener(this);
        containersAttachments.setOnClickListener(this);
        RelativeLayout dialogViewReject, dialogViewReopen, dialogViewApprove;
        dialogViewApprove = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogApproveReason = (EditText)dialogViewApprove.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewApprove.findViewById(R.id.tviews_dialogs_textinput)).setText("Approver's Note");
        etextDialogApproveReason.setHint("Note");
        dialogApprove = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewApprove)
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_SUBMITTED) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("CM: " + etextNote);
                            } else
                                approversNote.append("CM: Approved");
                            approversNote.append(";");
                            activity.recruitment.setStatusName("Approved by Country Manager");
                            changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM, "DateProcessedByCountryManager", approversNote.toString());
                        } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM && activity.recruitment.getRhmID() != 0) {
                            if (etextDialogApproveReason.length() > 0){
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("RHM: "+ etextNote);
                            }else
                                approversNote.append("RHM: Approved");
                            approversNote.append(";");
                            activity.recruitment.setStatusName("Approved by Regional HR Manager");
                            changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRHM, "DateProcessedByRegionalHR", approversNote.toString());
                        } else if((activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM && activity.recruitment.getRhmID() == 0) || activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRHM) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("RM: " + etextNote);
                            } else
                                approversNote.append("RM: Approved");
                            approversNote.append(";");
                            activity.recruitment.setStatusName("Approved by Regional Manager");
                            changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM, "DateProcessedByRegionalManager", approversNote.toString());
                        } else if(activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM) {
                            if (etextDialogApproveReason.length() > 0){
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("Main HR: " + etextNote);
                            }else
                                approversNote.append("Main HR: Approved");
                            approversNote.append(";");
                            changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR, "DateProcessedByHR", approversNote.toString());
                        } else if(activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR) {
                            if (etextDialogApproveReason.length() > 0){
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("CEO: "+ etextNote);
                            }else
                                approversNote.append("CEO: Approved");
                            approversNote.append(";");
                            activity.recruitment.setStatusName("Approved by EVP");
                            changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialogViewReject = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogRejectReason = (EditText)dialogViewReject.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReject.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Rejection");
        dialogReject = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReject)
                .setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(etextDialogRejectReason.length() > 0) {
                            String etextNote = etextDialogRejectReason.getText().toString().trim();
                            if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_SUBMITTED) {
                                approversNote.append("CM: "+ etextNote);
                                approversNote.append(";");
                                activity.recruitment.setStatusName("Rejected by Country Manager");
                                changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_REJECTEDBYCM, "DateProcessedByCountryManager",approversNote.toString());
                            } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM && activity.recruitment.getRhmID() != 0) {
                                approversNote.append("RHM: "+ etextNote);
                                approversNote.append(";");
                                activity.recruitment.setStatusName("Rejected by Regional HR Manager");
                                changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_REJECTEDBYRHM, "DateProcessedByRegionalHR",approversNote.toString());
                            }else if ((activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM && activity.recruitment.getRhmID() == 0) || activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRHM) {
                                approversNote.append("RM: "+ etextNote);
                                approversNote.append(";");
                                activity.recruitment.setStatusName("Rejected by Regional Manager");
                                changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_REJECTEDBYRM, "DateProcessedByRegionalManager", approversNote.toString());
                            } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM) {
                                approversNote.append("Main HR: "+etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_REJECTEDBYMHR, "DateProcessedByHR", etextDialogRejectReason.getText().toString());
                            } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR) {
                                approversNote.append("CEO: "+ etextNote);
                                approversNote.append(";");
                                activity.recruitment.setStatusName("Rejected by EVP");
                                changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_REJECTEDBYCEO, "DateProcessedByCEO", etextDialogRejectReason.getText().toString());
                            }
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please input a reason for rejection");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        dialogViewReopen = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogReopenReason = (EditText)dialogViewReopen.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReopen.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Reopening");
        dialogReopen = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReopen)
                .setPositiveButton("Return", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(etextDialogReopenReason.length()>0) {
                            String etextNote, dateProcessedBy = "NA";
                            etextNote = etextDialogReopenReason.getText().toString().trim();
                            if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_SUBMITTED) {
                                approversNote.append("CM: " + etextNote);
                                activity.recruitment.setStatusName("Reopened by Country Manager");
                                dateProcessedBy = "DateProcessedByCountryManager";
                            } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM && activity.recruitment.getRhmID() == staffId) {
                                approversNote.append("RHM: " + etextNote);
                                activity.recruitment.setStatusName("Reopened by Regional HR Manager");
                                dateProcessedBy = "DateProcessedByRegionalHR";
                            } else if ((activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYCM && activity.recruitment.getRhmID() == 0) || activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRHM) {
                                approversNote.append("RM: " + etextNote);
                                activity.recruitment.setStatusName("Reopened by Regional Manager");
                                dateProcessedBy = "DateProcessedByRegionalManager";
                            } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYRM) {
                                approversNote.append("Main HR: "+etextNote);
                                dateProcessedBy = "DateProcessedByHR";
                            } else if (activity.recruitment.getStatusID() == Recruitment.RECRUITMENT_STATUSID_APPROVEDBYMHR) {
                                approversNote.append("CEO: "+etextNote);
                                activity.recruitment.setStatusName("Reopened by EVP");
                                dateProcessedBy = "DateProcessedByCEO";
                            }
                            approversNote.append(";");
                            changeApprovalStatus(Recruitment.RECRUITMENT_STATUSID_OPEN, dateProcessedBy, approversNote.toString());
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please input a reason for rejection");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == containersAttachments){
            linearNavFragmentActivity.changePage(new RFADetailAttachmentFragment());
        }else if(v == actionbarButtonBack || v == actionbarTitle){
            linearNavFragmentActivity.onBackPressed();
        }else if(v == buttonApprove){
            etextDialogApproveReason.setText("");
            dialogApprove.show();
        }else if(v == buttonReject){
            etextDialogRejectReason.setText("");
            dialogReject.show();
        }else if(v == buttonReopen) {
            etextDialogReopenReason.setText("");
            dialogReopen.show();
        }
    }

    private void changeApprovalStatus(final int statusID, final String keyForUpdatableDate, final String approverNotes){
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                String tempResult;
                try{
                    tempResult = app.onlineGateway.saveRecruitment(activity.recruitment.getJSONFromUpdatingRecruitment(statusID, keyForUpdatableDate, approverNotes, app), activity.recruitment.jsonize(app));
                }catch(Exception e){
                    tempResult = e.getMessage();
                }
                final String result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(result.equals("OK")){
                            activity.finishLoading();
                            Toast.makeText(activity, "Updated Successfully!", Toast.LENGTH_SHORT).show();
                            activity.finish();
                        }else{
                            activity.finishLoading(result);
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(activity.recruitment == null) {
            activity.startLoading();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Object tempResult;
                    try {
                        tempResult = app.onlineGateway.getRecruitmentDetail(activity.recruitmentHeaderID);
                    } catch(Exception e) {
                        tempResult = e.getMessage();
                    }
                    final Object result = tempResult;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if(result instanceof String)
                                activity.finishLoading(result.toString());
                            else{
                                try {
                                    activity.recruitment = new Recruitment((JSONObject) result);
                                    updateView();
                                    activity.finishLoading();
                                }catch(Exception e) {
                                    e.printStackTrace();
                                    activity.finishLoading(e.getMessage());
                                }
                            }
                        }
                    });
                }
            }).start();
        }else {
            try {
                updateView();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void updateView()throws Exception{
        fieldName.setText(activity.recruitment.getRequesterName());
        fieldEmail.setText(activity.recruitment.getEmail());
        fieldOffice.setText(activity.recruitment.getRequesterOfficeName());
        fieldDepartment.setText(activity.recruitment.getRequesterDepartmentName());
        fieldPhone.setText(activity.recruitment.getRequesterPhoneNumber());
        fieldCM.setText(activity.recruitment.getCMName());
        fieldDateRequested.setText(activity.recruitment.getDateRequested(app));
        fieldDateProcessedbyCM.setText(activity.recruitment.getDateProcessedByCM(app));
        fieldDateProcessedbyRHM.setText(activity.recruitment.getDateProcessedByRHM(app));
        fieldDateProcessedbyRM.setText(activity.recruitment.getDateProcessedBYRM(app));
        fieldDateProcessedbyMHR.setText(activity.recruitment.getDateProcessedByHR(app));
        fieldDateProcessedbyCEO.setText(activity.recruitment.getDateProcessedBYCEO(app));
        fieldPosition.setText(activity.recruitment.getPositionTypeName());
//        fieldPositionFor.setText(activity.recruitment.getReplacementFor());
        fieldReasonForPosition.setText(activity.recruitment.getReason());
        fieldOfficeOfDeployment.setText(activity.recruitment.getOfficeOfDeploymentName());
        fieldDepartmentOfDeployment.setText(activity.recruitment.getDepartmentToBeAssignedName());
        fieldTargettedStartDate.setText(activity.recruitment.getTargettedStartDate(app));
        fieldJobTitle.setText(activity.recruitment.getJobTitle());
        fieldPositionCategory.setText(activity.recruitment.getEmployeeCategoryName());
        fieldRevenue.setText(activity.recruitment.getAnnualRevenue() + " USD");
        fieldSalaryRange.setText(activity.recruitment.getSalaryRangeFrom() + " - " + activity.recruitment.getSalaryRangeTo() + " USD");
        fieldBonus.setText(activity.recruitment.getGrossBaseBonus() + " USD");
        fieldTimeBase.setText(activity.recruitment.getTimeBaseTypeName());
        fieldEmployment.setText(activity.recruitment.getEmploymentTymeName());
        fieldHoursPerWeek.setText(String.valueOf(activity.recruitment.getHorsePerWeek()));
        cboxBudgettedCost.setChecked(activity.recruitment.isBudgetedCost());
        cboxSpecificPerson.setChecked(activity.recruitment.isSpecificPerson());
        cboxPositionMayBePermanent.setChecked(activity.recruitment.isPositionMayBePermanent());
        fieldSeverancePayment.setText(activity.recruitment.getSeverancePayment());

        for(Recruitment.Benefit benefit :activity.recruitment.getBenefits()) {
            TextView tv = (TextView) activity.getLayoutInflater().inflate(R.layout.node_fragmentdetaillistitem, null);
            tv.setText(benefit.getBenefitName());
            tv.setTypeface(SaltApplication.myFont(activity));
            containerBenefits.addView(tv);
        }
        stringNotes = new StringBuilder();
        approversNote = new StringBuilder(activity.recruitment.getApproverNote());
        if(!activity.recruitment.getApproverNote().isEmpty()) {
            String tempStrings[] = activity.recruitment.getApproverNote().split(";");
            for (String str : tempStrings){
                stringNotes.append(str);
                stringNotes.append(System.getProperty("line.separator"));
            }
        } else {
            stringNotes.append(activity.recruitment.getApproverNote());
        }

        fieldApproverNote.setText(stringNotes.toString());

        if (activity.recruitment.getPositionTypeID() == Recruitment.TYPEKEY_REPLACEMENT) {
            tablerowSeveranceDetails.setVisibility(View.GONE);
            radioReplacementYes.setEnabled(true);
            radioReplacementNo.setEnabled(true);
            if(activity.recruitment.isRequiredReplacement()) {
                radioReplacementYes.setChecked(true);
            }else
                radioReplacementNo.setChecked(true);
        } else {
            radioReplacementYes.setEnabled(false);
            radioReplacementNo.setEnabled(false);
            tablerowSeveranceDetails.setVisibility(View.GONE);
            if (activity.recruitment.getPositionTypeID() == Recruitment.TYPEKEY_DISMISSALS) {
                tviewReasonFor.setText("Reason for Dismissal");
                tablerowSeveranceDetails.setVisibility(View.VISIBLE);
            }
        }
    }
}
