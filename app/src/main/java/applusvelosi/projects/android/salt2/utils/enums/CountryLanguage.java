package applusvelosi.projects.android.salt2.utils.enums;

import java.util.HashMap;
import java.util.Map;

public class CountryLanguage {

	private static Map<String,String> languageHash;

	static {
		languageHash = new HashMap<String, String>();
		languageHash.put("ar", "United Arab Emirates");
		languageHash.put("en", "United States");
		languageHash.put("ru", "Russia");
		languageHash.put("es", "Spain");
	}

	public static Map<String, String> getLanguageHash() {
		return languageHash;
	}

	public static String getValueOfLanguage(String key){
		return languageHash.get(key);
	}
}
