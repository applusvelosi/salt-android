package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.LeaveDetailActivity;

/**
 * Created by rickroydaban on 03/05/16.
 */
public class MyLeavesListFragment extends Fragment implements MyLeavesFragment.MyLeavesDataUpdateObserver, ListAdapterInterface, AdapterView.OnItemClickListener {
    private static MyLeavesListFragment instance;

    private ListView lv;
    private ListAdapter adapter;
    private List<Leave> myLeaves;
    private HashMap<Integer, Integer> mapPositions;

    public static MyLeavesListFragment getInstance(){
        if(instance == null)
            instance = new MyLeavesListFragment();
        return instance;
    }

    @Override
    public void update(List<Leave> myLeaves, HashMap<Integer, Integer> mapPositions) {
        if(this.myLeaves != null){
            this.myLeaves.clear();
            this.myLeaves.addAll(myLeaves);
            adapter.notifyDataSetChanged();
        }

        if(this.mapPositions != null){
            this.mapPositions.clear();
            this.mapPositions.putAll(mapPositions);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        lv = (ListView)inflater.inflate(R.layout.fragment_myleaves_list, null);
        myLeaves = new ArrayList<Leave>();
        mapPositions = new HashMap<Integer, Integer>();
        adapter = new ListAdapter(this);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        return lv;
    }

    @Override
    public View getView(int pos, View recyclableView, ViewGroup parent) {
        View view = recyclableView;
        LeaveNodeHolder holder;

        if(view == null){
            holder = new LeaveNodeHolder();
            view = getActivity().getLayoutInflater().inflate(R.layout.node_headerdetailstatus, null);
            holder.typeTV = (TextView)view.findViewById(R.id.tviews_nodes_headerdetailstatus_header);
            holder.statusTV = (TextView)view.findViewById(R.id.tviews_nodes_headerdetailstatus_status);
            holder.datesTV = (TextView)view.findViewById(R.id.tviews_nodes_headerdetailstatus_detail);

            view.setTag(holder);
        }

        holder = (LeaveNodeHolder)view.getTag();
        Leave leave = myLeaves.get(pos);
        holder.typeTV.setText(leave.getTypeDescription());
        holder.statusTV.setText(leave.getStatusDescription());
        if(leave.getStatusID() == Leave.LEAVESTATUSAPPROVEDKEY) holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.green));
        else if(leave.getStatusID() == Leave.LEAVESTATUSPENDINGID) holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.black));
        else holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.red));

        if(leave.getEndDate() != null)
            holder.datesTV.setText(leave.getStartDate()+" - "+leave.getEndDate());
        else
            holder.datesTV.setText(leave.getStartDate());

        return view;
    }

    @Override
    public int getCount() {
        return myLeaves.size();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), LeaveDetailActivity.class);
        intent.putExtra(LeaveDetailActivity.INTENTKEY_LEAVEPOS, mapPositions.get(myLeaves.get(position).getLeaveID()));
        startActivity(intent);
    }

    private class LeaveNodeHolder{
        public TextView typeTV, statusTV, datesTV;
    }

}
