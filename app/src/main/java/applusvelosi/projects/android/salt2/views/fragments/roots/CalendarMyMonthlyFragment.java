package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.adapters.grids.MyCalendarAdapter;
import applusvelosi.projects.android.salt2.adapters.spinners.SimpleSpinnerAdapter;
import applusvelosi.projects.android.salt2.models.CalendarEvent;
import applusvelosi.projects.android.salt2.models.CalendarEventLeave;
import applusvelosi.projects.android.salt2.models.CalendarItem;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.utils.customviews.ListAdapter;
import applusvelosi.projects.android.salt2.utils.interfaces.CalendarInterface;
import applusvelosi.projects.android.salt2.utils.interfaces.ListAdapterInterface;
import applusvelosi.projects.android.salt2.views.LeaveDetailActivity;

public class CalendarMyMonthlyFragment extends RootFragment implements OnItemSelectedListener, CalendarInterface {
	private static CalendarMyMonthlyFragment instance;
	//action bar buttons
	private RelativeLayout actionbarMenuButton, actionbarRefreshButton;
	private TextView actionbarNowButton, actionbarTitle;

	private ImageView buttonPrevMonth, buttonNextMonth;
	private Spinner spinnerMonth, spinnerYear;
	private GridView calendarView;

	private Calendar minCalendar, maxCalendar;

	private MyCalendarAdapter calendarAdapter;
	private ArrayList<Leave> leaves;
	private ArrayList<CountryHoliday> holidays;
	private HashMap<Integer, Integer> mapLeavePositions;

	private RelativeLayout dialogEventsContainer;
	private ListView dialogEventsLV;
	private AlertDialog dialogEvents;


	public static CalendarMyMonthlyFragment getInstance(){
		if(instance == null)
			instance = new CalendarMyMonthlyFragment();

		return instance;
	}

	public static void removeInstance(){
		if(instance != null)
			instance = null;
	}

	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_staffcalendar, null);
		actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarNowButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_now);
		actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
		actionbarTitle.setText("My Calendar");

		actionbarMenuButton.setOnClickListener(this);
		actionbarRefreshButton.setOnClickListener(this);
		actionbarNowButton.setOnClickListener(this);
		return actionbarLayout;
	}

	@Override
	public View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_myleaves_calendar, null);
		buttonPrevMonth = (ImageView)v.findViewById(R.id.buttons_calendarview_prevmonth);
		buttonNextMonth = (ImageView)v.findViewById(R.id.buttons_calendarview_nextmonth);
		spinnerMonth = (Spinner) v.findViewById(R.id.choices_calendarview_month);
		spinnerYear = (Spinner ) v.findViewById(R.id.choices_calendarview_year);
		calendarView = (GridView) v.findViewById(R.id.calendarview);

		app = ((SaltApplication)getActivity().getApplication());
		minCalendar = Calendar.getInstance();
		minCalendar.set(Calendar.MONTH, Calendar.JANUARY);
		minCalendar.set(Calendar.YEAR, Integer.parseInt(app.dropDownYears.get(0)));
		maxCalendar = Calendar.getInstance();
		maxCalendar.set(Calendar.MONTH, Calendar.DECEMBER);
		maxCalendar.set(Calendar.YEAR, Integer.parseInt(app.dropDownYears.get(app.dropDownYears.size()-1)));

		mapLeavePositions = new HashMap<Integer, Integer>();
		leaves = new ArrayList<Leave>();
		holidays = new ArrayList<CountryHoliday>();
		spinnerMonth.setAdapter(new SimpleSpinnerAdapter(getActivity(), app.dropDownMonths, SimpleSpinnerAdapter.NodeSize.SIZE_NORMAL));
		spinnerYear.setAdapter(new SimpleSpinnerAdapter(getActivity(), app.dropDownYears, SimpleSpinnerAdapter.NodeSize.SIZE_NORMAL));
		spinnerMonth.setSelection(Calendar.getInstance().get(Calendar.MONTH));
		spinnerYear.setSelection(app.dropDownYears.indexOf(String.valueOf(Calendar.getInstance().get(Calendar.YEAR))));



		calendarAdapter = new MyCalendarAdapter(this, minCalendar, maxCalendar, spinnerMonth, spinnerYear, buttonPrevMonth, buttonNextMonth, leaves, holidays);
		calendarView.setAdapter(calendarAdapter);

		buttonPrevMonth.setOnClickListener(this);
		buttonNextMonth.setOnClickListener(this);
		spinnerMonth.setOnItemSelectedListener(this);
		spinnerYear.setOnItemSelectedListener(this);

		dialogEventsContainer = (RelativeLayout) inflater.inflate(R.layout.dialog_list, null);
		((TextView) dialogEventsContainer.findViewById(R.id.tviews_dialogs_list_header)).setText("Events");
		dialogEventsLV = (ListView) dialogEventsContainer.findViewById(R.id.lists_dialogs_list);
		dialogEvents = new AlertDialog.Builder(getActivity()).setView(dialogEventsContainer)
				.setNeutralButton("Close", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).create();

		reloadAppHolidays();
		return v;
	}

	void reloadAppHolidays(){
		activity.startLoading();
		new Thread(new Runnable() {
			Object tempHolidayResult, tempLeaveResult;

			@Override
			public void run() {
				try{
					tempHolidayResult = app.onlineGateway.getOfficeHolidaysOrErrorMessage(app.getStaff().getOfficeID());
					tempLeaveResult = app.onlineGateway.getMyPendingAndApprovedLeaves();
				}catch(Exception e){
					tempHolidayResult = e.getMessage();
				}

				final Object holidayResult = tempHolidayResult;
				final Object leaveResult = tempLeaveResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {

					@Override
					public void run() {
						if(holidayResult instanceof String && !holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR))
							activity.finishLoading(holidayResult.toString());
						else if(leaveResult instanceof String && !holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR))
							activity.finishLoading(leaveResult.toString());
						else {
							holidays.clear();
							leaves.clear();
							if (leaveResult == null || leaveResult.toString().contains(SaltApplication.CONNECTION_ERROR) || holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR)) {
								activity.finishLoadingAndShowOutdatedData();
								if (holidayResult.toString().contains(SaltApplication.CONNECTION_ERROR))
									holidays.addAll(app.offlineGateway.deserializeMyCalendarHolidays());
								if (leaveResult == null || leaveResult.toString().contains(SaltApplication.CONNECTION_ERROR)) {
									leaves.addAll(app.offlineGateway.deserializeMyLeaves());
								}
							} else {
								ArrayList<Leave> tempLeaves = (ArrayList<Leave>) leaveResult;
								ArrayList<CountryHoliday> tempHolidays = (ArrayList<CountryHoliday>) holidayResult;
								holidays.addAll(tempHolidays);
								leaves.addAll(tempLeaves);
								app.offlineGateway.serializeMyLeaves(tempLeaves);
								app.offlineGateway.serializeMyCalendarHolidays(tempHolidays);
							}

							activity.finishLoading();
							calendarAdapter.updateItems();
						}
					}
				});
			}
		}).start();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		if(parent == calendarView){
			System.out.println("Calendar item selected");
		}else{
			calendarAdapter.updateItems();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {}

	@Override
	public void onClick(View v) {
		if(v == actionbarMenuButton){
			actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
			activity.toggleSidebar(actionbarMenuButton);
		}else if(v == actionbarRefreshButton){
			reloadAppHolidays();
		}else if(v == actionbarNowButton){
			calendarAdapter.showThisMonth();
		}else if(v == buttonPrevMonth){
			calendarAdapter.prevMonth();
		}else if(v == buttonNextMonth){
			calendarAdapter.nextMonth();
		}
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		buttonPrevMonth.setEnabled(false);
		spinnerMonth.setEnabled(false);
		calendarView.setEnabled(false);
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		buttonPrevMonth.setEnabled(true);
		spinnerMonth.setEnabled(true);
		calendarView.setEnabled(true);
	}

	@Override
	public void onCalendarItemClicked(final CalendarItem calendarItem) {
		try {
			dialogEventsLV.setAdapter(new ListAdapter(new ListAdapterInterface() {
				@Override
				public View getView(int pos, View recyclableView, ViewGroup parent) {
					View view = recyclableView;
					LeaveNodeHolder holder;

					if (view == null) {
						holder = new LeaveNodeHolder();
						view = getActivity().getLayoutInflater().inflate(R.layout.node_headerdetailstatus, null);
						holder.disclosureIndicator = (ImageView)view.findViewById(R.id.iviews_nodes_next);
						holder.typeTV = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_header);
						holder.statusTV = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_status);
						holder.datesTV = (TextView) view.findViewById(R.id.tviews_nodes_headerdetailstatus_detail);
						view.setTag(holder);
					}

					holder = (LeaveNodeHolder) view.getTag();
					CalendarEvent calendarEvent = calendarItem.getEvents().get(pos);
					if (calendarEvent instanceof CalendarEventLeave) {
						Leave leave = ((CalendarEventLeave) calendarItem.getEvents().get(pos)).getLeave();
						holder.typeTV.setText(leave.getTypeDescription());
						holder.statusTV.setText(leave.getStatusDescription());
						if (leave.getStatusID() == Leave.LEAVESTATUSAPPROVEDKEY)
							holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.green));
						else if (leave.getStatusID() == Leave.LEAVESTATUSPENDINGID)
							holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.black));
						else
							holder.statusTV.setTextColor(getActivity().getResources().getColor(R.color.red));

						if (leave.getEndDate() != null)
							holder.datesTV.setText(leave.getStartDate() + " - " + leave.getEndDate());
						else
							holder.datesTV.setText(leave.getStartDate());
					} else {
						holder.disclosureIndicator.setVisibility(View.INVISIBLE);
						holder.typeTV.setText(calendarEvent.getName());
					}

					return view;
				}

				@Override
				public int getCount() {
					return calendarItem.getEvents().size();
				}
			}));
			dialogEventsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if(calendarItem.getEvents().get(position) instanceof CalendarEventLeave ) {
						Intent intent = new Intent(getActivity(), LeaveDetailActivity.class);
						intent.putExtra(LeaveDetailActivity.INTENTKEY_LEAVEPOS, mapLeavePositions.get(((CalendarEventLeave) calendarItem.getEvents().get(position)).getLeave().getLeaveID()));
						startActivity(intent);
					}

				}
			});
			dialogEvents.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private class LeaveNodeHolder{
		public TextView typeTV, statusTV, datesTV;
		public ImageView disclosureIndicator;
	}

}

