package applusvelosi.projects.android.salt2.models.giftshospitality;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import applusvelosi.projects.android.salt2.SaltApplication;

/**
 * Created by Velosi on 28/03/2017.
 */

public class GiftHospitality implements Serializable {

    private float amountInEuro;
    private int applusCsrId, countryId, cmId, currencyId;
    private String applusCrsName, approverNote, cmEmail, companyName, countryName;
    private String currencyName, currencySymbol, description;
    private String dateCreated, dateReceivedGiven, dateSubmitted, dateRejected, dateProcessedByCM, dateProcessedByRM, dateProcessedByCEO, dateApprovedByApplus;
    private float exchangeRate;
    private String giftType;
    private int giftHospitalityId;
    private boolean isActive, isDonation, isPublicOfficial, isReceivedGiven;
    private int processedByCMId, processedByRMId, processedByCEOId;
    private String processedByCMName, processedByRMName, processedByCEOName;
    private String rmEmail, reason, recipientCity;
    private List<GiftRecipient> recipients;
    private String referenceNo;
    private int rmId;
    private int requestorId;
    private String requestorEmail, requestorName;
    private int requestorOfficeId;
    private String requestorOfficeName;
    private int statusId;
    private String statusName;
    private float totalAmountInLc;


    private static final String GIFTHOSPITALITYTYPE_OPEN = "60-Open";
    private static final String GIFTHOSPITALITYTYPE_SUBMITTED = "61-Submitted";
    private static final String GIFTHOSPITALITYTYPE_CANCELLED = "62-Cancelled";
    private static final String GIFTHOSPITALITYTYPE_APPROVEDBYCM = "63-Approved by CM";
    private static final String GIFTHOSPITALITYTYPE_REJECTEDBYCM = "64-Rejected by CM";
    private static final String GIFTHOSPITALITYTYPE_APPROVEDBYRM = "65-Approved by RM";
    private static final String GIFTHOSPITALITYTYPE_REJECTEDBYRM = "66-Rejected by RM";
    private static final String GIFTHOSPITALITYTYPE_APPROVEDBYCEO = "67-Approved by CEO";
    private static final String GIFTHOSPITALITYTYPE_REJECTEDBYCEO = "68-Approved by CEO";
    private static final String GIFTHOSPITALITYTYPE_APPROVEDBYAPPLUS = "90-Approved by Applus";
    private static final String GIFTHOSPITALITYTYPE_REJECTEDBYAPPLUS = "91-Rejected by Applus";

    public static int GIFTHOSPITALITYID_OPEN, GIFTHOSPITALITYID_SUBMITTED,  GIFTHOSPITALITYID_CANCELLED, GIFTHOSPITALITYID_APPROVEDBYCM, GIFTHOSPITALITYRID_REJECTEDBYCM,
            GIFTHOSPITALITYID_APPROVEDBYRM, GIFTHOSPITALITYID_REJECTEDBYRM, GIFTHOSPITALITYID_APPROVEDBYCEO, GIFTHOSPITALITYID_REJECTEDBYCEO,
            GIFTHOSPITALITYID_APPROVEDBYAPPLUS, GIFTHOSPITALITYID_REJECTEDBYAPPLUS;

    private static String GIFTHOSPITALITYDESC_OPEN, GIFTHOSPITALITYDESC_SUBMITTED, GIFTHOSPITALITYDESC_APPROVEDBYCM, GIFTHOSPITALITYDESC_REJECTEDBYCM,
            GIFTHOSPITALITYDESC_APPROVEDBYRM, GIFTHOSPITALITYDESC_REJECTEDBYRM, GIFTHOSPITALITYDESC_CANCELLED,
            GIFTHOSPITALITYDESC_APPROVEDBYCEO, GIFTHOSPITALITYDESC_REJECTEDBYCEO, GIFTHOSPITALITYDESC_APPROVEDBYAPPLUS, GIFTHOSPITALITYDESC_REJECTEDBYAPPLUS;

//    private static ArrayList<String> statusDescriptionList;

    static {
        GIFTHOSPITALITYID_OPEN = Integer.parseInt(GIFTHOSPITALITYTYPE_OPEN.split("-")[0]);
        GIFTHOSPITALITYID_SUBMITTED = Integer.parseInt(GIFTHOSPITALITYTYPE_SUBMITTED.split("-")[0]);
        GIFTHOSPITALITYID_CANCELLED = Integer.parseInt(GIFTHOSPITALITYTYPE_CANCELLED.split("-")[0]);
        GIFTHOSPITALITYID_APPROVEDBYCM = Integer.parseInt(GIFTHOSPITALITYTYPE_APPROVEDBYCM.split("-")[0]);
        GIFTHOSPITALITYRID_REJECTEDBYCM = Integer.parseInt(GIFTHOSPITALITYTYPE_REJECTEDBYCM.split("-")[0]);
        GIFTHOSPITALITYID_APPROVEDBYRM = Integer.parseInt(GIFTHOSPITALITYTYPE_APPROVEDBYRM.split("-")[0]);
        GIFTHOSPITALITYID_REJECTEDBYRM = Integer.parseInt(GIFTHOSPITALITYTYPE_REJECTEDBYRM.split("-")[0]);
        GIFTHOSPITALITYID_APPROVEDBYCEO = Integer.parseInt(GIFTHOSPITALITYTYPE_APPROVEDBYCEO.split("-")[0]);
        GIFTHOSPITALITYID_REJECTEDBYCEO = Integer.parseInt(GIFTHOSPITALITYTYPE_REJECTEDBYCEO.split("-")[0]);
        GIFTHOSPITALITYID_APPROVEDBYAPPLUS = Integer.parseInt(GIFTHOSPITALITYTYPE_APPROVEDBYAPPLUS.split("-")[0]);
        GIFTHOSPITALITYID_REJECTEDBYAPPLUS = Integer.parseInt(GIFTHOSPITALITYTYPE_REJECTEDBYAPPLUS.split("-")[0]);

        GIFTHOSPITALITYDESC_OPEN = GIFTHOSPITALITYTYPE_OPEN.split("-")[1];
        GIFTHOSPITALITYDESC_SUBMITTED = GIFTHOSPITALITYTYPE_SUBMITTED.split("-")[1];
        GIFTHOSPITALITYDESC_CANCELLED = GIFTHOSPITALITYTYPE_CANCELLED.split("-")[1];
        GIFTHOSPITALITYDESC_APPROVEDBYCM = GIFTHOSPITALITYTYPE_APPROVEDBYCM.split("-")[1];
        GIFTHOSPITALITYDESC_REJECTEDBYCM = GIFTHOSPITALITYTYPE_REJECTEDBYCM.split("-")[1];
        GIFTHOSPITALITYDESC_APPROVEDBYRM = GIFTHOSPITALITYTYPE_APPROVEDBYRM.split("-")[1];
        GIFTHOSPITALITYDESC_REJECTEDBYRM = GIFTHOSPITALITYTYPE_REJECTEDBYRM.split("-")[1];
        GIFTHOSPITALITYDESC_APPROVEDBYCEO = GIFTHOSPITALITYTYPE_APPROVEDBYCEO.split("-")[1];
        GIFTHOSPITALITYDESC_REJECTEDBYCEO = GIFTHOSPITALITYTYPE_REJECTEDBYCEO.split("-")[1];
        GIFTHOSPITALITYDESC_APPROVEDBYAPPLUS = GIFTHOSPITALITYTYPE_APPROVEDBYAPPLUS.split("-")[1];
        GIFTHOSPITALITYDESC_REJECTEDBYAPPLUS = GIFTHOSPITALITYTYPE_REJECTEDBYAPPLUS.split("-")[1];
    }

    public GiftHospitality(JSONObject jsonGiftHospitality) throws Exception{
        isActive = jsonGiftHospitality.getBoolean("Active");
        amountInEuro = (float)jsonGiftHospitality.getDouble("AmountInEUR");
        applusCsrId = jsonGiftHospitality.getInt("ApplusCsrId");
        applusCrsName = jsonGiftHospitality.getString("ApplusCsrName");
        approverNote = jsonGiftHospitality.getString("ApproverNote");
        cmEmail = jsonGiftHospitality.getString("CMEmail");
        companyName = jsonGiftHospitality.getString("CompanyName");
        countryId = jsonGiftHospitality.getInt("CountryID");
        cmId = jsonGiftHospitality.getInt("CountryManager");
        countryName = jsonGiftHospitality.getString("CountryName");
        currencyId = jsonGiftHospitality.getInt("CurrencyID");
        currencyName = jsonGiftHospitality.getString("CurrencyName");
        currencySymbol = jsonGiftHospitality.getString("CurrencySymbol");
        dateApprovedByApplus = jsonGiftHospitality.getString("DateApprovedApplus");
        dateCreated = jsonGiftHospitality.getString("DateCreated");
        dateProcessedByCEO = jsonGiftHospitality.getString("DateProcessedByCEO");
        dateProcessedByCM = jsonGiftHospitality.getString("DateProcessedByCountryManager");
        dateProcessedByRM = jsonGiftHospitality.getString("DateProcessedByRegionalManager");
        dateReceivedGiven = jsonGiftHospitality.getString("DateReceivedGiven");
        dateRejected = jsonGiftHospitality.getString("DateRejected");
        dateSubmitted = jsonGiftHospitality.getString("DateSubmitted");
        description = jsonGiftHospitality.getString("Description");
        exchangeRate = (float)jsonGiftHospitality.getDouble("ExchangeRate");
        giftType = jsonGiftHospitality.getString("GiftType");
        giftHospitalityId = jsonGiftHospitality.getInt("GiftsHospitalityID");
        isDonation = jsonGiftHospitality.getBoolean("IsDonation");
        isPublicOfficial = jsonGiftHospitality.getBoolean("IsPublicOfficial");
        isReceivedGiven = jsonGiftHospitality.getBoolean("IsReceivedGiven");
        processedByCMId = jsonGiftHospitality.getInt("ProcessedByCMId");
        processedByCMName = jsonGiftHospitality.getString("ProcessedByCMName");
        processedByCEOId = jsonGiftHospitality.getInt("ProcessedByCeoID");
        processedByCEOName = jsonGiftHospitality.getString("ProcessedByCeoName");
        processedByRMId = jsonGiftHospitality.getInt("ProcessedByRMId");
        processedByRMName = jsonGiftHospitality.getString("ProcessedByRMName");
        reason = jsonGiftHospitality.getString("Reason");
        recipientCity = jsonGiftHospitality.getString("RecipientCity");
        rmEmail = jsonGiftHospitality.getString("RMEmail");
        JSONArray jsonRecipients = jsonGiftHospitality.getJSONArray("Recipients");
        recipients = new ArrayList<GiftRecipient>();
        for (int i = 0; i <jsonRecipients.length(); i++)
            recipients.add(new GiftRecipient(jsonRecipients.getJSONObject(i)));
        referenceNo = jsonGiftHospitality.getString("ReferenceNo");
        rmId = jsonGiftHospitality.getInt("RegionalManager");
        requestorEmail = jsonGiftHospitality.getString("RequestorEmail");
        requestorId = jsonGiftHospitality.getInt("RequestorID");
        requestorName = jsonGiftHospitality.getString("RequestorName");
        requestorOfficeId = jsonGiftHospitality.getInt("RequestorOfficeID");
        requestorOfficeName = jsonGiftHospitality.getString("RequestorOfficeName");
        statusId = jsonGiftHospitality.getInt("StatusID");
        statusName = jsonGiftHospitality.getString("StatusName");
        totalAmountInLc = (float) jsonGiftHospitality.getDouble("TotalAmount");
    }

    public String getDateApprovedByApplus(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateApprovedByApplus);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899")) ? "" : tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }


    public String getDateCreated(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateCreated);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDateProcessedByCEO(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateProcessedByCEO);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDateProcessedByCM(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateProcessedByCM);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDateProcessedByRM(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateProcessedByRM);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDateReceivedGiven(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDate(dateReceivedGiven);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDateRejected(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateRejected);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getDateSubmitted(SaltApplication app) {
        try {
            String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateSubmitted);
            return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
        }catch (Exception e) {
            return e.getMessage();
        }
    }

    public static String getGHRStatusDescriptionForKey(int key){
        if (key == GIFTHOSPITALITYID_OPEN)
            return GIFTHOSPITALITYDESC_OPEN;
        else if(key == GIFTHOSPITALITYID_SUBMITTED)
            return GIFTHOSPITALITYDESC_SUBMITTED;
        else if (key == GIFTHOSPITALITYID_CANCELLED)
            return GIFTHOSPITALITYDESC_CANCELLED;
        else if (key == GIFTHOSPITALITYID_APPROVEDBYCM)
            return GIFTHOSPITALITYDESC_APPROVEDBYCM;
        else if (key == GIFTHOSPITALITYRID_REJECTEDBYCM)
            return GIFTHOSPITALITYDESC_REJECTEDBYCM;
        else if (key == GIFTHOSPITALITYID_APPROVEDBYRM)
            return GIFTHOSPITALITYDESC_APPROVEDBYRM;
        else if (key == GIFTHOSPITALITYID_REJECTEDBYRM)
            return GIFTHOSPITALITYDESC_REJECTEDBYRM;
        else if (key == GIFTHOSPITALITYID_APPROVEDBYCEO)
            return GIFTHOSPITALITYDESC_APPROVEDBYCEO;
        else if (key == GIFTHOSPITALITYID_REJECTEDBYCEO)
            return GIFTHOSPITALITYDESC_REJECTEDBYCEO;
        else if (key == GIFTHOSPITALITYID_APPROVEDBYAPPLUS)
            return GIFTHOSPITALITYDESC_APPROVEDBYAPPLUS;
        else
            return GIFTHOSPITALITYDESC_REJECTEDBYAPPLUS;
    }

    public float getAmountInEuro() {
        return amountInEuro;
    }

    public int getApplusCsrId() {
        return applusCsrId;
    }

    public String getApplusCrsName() {
        return applusCrsName;
    }

    public String getApproverNote() {
        return approverNote;
    }

    public String getCmEmail() {
        return cmEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public int getCountryId() {
        return countryId;
    }

    public int getCmId() {
        return cmId;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getDescription() {
        return description;
    }

    public float getExchangeRate() {
        return exchangeRate;
    }

    public String getGiftType() {
        return giftType;
    }

    public int getGiftHospitalityId() {
        return giftHospitalityId;
    }

    public boolean isDonation() {
        return isDonation;
    }

    public boolean isPublicOfficial() {
        return isPublicOfficial;
    }

    public boolean isReceivedGiven() {
        return isReceivedGiven;
    }

    public int getProcessedByCMId() {
        return processedByCMId;
    }

    public String getProcessedByCMName() {
        return processedByCMName;
    }

    public int getProcessedByCEOId() {
        return processedByCEOId;
    }

    public String getProcessedByCEOName() {
        return processedByCEOName;
    }

    public int getProcessedByRMId() {
        return processedByRMId;
    }

    public String getProcessedByRMName() {
        return processedByRMName;
    }

    public String getRmEmail() {
        return rmEmail;
    }

    public String getReason() {
        return reason;
    }

    public String getRecipientCity() {
        return recipientCity;
    }

    public List<GiftRecipient> getRecipients() {
        return recipients;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public int getRmId() {
        return rmId;
    }

    public int getRequestorId() {
        return requestorId;
    }

    public String getRequestorEmail() {
        return requestorEmail;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public int getRequestorOfficeId() {
        return requestorOfficeId;
    }

    public String getRequestorOfficeName() {
        return requestorOfficeName;
    }

    public int getStatusId() {
        return statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public float getTotalAmountInLc() {
        return totalAmountInLc;
    }

    public String getJSONFromUpdatingGift(int statusID, String keyForUpdatableDate, String approverNote, SaltApplication app) throws Exception {
        Map<String, Object> tempMap = new HashMap<String, Object>();
        tempMap.put("Active", isActive);
        tempMap.put("AmountInEUR", amountInEuro);
        tempMap.put("ApplusCsrId", applusCsrId);
        tempMap.put("ApplusCsrName", applusCrsName);
        tempMap.put("ApproverNote", approverNote);
        tempMap.put("CMEmail", cmEmail);
        tempMap.put("CompanyName", companyName);
        tempMap.put("CountryID", countryId);
        tempMap.put("CountryManager", cmId);
        tempMap.put("CountryName", countryName);
        tempMap.put("CurrencyID", currencyId);
        tempMap.put("CurrencyName", currencyName);
        tempMap.put("CurrencySymbol", currencySymbol);
        tempMap.put("DateApprovedApplus", dateApprovedByApplus);
        tempMap.put("DateCreated", dateCreated);
        tempMap.put("DateProcessedByCEO", dateProcessedByCEO);
        tempMap.put("DateProcessedByCountryManager", dateProcessedByCM);
        tempMap.put("DateProcessedByRegionalManager", dateProcessedByRM);
        tempMap.put("DateRejected", dateRejected);
        tempMap.put("DateReceivedGiven", dateReceivedGiven);
        tempMap.put("DateSubmitted", dateSubmitted);
        tempMap.put("Description", description);
        tempMap.put("ExchangeRate", exchangeRate);
        tempMap.put("GiftType", giftType);
        tempMap.put("GiftsHospitalityID", giftHospitalityId);
        tempMap.put("IsDonation", isDonation);
        tempMap.put("IsPublicOfficial", isPublicOfficial);
        tempMap.put("IsReceivedGiven", isReceivedGiven);
        tempMap.put("ProcessedByCMId", processedByCMId);
        tempMap.put("ProcessedByCMName", processedByCMName);
        tempMap.put("ProcessedByCeoID", processedByCEOId);
        tempMap.put("ProcessedByCeoName", processedByCEOName) ;
        tempMap.put("ProcessedByRMId", processedByRMId);
        tempMap.put("ProcessedByRMName", processedByRMName);
        tempMap.put("RMEmail", rmEmail);
        tempMap.put("Reason", reason);
        tempMap.put("RecipientCity", recipientCity);
        List<JsonObject> jsonRecipients = new ArrayList<>();
        for (GiftRecipient recipient : recipients)
            jsonRecipients.add(recipient.getJSONObject());
        tempMap.put("Recipients", jsonRecipients);
        tempMap.put("ReferenceNo", referenceNo);
        tempMap.put("RegionalManager", rmId);
        tempMap.put("RequestorEmail", requestorEmail);
        tempMap.put("RequestorID", requestorId);
        tempMap.put("RequestorName", requestorName);
        tempMap.put("RequestorOfficeID", requestorOfficeId);
        tempMap.put("RequestorOfficeName", requestorOfficeName);
        tempMap.put("StatusID", statusID);
        tempMap.put("StatusName", statusName);
        tempMap.put("TotalAmount", totalAmountInLc);
        if(!keyForUpdatableDate.equals("NA")) {
            tempMap.put(keyForUpdatableDate, app.onlineGateway.epochizeDate(new Date()));
            if (app.getStaff().getStaffID() == processedByCEOId) {
                tempMap.put("DateProcessedByRegionalManager", app.onlineGateway.epochizeDate(new Date()));
                tempMap.put("DateProcessedByCEO", app.onlineGateway.epochizeDate(new Date()));
            }
        }
        return app.gson.toJson(tempMap, app.types.hashmapOfStringObject);
    }

    public String jsonize(SaltApplication app) {
        Map<String, Object> tempMap = new HashMap<String, Object>();

        tempMap.put("Active", isActive);
        tempMap.put("AmountInEUR", amountInEuro);
        tempMap.put("ApplusCsrId", applusCsrId);
        tempMap.put("ApplusCsrName", applusCrsName);
        tempMap.put("ApproverNote", approverNote);
        tempMap.put("CMEmail", cmEmail);
        tempMap.put("CompanyName", companyName);
        tempMap.put("CountryID", countryId);
        tempMap.put("CountryManager", cmId);
        tempMap.put("CountryName", countryName);
        tempMap.put("CurrencyID", currencyId);
        tempMap.put("CurrencyName", currencyName);
        tempMap.put("CurrencySymbol", currencySymbol);
        tempMap.put("DateApprovedApplus", dateApprovedByApplus);
        tempMap.put("DateCreated", dateCreated);
        tempMap.put("DateProcessedByCEO", dateProcessedByCEO);
        tempMap.put("DateProcessedByCountryManager", dateProcessedByCM);
        tempMap.put("DateProcessedByRegionalManager", dateProcessedByRM);
        tempMap.put("DateRejected", dateRejected);
        tempMap.put("DateSubmitted", dateSubmitted);
        tempMap.put("DateReceivedGiven", dateReceivedGiven);
        tempMap.put("Description", description);
        tempMap.put("ExchangeRate", exchangeRate);
        tempMap.put("GiftType", giftType);
        tempMap.put("GiftsHospitalityID", giftHospitalityId);
        tempMap.put("IsDonation", isDonation);
        tempMap.put("IsPublicOfficial", isPublicOfficial);
        tempMap.put("IsReceivedGiven", isReceivedGiven);
        tempMap.put("ProcessedByCMId", processedByCMId);
        tempMap.put("ProcessedByCMName", processedByCMName);
        tempMap.put("ProcessedByCeoID", processedByCEOId);
        tempMap.put("ProcessedByCeoName", processedByCEOName) ;
        tempMap.put("ProcessedByRMId", processedByRMId);
        tempMap.put("ProcessedByRMName", processedByRMName);
        tempMap.put("RMEmail", rmEmail);
        tempMap.put("Reason", reason);
        tempMap.put("RecipientCity", recipientCity);
        tempMap.put("Recipients", new ArrayList<>());
        tempMap.put("ReferenceNo", referenceNo);
        tempMap.put("RegionalManager", rmId);
        tempMap.put("RequestorEmail", requestorEmail);
        tempMap.put("RequestorID", requestorId);
        tempMap.put("RequestorName", requestorName);
        tempMap.put("RequestorOfficeID", requestorOfficeId);
        tempMap.put("RequestorOfficeName", requestorOfficeName);
        tempMap.put("StatusID", statusId);
        tempMap.put("StatusName", statusName);
        tempMap.put("TotalAmount", totalAmountInLc);

        return  app.gson.toJson(tempMap, app.types.hashmapOfStringObject);
    }
}

