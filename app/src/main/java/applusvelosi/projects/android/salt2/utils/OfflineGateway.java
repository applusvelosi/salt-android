package applusvelosi.projects.android.salt2.utils;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Holiday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.models.Office;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.claimheaders.BusinessAdvance;
import applusvelosi.projects.android.salt2.models.claimheaders.Claim;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimNotPaidByCC;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimPaidByCC;
import applusvelosi.projects.android.salt2.models.claimheaders.LiquidationOfBA;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;

public class OfflineGateway {

	private final String PREFERENCE_KEY = "saltsharedprefskey";
	//keys
	private final String KEYJSONSTAFF = "ogJSONStaff";
	private final String KEYJSONOFFICE = "ogJSONOffice";
	private final String KEYJSONMYLEAVES = "ogJSONMyLeaveList";
	private final String KEYJSONMYCLAIMS = "ogJSONMyClaimList";
	private final String KEYJSONNATIONALHOLIDAYS = "ogJSONNationalHolidays";
	private final String KEYJSONLOCALHOLIDAYS = "ogJSONLocalHolidays";
	private final String KEYMYCALENDARHOLIDAYS = "ogJSONMyCalendarHolidays";
	private final String KEYJSONCURRENCIES = "ogJSONCurrencies";

	private final String KEYLASTSIGNEDUSERNAME = "keylastsignedusername";

	//tutorials
	private final String KEY_TUTS_HOME = "keytutshome";
	private final String KEY_TUTS_MYLEAVES = "keytutsmyleaves";

	private SaltApplication app;
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	public OfflineGateway(SaltApplication app){
		this.app = app;
		prefs = app.getSharedPreferences(PREFERENCE_KEY, 0);
		editor = prefs.edit();
	}
	
	public boolean isLoggedIn(){
		return (prefs.contains(KEYJSONSTAFF))?true:false;
	}
	
	public void logout(){
		String lastSignedUsername = getLastSignedUsername();
		boolean hasFinishHomeTutorial = hasFinishedHomeTutorial();
		boolean hasFinishMyLeaveTutorial = hasFinishedMyLeaveTutorial();

		editor.clear().commit();

		editor.putString(KEYLASTSIGNEDUSERNAME, lastSignedUsername).commit();
		editor.putBoolean(KEY_TUTS_HOME, hasFinishHomeTutorial).commit();
		editor.putBoolean(KEY_TUTS_MYLEAVES, hasFinishMyLeaveTutorial).commit();
	}

	public void setLastSignedUsername(String lastSignedUsername){
		editor.putString(KEYLASTSIGNEDUSERNAME, lastSignedUsername).commit();
	}

	public String getLastSignedUsername(){
		return prefs.getString(KEYLASTSIGNEDUSERNAME, "");
	}

	public void serializeCurrencies(ArrayList<Currency> currencies){
		editor.putString(KEYJSONCURRENCIES, app.gson.toJson(currencies, app.types.arrayListOfCurrencies)).commit();
	}
	
	public void serializeStaffData(Staff staff, Office office){
		editor.putString(KEYJSONSTAFF, app.gson.toJson(staff, app.types.staff));
		editor.putString(KEYJSONOFFICE, app.gson.toJson(office.getMap(), app.types.hashmapOfStringString)).commit();
	}

	public void serializeNationHolidays(SaltApplication key, ArrayList<CountryHoliday> holidays){
		editor.putString(KEYJSONNATIONALHOLIDAYS, app.gson.toJson(holidays, app.types.arrayListOfHolidays)).commit();
	}
	
	public void serializeLocalHolidays(SaltApplication key, ArrayList<Holiday> holidays){
		editor.putString(KEYJSONLOCALHOLIDAYS, app.gson.toJson(holidays, app.types.arrayListOfLocalHolidays)).commit();
	}

    public void serializeMyCalendarHolidays(ArrayList<CountryHoliday> holidays){
        editor.putString(KEYMYCALENDARHOLIDAYS, app.gson.toJson(holidays, app.types.arrayListOfHolidays)).commit();
    }

	public void serializeMyLeaves(ArrayList<Leave> myLeaves){
		editor.putString(KEYJSONMYLEAVES, app.gson.toJson(myLeaves, app.types.arrayListOfLeaves)).commit();
	}

	public void serializeMyClaims(ArrayList<ClaimHeader> claimHeaders){
        editor.putString(KEYJSONMYCLAIMS, app.gson.toJson(claimHeaders, app.types.arrayListOfClaims)).commit();
    }

//	public void clearMyClaimItems(String key){
//		editor.remove(key).apply();
//	}

	public void serializeMyClaimItems(int claimID, List<ClaimItem> claimItems){
		editor.putString("CLAIM"+claimID, app.gson.toJson(claimItems, app.types.arrayListOfClaimItems)).commit();
	}

	public ArrayList<ClaimItem> deserializeMyClaimItems(int claimHeaderID){
		return app.gson.fromJson(prefs.getString("CLAIM"+claimHeaderID, "[]"), app.types.arrayListOfClaimItems);
	}

    public Staff deserializeStaff(){
		return app.gson.fromJson(prefs.getString(KEYJSONSTAFF, "{}"), app.types.staff);
	}
	
	public Office deserializeStaffOffice(){
		HashMap<String, String>staffOfficeMap = app.gson.fromJson(prefs.getString(KEYJSONOFFICE, "{}"), app.types.hashmapOfStringString);
		return new Office(staffOfficeMap);
	}
		
	public ArrayList<CountryHoliday> deserializeNationalHolidays(){
		return app.gson.fromJson(prefs.getString(KEYJSONNATIONALHOLIDAYS, "[]"), app.types.arrayListOfHolidays);
	}

	public ArrayList<Holiday> deserializeLocalHolidays(){
		return app.gson.fromJson(prefs.getString(KEYJSONLOCALHOLIDAYS, "[]"), app.types.arrayListOfLocalHolidays);
	}

	public ArrayList<CountryHoliday> deserializeMyCalendarHolidays(){
		return app.gson.fromJson(prefs.getString(KEYMYCALENDARHOLIDAYS, "[]"), app.types.arrayListOfHolidays);
	}

	public ArrayList<Leave> deserializeMyLeaves(){
		return app.gson.fromJson(prefs.getString(KEYJSONMYLEAVES, "[]"), app.types.arrayListOfLeaves);
	}

	public ArrayList<ClaimHeader> deserializeMyClaims() throws Exception{
		ArrayList<ClaimHeader> savedClaimHeaders = app.gson.fromJson(prefs.getString(KEYJSONMYCLAIMS, "[]"), app.types.arrayListOfClaims);
		ArrayList<ClaimHeader> claimHeaders = new ArrayList<ClaimHeader>();
		for(ClaimHeader savedClaimHeader :savedClaimHeaders){
			if(savedClaimHeader.getTypeID() == ClaimHeader.TYPEKEY_CLAIMS){
				Claim claim = new Claim(savedClaimHeader);
				claimHeaders.add((claim.isPaidByCC()) ? new ClaimPaidByCC(savedClaimHeader):new ClaimNotPaidByCC(savedClaimHeader));
			}else if(savedClaimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES)
				claimHeaders.add(new BusinessAdvance(savedClaimHeader));
			else if(savedClaimHeader.getTypeID() == ClaimHeader.TYPEKEY_LIQUIDATION)
				claimHeaders.add(new LiquidationOfBA(savedClaimHeader));
			else
				throw new Exception("Invalid Claim type. Please contact admin");
		}

		return claimHeaders;
	}

	public ArrayList<Currency> deserializeCurrencies(){
		return app.gson.fromJson(prefs.getString(KEYJSONCURRENCIES, "[]"), app.types.arrayListOfCurrencies);
	}

	public boolean hasFinishedHomeTutorial(){
		if(prefs.contains(KEY_TUTS_HOME))
			return true;
		else{
			editor.putBoolean(KEY_TUTS_HOME, true).commit();
			return false;
		}
	}

	public boolean hasFinishedMyLeaveTutorial(){
		if(prefs.contains(KEY_TUTS_MYLEAVES))
			return true;
		else{
			editor.putBoolean(KEY_TUTS_MYLEAVES, true).commit();
			return false;
		}
	}

}
