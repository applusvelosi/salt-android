package applusvelosi.projects.android.salt2.utils.enums;

public enum LeaveStatuses {

	Submitted,
	Cancelled,
	Approved,
	Rejected
	
}
