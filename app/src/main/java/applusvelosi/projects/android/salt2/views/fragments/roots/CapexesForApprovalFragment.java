package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.adapters.lists.CapexForApprovalAdapter;
import applusvelosi.projects.android.salt2.models.capex.CapexHeader;
import applusvelosi.projects.android.salt2.views.CapexApprovalDetailActivity;

/**
 * Created by Velosi on 10/9/15.
 */
public class CapexesForApprovalFragment extends RootFragment implements AdapterView.OnItemClickListener, TextWatcher{
    private static CapexesForApprovalFragment instance;
    //action bar buttons
    private RelativeLayout actionbarMenuButton, actionbarRefreshButton;

    private EditText etextSearchByName;
    private ListView lv;
    private CapexForApprovalAdapter adapter;
    private List<CapexHeader> capexes, tempCapex;

    public static CapexesForApprovalFragment getInstance(){
        if(instance == null)
            instance = new CapexesForApprovalFragment();
        return instance;
    }

    public static void removeInstance() {
        if (instance != null)
            instance = null;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        RelativeLayout actionbarLayout = (RelativeLayout)activity.getLayoutInflater().inflate(R.layout.actionbar_menurefresh, null);
        actionbarMenuButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
        actionbarRefreshButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
        ((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("CAPEX For Approval");
        actionbarMenuButton.setOnClickListener(this);
        actionbarRefreshButton.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listview, null);
        lv = (ListView)view.findViewById(R.id.lists_lv);
        capexes = new ArrayList<CapexHeader>();
        tempCapex = new ArrayList<CapexHeader>();
        etextSearchByName = (EditText)view.findViewById(R.id.etexts_itemforapproval_name);
        etextSearchByName.setHint("Search With Name or Office");
        etextSearchByName.setVisibility(View.VISIBLE);
        etextSearchByName.addTextChangedListener(this);

        adapter = new CapexForApprovalAdapter(activity, tempCapex);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        syncToServer();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        filteredCapexByName();
        syncToServer();
    }

    private void syncToServer(){
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object tempCapexForApprovalResult;

                try{
                    tempCapexForApprovalResult = app.onlineGateway.getCapexesForApproval();
                }catch(Exception e){
                    tempCapexForApprovalResult = e.getMessage();
                }

                final Object capexForApprovalResult = tempCapexForApprovalResult;

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(capexForApprovalResult instanceof String)
                            activity.finishLoading(capexForApprovalResult.toString());
                        else{
                            activity.finishLoading();
                            capexes.clear();
                            capexes.addAll((ArrayList<CapexHeader>)capexForApprovalResult);
                            filteredCapexByName();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }).start();
    }

    private void filteredCapexByName() {
        tempCapex.clear();
        if (etextSearchByName.getText().length() > 0){
            for (CapexHeader capex : capexes){
                if (capex.getRequesterName().toLowerCase().contains(etextSearchByName.getText().toString().toLowerCase()) || capex.getOfficeName().toLowerCase().contains(etextSearchByName.getText().toString().toLowerCase()) || etextSearchByName.getText().length() < 1)
                    tempCapex.add(capex);
            }
        }else
            tempCapex.addAll(capexes);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if(v == actionbarMenuButton){
            actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
            activity.toggleSidebar(actionbarMenuButton);
        }else if(v == actionbarRefreshButton){
            syncToServer();
        }
    }

    @Override
    public void disableUserInteractionsOnSidebarShown() {
        lv.setEnabled(false);
        etextSearchByName.setEnabled(false);
    }

    @Override
    public void enableUserInteractionsOnSidebarHidden() {

        lv.setEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(activity, CapexApprovalDetailActivity.class);
        intent.putExtra(CapexApprovalDetailActivity.INTENTKEY_CAPEXHEADERID, tempCapex.get(position).getCapexID());
        startActivity(intent);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filteredCapexByName();
    }
}
