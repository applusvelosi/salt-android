package applusvelosi.projects.android.salt2.models;

import org.json.JSONObject;

/**
 * Created by Velosi on 1/14/16.
 */
public class Dashboard {

    private int advanceApproval, advanceApprovedByAccounts, advanceApprovedByCM, advanceApprovedByManager, advanceLiquidated, advanceOpen, advancePaid, advancesPayment, advanceSubmitted;
    private int capexApproval;
    private int claimApproval, claimApprovedByAccounts, claimApprovedByManager, claimOpen, claimPaid, claimPayment, claimSubmitted;
    private int giftHospitalityApproval, contractTenderApproval;
    private int leaveApproval;
    private int liquidationApproval, liquidationApprovedByAccounts, liquidationApprovedByManager, liquidationLiquidated, liquidationOpen, liquidationSubmitted;
    private int pendingBT, pendingHospitalization, pendingSL, pendingUnpaid, pendingVL;
    private int recruitmentApproval;
    private float remSL, remVL;
    private int usedBT, usedHospitalization, usedSL, usedUnpaid, usedVL;

    public Dashboard(JSONObject jsonDashboard) throws Exception{
        advanceApproval = jsonDashboard.getInt("advanceApproval");
        advanceApprovedByAccounts = jsonDashboard.getInt("advanceApprovedByAccounts");
        advanceApprovedByCM = jsonDashboard.getInt("advanceApprovedByCM");
        advanceApprovedByManager = jsonDashboard.getInt("advanceApprovedByManager");
        advanceLiquidated = jsonDashboard.getInt("advanceLiquidated");
        advanceOpen = jsonDashboard.getInt("advanceOpen");
        advancePaid = jsonDashboard.getInt("advancePaid");
        advancesPayment = jsonDashboard.getInt("advancesPayment");
        advanceSubmitted = jsonDashboard.getInt("advanceSubmitted");
        capexApproval = jsonDashboard.getInt("capexApproval");
        claimApproval = jsonDashboard.getInt("claimApproval");
        claimApprovedByAccounts = jsonDashboard.getInt("claimApprovedByAccounts");
        claimApprovedByManager = jsonDashboard.getInt("claimApprovedByManager");
        claimOpen = jsonDashboard.getInt("claimOpen");
        claimPaid = jsonDashboard.getInt("claimPaid");
        claimPayment = jsonDashboard.getInt("claimPayment");
        claimSubmitted = jsonDashboard.getInt("claimSubmitted");
        giftHospitalityApproval = jsonDashboard.getInt("giftHospitalityApproval");
        contractTenderApproval = jsonDashboard.getInt("contractApproval");
        leaveApproval = jsonDashboard.getInt("leaveApproval");
        liquidationApproval = jsonDashboard.getInt("liquidationApproval");
        liquidationApprovedByAccounts = jsonDashboard.getInt("liquidationApprovedByAccounts");
        liquidationApprovedByManager = jsonDashboard.getInt("liquidationApprovedByManager");
        liquidationLiquidated = jsonDashboard.getInt("liquidationLiquidated");
        liquidationOpen = jsonDashboard.getInt("liquidationOpen");
        liquidationSubmitted = jsonDashboard.getInt("liquidationSubmitted");
        pendingBT = jsonDashboard.getInt("pendingBT");
        pendingHospitalization = jsonDashboard.getInt("pendingHospitalization");
        pendingSL = jsonDashboard.getInt("pendingSL");
        pendingUnpaid = jsonDashboard.getInt("pendingUnpaid");
        pendingVL = jsonDashboard.getInt("pendingVL");
        recruitmentApproval = jsonDashboard.getInt("recruitmentApproval");
        remSL = (float)jsonDashboard.getDouble("remainingSL");
        remVL = (float)jsonDashboard.getDouble("remainingVL");
        usedBT = jsonDashboard.getInt("usedBT");
        usedHospitalization = jsonDashboard.getInt("usedHospitalization");
        usedSL = jsonDashboard.getInt("usedSL");
        usedUnpaid = jsonDashboard.getInt("usedUnpaid");
        usedVL = jsonDashboard.getInt("usedVL");
    }

    public int getAdvanceApproval(){ return advanceApproval; }
    public int getAdvanceApprovedByAccounts(){ return advanceApprovedByAccounts; }
    public int getAdvanceApprovedByCM(){ return advanceApprovedByCM; }
    public int getAdvanceApprovedByManager(){ return advanceApprovedByManager; }
    public int getAdvanceLiquidated(){ return advanceLiquidated; }
    public int getAdvanceOpen(){ return advanceOpen; }
    public int getAdvancePaid(){ return advancePaid; }
    public int getAdvancesPayment(){ return advancesPayment; }
    public int getAdvanceSubmitted(){ return advanceSubmitted; }
    public int getCapexApproval(){ return capexApproval; }
    public int getClaimApproval(){ return claimApproval; }
    public int getClaimApprovedByAccounts(){ return claimApprovedByAccounts; }
    public int getClaimApprovedByManager(){ return claimApprovedByManager; }
    public int getClaimOpen(){ return claimOpen; }
    public int getClaimPaid(){ return claimPaid; }
    public int getClaimPayment(){ return claimPayment; }
    public int getClaimSubmitted(){ return claimSubmitted; }
    public int getGiftsHospitalityApproval() { return giftHospitalityApproval; }
    public int getContractTenderApproval() { return contractTenderApproval; }
    public int getLeaveApproval(){ return leaveApproval; }
    public int getLiquidationApproval(){ return liquidationApproval; }
    public int getLiquidationApprovedByAccounts(){ return liquidationApprovedByAccounts; }
    public int getLiquidationApprovedByManager(){ return liquidationApprovedByManager; }
    public int getLiquidationLiquidated(){ return liquidationLiquidated; }
    public int getLiquidationOpen(){ return liquidationOpen; }
    public int getLiquidationSubmitted(){ return liquidationSubmitted; }
    public int getPendingBT(){ return pendingBT; }
    public int getPendingHospitalization(){ return pendingHospitalization; }
    public int getPendingSL(){ return pendingSL; }
    public int getPendingUnpaid(){ return pendingUnpaid; }
    public int getPendingVL(){ return pendingVL; }
    public int getRecruitmentApproval(){ return recruitmentApproval; }
    public float getRemSL(){ return remSL; }
    public float getRemVL(){ return remVL; }
    public int getUsedBT(){ return usedBT; }
    public int getUsedHospitalization(){ return usedHospitalization; }
    public int getUsedSL(){ return usedSL; }
    public int getUsedUnpaid(){ return usedUnpaid; }
    public int getUsedVL(){ return usedVL; }

}
