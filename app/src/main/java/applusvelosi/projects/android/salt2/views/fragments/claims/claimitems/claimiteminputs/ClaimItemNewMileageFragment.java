package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.claimitems.MilageClaimItem;

/**
 * Created by Velosi on 10/26/15.
 */
public class ClaimItemNewMileageFragment extends ItemInputFragment{
    private TextView tviewMileageType;
    private EditText etextMileageFrom, etextMileageTo, etextMileageRate, etextMileageMileage;

    private float mileageCost;

    @Override
    protected View getInflatedLayout(LayoutInflater inflater) {
        return inflater.inflate(R.layout.fragment_claimitem_newmileage, null);
    }

    @Override
    protected void initAdditionalViewComponents(View v) {
        tviewMileageType = (TextView)v.findViewById(R.id.tviews_claimiteminput_mileagetype);

        etextMileageMileage = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileagemileage);
        etextMileageRate = (EditText)v.findViewById(R.id.tviews_claimiteminput_mileagerate);
        etextMileageFrom = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileagefrom);
        etextMileageTo = (EditText)v.findViewById(R.id.etexts_claimiteminput_mileageto);
//        cboxIsReturn = (CheckBox)v.findViewById(R.id.cboxs_claimiteminput_mileagereturn);
        etextMileageMileage.setFilters(new InputFilter[] { new DigitsInputFilter(7,2)});
        etextMileageRate.setFilters(new InputFilter[] { new DigitsInputFilter(3,2)});
        tviewMileageType.setOnClickListener(this);
        etextMileageMileage.addTextChangedListener(this);
        etextMileageRate.addTextChangedListener(this);
    }


    @Override
    protected void saveToServer() {
        try {
            if(etextMileageFrom.length() > 0) {
                if(etextMileageTo.length() > 0) {
                    if (tviewsDate.length() > 0) {
                        if (etextMileageMileage.length() > 0) {
                            if (etextDesc.length() > 0) {
                                activity.claimItem.setDateCreated(app.onlineGateway.epochizeDate(new Date()));
                                activity.startLoading();
                                actionbarDone.setEnabled(false);
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Object tempResult;
                                        try {
                                            int mileageTypeID = (tviewMileageType.getText().equals(MilageClaimItem.MILEAGETYPE_VAL_KILOMETER)) ? MilageClaimItem.MILEAGETYPE_KEY_KILOMETER : MilageClaimItem.MILEAGETYPE_KEY_MILE;
                                            float mileageRate = Float.parseFloat(etextMileageRate.getText().toString());
                                            int mileage = Integer.parseInt(etextMileageMileage.getText().toString());
                                            String mileageFrom = etextMileageFrom.getText().toString();
                                            String mileageTo = etextMileageTo.getText().toString();

                                            MilageClaimItem milageClaimItem = new MilageClaimItem(activity.claimItem, mileageTypeID, mileageRate, mileage, mileageFrom, mileageTo);
                                            tempResult = app.onlineGateway.saveClaimLineItem(milageClaimItem, 0, attachedFile);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            tempResult = e.getMessage();
                                        }

                                        final Object result = tempResult;
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                actionbarDone.setEnabled(true);
                                                if (result instanceof String) {
                                                    activity.finishLoading(result.toString());
                                                } else {
                                                    activity.finishLoading();
                                                    Toast.makeText(activity, "Claim Item Created Successfully", Toast.LENGTH_SHORT).show();
                                                    activity.finish();
                                                }
                                            }
                                        });
                                    }
                                }).start();
                            } else app.showMessageDialog(activity, "Description is required");
                        } else app.showMessageDialog(activity, "Amount is required");
                    } else app.showMessageDialog(activity, "Expense date is required");
                }else app.showMessageDialog(activity,"Destination is required");
            }else app.showMessageDialog(activity, "Origin is required");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SALTX "+e.getMessage());
        }
    }


    @Override
    public void onStart() {
        super.onResume();
        etextMileageRate.setText(String.valueOf(app.getStaffOffice().getMileageRate()));
    }


    void getMileageCost(){
        int mileageKeyType = (tviewMileageType.getText().equals(MilageClaimItem.MILEAGETYPE_VAL_KILOMETER))?MilageClaimItem.MILEAGETYPE_KEY_KILOMETER:MilageClaimItem.MILEAGETYPE_KEY_MILE;

        if((mileageKeyType==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER && app.getStaffOffice().getMileageType()==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER) || (mileageKeyType==MilageClaimItem.MILEAGETYPE_KEY_MILE && app.getStaffOffice().getMileageType()==MilageClaimItem.MILEAGETYPE_KEY_MILE))
            mileageCost = Float.parseFloat(etextMileageMileage.getText().toString());
        else if(app.getStaffOffice().getMileageType() == MilageClaimItem.MILEAGETYPE_KEY_MILE && mileageKeyType==MilageClaimItem.MILEAGETYPE_KEY_KILOMETER)
            mileageCost = Float.parseFloat(etextMileageMileage.getText().toString()) / Float.parseFloat("0.62");
        else if(app.getStaffOffice().getMileageType() == MilageClaimItem.MILEAGETYPE_KEY_KILOMETER && mileageKeyType == MilageClaimItem.MILEAGETYPE_KEY_MILE)
            mileageCost = Float.parseFloat(etextMileageMileage.getText().toString()) * Float.parseFloat("0.62");
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v == tviewMileageType) {
            tviewMileageType.setText((tviewMileageType.getText().equals(MilageClaimItem.MILEAGETYPE_VAL_KILOMETER)) ? MilageClaimItem.MILEAGETYPE_VAL_MILE : MilageClaimItem.MILEAGETYPE_VAL_KILOMETER);
            etextMileageMileage.setText("0");
        }
    }



    @Override
    public void afterTextChanged(Editable s){
        super.afterTextChanged(s);
        etextAmount.removeTextChangedListener(this);

        try{
            etextMileageMileage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        etextMileageMileage.setText("");
                        etextMileageMileage.setHint("");
                    }
                }
            });

            if(!app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                etextForex.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            System.out.println("please enter a forex");
                        }else {
                            if(etextForex.length() == 0)
                                etextForex.setText(String.valueOf(forex));
                            else
                                etextForex.setHint("0.00");
                        }
                    }
                });
            }

            if (s.hashCode() == etextMileageMileage.getText().hashCode()) {
                int value = Integer.parseInt(etextMileageMileage.getText().toString());
                if (value > 0) {
                    etextMileageMileage.removeTextChangedListener(this);
                    etextMileageMileage.setText(String.valueOf(value));
                    etextMileageMileage.setSelection(etextMileageMileage.length());
                    etextMileageMileage.addTextChangedListener(this);
                }
                etextAmount.addTextChangedListener(this);
            }else if(s.hashCode() == etextMileageRate.getText().hashCode())
                activity.claimItem.setMileageRate(Float.parseFloat(etextMileageRate.getText().toString()));
            else if(s.hashCode() == etextForex.getText().hashCode())
                activity.claimItem.setForex(Float.parseFloat(etextForex.getText().toString()));
            else if (s.hashCode() == etextSapTaxValue.getText().hashCode())
                activity.claimItem.setSapTaxCode(tvSapTaxCode.getText().toString());
            else if(s.hashCode() == etextTax.getText().hashCode() && !etextTax.getText().toString().contains("Tax Not Applicable"))
                activity.claimItem.setTaxAmount(Float.parseFloat(etextTax.getText().toString()));


            float amount,totalLC;
            getMileageCost();

            amount = mileageCost * Float.parseFloat(etextMileageRate.getText().toString());
            etextAmount.setText(SaltApplication.decimalFormat.format(amount));
            activity.claimItem.setAmount(amount);

            totalLC = amount * Float.parseFloat(etextForex.getText().toString());
            etextLocalAmount.setText(SaltApplication.decimalFormat.format(totalLC));
            activity.claimItem.setAmountLC(totalLC);

            if (app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                if (isSapTax)
                    totalLC /= (1 + Float.parseFloat(etextSapTaxValue.getText().toString()));
                else if(cboxTaxable.isShown()){
                    if (cboxTaxable.isChecked())
                        totalLC /= (1 + Float.parseFloat(etextTax.getText().toString()));
                }
            }

            etextBeforeTaxAmt.setText(SaltApplication.decimalFormat.format(totalLC));
            if(s.hashCode() == etextBillNotes.getText().hashCode())
                activity.claimItem.setNotes(etextBillNotes.getText().toString());
            else if(s.hashCode() == etextDesc.getText().hashCode())
                activity.claimItem.setDescription(etextDesc.getText().toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
