package applusvelosi.projects.android.salt2.models;

public class CalendarEvent {

	public enum CalendarEventDuration{
		AM,
		PM,
		AllDay;
	}

	private String name;
	private int resColorID;
	private CalendarEventDuration duration;
	private boolean shouldFill;
	private boolean hasLeave;
	private boolean isHoliday;
	
	public CalendarEvent(String name, int resColorID, CalendarEventDuration duration, boolean shouldFill, boolean isHoliday, boolean hasLeave){
		this.name = name;
		this.resColorID = resColorID;
		this.duration = duration;
		this.shouldFill = shouldFill;
		this.hasLeave = hasLeave;
		this.isHoliday = isHoliday;
	}

	public CalendarEvent(String eventName, CalendarEventDuration duration, boolean isHoliday, boolean hasLeave){
		this.name = eventName;
		this.duration = duration;
		this.isHoliday = isHoliday;
		this.hasLeave = hasLeave;
	}

	public boolean isHoliday() { return isHoliday; }

	public boolean hasLeave() { return hasLeave; }
	
	public String getName(){
		return name;
	}
	
	public int getColor(){
		return resColorID;
	}
	
	public CalendarEventDuration getDuration(){
		return duration;
	}
	
	public boolean shouldFill(){
		return shouldFill;
	}
}
