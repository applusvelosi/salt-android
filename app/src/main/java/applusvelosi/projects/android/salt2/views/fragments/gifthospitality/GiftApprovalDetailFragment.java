package applusvelosi.projects.android.salt2.views.fragments.gifthospitality;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.math.RoundingMode;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftHospitality;
import applusvelosi.projects.android.salt2.views.GiftApprovalDetailActivity;
import applusvelosi.projects.android.salt2.views.dialogs.DialogGiftHospitalityList;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 05/04/2017.
 */

public class GiftApprovalDetailFragment extends LinearNavActionbarFragment {

    private GiftApprovalDetailActivity activity;
    private RelativeLayout actionbarButtonBack;
    private TextView actionBarTitle;
    private TextView buttonApprove, buttonReject;
    private TextView fieldRefNumber, fieldRequestorName, fieldCountryManager, fieldRegionalManager, fieldCEO, fieldCSR, fieldStatus, fieldOffice,
            fieldDateSubmitted, fieldDateProcessedByCM, fieldDateProcessedByRM, fieldDateProcessedByCEO, fieldDateProcessedByCSR,
            fieldTotalAmount, fieldEuroRate, fieldAmountInEuro, fieldGiftType, fieldIsReceived, fieldDateReceived, fieldCountry, fieldCompanyName, fieldCity,
            fieldIsDonation, fieldIsPublicOfficial, fieldDescription, fieldReason, fieldApproversNote, tvRecipientsCnt;
    private RelativeLayout containersRecipients;

    private RelativeLayout dialogViewReject, dialogViewApprove;
    private EditText etextDialogRejectReason, etextDialogApproveReason;
    private AlertDialog diaglogReject, dialogApprove;
    private StringBuilder stringNotes, approversNote;

    private DialogGiftHospitalityList dialogGiftRecipient;

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (GiftApprovalDetailActivity) getActivity();
        RelativeLayout actionbarLayout = (RelativeLayout) linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backonly, null);
        actionbarButtonBack = (RelativeLayout) actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionBarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        actionBarTitle.setText("Gifts & Hospitality For Approval");
        actionBarTitle.setPadding(50,0,0,0);
        actionbarButtonBack.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_giftforapproval_detail, null);
        buttonApprove = (TextView)view.findViewById(R.id.buttons_giftforapprovaldetail_approve);
        buttonReject = (TextView)view.findViewById(R.id.buttons_giftforapprovaldetail_reject);
        fieldRefNumber = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_refnumber);
        fieldRequestorName = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_requestorname);
        fieldCountryManager = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_cm);
        fieldRegionalManager = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_rm);
        fieldCEO = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_ceo);
        fieldCSR = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_csr);
        fieldOffice = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_requestoroffice);
        fieldDateSubmitted = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_datesubmitted);
        fieldDateProcessedByCM = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_processedbycm);
        fieldDateProcessedByRM = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_processedbyrm);
        fieldDateProcessedByCEO = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_processedbyceo);
        fieldDateProcessedByCSR = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_processedbyappluscsr);
        fieldDateReceived = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_datereceived);
        fieldTotalAmount = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_localamount);
        fieldEuroRate = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_eurorate);
        fieldAmountInEuro = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_amountineuro);
        fieldGiftType = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_gifttype);
        fieldIsReceived = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_alreadyreceived);
        fieldCountry = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_country);
        fieldCompanyName = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_companyname);
        fieldCity = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_city);
        fieldIsDonation = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_isdonation);
        fieldIsPublicOfficial = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_ispublicofficial);
        fieldDescription = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_description);
        fieldReason = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_reason);
        fieldApproversNote = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_approvernote);
        fieldStatus = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_status);
        containersRecipients = (RelativeLayout) view.findViewById(R.id.container_giftforapprovaldetail_recipeints);
        tvRecipientsCnt = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_recipientcount);
        containersRecipients.setOnClickListener(this);

        dialogViewApprove = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogApproveReason = (EditText)dialogViewApprove.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewApprove.findViewById(R.id.tviews_dialogs_textinput)).setText("Approver's Note");
        etextDialogApproveReason.setHint("Note");
        dialogApprove = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewApprove)
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_SUBMITTED) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                if (activity.giftHeader.getRequestorOfficeId() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                            } else {
                                if (activity.giftHeader.getRequestorOfficeId() != 41)
                                    approversNote.append("CM: Approved");
                                else
                                    approversNote.append("CFO: Approved");
                            }
                            approversNote.append(";");
                            changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCM, "DateProcessedByCountryManager", approversNote.toString());
                        } else if (activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCM) {
                            if (activity.giftHeader.getProcessedByRMId() == activity.giftHeader.getProcessedByCEOId()) {
                                if (etextDialogApproveReason.length() > 0) {
                                    String etextNote = etextDialogApproveReason.getText().toString().trim();
                                    approversNote.append("CEO: " + etextNote);
                                } else {
                                    approversNote.append("CEO: Approved");
                                }
                                approversNote.append(";");
                                changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                            } else {
                                if (etextDialogApproveReason.length() > 0) {
                                    String etextNote = etextDialogApproveReason.getText().toString().trim();
                                    approversNote.append("RM: " + etextNote);
                                } else {
                                    approversNote.append("RM: Approved");
                                }
                                approversNote.append(";");
                                changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYRM, "DateProcessedByRegionalManager", approversNote.toString());
                            }
                        } else if (activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCEO) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("Applus CSR: " + etextNote);
                            } else {
                                approversNote.append("Applus CSR: Approved");
                            }
                            approversNote.append(";");
                            changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYAPPLUS, "DateApprovedApplus", approversNote.toString());
                        } else if (activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYRM) {
                            if (activity.giftHeader.getProcessedByRMId() == activity.giftHeader.getProcessedByCEOId()) {
                                if (etextDialogApproveReason.length() > 0) {
                                    String etextNote = etextDialogApproveReason.getText().toString().trim();
                                    approversNote.append("CEO: " + etextNote);
                                } else {
                                    approversNote.append("CEO: Approved");
                                }
                            }
                            approversNote.append(";");
                            changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialogViewReject = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
        etextDialogRejectReason =  (EditText)dialogViewReject.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReject.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Rejection");
        diaglogReject = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReject)
                .setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(etextDialogRejectReason.length() > 0) {
                            String etextNote = etextDialogRejectReason.getText().toString();
                            if(activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_SUBMITTED) {
                                if(activity.giftHeader.getRequestorOfficeId() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYRID_REJECTEDBYCM, "DateProcessedByCountryManager", approversNote.toString());
                            }else if(activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCM) {
                                if (activity.giftHeader.getProcessedByRMId() == activity.giftHeader.getProcessedByCEOId()) {
                                    approversNote.append("CEO: " + etextNote);
                                    approversNote.append(";");
                                    changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_REJECTEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                                } else {
                                    approversNote.append("RM: " + etextNote);
                                    approversNote.append(";");
                                    changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_REJECTEDBYRM, "DateProcessedByRegionalManager", approversNote.toString());
                                }
                            } else if (activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYCEO) {
                                approversNote.append("Applus CSR: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_REJECTEDBYAPPLUS, "DateApprovedApplus", approversNote.toString());
                            } else if(activity.giftHeader.getStatusId() == GiftHospitality.GIFTHOSPITALITYID_APPROVEDBYRM) {
                                approversNote.append("CFO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(GiftHospitality.GIFTHOSPITALITYID_REJECTEDBYCEO, "DateProcessedByCEO", approversNote.toString());
                            }
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for rejection");
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        return view;
    }

    private void changeApprovalStatus(final int statusID, final String keyForUpdatableDate, final String approverNotes) {
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                String tempResult;
                try{
                    tempResult = app.onlineGateway.saveGiftHospitality(activity.giftHeader.getJSONFromUpdatingGift(statusID, keyForUpdatableDate, approverNotes, app), activity.giftHeader.jsonize(app));
                }catch(Exception e){
                    tempResult = e.getMessage();
                }
                final String result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(result.equals("OK")){
                            activity.finishLoading();
                            Toast.makeText(activity, "Updated Successfully!", Toast.LENGTH_SHORT).show();
                            activity.finish();
                        }else{
                            activity.finishLoading(result);
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        if (v == actionbarButtonBack)
            linearNavFragmentActivity.onBackPressed();
        else if (v == buttonApprove) {
            etextDialogApproveReason.setText("");
            dialogApprove.show();
        } else if(v == buttonReject){
            etextDialogRejectReason.setText("");
            diaglogReject.show();
        } else if (v == containersRecipients) {
            if (dialogGiftRecipient == null)
                dialogGiftRecipient = new DialogGiftHospitalityList(activity.giftHeader, activity, activity.giftHeader.getRecipients());
            dialogGiftRecipient.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activity.giftHeader == null) {
            activity.startLoading();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Object tempResult;
                    try {
                        tempResult = app.onlineGateway.getGiftHeaderDetail(activity.giftHeaderID);
                    }catch (Exception e){
                        tempResult = e.getMessage();
                    }
                    final Object result = tempResult;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (result instanceof String){
                                activity.finishLoading();
                            }else{
                                try {
                                    activity.giftHeader = new GiftHospitality((JSONObject)result);
                                    updateView();
                                    activity.finishLoading();
                                }catch (Exception e) {
                                    activity.finishLoading(e.getMessage());
                                }
                            }
                        }
                    });
                }
            }).start();
        } else {
            try {
                updateView();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateView() {

        fieldRefNumber.setText(activity.giftHeader.getReferenceNo());
        fieldRequestorName.setText(activity.giftHeader.getRequestorName());
        fieldCountryManager.setText(activity.giftHeader.getProcessedByCMName());
        fieldRegionalManager.setText(activity.giftHeader.getProcessedByRMName());
        fieldCEO.setText(activity.giftHeader.getProcessedByCEOName());
        fieldCSR.setText(activity.giftHeader.getApplusCrsName());
        fieldOffice.setText(activity.giftHeader.getRequestorOfficeName());
        fieldDateSubmitted.setText(activity.giftHeader.getDateSubmitted(app));
        fieldDateProcessedByCM.setText(activity.giftHeader.getDateProcessedByCM(app));
        fieldDateProcessedByRM.setText(activity.giftHeader.getDateProcessedByRM(app));
        fieldDateProcessedByCEO.setText(activity.giftHeader.getDateProcessedByCEO(app));
        fieldDateProcessedByCSR.setText(activity.giftHeader.getDateApprovedByApplus(app));
        fieldDateReceived.setText(activity.giftHeader.getDateReceivedGiven(app));
        fieldTotalAmount.setText(activity.giftHeader.getCurrencySymbol() +" "+ SaltApplication.decimalFormat.format(activity.giftHeader.getTotalAmountInLc()));
        fieldEuroRate.setText(SaltApplication.sapTaxDecimalFormat.format(activity.giftHeader.getExchangeRate()));
        SaltApplication.decimalFormat.setRoundingMode(RoundingMode.CEILING);
        fieldAmountInEuro.setText("EUR " +  SaltApplication.decimalFormat.format(activity.giftHeader.getAmountInEuro()));
        fieldGiftType.setText(activity.giftHeader.getGiftType());
        fieldIsReceived.setText((activity.giftHeader.isReceivedGiven())?"Yes":"No");
        fieldCountry.setText(activity.giftHeader.getCountryName());
        fieldCompanyName.setText(activity.giftHeader.getCompanyName());
        fieldCity.setText(activity.giftHeader.getRecipientCity());
        fieldIsDonation.setText((activity.giftHeader.isDonation())?"Yes":"No");
        fieldIsPublicOfficial.setText(activity.giftHeader.isPublicOfficial()?"Yes":"No");
        fieldDescription.setText(activity.giftHeader.getDescription());
        fieldReason.setText(activity.giftHeader.getReason());
        fieldApproversNote.setText(activity.giftHeader.getApproverNote());
        String statusDesc = GiftHospitality.getGHRStatusDescriptionForKey(activity.giftHeader.getStatusId());
        fieldStatus.setText(statusDesc);
        stringNotes = new StringBuilder();
        approversNote = new StringBuilder(activity.giftHeader.getApproverNote());
        if(!activity.giftHeader.getApproverNote().isEmpty()) {
            String tempStrings[] = activity.giftHeader.getApproverNote().split(";");
            for (String str : tempStrings) {
                stringNotes.append(str);
                stringNotes.append(System.getProperty("line.separator"));
            }
        } else {
            stringNotes.append(activity.giftHeader.getApproverNote());
        }
        fieldApproversNote.setText(stringNotes.toString());
        tvRecipientsCnt.setText(String.valueOf(activity.giftHeader.getRecipients().size()));
        buttonApprove.setOnClickListener(GiftApprovalDetailFragment.this);
        buttonReject.setOnClickListener(GiftApprovalDetailFragment.this);
    }
}
