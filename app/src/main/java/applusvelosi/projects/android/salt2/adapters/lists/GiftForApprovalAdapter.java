package applusvelosi.projects.android.salt2.adapters.lists;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.giftshospitality.GiftHospitality;
import applusvelosi.projects.android.salt2.views.HomeActivity;

/**
 * Created by Velosi on 04/04/2017.
 */

public class GiftForApprovalAdapter extends BaseAdapter {
    HomeActivity activity;
    List<GiftHospitality> giftHospitalities;
    SaltApplication app;

    public GiftForApprovalAdapter(HomeActivity activity, List<GiftHospitality> giftHospitalities, SaltApplication app) {
        this.activity = activity;
        this.giftHospitalities = giftHospitalities;
        this.app = app;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        GiftNodeHolder holder;
        if(view == null) {
            holder = new GiftNodeHolder();
            view = activity.getLayoutInflater().inflate(R.layout.node_myclaims, null);
            holder.tvDateSubmitted = (TextView)view.findViewById(R.id.tviews_myclaims_node_date);
            holder.tvOffice = (TextView)view.findViewById(R.id.tviews_myclaims_node_claimnumber);
            holder.tvName = (TextView)view.findViewById(R.id.tviews_cells_myclaim_node_name);
            holder.tvStatus = (TextView)view.findViewById(R.id.tviews_myclaims_node_status);
            holder.tvAmount = (TextView)view.findViewById(R.id.tviews_myclaims_node_total);

            view.setTag(holder);
        }

        holder = (GiftNodeHolder)view.getTag();
        GiftHospitality gift = giftHospitalities.get(position);
        String statusDesc = GiftHospitality.getGHRStatusDescriptionForKey(gift.getStatusId());
        holder.tvDateSubmitted.setText(gift.getDateSubmitted(app));
        holder.tvOffice.setText(gift.getRequestorOfficeName());
        holder.tvName.setText(gift.getRequestorName());
        holder.tvStatus.setText(statusDesc);
        holder.tvAmount.setText(gift.getCurrencySymbol()+" "+ SaltApplication.decimalFormat.format(gift.getTotalAmountInLc()));

        return view;
    }

    @Override
    public int getCount() {
        return giftHospitalities.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return giftHospitalities.get(position);
    }

    private class GiftNodeHolder {
        public TextView tvDateSubmitted, tvOffice, tvStatus, tvName, tvAmount;
    }
}
