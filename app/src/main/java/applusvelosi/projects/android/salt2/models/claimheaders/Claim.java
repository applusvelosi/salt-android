package applusvelosi.projects.android.salt2.models.claimheaders;

import org.json.JSONObject;

import java.util.List;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;

/**
 * Created by Velosi on 1/21/16.
 */
public class Claim extends ClaimHeader {

    public Claim(SaltApplication app, int costCenterID, String costCenterName, boolean isPaidByCC) {
        super(app, costCenterID, costCenterName, ClaimHeader.TYPEKEY_CLAIMS, isPaidByCC, 0, "");
    }

    public Claim(SaltApplication app, int hasYesNoDocFlag, List<Document> docs, int costCenterID, String costCenterName, boolean isPaidByCC){
        super(app, hasYesNoDocFlag, docs, costCenterID, costCenterName, ClaimHeader.TYPEKEY_CLAIMS, isPaidByCC , 0,"");
    }

    public Claim(JSONObject jsonClaim) throws Exception{
        super(jsonClaim);
    }

    public Claim(ClaimHeader claim) {
        super(claim);
    }

    public Claim(){

    }

    public boolean isPaidByCC(){ return isPaidByCC; }

}
