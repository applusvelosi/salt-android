package applusvelosi.projects.android.salt2.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import applusvelosi.projects.android.salt2.SaltApplication;

public class FileManager {
	public static final String ACCEPTEDFILE_PDF = ".pdf";
	public static final String ACCEPTEDFILE_PNG = ".png";
	public static final String ACCEPTEDFILE_JPG = ".jpg";
	public static final String ACCEPTEDFILE_JPEG = ".jpeg";
	public static final String ACCEPTEDFILE_GIF = ".gif";
	public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

	private ArrayList<String> acceptedFiles;
	private final String DIR_DOWNLOADEDATTACHMENTS = Environment.getExternalStorageDirectory()+"/salt/downloads/attachments/";
	private final String DIR_CAPTUREDATTACHMENTS = Environment.getExternalStorageDirectory()+"/salt/captured/attachments/";
    private final String imageViewRoot = OnlineGateway.rootURL+"ImageViewer.aspx";

	
	public FileManager(){
		acceptedFiles = new ArrayList<String>();
		acceptedFiles.add(ACCEPTEDFILE_PDF);
		acceptedFiles.add(ACCEPTEDFILE_PNG);
		acceptedFiles.add(ACCEPTEDFILE_JPG);
		acceptedFiles.add(ACCEPTEDFILE_JPEG);
		acceptedFiles.add(ACCEPTEDFILE_GIF);
	}

	public String getDirForDownloadedAttachments(){
		File dir = new File(DIR_DOWNLOADEDATTACHMENTS);
		if(!dir.exists())
			dir.mkdirs();
		
		return DIR_DOWNLOADEDATTACHMENTS;
	}
	
	public String getDirForCapturedAttachments(){
		File dir = new File(DIR_CAPTUREDATTACHMENTS);
		if(!dir.exists())
			dir.mkdirs();
		
		return DIR_CAPTUREDATTACHMENTS;
	}

	public static String getFilePathFromUri_BelowKitKat(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public static String uriToFilename(Context context, Uri uri) {
		String path;

		if (Build.VERSION.SDK_INT < 11) {
			path = FileManager.getRealPathFromURI_BelowAPI11(context, uri);
		} else if (Build.VERSION.SDK_INT < 19) {
			path = FileManager.getRealPathFromURI_API11to18(context, uri);
		} else {
			path = FileManager.getRealPathFromURI_API19(context, uri);
		}

		return path;
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public static String getRealPathFromURI_API19(Context context, Uri uri) {
		String filePath = "";
		if (DocumentsContract.isDocumentUri(context, uri)) {
			String wholeID = DocumentsContract.getDocumentId(uri);
			Log.d("wholeID", wholeID);
// Split at colon, use second item in the array
			String[] splits = wholeID.split(":");
			if (splits.length == 2) {
				String id = splits[1];

				String[] column = {MediaStore.Images.Media.DATA};
// where id is equal to
				String sel = MediaStore.Images.Media._ID + "=?";
				Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
						column, sel, new String[]{id}, null);
				int columnIndex = cursor.getColumnIndex(column[0]);
				if (cursor.moveToFirst()) {
					filePath = cursor.getString(columnIndex);
				}
				cursor.close();
			}
		} else {
			filePath = uri.getPath();
		}
		return filePath;
	}

	@SuppressLint("NewApi")
	public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
		String[] proj = {MediaStore.Images.Media.DATA};
		String result = null;
		CursorLoader cursorLoader = new CursorLoader(
				context,
				contentUri, proj, null, null, null);
		Cursor cursor = cursorLoader.loadInBackground();
		if (cursor != null) {
			int column_index =
					cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			result = cursor.getString(column_index);
		}
		return result;
	}

	public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri) {
		String[] proj = {MediaStore.Images.Media.DATA};
		Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

//	/**
//	 * Get a file path from a Uri. This will get the the path for Storage Access
//	 * Framework Documents, as well as the _data field for the MediaStore and
//	 * other file-based ContentProviders.
//	 *
//	 * @param context The context.
//	 * @param uri The Uri to query.
//	 * @author paulburke
//	 */
//	@TargetApi(Build.VERSION_CODES.KITKAT)
//	public static String getPath(final Context context, final Uri uri) {
//
//		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//
//		// DocumentProvider
//		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//			// ExternalStorageProvider
//			if (isExternalStorageDocument(uri)) {
//				final String docId = DocumentsContract.getDocumentId(uri);
//				final String[] split = docId.split(":");
//				final String type = split[0];
//
//				if ("primary".equalsIgnoreCase(type)) {
//					return Environment.getExternalStorageDirectory() + "/" + split[1];
//				}
//
//				// TODO handle non-primary volumes
//			}
//			// DownloadsProvider
//			else if (isDownloadsDocument(uri)) {
//
//				final String id = DocumentsContract.getDocumentId(uri);
//				final Uri contentUri = ContentUris.withAppendedId(
//						Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//				return getDataColumn(context, contentUri, null, null);
//			}
//			// MediaProvider
//			else if (isMediaDocument(uri)) {
//				final String docId = DocumentsContract.getDocumentId(uri);
//				final String[] split = docId.split(":");
//				final String type = split[0];
//
//				Uri contentUri = null;
//				if ("image".equals(type)) {
//					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//				} else if ("video".equals(type)) {
//					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//				} else if ("audio".equals(type)) {
//					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//				}
//
//				final String selection = "_id=?";
//				final String[] selectionArgs = new String[] {
//						split[1]
//				};
//
//				return getDataColumn(context, contentUri, selection, selectionArgs);
//			}
//		}
//		// MediaStore (and general)
//		else if ("content".equalsIgnoreCase(uri.getScheme())) {
//
//			// Return the remote address
//			if (isGooglePhotosUri(uri))
//				return uri.getLastPathSegment();
//
//			return getDataColumn(context, uri, null, null);
//		}
//		// File
//		else if ("file".equalsIgnoreCase(uri.getScheme())) {
//			return uri.getPath();
//		}
//
//		return null;
//	}
//
//	/**
//	 * Get the value of the data column for this Uri. This is useful for
//	 * MediaStore Uris, and other file-based ContentProviders.
//	 *
//	 * @param context The context.
//	 * @param uri The Uri to query.
//	 * @param selection (Optional) Filter used in the query.
//	 * @param selectionArgs (Optional) Selection arguments used in the query.
//	 * @return The value of the _data column, which is typically a file path.
//	 */
//	private static String getDataColumn(Context context, Uri uri, String selection,
//									   String[] selectionArgs) {
//
//		Cursor cursor = null;
//		final String column = "_data";
//		final String[] projection = {
//				column
//		};
//
//		try {
//			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
//					null);
//			if (cursor != null && cursor.moveToFirst()) {
//				final int index = cursor.getColumnIndexOrThrow(column);
//				return cursor.getString(index);
//			}
//		} finally {
//			if (cursor != null)
//				cursor.close();
//		}
//		return null;
//	}
//
//
//	/**
//	 * @param uri The Uri to check.
//	 * @return Whether the Uri authority is ExternalStorageProvider.
//	 */
//	private static boolean isExternalStorageDocument(Uri uri) {
//		return "com.android.externalstorage.attachments".equals(uri.getAuthority());
//	}
//
//	/**
//	 * @param uri The Uri to check.
//	 * @return Whether the Uri authority is DownloadsProvider.
//	 */
//	private static boolean isDownloadsDocument(Uri uri) {
//		return "com.android.providers.downloads.attachments".equals(uri.getAuthority());
//	}
//
//	/**
//	 * @param uri The Uri to check.
//	 * @return Whether the Uri authority is MediaProvider.
//	 */
//	private static boolean isMediaDocument(Uri uri) {
//		return "com.android.providers.media.attachments".equals(uri.getAuthority());
//	}
//
//	/**
//	 * @param uri The Uri to check.
//	 * @return Whether the Uri authority is Google Photos.
//	 */
//	private static boolean isGooglePhotosUri(Uri uri) {
//		return "com.google.android.apps.photos.content".equals(uri.getAuthority());
//	}


	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static boolean checkPermission(final Context context) {
		int currentAPIVersion = Build.VERSION.SDK_INT;
		if(currentAPIVersion>= Build.VERSION_CODES.KITKAT) {
			if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
				if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
					alertBuilder.setCancelable(true);
					alertBuilder.setTitle("Permission necessary");
					alertBuilder.setMessage("External storage permission is necessary");
					alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
						public void onClick(DialogInterface dialog, int which) {
							ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
						}
					});
					AlertDialog alert = alertBuilder.create();
					alert.show();

				} else {
					ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
				}
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	
	//usable when clicking on the image link in the claimitemdetailsfragment
//	public void downloadAttachment(final HomeActivity activity, final ClaimItem claimItem, final SaltProgressDialog pd, final AttachmentDownloadListener adl) throws Exception{ //will download file if file does not exist yet otherwise just open the file from the system
//        final File file = new File(getDirForDownloadedAttachments()+claimItem.getAttachmentName());
//        if(!file.exists()){
//
//        	pd.show();
//        	new Thread(new Runnable() {
//				private String exception;
//
//				@Override
//				public void run() {
//		        	try {
//		                URL url = new URL("http://salttest.velosi.com/ImageViewer.aspx?id="+claimItem.getItemID()+"&dID="+claimItem.getAttachmentDocumentID());
//		                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//		                urlConnection.setRequestMethod("GET");
//		                urlConnection.setDoOutput(true);
//		                urlConnection.connect();
//
//		                FileOutputStream fileOutput = new FileOutputStream(new File(getDirForDownloadedAttachments(), claimItem.getAttachmentName()));
//		                InputStream inputStream = urlConnection.getInputStream();
//		                byte[] buffer = new byte[1024];
//		                int bufferLength = 0;
//
//		                while ( (bufferLength = inputStream.read(buffer)) > 0 )
//		                    fileOutput.write(buffer, 0, bufferLength);
//
//		                fileOutput.close();
//
//			        } catch (Exception e) {
//			        	e.printStackTrace();
//			        	exception = e.getMessage();
//			        }
//
//            		new Handler(Looper.getMainLooper()).post(new Runnable() {
//
//            			@Override
//            			public void run() {
//            				pd.dismiss();
//            				if(exception == null)
//                				adl.onAttachmentDownloadFinish(file);
//            				else
//            					adl.onAttachmentDownloadFailed(exception);
//            			}
//            		});
//				}
//			}).start();
//        }else
//        	adl.onAttachmentDownloadFinish(file);
//	}

//	public void openAttachment(HomeActivity activity, String ext, File file) throws Exception{
//		if(!acceptedFiles.contains(ext.toLowerCase()))
//			throw new Exception("Cannot accept file with "+ext+" extension");
//
//    	if(ext.equals(ACCEPTEDFILE_PDF)){
//    		Uri path = Uri.fromFile(file);
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(path, "application/pdf");
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//            try {
//                activity.startActivity(intent);
//            }
//            catch (ActivityNotFoundException e) {
//                Toast.makeText(activity,  "No Application Available to Open PDF",  Toast.LENGTH_SHORT).show();
//            }
//    	}else{
//    		Uri path = Uri.fromFile(file);
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(path, "image/*");
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//            try {
//            	activity.startActivity(intent);
//            }
//            catch (ActivityNotFoundException e) {
//                Toast.makeText(activity,  "No Application Available to Open Image",  Toast.LENGTH_SHORT).show();
//            }
//    	}
//	}

	//usable when clicking on the image link in the claimitemdetailsfragment
	public void downloadDocument(final SaltApplication app, final int documentID, final int refID, final int obTypeID, final String outputFilename, final AttachmentDownloadListener adl) throws Exception { //will download file if file does not exist yet otherwise just open the file from the system
        final File file = new File(getDirForDownloadedAttachments()+outputFilename);
		new Thread(new Runnable() {
			private String exception;

			@Override
			public void run() {
				try {
					String fileUrl = imageViewRoot+"?dID="+documentID+"&refID="+refID+"&obTypeID="+obTypeID;
					System.out.println("Downloading file from "+fileUrl);

					URL url = new URL(fileUrl);
					HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
					urlConnection.setReadTimeout(30000);
					urlConnection.setRequestMethod("GET");
					urlConnection.setDoOutput(true);
					urlConnection.connect();


					FileOutputStream fileOutput = new FileOutputStream(new File(getDirForDownloadedAttachments(), outputFilename));
					InputStream inputStream = urlConnection.getInputStream();
					byte[] buffer = new byte[1024];
					int bufferLength = 0;

					while ( (bufferLength = inputStream.read(buffer)) > 0 )
						fileOutput.write(buffer, 0, bufferLength);

					fileOutput.close();
				} catch (Exception e) {
					e.printStackTrace();
					exception = e.getMessage();
				}
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						if(exception == null)
							adl.onAttachmentDownloadFinish(file);
						else
							adl.onAttachmentDownloadFailed(exception);
					}
				});
			}
		}).start();
	}

	public void openDocument(Context context, File documentFile){
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(documentFile).toString());
		String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
		System.out.println("SALTX extension "+extension+" mimtype "+mimetype);
		intent.setDataAndType(Uri.fromFile(documentFile), (extension.equalsIgnoreCase("") || mimetype == null)?"text/*":mimetype);
		context.startActivity(intent);
	}

	public void saveToTextFile(String contents){
	    try{
	        File gpxfile = new File(getDirForCapturedAttachments(), "test.txt");
	        FileWriter writer = new FileWriter(gpxfile);
	        writer.append(contents);
	        writer.flush();
	        writer.close();
	    }catch(IOException e){
	         e.printStackTrace();
	    }
	}
	
	public interface AttachmentDownloadListener{
		void onAttachmentDownloadFinish(File downloadedFile);
		void onAttachmentDownloadFailed(String errorMessage);
	}
}
