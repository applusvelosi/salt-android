package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs;


import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.views.dialogs.DialogItemCategories;

/**
 * Created by Velosi on 12/14/15.
 */
public class ClaimItemEditFragment extends ItemInputFragment implements DialogItemCategories.DialogClaimItemCategoryInterface {
    private TextView tvCategory;

    private DialogItemCategories dialogCategories;

    @Override
    protected View getInflatedLayout(LayoutInflater inflater){
        return inflater.inflate(R.layout.fragment_claimitem_editclaim, null);
    }

    @Override
    protected void initAdditionalViewComponents(View v) {
        tvCategory = (TextView)v.findViewById(R.id.tviews_claimiteminput_category);
        tvCategory.setOnClickListener(this);
        tvCategory.setText(activity.claimItem.getCategoryName());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v == tvCategory) {
            if (activity.getCategories() == null) {
                activity.startLoading();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Object tempResult;
                        try {
                            tempResult = app.onlineGateway.getClaimItemCategoryByOffice(activity.claimHeader.getTypeID());
                        } catch (Exception e) {
                            e.printStackTrace();
                            tempResult = e.getMessage();
                        }

                        final Object result = tempResult;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                activity.finishLoading();
                                if (result instanceof String) {
                                    Toast.makeText(activity, result.toString(), Toast.LENGTH_SHORT).show();
                                } else {
                                    activity.updateCategoryList(activity, (ArrayList<Category>) result);
                                    if (dialogCategories == null)
                                        dialogCategories = new DialogItemCategories(activity, ClaimItemEditFragment.this);
                                    dialogCategories.show();
                                }
                            }
                        });
                    }
                }).start();
            } else {
                if (dialogCategories == null)
                    dialogCategories = new DialogItemCategories(activity, ClaimItemEditFragment.this);
                dialogCategories.show();
            }
        }
    }

    @Override
    protected void saveToServer() {
        if (!"Entertainment and Gifts".equals(activity.claimItem.getCategory().getName()) || activity.claimItem.getCategory().getName().compareTo("Entertainment and Gifts") == 0 && activity.claimItem.getAttendees().size() > 0) {
            if (tviewsDate.length() > 0) {
                if (etextAmount.length() > 0) {
                    if (etextDesc.length() > 0) {
                        actionbarDone.setEnabled(false);
                        activity.claimItem.setDateCreated(app.onlineGateway.epochizeDate(new Date()));
                        activity.startLoading();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Object tempResult;
                                try {
                                    tempResult = app.onlineGateway.saveClaimLineItem(activity.claimItem, 0, attachedFile);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    tempResult = e.getMessage();
                                }

                                final Object result = tempResult;
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        actionbarDone.setEnabled(true);
                                        if (result instanceof String) {
                                            activity.finishLoading(result.toString());
                                        } else {
                                            activity.finishLoading();
                                            Toast.makeText(activity, "Please refresh in the claim list to reflect the changes", Toast.LENGTH_LONG).show();
                                            activity.finish();
                                        }
                                    }
                                });
                            }
                        }).start();
                    } else
                        app.showMessageDialog(activity, "Description is required");
                } else
                    app.showMessageDialog(activity, "Amount is required");
            } else
                app.showMessageDialog(activity, "Expense date is required");
        } else
            app.showMessageDialog(activity, "At least 1 attendee is required");
    }

    @Override
    public void afterTextChanged(Editable s) {
        super.afterTextChanged(s);
        try{
            etextAmount.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        etextAmount.setHint("");
                        etextAmount.setText("");
                    }
                    else
                        etextAmount.setText(String.valueOf(activity.claimItem.getForeignAmount()));
                }
            });

            if(!app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                etextForex.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            System.out.println("please enter a forex");
                        }else {
                            if(etextForex.length() == 0)
                                etextForex.setText(String.valueOf(forex));
                            else
                                etextForex.setHint("0.00");
                        }
                    }
                });
            }

            if(s.hashCode() == etextAmount.getText().hashCode()) {
                float amt = Float.parseFloat(etextAmount.getText().toString());

                if(activity.claimItem.getCategoryTypeID() == Category.TYPE_BUSINESSADVANCE && amt>0){
                    etextAmount.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER);
                    int cashAdvanceAmnt = Integer.parseInt(etextAmount.getText().toString());
                    cashAdvanceAmnt*=-1;
                    etextAmount.removeTextChangedListener(this);
                    etextAmount.setText(String.valueOf(cashAdvanceAmnt));
                    etextAmount.setSelection(etextAmount.length());
                    etextAmount.addTextChangedListener(this);
                }
                activity.claimItem.setAmount(amt);
            }else if(s.hashCode() == etextForex.getText().hashCode())
                activity.claimItem.setForex(Float.parseFloat(etextForex.getText().toString()));
            else if(s.hashCode() == etextSapTaxValue.getText().hashCode())
                activity.claimItem.setSapTaxCode(tvSapTaxCode.getText().toString());
            else if(s.hashCode() == etextTax.getText().hashCode() && !etextTax.getText().toString().contains("Tax Not Applicable"))
                activity.claimItem.setTaxAmount(Float.parseFloat(etextTax.getText().toString()));

            float totalLC = Float.parseFloat(etextAmount.getText().toString()) * (Float.parseFloat(etextForex.getText().toString()));
            etextLocalAmount.setText(SaltApplication.decimalFormat.format(totalLC));
            activity.claimItem.setAmountLC(totalLC);

            if (app.getStaffOffice().getBaseCurrencyThree().equals(activity.claimItem.getForeignCurrencyName())) {
                if (isSapTax)
                    totalLC /= (1 + Float.parseFloat(etextSapTaxValue.getText().toString()));
                else if (cboxTaxable.isShown()){
                    if (cboxTaxable.isChecked())
                        totalLC /= (1 + Float.parseFloat(etextTax.getText().toString())) ;
                }
            }

            etextBeforeTaxAmt.setText(SaltApplication.decimalFormat.format(totalLC));
            if(s.hashCode() == etextBillNotes.getText().hashCode())
                activity.claimItem.setNotes(etextBillNotes.getText().toString());
            else if(s.hashCode() == etextDesc.getText().hashCode())
                activity.claimItem.setDescription(etextDesc.getText().toString());
        }catch(Exception e){
            e.printStackTrace();
            etextLocalAmount.setText("0.00");
        }
    }


    @Override
    public void onCategorySelected(Category category) {
        activity.claimItem.setCategory(category);
        tvCategory.setText(category.getName());
    }

}
