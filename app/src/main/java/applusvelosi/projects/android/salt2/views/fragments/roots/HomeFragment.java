package applusvelosi.projects.android.salt2.views.fragments.roots;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Dashboard;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.views.HomeActivity;
import applusvelosi.projects.android.salt2.views.NewLeaveRequestActivity;

import static applusvelosi.projects.android.salt2.R.id.view_home_capexcontractapprovalseparator;
import static applusvelosi.projects.android.salt2.R.id.view_home_claimcapexapprovalseparator;
import static applusvelosi.projects.android.salt2.R.id.view_home_contracthrapprovalseparator;
import static applusvelosi.projects.android.salt2.R.id.view_home_hrgiftapprovalseparator;
import static applusvelosi.projects.android.salt2.R.id.view_home_leaveclaimapprovalseparator;

public class HomeFragment extends RootFragment {
	private static HomeFragment instance;
	//action bar buttons
	private RelativeLayout actionbarMenuButton, actionbarRefresh, actionbarNewLeaveButton,/* actionbarNewClaimButton,*/ actionbarHelpButton;

	private TextView tvCurrDateTime, tvStaffName;
	private SimpleDateFormat homeDateTimeFormat;
//	private HorizontalScrollView approverCounter;

	private LinearLayout containerLeaveForApproval, containerClaimForApproval, containerCapexForApproval, containerContractForApproval, containerHrForApproval, containerGhrForApproval;
	private TextView tviewLeavesForApproval, tviewClaimsForApproval, tviewRecruitmentsForApproval, tviewCapexForApproval, tviewGiftsForApproval, tviewContractsForApproval;

	private View view;

	//ShowCaseViews
	private ShowcaseView /*newClaimSV,*/ newLeaveSV, showSidebarSV;

	public static HomeFragment getInstance() {
		if (instance == null)
			instance = new HomeFragment();

		return instance;
	}

	public static void removeInstance() {
		if (instance != null)
			instance = null;
	}

	@Override
	protected RelativeLayout setupActionbar() {
		RelativeLayout actionbarLayout = (RelativeLayout) activity.getLayoutInflater().inflate(R.layout.actionbar_home, null);
		actionbarMenuButton = (RelativeLayout) actionbarLayout.findViewById(R.id.buttons_actionbar_menu);
		actionbarRefresh = (RelativeLayout) actionbarLayout.findViewById(R.id.buttons_actionbar_refresh);
		actionbarNewLeaveButton = (RelativeLayout) actionbarLayout.findViewById(R.id.buttons_actionbar_newleaverequest);
//		actionbarNewClaimButton = (RelativeLayout) actionbarLayout.findViewById(R.id.buttons_actionbar_newclaim);
		actionbarHelpButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_help);
		((TextView) actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Home");

		actionbarMenuButton.setOnClickListener(this);
		actionbarRefresh.setOnClickListener(this);
		actionbarNewLeaveButton.setOnClickListener(this);
//		actionbarNewClaimButton.setOnClickListener(this);
		actionbarHelpButton.setOnClickListener(this);

		DisplayMetrics displaymetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		actionbarMenuButton.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		activity.updateMaxSidebarShowWidth(displaymetrics.widthPixels - actionbarMenuButton.getMeasuredWidth() - 100);

		if(!app.offlineGateway.hasFinishedHomeTutorial())
			showTutorial();
		return actionbarLayout;
	}

	@Override
	public View createView(LayoutInflater li, ViewGroup vg, Bundle b) {
		view = li.inflate(R.layout.fragment_home_temp, null);
		Staff staff = app.getStaff();
//		approverCounter = (HorizontalScrollView) view.findViewById(R.id.scrollview_approvercounter);
		LinearLayout containerHomeApproval = (LinearLayout) view.findViewById(R.id.containers_home_approvals);
		tvStaffName = (TextView) view.findViewById(R.id.tviews_home_name);
		tvCurrDateTime = (TextView) view.findViewById(R.id.tviews_home_currdatetime);
		homeDateTimeFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault());

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						tvCurrDateTime.setText(homeDateTimeFormat.format(new Date()));
					}
				});
			}
		}, 0, 1000);

		if (app.getStaff().isUser()) {
			containerHomeApproval.setVisibility(View.GONE);
			view.findViewById(R.id.containers_home_approvals).setVisibility(View.GONE);
			view.findViewById(R.id.tviews_home_forapproval).setVisibility(View.GONE);
		} else {
			containerLeaveForApproval = (LinearLayout) view.findViewById(R.id.layout_home_leaveapprovalcontainer);
			containerClaimForApproval = (LinearLayout) view.findViewById(R.id.layout_home_expenseapprovalcontainer);
			containerCapexForApproval = (LinearLayout) view.findViewById(R.id.layout_home_capexapprovalcontainer);
			containerContractForApproval = (LinearLayout) view.findViewById(R.id.layout_home_contractapprovalcontainer);
			containerHrForApproval = (LinearLayout) view.findViewById(R.id.layout_home_recruitmentapprovalcontainer);
			containerGhrForApproval = (LinearLayout) view.findViewById(R.id.layout_home_giftapprovalcontainer);

			containerLeaveForApproval.setVisibility(View.VISIBLE);
			view.findViewById(view_home_leaveclaimapprovalseparator).setVisibility(View.VISIBLE);
			tviewLeavesForApproval = (TextView) view.findViewById(R.id.tviews_home_leavesforapproval);
			containerLeaveForApproval.setOnClickListener(this);

			if (!staff.isAccount()) {
				containerClaimForApproval.setVisibility(View.VISIBLE);
				tviewClaimsForApproval = (TextView) view.findViewById(R.id.tviews_home_claimsforapproval);
				containerClaimForApproval.setOnClickListener(this);
				view.findViewById(view_home_claimcapexapprovalseparator).setVisibility(View.VISIBLE);
			}
			if (staff.isAdmin() || staff.isCM()) {
				containerCapexForApproval.setVisibility(View.VISIBLE);
				tviewCapexForApproval = (TextView) view.findViewById(R.id.tviews_home_capexforapproval);
				containerCapexForApproval.setOnClickListener(this);
				view.findViewById(view_home_capexcontractapprovalseparator).setVisibility(View.VISIBLE);
			}
			containerContractForApproval.setVisibility(View.VISIBLE);
			tviewContractsForApproval = (TextView) view.findViewById(R.id.tviews_home_contractsforapproval);
			containerContractForApproval.setOnClickListener(this);

			if (!staff.isManager()) {
				view.findViewById(view_home_contracthrapprovalseparator).setVisibility(View.VISIBLE);
				containerHrForApproval.setVisibility(View.VISIBLE);
				tviewRecruitmentsForApproval = (TextView) view.findViewById(R.id.tviews_home_recruitmentsforapproval);
				containerHrForApproval.setOnClickListener(this);
				containerGhrForApproval.setVisibility(View.VISIBLE);
				view.findViewById(view_home_hrgiftapprovalseparator).setVisibility(View.VISIBLE);
				tviewGiftsForApproval = (TextView) view.findViewById(R.id.tviews_home_giftsforapproval);
				containerGhrForApproval.setOnClickListener(this);

			}
		}

		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				Object tempResult;
//				try {
//					String path = app.fileManager.getDirForCapturedAttachments()+"chick.jpg";
//					String path = app.fileManager.getDirForCapturedAttachments() + "8mbfile.pdf";
//					String path = app.fileManager.getDirForCapturedAttachments()+"7mbfile.pdf";
//					String path = app.fileManager.getDirForCapturedAttachments()+"5mbfile.pdf";
//					String path = app.fileManager.getDirForCapturedAttachments()+"100kb.jpg";
//					String path = app.fileManager.getDirForCapturedAttachments()+"test1.jpg";
//					System.out.println("SALTX " + path);
//					tempResult = app.onlineGateway.saveClaimLineItem(null, null, new File(path), null);
//					tempResult = app.onlineGateway.uploadToK2(new File(path));
//				} catch (Exception e) {
//					e.printStackTrace();
//					tempResult = e.getMessage();
//				}
//				final Object result = tempResult;
//				new Handler(Looper.getMainLooper()).post(new Runnable() {
//					@Override
//					public void run() {
//						System.out.println("SALTX result " + result);
//					}
//				});
//			}
//		}).start();
		return view;
	}

	@Override
	public void disableUserInteractionsOnSidebarShown() {
		containerLeaveForApproval.setOnClickListener(null);
		containerClaimForApproval.setOnClickListener(null);
//		approverCounter.setOnTouchListener(new View.OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				return true;
//			}
//		});
	}

	@Override
	public void enableUserInteractionsOnSidebarHidden() {
		containerClaimForApproval.setOnClickListener(this);
		containerClaimForApproval.setOnClickListener(this);
//		approverCounter.setOnTouchListener(null);
	}

	@Override
	public void onResume() {
		super.onResume();
		refresh();
	}

	public void refresh() {
		activity.startLoading();
		app.dashboard = null;
		new Thread(new Runnable() {
			@Override
			public void run() {
				Object tempResult;
				try{
					app.onlineGateway.updateStaff(app.getStaff().getStaffID(), app.getStaff().getSecurityLevel(), app.getStaff().getOfficeID());
					tempResult = app.onlineGateway.getDashboard();
				}catch(Exception e){
					e.printStackTrace();
					tempResult = e.getMessage();
				}

				final Object result = tempResult;
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						if(result instanceof String) activity.finishLoading(result.toString());
						else {
							activity.finishLoading();
							tvStaffName.setText(app.getStaff().getLname() + ", " + app.getStaff().getFname());
							app.dashboard = (Dashboard)result;

//							if(!app.getStaff().isUser() && !app.getStaff().isAccount() && !app.getStaff().isAM()){
//								int leaveApproval = app.dashboard.getLeaveApproval();
//								int claimApproval = (app.getStaff().isAccount())?0:app.dashboard.getClaimApproval() + app.dashboard.getAdvanceApproval() + app.dashboard.getLiquidationApproval();
//								int rcmtApproval = app.dashboard.getRecruitmentApproval();
//								int capexApproval = app.dashboard.getCapexApproval();
//								int giftHospitalityApproval = app.dashboard.getGiftsHospitalityApproval();

//								tviewLeavesForApproval.setText(String.valueOf(leaveApproval));
//								tviewClaimsForApproval.setText(String.valueOf(claimApproval));
//								tviewRecruitmentsForApproval.setText(String.valueOf(rcmtApproval));
//								tviewCapexForApproval.setText(String.valueOf(capexApproval));
//								tviewGiftsForApproval.setText(String.valueOf(giftHospitalityApproval));
//
//								tviewLeavesForApproval.setTextColor((leaveApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
//								tviewClaimsForApproval.setTextColor((claimApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
//								tviewRecruitmentsForApproval.setTextColor((rcmtApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
//								tviewCapexForApproval.setTextColor((capexApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
//								tviewGiftsForApproval.setTextColor((giftHospitalityApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
//							}

							if (!app.getStaff().isUser()) {
								int leaveApproval = app.dashboard.getLeaveApproval();
								int claimApproval = (app.getStaff().isAccount())?0:app.dashboard.getClaimApproval() + app.dashboard.getAdvanceApproval() + app.dashboard.getLiquidationApproval();
								int rcmtApproval = app.dashboard.getRecruitmentApproval();
								int capexApproval = app.dashboard.getCapexApproval();
								int giftHospitalityApproval = app.dashboard.getGiftsHospitalityApproval();
								int contractApproval = app.dashboard.getContractTenderApproval();

								tviewLeavesForApproval.setText(String.valueOf(leaveApproval));
								tviewLeavesForApproval.setTextColor((leaveApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));

								if (!app.getStaff().isAccount()) {
									tviewClaimsForApproval.setText(String.valueOf(claimApproval));
									tviewClaimsForApproval.setTextColor((claimApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
								}

								if (app.getStaff().isAdmin() || app.getStaff().isCM()) {
									tviewCapexForApproval.setTextColor((capexApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
									tviewCapexForApproval.setText(String.valueOf(capexApproval));
								}
								tviewContractsForApproval.setTextColor((contractApproval > 0) ? activity.getResources().getColor(R.color.red)  : activity.getResources().getColor(R.color.black2));
								tviewContractsForApproval.setText(String.valueOf(contractApproval));

								if (!app.getStaff().isManager()) {
									tviewRecruitmentsForApproval.setText(String.valueOf(rcmtApproval));
									tviewRecruitmentsForApproval.setTextColor((rcmtApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
									tviewGiftsForApproval.setText(String.valueOf(giftHospitalityApproval));
									tviewGiftsForApproval.setTextColor((giftHospitalityApproval > 0) ? activity.getResources().getColor(R.color.red) : activity.getResources().getColor(R.color.black2));
								}
							}

							((TextView)view.findViewById(R.id.labels_home_vacation_used)).setText(String.valueOf(app.dashboard.getUsedVL()));
							((TextView)view.findViewById(R.id.labels_home_vacation_pending)).setText(String.valueOf(app.dashboard.getPendingVL()));
							((TextView)view.findViewById(R.id.labels_home_vacation_rem)).setText(SaltApplication.decimalFormat.format(app.dashboard.getRemVL()));
							((TextView)view.findViewById(R.id.labels_home_sick_used)).setText(String.valueOf(app.dashboard.getUsedSL()));
							((TextView)view.findViewById(R.id.labels_home_sick_pending)).setText(String.valueOf(app.dashboard.getPendingSL()));
							((TextView)view.findViewById(R.id.labels_home_sick_rem)).setText(SaltApplication.decimalFormat.format(app.dashboard.getRemSL()));
							((TextView)view.findViewById(R.id.labels_home_unpaid_used)).setText(String.valueOf(app.dashboard.getUsedUnpaid()));
							((TextView)view.findViewById(R.id.labels_home_unpaid_pending)).setText(String.valueOf(app.dashboard.getPendingUnpaid()));
							((TextView)view.findViewById(R.id.labels_home_hospitalization_used)).setText(String.valueOf(app.dashboard.getUsedHospitalization()));
							((TextView)view.findViewById(R.id.labels_home_hospitalization_pending)).setText(String.valueOf(app.dashboard.getPendingHospitalization()));
							((TextView)view.findViewById(R.id.labels_home_businesstrip_used)).setText(String.valueOf(app.dashboard.getUsedBT()));
							((TextView)view.findViewById(R.id.labels_home_businesstrip_pending)).setText(String.valueOf(app.dashboard.getPendingBT()));
							((TextView)view.findViewById(R.id.labels_home_open_claims)).setText(String.valueOf(app.dashboard.getClaimOpen()));
							((TextView)view.findViewById(R.id.labels_home_open_advances)).setText(String.valueOf(app.dashboard.getAdvanceOpen()));
							((TextView)view.findViewById(R.id.labels_home_open_liquidations)).setText(String.valueOf(app.dashboard.getLiquidationOpen()));
							((TextView)view.findViewById(R.id.labels_home_submitted_claims)).setText(String.valueOf(app.dashboard.getClaimSubmitted()));
							((TextView)view.findViewById(R.id.labels_home_submitted_advances)).setText(String.valueOf(app.dashboard.getAdvanceSubmitted()));
							((TextView)view.findViewById(R.id.labels_home_submitted_liquidations)).setText(String.valueOf(app.dashboard.getLiquidationSubmitted()));
							((TextView)view.findViewById(R.id.labels_home_approvedbymanager_claims)).setText(String.valueOf(app.dashboard.getClaimApprovedByManager()));
							((TextView)view.findViewById(R.id.labels_home_approvedbymanager_advances)).setText(String.valueOf(app.dashboard.getAdvanceApprovedByManager()));
							((TextView)view.findViewById(R.id.labels_home_approvedbymanager_liquidations)).setText(String.valueOf(app.dashboard.getLiquidationApprovedByManager()));
							((TextView)view.findViewById(R.id.labels_home_approvedbycm_advances)).setText(String.valueOf(app.dashboard.getAdvanceApprovedByCM()));
							((TextView)view.findViewById(R.id.labels_home_approvedbyaccounts_claims)).setText(String.valueOf(app.dashboard.getClaimApprovedByAccounts()));
							((TextView)view.findViewById(R.id.labels_home_approvedbyaccounts_advances)).setText(String.valueOf(app.dashboard.getAdvanceApprovedByAccounts()));
							((TextView)view.findViewById(R.id.labels_home_approvedbyaccounts_liquidations)).setText(String.valueOf(app.dashboard.getLiquidationApprovedByAccounts()));
							((TextView)view.findViewById(R.id.labels_home_paid_claims)).setText(String.valueOf(app.dashboard.getClaimPaid()));
							((TextView)view.findViewById(R.id.labels_home_paid_advances)).setText(String.valueOf(app.dashboard.getAdvancePaid()));
							((TextView)view.findViewById(R.id.labels_home_liquidated_advances)).setText(String.valueOf(app.dashboard.getAdvanceLiquidated()));
							((TextView)view.findViewById(R.id.labels_home_liqudated_liquidations)).setText(String.valueOf(app.dashboard.getLiquidationLiquidated()));
						}
					}
				});
			}
		}).start();
	}

	@Override
	public void onClick(View v) {
		if (v == actionbarMenuButton) {
			actionbarMenuButton.setEnabled(false); //can only be disabled after slide animation
			activity.toggleSidebar(actionbarMenuButton);
		} else if (v == actionbarRefresh) {
			refresh();
		} else if (v == actionbarNewLeaveButton) {
			startActivity(new Intent(activity, NewLeaveRequestActivity.class));
		/*} else if (v == actionbarNewClaimButton) {
			startActivity(new Intent(activity, NewClaimHeaderActivity.class));*/
		} else if (v == containerLeaveForApproval) {
			activity.linkToLeavesForApproval(HomeActivity.SIDEBARITEM_LEAVESFORAPPROVAL_KEY);
		} else if (v == containerClaimForApproval) {
			activity.linkToClaimsForApproval(HomeActivity.SIDEBARITEM_CLAIMSFORAPPROVAL_KEY);
		} else if (v == containerHrForApproval) {
			activity.linkToRecruitmentsForApproval(HomeActivity.SIDEBARITEM_RECRUITMENTSFORAPPROVAL_KEY);
		} else if (v == containerCapexForApproval) {
			activity.linkToCapexForApproval(HomeActivity.SIDEBARITEM_CAPEXESFORAPPROVAL_KEY);
		} else if (v == containerGhrForApproval) {
			activity.linkToGiftsForApproval(HomeActivity.SIDEBARITEM_GIFTSFORAPPROVAL_KEY);
		} else if (v == containerContractForApproval) {
			activity.linkToContractForApproval(HomeActivity.SIDEBARITEM_CONTRACTSFORAPPROVAL_KEY);
		} else if(v == actionbarHelpButton){
			showTutorial();
		}
	}

	private void showTutorial(){
//		newClaimSV = new ShowcaseView.Builder(activity) //new claim button tutorial
//				.setTarget(new ViewTarget(actionbarNewClaimButton))
//				.setContentTitle("New Claim Request")
//				.setContentText("Clicking this button will navigate you to create a new claim header")
//				.setStyle(R.style.CustomShowcaseTheme2)
//				.build();

		newLeaveSV = new ShowcaseView.Builder(activity) //new leave button tutorial
				.setTarget(new ViewTarget(actionbarNewLeaveButton))
				.setContentTitle("New Leave Request")
				.setContentText("Clicking this button will navigate you to file a new leave request")
				.setStyle(R.style.CustomShowcaseTheme2)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						newLeaveSV.hide();
//						newClaimSV.show();
					}
				})
				.build();

		showSidebarSV = new ShowcaseView.Builder(activity) //menu button tutorial
				.setTarget(new ViewTarget(actionbarMenuButton))
				.setContentTitle("Show Sidebar")
				.setContentText("Click this button to show the sidebar")
				.setStyle(R.style.CustomShowcaseTheme2)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						showSidebarSV.hide();
						newLeaveSV.show();
					}
				})
				.build();

		newLeaveSV.hide();
//		newClaimSV.hide();
	}
}