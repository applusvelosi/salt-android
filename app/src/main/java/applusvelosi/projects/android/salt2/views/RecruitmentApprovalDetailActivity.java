package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.recruitments.Recruitment;
import applusvelosi.projects.android.salt2.views.fragments.recruitment.RecruitmentForApprovalDetailFragment;

/**
 * Created by Velosi on 11/19/15.
 */
public class RecruitmentApprovalDetailActivity extends LinearNavFragmentActivity {
    public static final String INTENTKEY_RECRUITMENT = "intentkeyrecruitment";
    public static final String INTENTKEY_RECRUITMENTID = "intentkeyrecruitmentid";
    public int recruitmentHeaderID;
    public Recruitment recruitment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recruitmentHeaderID = getIntent().getExtras().getInt(INTENTKEY_RECRUITMENTID);
//        tempRecruitment = (Recruitment)getIntent().getExtras().getSerializable(INTENTKEY_RECRUITMENT);
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new RecruitmentForApprovalDetailFragment()).commit();
    }
}
