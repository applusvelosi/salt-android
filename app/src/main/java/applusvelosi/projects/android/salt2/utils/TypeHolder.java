package applusvelosi.projects.android.salt2.utils;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import applusvelosi.projects.android.salt2.models.ClaimTrail;
import applusvelosi.projects.android.salt2.models.CountryHoliday;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.Holiday;
import applusvelosi.projects.android.salt2.models.Leave;
import applusvelosi.projects.android.salt2.models.Office;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.capex.CapexHeader;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.models.recruitments.Recruitment;

public class TypeHolder {

	public Type staff = new TypeToken<Staff>(){}.getType();
	public Type office = new TypeToken<Office>(){}.getType();
	public Type leave = new TypeToken<Leave>(){}.getType();
	public Type claim = new TypeToken<ClaimHeader>(){}.getType();
	public Type capex = new TypeToken<CapexHeader>(){}.getType();
	public Type recruitment = new TypeToken<Recruitment>(){}.getType();
	public Type claimItem = new TypeToken<ClaimItem>(){}.getType();
	public Type currency = new TypeToken<Currency>(){}.getType();
	public Type arrayListOfString = new TypeToken<ArrayList<String>>(){}.getType();
	public Type arrayListOfHolidays = new TypeToken<ArrayList<CountryHoliday>>(){}.getType();
	public Type arrayListOfLocalHolidays = new TypeToken<ArrayList<Holiday>>(){}.getType();
	public Type arrayListOfLeaves = new TypeToken<ArrayList<Leave>>(){}.getType();
	public Type arrayListOfClaims = new TypeToken<ArrayList<ClaimHeader>>(){}.getType();
	public Type arrayListOfClaimItems = new TypeToken<ArrayList<ClaimItem>>(){}.getType();
	public Type arrayListOfCurrencies = new TypeToken<ArrayList<Currency>>(){}.getType();
	public Type arrayListOfDocuments = new TypeToken<ArrayList<Document>>(){}.getType();
	public Type arrayListOfClaimTrails = new TypeToken<ArrayList<ClaimTrail>>(){}.getType();
	public Type hashmapOfStringObject = new TypeToken<HashMap<String, Object>>(){}.getType();
	public Type hashmapOfStringString = new TypeToken<HashMap<String, String>>(){}.getType();
	public Type arrayListOfHashmapOfStringObject = new TypeToken<ArrayList<HashMap<String, Object>>>(){}.getType();
}
