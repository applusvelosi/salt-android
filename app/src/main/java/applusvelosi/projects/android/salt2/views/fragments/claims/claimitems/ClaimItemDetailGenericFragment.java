package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.ClaimItemAttendee;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs.ClaimItemDetailFragment;

public class ClaimItemDetailGenericFragment extends ClaimItemDetailFragment {
	private LinearLayout containersAttendees;

	public static ClaimItemDetailGenericFragment newInstance(int pos) {
		ClaimItemDetailGenericFragment frag = new ClaimItemDetailGenericFragment();
		Bundle b = new Bundle();
		b.putInt(KEY_ITEMPOS, pos);
		frag.setArguments(b);
		return frag;
	}

	@Override
	protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_claimitem_catgeneric_details, null);

		((TextView)view.findViewById(R.id.tviews_claimitemdetail_id)).setText(claimItem.getItemNumber());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_category)).setText(claimItem.getCategoryName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_expenseDate)).setText(claimItem.getExpenseDate(app));
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_amount)).setText(SaltApplication.decimalFormat.format(claimItem.getForeignAmount())+" "+claimItem.getForeignCurrencyName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_amountLocal)).setText(SaltApplication.decimalFormat.format(claimItem.getLocalAmount())+" "+claimItem.getLocalCurrencyName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_exRate)).setText(String.valueOf(claimItem.getStandardExchangeRate()));
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_desc)).setText(claimItem.getDescription());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_project)).setText((app.getStaffOffice().hasUsesSap() && app.getStaffOffice().hasUsesSapService())?claimItem.getWbsCode():claimItem.getProjectName());
		((TextView)view.findViewById(R.id.tviews_claimitemdetail_notesforreceipt)).setText(claimItem.getAttachmentNote());

		if (claimItem.isTaxApplied()) {
			if (app.getStaffOffice().hasUsesSap() && app.getStaffOffice().hasUsesSapService()) {
				((TextView) view.findViewById(R.id.tviews_claimiteminput_saptaxcode)).setText(claimItem.getSapTaxCode());
				((TextView) view.findViewById(R.id.tviews_claimitemdetail_taxRate)).setText(String.valueOf(claimItem.getTaxAmount()));
			} else {
				(view.findViewById(R.id.tviews_claimiteminput_saptaxcode)).setVisibility(View.GONE);
				((TextView) view.findViewById(R.id.tviews_claimitemdetail_taxRate)).setText(String.valueOf(claimItem.getTaxAmount()));
			}
		}else
			((TextView) view.findViewById(R.id.tviews_claimiteminput_saptaxcode)).setText("No");

			containersAttendees = (LinearLayout)view.findViewById(R.id.containers_claimitemdetail_attendees);
		if(claimItem.isBillable()) {
			((TextView)view.findViewById(R.id.tviews_claimitemdetail_billto)).setText(claimItem.getBillableCompanyName());
			((TextView)view.findViewById(R.id.tviews_claimitemdetail_notesorbillstoclient)).setText(claimItem.getNotes());
		} else {
			((TextView) view.findViewById(R.id.tviews_claimitemdetail_billto)).setText("No");
			view.findViewById(R.id.trs_claimitemdetail_notestoclient).setVisibility(View.GONE);
		}
		hasYesNoDocFlag = (TextView)view.findViewById(R.id.tviews_claimitemdetail_hasAttachmentFlag);
		receiptDoc = (TextView)view.findViewById(R.id.tviews_claimitemdetail_receipt);
		if(claimItem.hasReceipt()){
			receiptDoc.setText(claimItem.getAttachmentName());
			receiptDoc.setTextColor(activity.getResources().getColor(R.color.orange_velosi));
			receiptDoc.setOnClickListener(this);
		}else{
			receiptDoc.setText("No Attachment");
		}

		if(claimItem.getHasAttachmentFlag() == 1)
			hasYesNoDocFlag.setText("Yes");
		else if(claimItem.getHasAttachmentFlag() == 2)
			hasYesNoDocFlag.setText("No");
		else
			hasYesNoDocFlag.setText("N/A");

		for(ClaimItemAttendee attendee :claimItem.getAttendees()){
			LinearLayout v = (LinearLayout)inflater.inflate(R.layout.node_claimitemdetail_attendee, null);
            ((TextView)v.findViewById(R.id.tviews_nodes_claimitemdetail_attendee)).setText(attendee.getName());
            ((TextView)v.findViewById(R.id.tviews_nodes_claimitemdetail_jobtitle)).setText(attendee.getJobTitle());
            ((TextView)v.findViewById(R.id.tviews_nodes_claimitemdetail_notes)).setText(attendee.getNote());
			containersAttendees.addView(v);
		}

		return view;
	}
}
