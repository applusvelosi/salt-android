package applusvelosi.projects.android.salt2.views.fragments.claims.claimitems;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.Staff;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.utils.customviews.CustomViewPager;
import applusvelosi.projects.android.salt2.views.ClaimDetailActivity;
import applusvelosi.projects.android.salt2.views.ManageClaimItemActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs.ClaimItemDetailFragment;

/**
 * Created by Velosi on 2/22/16.
 */
public class ClaimItemCarousel extends LinearNavActionbarFragment implements ViewPager.OnPageChangeListener, View.OnClickListener{
    private static final String KEY_POS = "keypos";

    private ClaimDetailActivity activity;
    private ClaimItemCarouselAdapter adapter;
    private ArrayList<ClaimItemDetailFragment> claimItemDetailFragments;

    private TextView actionbarEditButton, actionbarReturnButton, actionbarApproveButton, actionbarRejectButton, actionbarCancelButton, actionbarTitle;
    private RelativeLayout actionbarBackButton;

    private LinearLayout containerPageIndicators;
    private CustomViewPager pager;

    private RelativeLayout dialogViewReject, dialogViewReturn;
    private EditText tviewDialogRejectReason, tviewDialogReturnReason;
    private AlertDialog diaglogReject, diaglogReturn;

    private int prevSelectedPage = 0;


    public static ClaimItemCarousel newInstance(int pos){
        ClaimItemCarousel frag = new ClaimItemCarousel();
        Bundle b = new Bundle();
        b.putInt(KEY_POS, pos);
        frag.setArguments(b);

        return frag;
    }

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (ClaimDetailActivity)getActivity();
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_claimitemdetail, null);
        actionbarBackButton = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        actionbarEditButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_edit);
        actionbarApproveButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_approve);
        actionbarReturnButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_return);
        actionbarRejectButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_reject);
        actionbarCancelButton = (TextView)actionbarLayout.findViewById(R.id.buttons_actionbar_cancel);
        actionbarTitle = (TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title);
        actionbarTitle.setText("Claim Item Detail");

        actionbarBackButton.setOnClickListener(this);
        actionbarApproveButton.setOnClickListener(this);
        actionbarReturnButton.setOnClickListener(this);
        actionbarRejectButton.setOnClickListener(this);
        actionbarCancelButton.setOnClickListener(this);
        actionbarEditButton.setOnClickListener(this);

        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_claimitemcarousel, null);
        containerPageIndicators = (LinearLayout)view.findViewById(R.id.containers_claimitemcarousel_indicators);
        pager = (CustomViewPager)view.findViewById(R.id.pager);
        claimItemDetailFragments = new ArrayList<>();
        containerPageIndicators.removeAllViews();
        for(int i=0; i<activity.claimItems.size(); i++) {
            LayoutInflater.from(activity).inflate((i == getArguments().getInt(KEY_POS))?R.layout.pageindicator_focus:R.layout.pageindicator_blur, containerPageIndicators);
            claimItemDetailFragments.add((activity.claimItems.get(i).getCategoryTypeID() == Category.TYPE_MILEAGE) ? ClaimItemDetailMileageFragment.newInstance(i) : ClaimItemDetailGenericFragment.newInstance(i));
        }
        adapter = new ClaimItemCarouselAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(this);
        pager.setCurrentItem(getArguments().getInt(KEY_POS));
        updateHeader(getArguments().getInt(KEY_POS));

        dialogViewReject = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
        tviewDialogRejectReason =  (EditText)dialogViewReject.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReject.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Rejection");
        diaglogReject = new AlertDialog.Builder(activity).setTitle("").setView(dialogViewReject)
                .setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(tviewDialogRejectReason.length() > 0) {
                            if (activity.claimHeader.isPaidByCompanyCard()) {
                                updateItemStatus(ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION, ClaimItem.UPDATABLEDATE.DATEREJECTED, tviewDialogRejectReason.getText().toString());
                            } else {
                                if (app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
                                    updateItemStatus(ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER, ClaimItem.UPDATABLEDATE.DATEREJECTED, tviewDialogRejectReason.getText().toString());
                                else
                                    updateItemStatus(ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER, ClaimItem.UPDATABLEDATE.DATEREJECTED, tviewDialogRejectReason.getText().toString());
                            }
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for rejection");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialogViewReturn = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
        tviewDialogReturnReason =  (EditText)dialogViewReturn.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReturn.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Returning");
        diaglogReturn = new AlertDialog.Builder(activity).setTitle("").setView(dialogViewReturn)
                .setPositiveButton("Return", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(tviewDialogReturnReason.length() > 0)
                            updateItemStatus(ClaimHeader.STATUSKEY_RETURN, ClaimItem.UPDATABLEDATE.NONE, tviewDialogReturnReason.getText().toString());
                        else
                            app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for returning");
                    }
                }).setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == actionbarBackButton){
            linearNavFragmentActivity.onBackPressed();
        }else if(v == actionbarTitle){
            linearNavFragmentActivity.onBackPressed();
        }else if(v == actionbarEditButton){
            Intent intent = new Intent(new Intent(activity, ManageClaimItemActivity.class));
            intent.putExtra(ManageClaimItemActivity.INTENTKEY_CLAIMHEADER, activity.claimHeader);
            intent.putExtra(ManageClaimItemActivity.INTENTKEY_CLAIMITEM, activity.claimItems.get(pager.getCurrentItem()));
            startActivity(intent);
        }else if(v == actionbarApproveButton){
//            if(activity.claimHeader.getTypeID()== ClaimHeader.TYPEKEY_ADVANCES && activity.claimHeader.getStatusID()==ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER){
//                if(app.getStaff().getUserPosition()== Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
//                    updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYCM, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYDIRECTOR, "Approved");
//                else
//                    app.showMessageDialog(activity, "Unable to process. You are not a country manager");
//            }else
//                updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYAPPROVER, "Approved");
            if(app.getStaff().getUserPosition()== Staff.USERPOSITION.USERPOSITION_CM || app.getStaff().getUserPosition() == Staff.USERPOSITION.USERPOSITION_CFO)
                updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYCM, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYDIRECTOR, "Approved");
            else
                updateItemStatus(ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER, ClaimItem.UPDATABLEDATE.DATEAPPROVEDBYAPPROVER, "Approved");
        }else if(v == actionbarRejectButton){
            tviewDialogRejectReason.setText("");
            diaglogReject.show();
        }else if(v == actionbarReturnButton){
            tviewDialogReturnReason.setText("");
            diaglogReturn.show();
        }
    }

    private void updateItemStatus(final int statusID, final ClaimItem.UPDATABLEDATE updatabledate, final String approverNotes){
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                ClaimItem oldClaimItem = new ClaimItem(activity.claimItems.get(pager.getCurrentItem()));
                activity.claimItems.get(pager.getCurrentItem()).updateApproverStatus(statusID, updatabledate, app.onlineGateway.epochizeDate(new Date()), approverNotes ,app.getStaff());

                Object tempResult;
                try{
                    tempResult = app.onlineGateway.saveClaimLineItem(activity.claimItems.get(pager.getCurrentItem()), 0, null);
                }catch(Exception e){
                    e.printStackTrace();
                    tempResult = e.getMessage();
                }

                final Object result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        activity.finishLoading();
                        if(result instanceof String)
                            app.showMessageDialog(activity, "Unable to update "+activity.claimItems.get(pager.getCurrentItem()).getItemNumber()+": "+result);
                        else{
                            try {
                                activity.claimItems.clear();
                                activity.claimItems.addAll((ArrayList<ClaimItem>) result);
                                adapter.notifyDataSetChanged();
                                updateHeader(pager.getCurrentItem());
                                linearNavFragmentActivity.onBackPressed();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }).start();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        updateHeader(position);
    }

    private void updateHeader(int position){
        ((ImageView)containerPageIndicators.getChildAt(prevSelectedPage)).setImageResource(R.drawable.bg_pageindicator_blur);
        ((ImageView)containerPageIndicators.getChildAt(position)).setImageResource(R.drawable.bg_pageindicator_focus);
        prevSelectedPage = position;

        if(activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_OPEN ){
            actionbarEditButton.setVisibility(View.VISIBLE);
            actionbarApproveButton.setVisibility(View.GONE);
            actionbarRejectButton.setVisibility(View.GONE);
            actionbarReturnButton.setVisibility(View.GONE);
            actionbarCancelButton.setVisibility(View.GONE);
        }else if(activity.claimHeader.getApproverID() == app.getStaff().getStaffID() || activity.claimItems.get(pager.getCurrentItem()).getStatusID() != ClaimHeader.STATUSKEY_PAID/*activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_SUBMITTED || activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYAPPROVER || activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_APPROVEDBYCM || activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYCOUNTRYMANAGER  || activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDBYAPPROVER || activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_REJECTEDFORSALARYDEDUCTION || activity.claimItems.get(pager.getCurrentItem()).getStatusID() == ClaimHeader.STATUSKEY_RETURN*/){
            if (app.getStaff().isApprover()) {
                actionbarEditButton.setVisibility(View.GONE);
                actionbarApproveButton.setVisibility(View.VISIBLE);
                actionbarRejectButton.setVisibility(View.VISIBLE);
                actionbarCancelButton.setVisibility(View.GONE);
                actionbarReturnButton.setVisibility((activity.claimHeader.isPaidByCompanyCard() || activity.claimHeader.getTypeID() == ClaimHeader.TYPEKEY_ADVANCES)?View.GONE:View.VISIBLE);
            }else{
                actionbarEditButton.setVisibility(View.GONE);
                actionbarApproveButton.setVisibility(View.GONE);
                actionbarRejectButton.setVisibility(View.GONE);
                actionbarCancelButton.setVisibility(View.GONE);
                actionbarReturnButton.setVisibility(View.GONE);
            }
        }else{
            actionbarEditButton.setVisibility(View.GONE);
            actionbarApproveButton.setVisibility(View.GONE);
            actionbarRejectButton.setVisibility(View.GONE);
            actionbarCancelButton.setVisibility(View.GONE);
            actionbarReturnButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ClaimItemCarouselAdapter extends FragmentPagerAdapter{

        public ClaimItemCarouselAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return claimItemDetailFragments.get(position);
        }

        @Override
        public int getCount() {
            return claimItemDetailFragments.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (position >= getCount()) {
                android.support.v4.app.FragmentManager manager = ((Fragment) object).getFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove((Fragment) object);
                trans.commit();
            }
        }

        @Override
        public int getItemPosition (Object object) {
            int index = claimItemDetailFragments.indexOf (object);
            if (index == -1)
                return POSITION_NONE;
            else
                return index;
        }
    }
}
