package applusvelosi.projects.android.salt2.models.claimheaders;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.CostCenter;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;

public class ClaimHeader implements Serializable{
    public static final String KEY_TYPEID = "ClaimTypeID";
	protected String accountManagerEmail, accountName, accountEmail;
	protected int accountID;
	protected boolean isActive;
	protected int approverID;
	protected String approverName, approverEmail, bacNumber;
	protected int baIDCharged, claimID;
	protected String claimNumber;
	protected int statusID, typeID;
	protected String typeName;
	protected int costCenterID;
	protected String costCenterName;
	protected int cmID, claimCMId;
	protected String cmEmail, cmName;
	protected int createdByID;
	protected String createdByName, currencySymbol,
			dateApprovedByAccount, dateApprovedByApprover, dateApprovedByCM, dateCancelled, dateCreated,
			dateModified, datePaid, dateRejected, dateSubmitted,
			hrOfficerEmail;
	protected boolean isPaidByCC;
	protected int modifiedByID;
	protected String modifiedByName;
	protected int officeID;
	protected String officeName;
	protected int officeToCharged, parentClaimID;
	protected String parentClaimNumber;
	protected int rejectedBy;
	protected String rejectedByName, staffEmail;
//	protected String secondaryApproverEmail, secondaryApproverName;
//	protected int secondaryApproverId;
	protected int staffID;
	protected String staffName, statusName;
	protected float totalAmount, totalAmountInLC, totalComputedApprovedInLC, totalComputedForDeductionInLC,
			totalComputedForPaymentInLC, totalComputedInLC, totalComputedRejectedInLC;
	protected String sapExpenseID, sapPaymentID, approversNote;
	private ArrayList<ClaimItem> claimItems;
	protected int hasYesNoDoc;
	protected String attachedCert;
	protected String docName;
	protected List<Document> attachments;
	protected String paymentMethod;

	//COPY Constructor
	public ClaimHeader(ClaimHeader source){
		accountManagerEmail = source.accountManagerEmail;
		accountName = source.accountName;
		accountEmail = source.accountEmail;
		accountID = source.accountID;
		approverID = source.approverID;
		approverName = source.approverName;
		approverEmail = source.approverEmail;
		approversNote = source.approversNote;
		attachedCert = source.attachedCert;
		attachments = source.attachments;
		bacNumber = source.bacNumber;
		baIDCharged = source.baIDCharged;
		claimCMId = source.claimCMId;
		claimID = source.claimID;
		claimItems = source.claimItems;
		claimNumber = source.claimNumber;
		costCenterID = source.costCenterID;
		costCenterName = source.costCenterName;
		cmID = source.cmID;
		cmEmail = source.cmEmail;
		cmName = source.cmName;
		createdByID = source.createdByID;
		createdByName = source.createdByName;
		currencySymbol = source.currencySymbol;
		dateApprovedByAccount = source.dateApprovedByAccount;
		dateApprovedByApprover = source.dateApprovedByApprover;
		dateApprovedByCM = source.dateApprovedByCM;
		dateCancelled = source.dateCancelled;
		dateCreated = source.dateCreated;
		dateModified = source.dateModified;
		datePaid = source.datePaid;
		dateRejected = source.dateRejected;
		dateSubmitted = source.dateSubmitted;
		hasYesNoDoc = source.hasYesNoDoc;
		hrOfficerEmail = source.hrOfficerEmail;
		docName = source.docName;
		isPaidByCC = source.isPaidByCC;
		isActive = source.isActive;
		modifiedByID = source.modifiedByID;
		modifiedByName = source.modifiedByName;
		officeID = source.officeID;
		officeName = source.officeName;
		officeToCharged = source.officeToCharged;
		parentClaimID = source.parentClaimID;
		parentClaimNumber = source.parentClaimNumber;
		paymentMethod = source.paymentMethod;
		rejectedBy = source.rejectedBy;
		rejectedByName = source.rejectedByName;
		sapExpenseID = source.sapExpenseID;
		sapPaymentID = source.sapPaymentID;
//		secondaryApproverEmail = source.secondaryApproverEmail;
//		secondaryApproverId = source.secondaryApproverId;
//		secondaryApproverName = source.secondaryApproverName;
		staffEmail = source.staffEmail;
		staffID = source.staffID;
		staffName = source.staffName;
		statusID = source.statusID;
		statusName = source.statusName;
		totalAmount = source.totalAmount;
		totalAmountInLC = source.totalAmountInLC;
		totalComputedApprovedInLC = source.totalComputedApprovedInLC;
		totalComputedForDeductionInLC = source.totalComputedForDeductionInLC;
		totalComputedForPaymentInLC = source.totalComputedForPaymentInLC;
		totalComputedInLC = source.totalComputedInLC;
		totalComputedRejectedInLC = source.totalComputedRejectedInLC;
		typeID = source.typeID;
		typeName = source.typeName;
	}

	public ClaimHeader(JSONObject jsonClaim) throws Exception {
		accountManagerEmail = jsonClaim.getString("AccountManagerEmail");
		accountName = jsonClaim.getString("AccountName");
		accountEmail = jsonClaim.getString("AccountsEmail");
		accountID = jsonClaim.getInt("AccountsID");
		attachedCert = jsonClaim.getString("AttachedCER");
		isActive = jsonClaim.getBoolean("Active");
		approverID = jsonClaim.getInt("ApproverID");
		approverName = jsonClaim.getString("ApproverName");
		approverEmail = jsonClaim.getString("ApproversEmail");
		approversNote = jsonClaim.getString("ApproversNote");
		bacNumber = jsonClaim.getString("BACNumber");
		baIDCharged = jsonClaim.getInt("BusinessAdvanceIDCharged");
		claimCMId = jsonClaim.getInt("ClaimCountryManagerId");
		claimID = jsonClaim.getInt("ClaimID");
//		JSONArray jsonLineItems = jsonClaim.getJSONArray("ClaimLineItems");
		claimNumber = jsonClaim.getString("ClaimNumber");
		statusID = jsonClaim.getInt("ClaimStatus");
		typeID = jsonClaim.getInt("ClaimTypeID");
		typeName = jsonClaim.getString("ClaimTypeName");
		costCenterID = jsonClaim.getInt("CostCenterID");
		costCenterName = jsonClaim.getString("CostCenterName");
		cmID = jsonClaim.getInt("CountryManager");
		cmEmail = jsonClaim.getString("CountryManagerEmail");
		cmName = jsonClaim.getString("CountryManagerName");
		createdByID = jsonClaim.getInt("CreatedBy");
		createdByName = jsonClaim.getString("CreatedByName");
		currencySymbol = jsonClaim.getString("CurrencySymbol");
		dateApprovedByAccount = jsonClaim.getString("DateApprovedByAccount");
		dateApprovedByApprover = jsonClaim.getString("DateApprovedByApprover");
		dateApprovedByCM = jsonClaim.getString("DateApprovedByDirector");
		dateCancelled = jsonClaim.getString("DateCancelled");
		dateCreated = jsonClaim.getString("DateCreated");
		dateModified = jsonClaim.getString("DateModified");
		datePaid = jsonClaim.getString("DatePaid");
		dateRejected = jsonClaim.getString("DateRejected");
		dateSubmitted = jsonClaim.getString("DateSubmitted");
		docName = jsonClaim.getString("DocName");
		JSONArray jsonDocs = jsonClaim.getJSONArray("Documents");
		attachments = new ArrayList<>();
		for (int i = 0; i < jsonDocs.length(); ++i)
			attachments.add(new Document(jsonDocs.getJSONObject(i)));
		hasYesNoDoc = jsonClaim.getInt("HasYesNoDoc");
		hrOfficerEmail = jsonClaim.getString("HROfficerEmail");
		isPaidByCC = jsonClaim.getBoolean("IsPaidByCompanyCC");
		statusName = jsonClaim.getString("StatusName");
		modifiedByID = jsonClaim.getInt("ModifiedBy");
		modifiedByName = jsonClaim.getString("ModifiedByName");
		officeID = jsonClaim.getInt("OfficeID");
		officeName = jsonClaim.getString("OfficeName");
		officeToCharged = jsonClaim.getInt("OfficeToCharge");
		paymentMethod = jsonClaim.getString("PaymentMethod");
		parentClaimID = jsonClaim.getInt("ParentClaimID");
		parentClaimNumber = jsonClaim.getString("ParentClaimNumber");
		rejectedBy = jsonClaim.getInt("RejectedBy");
		rejectedByName = jsonClaim.getString("RejectedByName");
		staffEmail = jsonClaim.getString("StaffEmail");
		staffID = jsonClaim.getInt("StaffID");
		staffName = jsonClaim.getString("StaffName");
//		secondaryApproverEmail = jsonClaim.getString("SecondaryApproverEmail");
//		secondaryApproverId = jsonClaim.getInt("SecondaryApproverId");
//		secondaryApproverName = jsonClaim.getString("SecondaryApproverName");
		sapExpenseID = jsonClaim.getString("SAPExpenseID");
		sapPaymentID = jsonClaim.getString("SAPPaymentID");
		totalAmount = (float)jsonClaim.getDouble("TotalAmount");
		totalAmountInLC = (float)jsonClaim.getDouble("TotalAmountInLC");
		totalComputedApprovedInLC = (float)jsonClaim.getDouble("TotalComputedApprovedInLC");
		totalComputedForDeductionInLC = (float)jsonClaim.getDouble("TotalComputedForDeductionInLC");
		totalComputedForPaymentInLC = (float)jsonClaim.getDouble("TotalComputedForPaymentInLC");
		totalComputedInLC = (float)jsonClaim.getDouble("TotalComputedInLC");
		totalComputedRejectedInLC = (float)jsonClaim.getDouble("TotalComputedRejectedInLC");
	}

	//new claim header
	protected ClaimHeader(SaltApplication app, int costCenterID, String costCenterName, int claimTypeID, boolean isPaidByCC, int baIDCharged, String bacNumber)
	{
		String staffName = app.getStaff().getFname()+" "+app.getStaff().getLname();
		accountManagerEmail = "";
		accountName = app.getStaff().getAccountName();
		accountEmail = app.getStaff().getAccountEmail();
		accountID = app.getStaff().getAccountID();
		isActive = true;
		approverID = app.getStaff().getExpenseApproverID();
		approverName = app.getStaff().getExpenseApproverName();
		approverEmail = app.getStaff().getExpenseApproverEmail();
		approversNote = "";
		attachedCert = "";
		this.baIDCharged = baIDCharged;
		this.bacNumber = bacNumber;
		claimCMId = 0;
		claimID = app.getStaffOffice().getCMID();;
		claimNumber = "";
		attachments = new ArrayList<Document>();
		statusID = STATUSKEY_OPEN;
		typeID = claimTypeID;
		typeName = getTypeDescriptionForKey(claimTypeID);
		claimItems = new ArrayList<ClaimItem>();
		this.costCenterID = costCenterID;
		this.costCenterName = costCenterName;
		cmID = app.getStaffOffice().getCMID();
		cmEmail = "";
		cmName = "";
		createdByID = app.getStaff().getStaffID();
		createdByName = staffName;
		currencySymbol = "";
		dateApprovedByAccount = "/Date(-2208988800000+0000)/";
		dateApprovedByApprover = "/Date(-2208988800000+0000)/";
		dateApprovedByCM = "/Date(-2208988800000+0000)/";
		dateCancelled = "/Date(-2208988800000+0000)/";
		dateCreated = app.onlineGateway.epochizeDate(new Date());
		dateModified = "/Date(-2208988800000+0000)/";
		datePaid = "/Date(-2208988800000+0000)/";
		dateRejected = "/Date(-2208988800000+0000)/";
		dateSubmitted = "/Date(-2208988800000+0000)/";
		docName = "";
		hasYesNoDoc = 0;
		hrOfficerEmail = "";
		this.isPaidByCC = isPaidByCC;
		modifiedByID = app.getStaff().getStaffID();
		modifiedByName = staffName;
		officeID = app.getStaff().getOfficeID();
		officeName = app.getStaffOffice().getName();
		officeToCharged = app.getStaff().getOfficeID();
		parentClaimID = 0;
		parentClaimNumber = "";
		paymentMethod = "";
		rejectedBy = 0;
		rejectedByName = "";
		sapExpenseID = "";
		sapPaymentID = "";
//		secondaryApproverEmail = "";
//		secondaryApproverId = 0;
//		secondaryApproverName = "";
		staffEmail = app.getStaff().getEmail();
		staffID = app.getStaff().getStaffID();
		this.staffName = staffName;
		statusName = STATUSDESC_OPEN;
		totalAmount = 0;
		totalAmountInLC = 0;
		totalComputedApprovedInLC = 0;
		totalComputedForDeductionInLC = 0;
		totalComputedForPaymentInLC = 0;
		totalComputedInLC = 0;
		totalComputedRejectedInLC = 0;
	}

	//new splitted claim header for claimitems return status
	protected ClaimHeader(SaltApplication app, int hasYesNoDocFlag, List<Document> documents,  int costCenterID, String costCenterName, int claimTypeID, boolean isPaidByCC, int baIDCharged, String bacNumber)
	{
		String staffName = app.getStaff().getFname()+" "+app.getStaff().getLname();
		accountManagerEmail = "";
		accountName = app.getStaff().getAccountName();
		accountEmail = app.getStaff().getAccountEmail();
		accountID = app.getStaff().getAccountID();
		isActive = true;
		approverID = app.getStaff().getExpenseApproverID();
		approverName = app.getStaff().getExpenseApproverName();
		approverEmail = app.getStaff().getExpenseApproverEmail();
		approversNote = "";
		attachedCert = "";
		this.baIDCharged = baIDCharged;
		this.bacNumber = bacNumber;
		claimID = 0;
		claimNumber = "";
		attachments = documents;
		statusID = STATUSKEY_OPEN;
		typeID = claimTypeID;
		typeName = getTypeDescriptionForKey(claimTypeID);
		claimItems = new ArrayList<ClaimItem>();
		this.costCenterID = costCenterID;
		this.costCenterName = costCenterName;
		claimCMId = 0;
		cmID = app.getStaffOffice().getCMID();
		cmEmail = "";
		cmName = "";
		createdByID = app.getStaff().getStaffID();
		createdByName = staffName;
		currencySymbol = "";
		dateApprovedByAccount = "/Date(-2208988800000+0000)/";
		dateApprovedByApprover = "/Date(-2208988800000+0000)/";
		dateApprovedByCM = "/Date(-2208988800000+0000)/";
		dateCancelled = "/Date(-2208988800000+0000)/";
		dateCreated = app.onlineGateway.epochizeDate(new Date());
		dateModified = "/Date(-2208988800000+0000)/";
		datePaid = "/Date(-2208988800000+0000)/";
		dateRejected = "/Date(-2208988800000+0000)/";
		dateSubmitted = "/Date(-2208988800000+0000)/";
		docName = "";
		hasYesNoDoc = hasYesNoDocFlag;
		hrOfficerEmail = "";
		this.isPaidByCC = isPaidByCC;
		modifiedByID = app.getStaff().getStaffID();
		modifiedByName = staffName;
		officeID = app.getStaff().getOfficeID();
		officeName = app.getStaffOffice().getName();
		officeToCharged = app.getStaff().getOfficeID();
		parentClaimID = 0;
		parentClaimNumber = "";
		paymentMethod = "";
		rejectedBy = 0;
		rejectedByName = "";
		sapExpenseID = "";
		sapPaymentID = "";
//		secondaryApproverEmail = "";
//		secondaryApproverId = 0;
//		secondaryApproverName = "";
		staffEmail = app.getStaff().getEmail();
		staffID = app.getStaff().getStaffID();
		this.staffName = staffName;
		statusName = STATUSDESC_OPEN;
		totalAmount = 0.0f;
		totalAmountInLC = 0.0f;
		totalComputedApprovedInLC = 0.0f;
		totalComputedForDeductionInLC = 0.0f;
		totalComputedForPaymentInLC = 0.0f;
		totalComputedInLC = 0.0f;
		totalComputedRejectedInLC = 0.0f;
	}

	public ClaimHeader(){

	}

	public void addAttachment(Document document) {
		this.attachments.add(0, document);
	}

	public void setHasDocument(boolean hasYesNoDoc) {
		this.hasYesNoDoc = (hasYesNoDoc)?1:0;
	}
	public void setCountryManagerId(int cmId) {
		claimCMId = cmId;
	}
	public void setAccountID(int accountId){
		accountID = accountId;
	}
    public void updateStatus(SaltApplication app, int statusID, ClaimItem.UPDATABLEDATE updatabledate, String note) throws Exception {
		int staffID = app.getStaff().getStaffID();
		String staffName = app.getStaff().getFname()+" "+app.getStaff().getLname();
        String latestDate = app.onlineGateway.epochizeDate(new Date());
		this.statusID = statusID;
		this.statusName = getStatusDescriptionForKey(statusID);
		modifiedByID = staffID;
		modifiedByName = staffName;
		approversNote = note;
		dateCreated = latestDate;

		switch (updatabledate) {
			case DATESUBMITTED: dateSubmitted = latestDate; break;
			case DATEAPPROVEDBYAPPROVER: dateApprovedByApprover = latestDate; break;
			case DATEAPPROVEDBYDIRECTOR: dateApprovedByCM = latestDate; break;
			case DATECANCELLED: dateCancelled = latestDate; break;
			case DATEPAID: datePaid = latestDate; break;
			case DATEREJECTED: {
				dateRejected = latestDate;
				rejectedBy = staffID;
				rejectedByName = staffName;
			} break;
		}
    }

	public void updateSplittedChildClaim(SaltApplication app, int statusID, int claimId, String claimNumber, int parentClaimID, String parentClaimNumber, int nextApproverId, ClaimItem.UPDATABLEDATE updatabledate, String note)
	{
		int staffID = app.getStaff().getStaffID();
		String staffName = app.getStaff().getFname()+" "+app.getStaff().getLname();
		String latestDate = app.onlineGateway.epochizeDate(new Date());
		this.statusName = getStatusDescriptionForKey(statusID);
		this.statusID = statusID;
		modifiedByID = staffID;
		modifiedByName = staffName;
		approversNote = note;
		this.parentClaimID = parentClaimID;
		this.parentClaimNumber = parentClaimNumber;
		this.claimID = claimId;
		this.claimNumber = claimNumber;
		claimCMId = nextApproverId;
		hasYesNoDoc = 0;
		totalAmount = 0.0f;
		totalAmountInLC = 0.0f;
		totalComputedApprovedInLC = 0.0f;
		totalComputedForDeductionInLC = 0.0f;
		totalComputedForPaymentInLC = 0.0f;
		totalComputedInLC = 0.0f;
		totalComputedRejectedInLC = 0.0f;

		switch (updatabledate){
			case DATESUBMITTED:
				dateSubmitted = latestDate;
				break;
			case DATEAPPROVEDBYAPPROVER:
				dateApprovedByApprover = latestDate;
				break;
			case DATEAPPROVEDBYDIRECTOR:
				dateApprovedByCM = latestDate;
				break;
			case DATECANCELLED:
				dateCancelled = latestDate;
				break;
			case DATEPAID:
				datePaid = latestDate;
				break;
			case DATEREJECTED: {
				dateRejected = latestDate;
				rejectedBy = staffID;
				rejectedByName = staffName;
			} break;
		}
	}

	public void editClaimHeader(CostCenter newCostCenter, boolean isPaidByCC, SaltApplication app){
		if(typeID == TYPEKEY_CLAIMS)
			this.isPaidByCC = isPaidByCC;
		costCenterID = newCostCenter.getCostCenterID();
		costCenterName = newCostCenter.getCostCenterName();
		dateModified = app.onlineGateway.epochizeDate(new Date());
		modifiedByName = app.getStaff().getFname()+" "+app.getStaff().getLname();
		modifiedByID = app.getStaff().getStaffID();
	}

	public String getAccountManagerEmail(){
		return accountManagerEmail;
	}
	public String getAccountName(){
		return accountName;
	}
	public String getAccountEmail() {
		return accountEmail;
	}
	public int getAccountID(){
		return accountID; }
	public boolean isActive(){
		return isActive; }
	public int getApproverID(){
		return approverID; }
	public int getStaffCMId(){
		return cmID;
	}
	public int getClaimCMId() {
		return claimCMId;
	}
	public String getApproverName(){
		return approverName; }
	public String getApproverEmail(){
		return approverEmail;
	}
	public boolean isPaidByCompanyCard(){
		return isPaidByCC;
	}
	public int getClaimID(){
		return claimID; }
	public ArrayList<ClaimItem> getClaimItems(){
		return claimItems; }
	public String getClaimNumber(){
		return claimNumber; }
	public int getStatusID(){
		return statusID; }
	public String getStatusName(){ return statusName; }
	public int getTypeID(){ return typeID; }
	public String getTypeName(){ return typeName; }
	public int getCostCenterID(){ return costCenterID; }
	public String getCostCenterName(){ return costCenterName; }
	public int getCreatedByID(){ return createdByID; }
	public String getCreatedByName(){ return createdByName; }
	public String getCurrencySymbol(){ return currencySymbol; }

	public String getDateApprovedByAccount(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateApprovedByAccount);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}

	public String getDateApprovedByApprover(SaltApplication app )throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateApprovedByApprover);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}

	public String getDateCancelled(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateCancelled);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}

	public String getDateCreated(SaltApplication app){
		String tempDate = app.onlineGateway.dJsonizeDate(dateCreated);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}
	public String getDateModified(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateModified);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}

	public String getDatePaid(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(datePaid);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}

	public String getDateRejected(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateRejected);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
	}

	public String getDateSubmitted(SaltApplication app)throws Exception {
		String tempDate = app.onlineGateway.dJsonizeDateForApproval(dateSubmitted);
		return (tempDate.contains("-Jan-1900") || tempDate.contains("-Dec-1899"))?"":tempDate;
//		return (dateSubmitted.contains("-2208988800000"))?"-":app.onlineGateway.dJsonizeDate(dateSubmitted);
	}
	public String getHrOfficerEmail(){ return hrOfficerEmail; }
	public int getModifiedByID(){ return modifiedByID; }
	public String getModifiedByName(){ return modifiedByName; }
	public int getOfficeID(){ return officeID; }
	public String getOfficeName(){ return officeName; }
	public int getOfficeToCharged(){ return officeToCharged; }
	public String getPaymentMethod() { return paymentMethod; }
	public int getParentClaimID() {
		return parentClaimID;
	}
	public int getRejectedBy(){ return rejectedBy; }
	public String getRejectedByName(){ return rejectedByName; }
	public int getStaffID(){ return staffID; }
	public String getStaffEmail(){ return staffEmail; }
	public String getStaffName(){ return staffName; }
	public float getTotalAmount(){ return totalAmount; }
	public float getTotalAmountInLC(){ return totalAmountInLC; }
	public float getTotalComputedApprovedInLC(){ return totalComputedApprovedInLC; }
	public float getTotalComputedForPaymentInLC(){ return totalComputedForPaymentInLC; }
	public float getTotalComputedInLC(){ return totalComputedInLC; }
	public float getTotalComputedRejectedInLC(){ return totalComputedRejectedInLC; }
	public String getSapExpenseID() {
		return sapExpenseID;
	}
	public String getSapPaymentID() {
		return sapPaymentID;
	}
	public String getApproversNote() {
		return approversNote;
	}
	public int hasDocumentFlag() {
		return hasYesNoDoc;
	}
	public String getAttachedCert() { return attachedCert; }
	public String getDocName() { return docName; }
	public List<Document> getAttachments() {
		return attachments;
	}

	public JsonObject jsonize(){
		JsonObject map = new JsonObject();
		map.addProperty("AccountManagerEmail", accountManagerEmail);
		map.addProperty("AccountName", accountName);
		map.addProperty("AccountsEmail", accountEmail);
		map.addProperty("AccountsID", accountID);
		map.addProperty("Active", isActive);
		map.addProperty("ApproverID", approverID);
		map.addProperty("ApproverName", approverName);
		map.addProperty("ApproversEmail", approverEmail);
		map.addProperty("ApproversNote", approversNote);
		map.addProperty("AttachedCER", attachedCert);
		map.addProperty("HasYesNoDoc", hasYesNoDoc);
		map.addProperty("BACNumber", bacNumber);
		map.addProperty("BusinessAdvanceIDCharged", baIDCharged);
		map.addProperty("ClaimID", claimID);
		map.addProperty("ClaimCountryManagerId", claimCMId);
		map.add("ClaimLineItems", new JsonArray());
		map.addProperty("ClaimNumber", claimNumber);
		map.addProperty("ClaimStatus", statusID);
		map.addProperty("ClaimTypeID", typeID);
		map.addProperty("ClaimTypeName", typeName);
		map.addProperty("CostCenterID", costCenterID);
		map.addProperty("CostCenterName", costCenterName);
		map.addProperty("CountryManager", cmID);
		map.addProperty("CountryManagerEmail", cmEmail);
		map.addProperty("CountryManagerName", cmName);
		map.addProperty("CreatedBy", createdByID);
		map.addProperty("CreatedByName", createdByName);
		map.addProperty("CurrencySymbol", currencySymbol);
		map.addProperty("DateApprovedByAccount", dateApprovedByAccount);
		map.addProperty("DateApprovedByApprover", dateApprovedByApprover);
		map.addProperty("DateApprovedByDirector", dateApprovedByCM);
		map.addProperty("DateCancelled", dateCancelled);
		map.addProperty("DateCreated", dateCreated);
		map.addProperty("DateModified", dateModified);
		map.addProperty("DatePaid", datePaid);
		map.addProperty("DateRejected", dateRejected);
		map.addProperty("DateSubmitted", dateSubmitted);
		map.addProperty("DocName", docName);
		JsonArray jsonAttachments = new JsonArray();
		for (Document document : attachments)
			jsonAttachments.add(document.getJSONObject());
		map.add("Documents", jsonAttachments);
		map.addProperty("HasYesNoDoc", hasYesNoDoc);
		map.addProperty("HROfficerEmail", hrOfficerEmail);
		map.addProperty("IsPaidByCompanyCC", isPaidByCC);
		map.addProperty("ModifiedBy", modifiedByID);
		map.addProperty("ModifiedByName", modifiedByName);
		map.addProperty("OfficeID", officeID);
		map.addProperty("OfficeName", officeName);
		map.addProperty("OfficeToCharge", officeToCharged);
		map.addProperty("ParentClaimID", parentClaimID);
		map.addProperty("ParentClaimNumber", parentClaimNumber);
		map.addProperty("PaymentMethod" , paymentMethod);
		map.addProperty("RejectedBy", rejectedBy);
		map.addProperty("RejectedByName", rejectedByName);
		map.addProperty("SAPExpenseID", sapExpenseID);
		map.addProperty("SAPPaymentID", sapPaymentID);
//		map.addProperty("SecondaryApproverEmail", secondaryApproverEmail);
//		map.addProperty("SecondaryApproverId", secondaryApproverId);
//		map.addProperty("SecondaryApproverName", secondaryApproverName);
		map.addProperty("StaffEmail", staffEmail);
		map.addProperty("StaffID", staffID);
		map.addProperty("StaffName", staffName);
		map.addProperty("StatusName", statusName);
		map.addProperty("TotalAmount", totalAmount);
		map.addProperty("TotalAmountInLC", totalAmountInLC);
		map.addProperty("TotalComputedApprovedInLC", totalComputedApprovedInLC);
		map.addProperty("TotalComputedForDeductionInLC", totalComputedForDeductionInLC);
		map.addProperty("TotalComputedForPaymentInLC", totalComputedForPaymentInLC);
		map.addProperty("TotalComputedInLC", totalComputedInLC);
		map.addProperty("TotalComputedRejectedInLC", totalComputedInLC);
		return map;
	}

	public static String getEmptyJSON() throws Exception{
		JsonObject map = new JsonObject();
		map.addProperty("AccountManagerEmail", "");
		map.addProperty("AccountName", "");
		map.addProperty("AccountsEmail", "");
		map.addProperty("AccountsID", 0);
		map.addProperty("Active", true);
		map.addProperty("ApproverID", 0);
		map.addProperty("ApproverName", "");
		map.addProperty("ApproversEmail", "");
		map.addProperty("AttachedCER","");
		map.addProperty("BACNumber", "");
		map.addProperty("BusinessAdvanceIDCharged", 0);
		map.addProperty("ClaimCountryManagerId", 0);
		map.addProperty("ClaimID", 0);
		map.add("ClaimLineItems", new JsonArray());
		map.addProperty("ClaimNumber", "");
		map.addProperty("ClaimStatus", STATUSKEY_OPEN);
		map.addProperty("ClaimTypeID", TYPEKEY_CLAIMS);
		map.addProperty("ClaimTypeName", TYPEDESC_CLAIMS);
		map.addProperty("CostCenterID", 0);
		map.addProperty("CostCenterName", "");
		map.addProperty("CountryManager", 0);
		map.addProperty("CountryManagerEmail", "");
		map.addProperty("CountryManagerName", "");
		map.addProperty("CreatedBy", 0);
		map.addProperty("CreatedByName", "");
		map.addProperty("CurrencySymbol", "");
		map.addProperty("DateApprovedByAccount", "/Date(-2208988800000+0000)/");
		map.addProperty("DateApprovedByApprover", "/Date(-2208988800000+0000)/");
		map.addProperty("DateApprovedByDirector", "/Date(-2208988800000+0000)/");
		map.addProperty("DateCancelled", "/Date(-2208988800000+0000)/");
		map.addProperty("DateCreated", "/Date(-2208988800000+0000)/");
		map.addProperty("DateModified", "/Date(-2208988800000+0000)/");
		map.addProperty("DatePaid", "/Date(-2208988800000+0000)/");
		map.addProperty("DateRejected", "/Date(-2208988800000+0000)/");
		map.addProperty("DateSubmitted", "/Date(-2208988800000+0000)/");
		map.add("Documents", new JsonArray());
		map.addProperty("DocName", "");
		map.addProperty("HROfficerEmail", "");
		map.addProperty("HasYesNoDoc", 0);
		map.addProperty("IsPaidByCompanyCC", false);
		map.addProperty("ModifiedBy", 0);
		map.addProperty("ModifiedByName", "");
		map.addProperty("OfficeID", 0);
		map.addProperty("OfficeName", "");
		map.addProperty("OfficeToCharge", 9);
		map.addProperty("PaymentMethod", "");
		map.addProperty("ParentClaimID", 0);
		map.addProperty("ParentClaimNumber", "");
		map.addProperty("RejectedBy", 0);
		map.addProperty("RejectedByName", "");
		map.addProperty("SAPExpenseID", "");
		map.addProperty("SAPPaymentID", "");
//		map.addProperty("SecondaryApproverEmail", "");
//		map.addProperty("SecondaryApproverId", 0);
//		map.addProperty("SecondaryApproverName", "");
		map.addProperty("StaffEmail", "");
		map.addProperty("StaffID", 0);
		map.addProperty("StaffName", "");
		map.addProperty("StatusName", "");
		map.addProperty("TotalAmount", 0);
		map.addProperty("TotalAmountInLC", 0);
		map.addProperty("TotalComputedApprovedInLC", 0);
		map.addProperty("TotalComputedForDeductionInLC", 0);
		map.addProperty("TotalComputedForPaymentInLC", 0);
		map.addProperty("TotalComputedInLC", 0);
		map.addProperty("TotalComputedRejectedInLC", 0);
		map.addProperty("SAPExpenseID","");

		return map.toString();
	}	
	
	//for creating new claimHeader item
//	private ClaimItem tempNewClaimItem, tempUpdatedClaimItem;
	
//	public void prepareForCreatingNewClamItem(SaltApplication app){
//		tempNewClaimItem = new ClaimItem(app);
//	}

//	public ClaimItem getPreparedClaimItemForCreation(){
//		return tempNewClaimItem;
//	}

//	public void prepareForUpdatingClaimItem(int claimItemPos, SaltApplication app){
//		tempUpdatedClaimItem = getClaimItems(app).get(claimItemPos);
//	}
//
//	public ClaimItem getPreparedClaimItemForEdit(){
//		return tempUpdatedClaimItem;
//	}

	protected String getStringedDate(String stringedDate){
		if(Integer.parseInt(stringedDate.split("-")[2]) == 1900)
			return "-";
		else return stringedDate;
	}

	private static final String TYPE_CLAIMS = "1-Claim";
	private static final String TYPE_ADVANCES = "2-Business Advance";
	private static final String TYPE_LIQUIDATION = "3-Liquidation of BA";

	private static final String STATUS_OPEN = "1-Open";
	private static final String STATUS_SUBMITTED = "2-Submitted";
	private static final String STATUS_APPROVEDBYAPPROVER = "3-Approved by Approver";
	private static final String STATUS_REJECTEDBYAPPROVER = "4-Rejected by Approver";
	private static final String STATUS_CANCELLED = "5-Cancelled";
	private static final String STATUS_PAID = "6-Paid";
	private static final String STATUS_APPROVEDBYCOUNTRYMANAGER = "7-Approved by CM";
	private static final String STATUS_REJECTEDBYCOUNTRYMANAGER = "9-Rejected by CM";
	private static final String STATUS_REJECTEDBYACCOUNTS = "11-Rejected by Accounts";
	private static final String STATUS_APPROVEDBYACCOUNTS = "16-Approved by Accounts";
	private static final String STATUS_RETURN = "17-Return";
    private static final String STATUS_LIQUIDATED = "23-Liquidated";
	private static final String STATUS_PAIDUNDERCOMPANYCARD = "24-Paid Under Company Card";
	private static final String STATUS_REJECTEDFORSALARYDEDUCTION = "25-Rejected For Salary Deduction";
	private static final String STATUS_SENTTOSAP = "69-Sent To SAP";

	public static int TYPEKEY_CLAIMS, TYPEKEY_ADVANCES, TYPEKEY_LIQUIDATION;
	public static String TYPEDESC_CLAIMS, TYPEDESC_ADVANCES, TYPEDESC_LIQUIDATION;
	public static int STATUSKEY_OPEN, STATUSKEY_SUBMITTED, STATUSKEY_APPROVEDBYAPPROVER, STATUSKEY_REJECTEDBYAPPROVER, STATUSKEY_CANCELLED,
			STATUSKEY_PAID, STATUSKEY_APPROVEDBYCM, STATUSKEY_REJECTEDBYCOUNTRYMANAGER, STATUSKEY_RETURN, STATUSKEY_SENTTOSAP,
			STATUSKEY_REJECTEDBYACCOUNTS, STATUSKEY_APPROVEDBYACCOUNTS, STATUSKEY_PAIDUNDERCOMPANYCARD, STATUSKEY_REJECTEDFORSALARYDEDUCTION, STATUSKEY_LIQUIDATED;
	public static String STATUSDESC_OPEN, STATUSDESC_SUBMITTED, STATUSDESC_APPROVEDBYAPPROVER, STATUSDESC_REJECTEDBYAPPROVER, STATUSDESC_CANCELLED,
			STATUSDESC_PAID, STATUSDESC_APPROVEDBYCM, STATUSDESC_REJECTEDBYCOUNTRYMANAGER, STATUSDESC_RETURN, STATUSDESC_SENTTOSAP,
			STATUSDESC_REJECTEDBYACCOUNTS, STATUSDESC_APPROVEDBYACCOUNTS, STATUSDESC_PAIDUNDERCOMPANYCARD, STATUSDESC_REJECTEDFORSALARYDEDUCTION, STATUSDESC_LIQUIDATED;

	private static ArrayList<String> typeDescriptionList, statusDescriptionList;

	static{
		TYPEKEY_CLAIMS = Integer.parseInt(TYPE_CLAIMS.split("-")[0]);
		TYPEKEY_ADVANCES = Integer.parseInt(TYPE_ADVANCES.split("-")[0]);
		TYPEKEY_LIQUIDATION = Integer.parseInt(TYPE_LIQUIDATION.split("-")[0]);

		TYPEDESC_CLAIMS = TYPE_CLAIMS.split("-")[1];
		TYPEDESC_ADVANCES = TYPE_ADVANCES.split("-")[1];
		TYPEDESC_LIQUIDATION = TYPE_LIQUIDATION.split("-")[1];

		STATUSKEY_OPEN = Integer.parseInt(STATUS_OPEN.split("-")[0]);
		STATUSKEY_SUBMITTED = Integer.parseInt(STATUS_SUBMITTED.split("-")[0]);
		STATUSKEY_APPROVEDBYAPPROVER = Integer.parseInt(STATUS_APPROVEDBYAPPROVER.split("-")[0]);
		STATUSKEY_REJECTEDBYAPPROVER = Integer.parseInt(STATUS_REJECTEDBYAPPROVER.split("-")[0]);
		STATUSKEY_CANCELLED = Integer.parseInt(STATUS_CANCELLED.split("-")[0]);
		STATUSKEY_PAID = Integer.parseInt(STATUS_PAID.split("-")[0]);
		STATUSKEY_APPROVEDBYCM = Integer.parseInt(STATUS_APPROVEDBYCOUNTRYMANAGER.split("-")[0]);
		STATUSKEY_REJECTEDBYCOUNTRYMANAGER = Integer.parseInt(STATUS_REJECTEDBYCOUNTRYMANAGER.split("-")[0]);
		STATUSKEY_REJECTEDBYACCOUNTS = Integer.parseInt(STATUS_REJECTEDBYACCOUNTS.split("-")[0]);
		STATUSKEY_APPROVEDBYACCOUNTS = Integer.parseInt(STATUS_APPROVEDBYACCOUNTS.split("-")[0]);
		STATUSKEY_RETURN = Integer.parseInt(STATUS_RETURN.split("-")[0]);
		STATUSKEY_PAIDUNDERCOMPANYCARD = Integer.parseInt(STATUS_PAIDUNDERCOMPANYCARD.split("-")[0]);
		STATUSKEY_REJECTEDFORSALARYDEDUCTION = Integer.parseInt(STATUS_REJECTEDFORSALARYDEDUCTION.split("-")[0]);
        STATUSKEY_LIQUIDATED = Integer.parseInt(STATUS_LIQUIDATED.split("-")[0]);
		STATUSKEY_SENTTOSAP = Integer.parseInt(STATUS_SENTTOSAP.split("-")[0]);

		STATUSDESC_OPEN = STATUS_OPEN.split("-")[1];
		STATUSDESC_SUBMITTED = STATUS_SUBMITTED.split("-")[1];
		STATUSDESC_APPROVEDBYAPPROVER = STATUS_APPROVEDBYAPPROVER.split("-")[1];
		STATUSDESC_REJECTEDBYAPPROVER = STATUS_REJECTEDBYAPPROVER.split("-")[1];
		STATUSDESC_CANCELLED = STATUS_CANCELLED.split("-")[1];
		STATUSDESC_PAID = STATUS_PAID.split("-")[1];
		STATUSDESC_APPROVEDBYCM = STATUS_APPROVEDBYCOUNTRYMANAGER.split("-")[1];
		STATUSDESC_REJECTEDBYCOUNTRYMANAGER = STATUS_REJECTEDBYCOUNTRYMANAGER.split("-")[1];
		STATUSDESC_REJECTEDBYACCOUNTS = STATUS_REJECTEDBYACCOUNTS.split("-")[1];
		STATUSDESC_APPROVEDBYACCOUNTS = STATUS_APPROVEDBYACCOUNTS.split("-")[1];
		STATUSDESC_RETURN = STATUS_RETURN.split("-")[1];
		STATUSDESC_PAIDUNDERCOMPANYCARD = STATUS_PAIDUNDERCOMPANYCARD.split("-")[1];
		STATUSDESC_REJECTEDFORSALARYDEDUCTION = STATUS_REJECTEDFORSALARYDEDUCTION.split("-")[1];
        STATUSDESC_LIQUIDATED = STATUS_LIQUIDATED.split("-")[1];
		STATUSDESC_SENTTOSAP = STATUS_SENTTOSAP.split("-")[1];

		typeDescriptionList = new ArrayList<String>();
		typeDescriptionList.add(TYPEDESC_CLAIMS);
		typeDescriptionList.add(TYPEDESC_ADVANCES);
		typeDescriptionList.add(TYPEDESC_LIQUIDATION);

		statusDescriptionList = new ArrayList<String>();
		statusDescriptionList.add(STATUSDESC_OPEN);
		statusDescriptionList.add(STATUSDESC_SUBMITTED);
		statusDescriptionList.add(STATUSDESC_APPROVEDBYAPPROVER);
		statusDescriptionList.add(STATUSDESC_REJECTEDBYAPPROVER);
		statusDescriptionList.add(STATUSDESC_CANCELLED);
		statusDescriptionList.add(STATUSDESC_PAID);
		statusDescriptionList.add(STATUSDESC_APPROVEDBYCM);
		statusDescriptionList.add(STATUSDESC_REJECTEDBYCOUNTRYMANAGER);
		statusDescriptionList.add(STATUSDESC_REJECTEDBYACCOUNTS);
		statusDescriptionList.add(STATUSDESC_APPROVEDBYACCOUNTS);
		statusDescriptionList.add(STATUSDESC_RETURN);
		statusDescriptionList.add(STATUSDESC_PAIDUNDERCOMPANYCARD);
		statusDescriptionList.add(STATUSDESC_REJECTEDFORSALARYDEDUCTION);
        statusDescriptionList.add(STATUSDESC_LIQUIDATED);
		statusDescriptionList.add(STATUSDESC_SENTTOSAP);
	}

	public static String getTypeDescriptionForKey(int key){
		if(key == TYPEKEY_ADVANCES) return TYPEDESC_ADVANCES;
		else if(key == TYPEKEY_CLAIMS) return TYPEDESC_CLAIMS;
		else return TYPEDESC_LIQUIDATION;
	}

	public static int getTypeKeyForDescription(String description){
		if(description == TYPEDESC_ADVANCES) return TYPEKEY_ADVANCES;
		else if(description == TYPEDESC_CLAIMS) return TYPEKEY_CLAIMS;
		else return TYPEKEY_LIQUIDATION;
	}

	public static String getStatusDescriptionForKey(int key){
		if(key == STATUSKEY_OPEN) return STATUSDESC_OPEN;
		else if(key == STATUSKEY_SUBMITTED) return STATUSDESC_SUBMITTED;
		else if(key == STATUSKEY_APPROVEDBYAPPROVER) return STATUSDESC_APPROVEDBYAPPROVER;
		else if(key == STATUSKEY_REJECTEDBYAPPROVER) return STATUSDESC_REJECTEDBYAPPROVER;
		else if(key == STATUSKEY_CANCELLED) return STATUSDESC_CANCELLED;
		else if(key == STATUSKEY_PAID) return STATUSDESC_PAID;
		else if(key == STATUSKEY_APPROVEDBYCM) return STATUSDESC_APPROVEDBYCM;
		else if(key == STATUSKEY_REJECTEDBYCOUNTRYMANAGER) return STATUSDESC_REJECTEDBYCOUNTRYMANAGER;
		else if(key == STATUSKEY_REJECTEDBYACCOUNTS) return STATUSDESC_REJECTEDBYACCOUNTS;
		else if(key == STATUSKEY_APPROVEDBYACCOUNTS) return STATUSDESC_APPROVEDBYACCOUNTS;
		else if(key == STATUSKEY_RETURN) return STATUSDESC_RETURN;
		else if(key == STATUSKEY_PAIDUNDERCOMPANYCARD) return STATUSDESC_PAIDUNDERCOMPANYCARD;
		else if(key == STATUSKEY_REJECTEDFORSALARYDEDUCTION) return STATUSDESC_REJECTEDFORSALARYDEDUCTION;
		else if(key == STATUSKEY_SENTTOSAP) return STATUSDESC_SENTTOSAP;
		else return STATUSDESC_LIQUIDATED;
	}
}
