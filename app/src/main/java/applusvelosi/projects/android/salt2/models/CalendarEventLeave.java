package applusvelosi.projects.android.salt2.models;

public class CalendarEventLeave extends CalendarEvent{

	private Leave leave;

	public CalendarEventLeave(Leave leave, int resColorID, CalendarEventDuration duration, boolean isHoliday, boolean hasLeave){
		super(leave.getTypeDescription(), resColorID, duration, (leave.getStatusID() == Leave.LEAVESTATUSAPPROVEDKEY), isHoliday, hasLeave);
		this.leave = leave;
	}

	public CalendarEventLeave(Leave leave, CalendarEventDuration duration, boolean isHoliday, boolean hasLeave){
		super(leave.getTypeDescription(), duration, isHoliday, hasLeave);
		this.leave = leave;
	}

	public Leave getLeave(){
		return leave;
	}
}
