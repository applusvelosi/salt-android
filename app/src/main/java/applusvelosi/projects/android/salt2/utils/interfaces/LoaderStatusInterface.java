package applusvelosi.projects.android.salt2.utils.interfaces;

public interface LoaderStatusInterface {

	void onLoadSuccess(Object result);
	void onLoadFailed(String failureMessage);
}
