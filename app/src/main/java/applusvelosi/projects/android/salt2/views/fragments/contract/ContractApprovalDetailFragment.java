package applusvelosi.projects.android.salt2.views.fragments.contract;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.SaltApplication;
import applusvelosi.projects.android.salt2.models.Document;
import applusvelosi.projects.android.salt2.models.contracttender.Contract;
import applusvelosi.projects.android.salt2.utils.FileManager;
import applusvelosi.projects.android.salt2.views.ContractApprovalDetailActivity;
import applusvelosi.projects.android.salt2.views.fragments.LinearNavActionbarFragment;

/**
 * Created by Velosi on 24/05/2017.
 */

public class ContractApprovalDetailFragment extends LinearNavActionbarFragment implements FileManager.AttachmentDownloadListener {

    private ContractApprovalDetailActivity activity;
    private RelativeLayout actionbarButtonBack;
    private TextView buttonApprove, buttonReject;
    private TextView fieldRefNum, fieldRequestorName, fieldCompany, fieldRequestDate, fieldRequestStatus, fieldContractType, fieldServiceLine, fieldProjLocation, fieldClient,
            fieldCompanyCurrency, fieldBillingCurrency, fieldProjValue, fieldProjValInEuro, fieldCapexRequired, fieldCapexReqValInEuro, fieldSubmissionDeadline, fieldEstStartDate,
            fieldGrossMargin, fieldStatus, fieldAwardProbability, fieldProjectDuration, fieldIsAgentRequired, fieldDocument, fieldCM, fieldRM, fieldRFM, fieldLM, fieldCFO, fieldEVP;
    private TextView fieldDateProcessedByCM, fieldDateProcessedByRFM, fieldDateProcessedByRM, fieldDateProcessedByLM, fieldDateProcessedByCFO, fieldDateProcessedByEVP;
    private TextView fieldApproverNote;
    private TextView fieldOperational, fieldHSQE, fieldHSQEResponsibilty, fieldCreditRisk, fieldPaymentBahavior, fieldRiskAssessment;
    private TextView fieldEquipment, fieldCompetentPersonnel, fieldInfrastructure, fieldTechnicalOther, fieldCheckCompetitor,  fieldEquipmentRequired, fieldEquipmentCost;
    private TextView fieldSelectionCriteria, fieldTeamBuildUp, fieldJobTobeStaffed, fieldProjRequirments, fieldValidCertificate, fieldTrainingSpecified, fieldAdditionalMembership;
    private TextView fieldBusinessCaseDoc;
    private TextView fieldAgentName, fieldAgentApproved, fieldAgentCommision, fieldJustification;
    private TextView fieldNegotiation, fieldDealBreakers, fieldCompetitors, fieldCompetingGrounds, fieldCompetitiveAdvantage, fieldExistingOrNew, fieldContractingStrategy, fieldInvoicing,
            fieldPaymentTerm, fieldPricing, fieldPricingAddition, fieldVisa, fieldImportExport, fieldCommercialOther;
    private TextView fieldLimitationLiability, fieldLiquidatedDamage, fieldConsequentialLoss, fieldIntellectualRights, fieldInsurance, fieldPersonnel, fieldEquipmentTax, fieldServices,
            fieldLegalOther, fieldOtherLocalTax, fieldPermanentEst, fieldBankPerformance, fieldNotes;

    private EditText etextDialogRejectReason, etextDialogApproveReason;
    private AlertDialog diaglogReject, dialogApprove;
    private StringBuilder stringNotes, approversNote;

    private int staffId;

    @Override
    protected RelativeLayout setupActionbar() {
        activity = (ContractApprovalDetailActivity)getActivity();
        RelativeLayout actionbarLayout = (RelativeLayout)linearNavFragmentActivity.getLayoutInflater().inflate(R.layout.actionbar_backonly, null);
        actionbarButtonBack = (RelativeLayout)actionbarLayout.findViewById(R.id.buttons_actionbar_back);
        ((TextView)actionbarLayout.findViewById(R.id.tviews_actionbar_title)).setText("Contract For Approval");
        actionbarButtonBack.setOnClickListener(this);
        return actionbarLayout;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_contractforapproval_detail, null);
        RelativeLayout dialogViewApprove, dialogViewReject;
        buttonApprove = (TextView)view.findViewById(R.id.buttons_contractforapprovaldetail_approve);
        buttonReject = (TextView)view.findViewById(R.id.buttons_contractforapprovaldetail_reject);
        fieldRefNum = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_refnumber);
        fieldRequestorName = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_requestorname);
        fieldCompany = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_company);
        fieldRequestStatus = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_requeststatus);
        fieldRequestDate = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_requestdate);
        fieldContractType = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_contracttype);
        fieldServiceLine = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_serviceline);
        fieldProjLocation = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_projectexecutionlocation);
        fieldClient = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_client);
        fieldCompanyCurrency = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_companycurrency);
        fieldBillingCurrency = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_billingcurrency);
        fieldProjValue = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_projectvalue);
        fieldProjValInEuro = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_projectvalueineuro);
        fieldCapexRequired = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_capexrequired);
        fieldCapexReqValInEuro = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_capexrequiredineuro);
        fieldSubmissionDeadline = (TextView)view.findViewById(R.id.views_contractforapprovaldetail_tendersubmissiondeadline);
        fieldEstStartDate = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_eststartdate);
        fieldGrossMargin = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_grossmargin);
        fieldStatus = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_status);
        fieldAwardProbability = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_probabilityofaward);
        fieldProjectDuration = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_projectduration);
        fieldIsAgentRequired = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_isagentrequired);
        fieldDocument = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_contractdoc);
        fieldCM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_cm);
        fieldRFM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_rfm);
        fieldRM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_rm);
        fieldLM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_lm);
        fieldCFO = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_cfo);
        fieldEVP = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_evp);
        fieldDateProcessedByCM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_processedbycm);
        fieldDateProcessedByRFM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_processedbyrfm);
        fieldDateProcessedByRM  = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_processedbyrm);
        fieldDateProcessedByLM = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_processedbylm);
        fieldDateProcessedByCFO = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_processedbycfo);
        fieldDateProcessedByEVP = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_processedbyevp);
        fieldApproverNote = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_approvernote);
        fieldOperational = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_operational);
        fieldHSQE = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_hsqe);
        fieldHSQEResponsibilty = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_resposibleforhse);
        fieldCreditRisk = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_creditrisk);
        fieldPaymentBahavior = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_paymentbehavior);
        fieldRiskAssessment = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_other);
        fieldEquipmentRequired = (TextView) view.findViewById(R.id.tviews_contractforapprovaldetail_equipmentrequired);
        fieldEquipment = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_equipment);
        fieldCompetentPersonnel = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_competentpersonnel);
        fieldInfrastructure = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_infrastructure);
        fieldTechnicalOther = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_otherresources);
        fieldCheckCompetitor = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_competitorsoffer);
        fieldEquipmentCost = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_isequipmentrequired);
        fieldSelectionCriteria = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_selectioncriteria);
        fieldTeamBuildUp = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_teambuiltup);
        fieldJobTobeStaffed = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_jobbestaffed);
        fieldProjRequirments = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_termsandconditions);
        fieldValidCertificate = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_availablecertificates);
        fieldTrainingSpecified = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_specifictraining);
        fieldAdditionalMembership = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_additionalmemberships);
        fieldBusinessCaseDoc = (TextView)view.findViewById(R.id.tviews_giftforapprovaldetail_businesscasedoc);
        fieldAgentName = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_agentname);
        fieldAgentApproved = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_agentapproved);
        fieldAgentCommision = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_agentcommision);
        fieldJustification = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_justification);
        fieldNegotiation = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_negotiations);
        fieldDealBreakers = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_dealbreakers);
        fieldCompetitors = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_competitors);
        fieldCompetingGrounds = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_competinggrounds);
        fieldCompetitiveAdvantage = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_competitiveadvantage);
        fieldExistingOrNew = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_existingornew);
        fieldContractingStrategy = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_contractingstrategy);
        fieldInvoicing = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_invoicing);
        fieldPaymentTerm = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_paymentterm);
        fieldPricing = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_pricing);
        fieldPricingAddition = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_pricingaddition);
        fieldVisa = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_visa);
        fieldImportExport = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_importexport);
        fieldCommercialOther = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_commercialother);
        fieldLimitationLiability = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_liabilitylimitation);
        fieldLiquidatedDamage = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_liquidateddamage);
        fieldConsequentialLoss = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_consequentialloss);
        fieldIntellectualRights = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_intellectualrights);
        fieldInsurance = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_insurance);
        fieldPersonnel = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_personaltax);
        fieldEquipmentTax = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_euipmenttax);
        fieldServices = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_servicestax);
        fieldLegalOther = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_othertax);
        fieldOtherLocalTax = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_otherlocaltaxes);
        fieldPermanentEst = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_permanentestablishment);
        fieldBankPerformance = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_bankguarantees);
        fieldNotes = (TextView)view.findViewById(R.id.tviews_contractforapprovaldetail_notes);

        staffId = app.getStaff().getStaffID();

        dialogViewApprove = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput, null);
        etextDialogApproveReason = (EditText)dialogViewApprove.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewApprove.findViewById(R.id.tviews_dialogs_textinput)).setText("Approver's Note");
        etextDialogApproveReason.setHint("Note");
        dialogApprove = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewApprove)
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_SUBMITTED) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                if(activity.contractHeader.getOfficeId() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: "+etextNote);
                            } else {
                                if(activity.contractHeader.getOfficeId() != 41)
                                    approversNote.append("CM: Approved");
                                else
                                    approversNote.append("CFO: Approved");
                            }
                            activity.contractHeader.setRequestStatusName("Approved by CM");
                            approversNote.append(";");
                            changeApprovalStatus(Contract.CONTRACTTENDERID_APPROVEDBYCM, "DateProcessedByCM", approversNote.toString());
                        } else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYCM && activity.contractHeader.getRfmId() == staffId) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("RFM: " + etextNote);
                            } else {
                                approversNote.append("RFM: Approved");
                            }
                            activity.contractHeader.setRequestStatusName("Approved by RFM");
                            approversNote.append(";");
                            changeApprovalStatus(Contract.CONTRACTTENDERID_APPROVEDBYRFM, "DateProcessedByRFM", approversNote.toString());
                        } else if ((activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYCM && activity.contractHeader.getRfmId() == 0) || activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYRFM) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                if (activity.contractHeader.getOfficeId() != 41)
                                    approversNote.append("RM: " + etextNote);
                                else
                                    approversNote.append("CEO: " + etextNote);
                            } else {
                                if (activity.contractHeader.getOfficeId() != 41)
                                    approversNote.append("RM: Approved");
                                else
                                    approversNote.append("CEO: Approved");
                            }
                            activity.contractHeader.setRequestStatusName("Approved by RM");
                            approversNote.append(";");
                            changeApprovalStatus(Contract.CONTRACTTENDERID_APPROVEDBYRM, "DateProcessedByRM", approversNote.toString());
                        } else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYRM && activity.contractHeader.getProjectValueEquiv() >= 500) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("LM: " + etextNote);
                            } else {
                                approversNote.append("LM: Approved");
                            }
                            activity.contractHeader.setRequestStatusName("Approved by LM");
                            approversNote.append(";");
                            changeApprovalStatus(Contract.CONTRACTTENDERID_APPROVEDBYLM, "DateProcessedByLM", approversNote.toString());
                        } else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYLM && activity.contractHeader.getProjectValueEquiv() >= 500) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("CFO: " + etextNote);
                            } else {
                                approversNote.append("CFO: Approved");
                            }
                            activity.contractHeader.setRequestStatusName("Approved by CFO");
                            approversNote.append(";");
                            changeApprovalStatus(Contract.CONTRACTTENDERID_APPROVEDBYCFO, "DateProcessedByCFO", approversNote.toString());
                        } else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYCFO && activity.contractHeader.getProjectValueEquiv() >= 500) {
                            if (etextDialogApproveReason.length() > 0) {
                                String etextNote = etextDialogApproveReason.getText().toString().trim();
                                approversNote.append("EVP: " + etextNote);
                            } else {
                                approversNote.append("EVP: Approved");
                            }
                            activity.contractHeader.setRequestStatusName("Approved by EVP");
                            approversNote.append(";");
                            changeApprovalStatus(Contract.CONTRACTTENDERID_APPROVEDBYEVP, "DateProcessedByEVP", approversNote.toString());
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialogViewReject = (RelativeLayout)LayoutInflater.from(linearNavFragmentActivity).inflate(R.layout.dialog_textinput,null);
        etextDialogRejectReason =  (EditText)dialogViewReject.findViewById(R.id.etexts_dialogs_textinput);
        ((TextView)dialogViewReject.findViewById(R.id.tviews_dialogs_textinput)).setText("Reason for Rejection");
        diaglogReject = new AlertDialog.Builder(linearNavFragmentActivity).setTitle(null).setView(dialogViewReject)
                .setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(etextDialogRejectReason.length() > 0){
                            String etextNote = etextDialogRejectReason.getText().toString();
                            if(activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_SUBMITTED) {
                                if (activity.contractHeader.getOfficeId() != 41) //office 41 is the headquarter
                                    approversNote.append("CM: " + etextNote);
                                else
                                    approversNote.append("CFO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Contract.CONTRACTTENDERID_REJECTEDBYCM, "DateProcessedByCM", approversNote.toString());
                            } else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYCM && activity.contractHeader.getRfmId() == staffId) {
                                approversNote.append("RFM: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Contract.CONTRACTTENDERID_REJECTEDBYRFM, "DateProcessedByRFM", approversNote.toString());
                            }else if ((activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYCM && activity.contractHeader.getRfmId() == 0) || activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYRFM) {
                                if(activity.contractHeader.getOfficeId() != 41)
                                    approversNote.append("RM: " + etextNote);
                                else
                                    approversNote.append("CEO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Contract.CONTRACTTENDERID_REJECTEDBYRM, "DateProcessedByRM", approversNote.toString());
                            }else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYRM) {
                                approversNote.append("LM: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Contract.CONTRACTTENDERID_REJECTEDBYLM, "DateProcessedByLM", approversNote.toString());
                            }else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYLM) {
                                approversNote.append("CFO: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Contract.CONTRACTTENDERID_REJECTEDBYCFO, "DateProcessedByCFO", approversNote.toString());
                            }else if (activity.contractHeader.getRequestStatusId() == Contract.CONTRACTTENDERID_APPROVEDBYCFO) {
                                approversNote.append("EVP: " + etextNote);
                                approversNote.append(";");
                                changeApprovalStatus(Contract.CONTRACTTENDERID_REJECTEDBYEVP, "DateProcessedByEVP", approversNote.toString());
                            }
                        }else
                            app.showMessageDialog(linearNavFragmentActivity, "Please put a reason for rejection");
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        return view;
    }

    private void changeApprovalStatus(final int statusId, final String keyForUpdatableDate, final String approverNote) {
        activity.startLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                String tempResult;
                try {
                    tempResult = app.onlineGateway.saveContract(activity.contractHeader.getJSONFromUpadatingContract(statusId, keyForUpdatableDate, approverNote, app), activity.contractHeader.getContractId());
                }catch (Exception e) {
                    tempResult = e.getMessage();
                }
                final String result = tempResult;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (!result.equals("OK")) {
                            activity.finishLoading(result);
                        } else {
                            activity.finishLoading();
                            Toast.makeText(activity, "Updated Successfully!", Toast.LENGTH_SHORT).show();
                            activity.finish();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        syncToServer();
    }

    private void syncToServer() {
        if (activity.contractHeader == null) {
            activity.startLoading();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Object tempResult;
                    try {
                        tempResult = app.onlineGateway.getContractDetail(activity.contractId);
                    } catch (Exception e) {
                        tempResult = e.getMessage();
                    }
                    final Object result = tempResult;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (result instanceof String ) {
                                activity.finishLoading();
                            } else {
                                try {
                                    activity.contractHeader = (Contract)result;
                                    updateView();
                                    activity.finishLoading();
                                } catch (Exception e) {
                                    activity.finishLoading(e.getMessage());
                                }
                            }
                        }
                    });
                }
            }).start();
        } else {
            try {
                updateView();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == actionbarButtonBack)
            linearNavFragmentActivity.onBackPressed();
        else if (v == buttonApprove) {
            etextDialogApproveReason.setText("");
            dialogApprove.show();
        } else if (v == buttonReject) {
            etextDialogRejectReason.setText("");
            diaglogReject.show();
        } else if (v == fieldDocument) {
            try {
                Document doc = activity.contractHeader.getContractDocument().get(0);
                int docID = doc.getDocID();
                int objectTypeID = doc.getObjectTypeID();
                int refID = doc.getRefID();
                String filename = doc.getDocName();
                activity.startLoading();
                app.fileManager.downloadDocument(app, docID, refID, objectTypeID, filename, this);
            }catch (Exception e) {
                e.printStackTrace();
                activity.finishLoading();
                app.showMessageDialog(activity, e.getMessage());
            }
        } else if (v == fieldBusinessCaseDoc) {
            try {
                Document doc = activity.contractHeader.getBusinessCaseDocument().get(0);
                int docID = doc.getDocID();
                int objectTypeID = doc.getObjectTypeID();
                int refID = doc.getRefID();
                String filename = doc.getDocName();
                activity.startLoading();
                app.fileManager.downloadDocument(app, docID, refID, objectTypeID, filename, this);
            }catch (Exception e) {
                e.printStackTrace();
                activity.finishLoading();
                app.showMessageDialog(activity, e.getMessage());
            }
        }
    }

    private void updateView() throws Exception {
        fieldRefNum.setText(activity.contractHeader.getContractNumber());
        fieldRequestorName.setText(activity.contractHeader.getRequestorName());
        fieldCompany.setText(activity.contractHeader.getOfficeName());
        fieldRequestDate.setText(activity.contractHeader.getDateSubmitted(app));
        fieldRequestStatus.setText(Contract.getStatusDescForKey(activity.contractHeader.getRequestStatusId()));
        fieldContractType.setText(Contract.getContractType(activity.contractHeader.getContractType()));
        fieldServiceLine.setText(Contract.getServiceLine(activity.contractHeader.getServiceLines()));
        fieldProjLocation.setText(activity.contractHeader.getProjectExcutionLocation());
        fieldClient.setText(activity.contractHeader.getEndClient());
        fieldCompanyCurrency.setText(activity.contractHeader.getCompanyCurrency());
        fieldBillingCurrency.setText(activity.contractHeader.getBillingCurrency());
        fieldEstStartDate.setText(activity.contractHeader.getDateStartDate(app));
        fieldSubmissionDeadline.setText(activity.contractHeader.getDateTenderSubmission(app));
        fieldProjValue.setText(String.valueOf(activity.contractHeader.getProjectValue()));
        fieldProjValInEuro.setText(String.valueOf(activity.contractHeader.getProjectValueEquiv()));
        fieldCapexRequired.setText(String.valueOf(activity.contractHeader.getCapexRequired()));
        fieldCapexReqValInEuro.setText(String.valueOf(activity.contractHeader.getCapexRequiredEquiv()));
        fieldGrossMargin.setText(SaltApplication.decimalFormat.format(activity.contractHeader.getGrossMargin()) + " %");
        fieldStatus.setText(Contract.getStatus(activity.contractHeader.getStatus()));
        fieldAwardProbability.setText(SaltApplication.decimalFormat.format(activity.contractHeader.getProbabilityOfAward()) + " %");
        fieldProjectDuration.setText(SaltApplication.decimalFormat.format(activity.contractHeader.getProjectDuration()) + " " + Contract.getDurationRange(activity.contractHeader.getProjectDurationRange()));
        fieldIsAgentRequired.setText(activity.contractHeader.isAgentRequired()?"Yes":"No");
        if (activity.contractHeader.getContractDocument().size() > 0) {
            fieldDocument.setText(activity.contractHeader.getContractDocument().get(0).getDocName());
            fieldDocument.setTextColor(linearNavFragmentActivity.getResources().getColor(R.color.orange_velosi));
            fieldDocument.setOnClickListener(ContractApprovalDetailFragment.this);
        } else {
            fieldDocument.setText("No Attachment");
        }

        fieldCM.setText(activity.contractHeader.getCmName());
        fieldRFM.setText(activity.contractHeader.getRfmName());
        fieldRM.setText(activity.contractHeader.getRmName());
        fieldLM.setText(activity.contractHeader.getLmName());
        fieldCFO.setText(activity.contractHeader.getCfoName());
        fieldEVP.setText(activity.contractHeader.getEvpName());

        fieldDateProcessedByCM.setText(activity.contractHeader.getDateProcessedByCm(app));
        fieldDateProcessedByRFM.setText(activity.contractHeader.getDateProcessedByRfm(app));
        fieldDateProcessedByRM.setText(activity.contractHeader.getDateProcessedByRm(app));
        fieldDateProcessedByLM.setText(activity.contractHeader.getDateProcessedByLm(app));
        fieldDateProcessedByCFO.setText(activity.contractHeader.getDateProcessedByCfo(app));
        fieldDateProcessedByEVP.setText(activity.contractHeader.getDateProcessedByEvp(app));


        fieldOperational.setText(activity.contractHeader.getOperational());
        fieldHSQE.setText(activity.contractHeader.getHsqe());
        fieldHSQEResponsibilty.setText(activity.contractHeader.getPrimaryResponsibility());
        fieldCreditRisk.setText(Contract.getCreditRisk(activity.contractHeader.getCreditRisk()));
        fieldPaymentBahavior.setText(Contract.getPaymentBehavior(activity.contractHeader.getPaymentBehavior()));
        fieldRiskAssessment.setText(activity.contractHeader.getRiskAssessmentOthers());

        fieldEquipment.setText(activity.contractHeader.getEquipmentAvailable() == 1?"Yes":"No");
        fieldCompetentPersonnel.setText(activity.contractHeader.getCompetentPersonnel() == 1?"Yes":"No");
        fieldInfrastructure.setText(activity.contractHeader.getInfrastructure() == 1?"Yes":"No");
        fieldTechnicalOther.setText(activity.contractHeader.getTechnicalAnalysisOthers());
        fieldCheckCompetitor.setText(activity.contractHeader.getCompetitorOffers());
        fieldEquipmentRequired.setText(activity.contractHeader.getEquipmentRequired());
        fieldEquipmentCost.setText(activity.contractHeader.getEquipmentCost() + " "+ activity.contractHeader.getBillingCurrency());

        fieldSelectionCriteria.setText(activity.contractHeader.getProjectManagerCriteria());
        fieldTeamBuildUp.setText(activity.contractHeader.getProjectTeamBuildup());
        fieldJobTobeStaffed.setText(activity.contractHeader.getJobStaff());
        fieldProjRequirments.setText(activity.contractHeader.getStaffAware());
        fieldValidCertificate.setText(activity.contractHeader.getCertificatesValid());
        fieldTrainingSpecified.setText(activity.contractHeader.getSpecificTraining());
        fieldAdditionalMembership.setText(activity.contractHeader.getAdditionalMembership());

        fieldAgentName.setText(activity.contractHeader.getAgentName());
        fieldAgentApproved.setText(activity.contractHeader.getAgentApproved() == 1 ? "Yes" : "No");
        fieldAgentCommision.setText(activity.contractHeader.getAgentCommission());
        fieldJustification.setText(activity.contractHeader.getJustification());

        if (activity.contractHeader.getBusinessCaseDocument().size() > 0) {
            fieldBusinessCaseDoc.setText(activity.contractHeader.getBusinessCaseDocument().get(0).getDocName());
            fieldBusinessCaseDoc.setTextColor(linearNavFragmentActivity.getResources().getColor(R.color.orange_velosi));
            fieldBusinessCaseDoc.setOnClickListener(ContractApprovalDetailFragment.this);
        } else {
            fieldBusinessCaseDoc.setText("No Attachment");
        }

        fieldNegotiation.setText(activity.contractHeader.getNegotiations());
        fieldDealBreakers.setText(activity.contractHeader.getDealBreakers());
        fieldCompetitors.setText(activity.contractHeader.getCompetitors());
        fieldCompetingGrounds.setText(activity.contractHeader.getCompetingGrounds());
        fieldCompetitiveAdvantage.setText(activity.contractHeader.getCompetingAdvantage());
        fieldExistingOrNew.setText(activity.contractHeader.getExistingOrNew());
        fieldContractingStrategy.setText(activity.contractHeader.getContractingStrategy());
        fieldInvoicing.setText(activity.contractHeader.getInvoicing());
        fieldPaymentTerm.setText(activity.contractHeader.getPaymentTerms());
        fieldPricing.setText(activity.contractHeader.getPricing());
        fieldPricingAddition.setText(activity.contractHeader.getPricingAdditional());
        fieldVisa.setText(activity.contractHeader.getVisa());
        fieldImportExport.setText(activity.contractHeader.getImportExport());
        fieldCommercialOther.setText(activity.contractHeader.getCommercialTermsOther());

        fieldLimitationLiability.setText(activity.contractHeader.getLimitationLiability());
        fieldLiquidatedDamage.setText(activity.contractHeader.getLiquidatedDamage());
        fieldConsequentialLoss.setText(activity.contractHeader.getConsequentialLoss());
        fieldIntellectualRights.setText(activity.contractHeader.getIntellectualPropertyRights());
        fieldInsurance.setText(activity.contractHeader.getInsurance());
        fieldPersonnel.setText(String.valueOf(activity.contractHeader.getPersonnel()));
        fieldEquipmentTax.setText(activity.contractHeader.getEquipment()+" %");
        fieldServices.setText(activity.contractHeader.getServices()+" %");
        fieldLegalOther.setText(activity.contractHeader.getLegalOthers()+ " % " + activity.contractHeader.getLegalOthers2());
        fieldOtherLocalTax.setText(activity.contractHeader.getOtherLocalTaxes()+" %");
        fieldPermanentEst.setText(activity.contractHeader.getPermanentEstablishment());
        fieldBankPerformance.setText(activity.contractHeader.getBankPerformanceGuarantee());
        fieldNotes.setText(activity.contractHeader.getNotes());

        stringNotes = new StringBuilder();
        approversNote = new StringBuilder(activity.contractHeader.getApproverNote());
        if(!activity.contractHeader.getApproverNote().isEmpty()) {
            String tempStrings[] = activity.contractHeader.getApproverNote().split(";");
            for (String str : tempStrings) {
                stringNotes.append(str);
                stringNotes.append(System.getProperty("line.separator"));
            }
        } else {
            stringNotes.append(activity.contractHeader.getApproverNote());
        }
        fieldApproverNote.setText(stringNotes.toString());

        approversNote = new StringBuilder(activity.contractHeader.getApproverNote());
        buttonApprove.setOnClickListener(ContractApprovalDetailFragment.this);
        buttonReject.setOnClickListener(ContractApprovalDetailFragment.this);
    }

    @Override
    public void onAttachmentDownloadFinish(File downloadedFile) {
        activity.finishLoading();
        app.fileManager.openDocument(linearNavFragmentActivity, downloadedFile);
    }

    @Override
    public void onAttachmentDownloadFailed(String errorMessage) {
        activity.finishLoading();
        app.showMessageDialog(linearNavFragmentActivity, errorMessage);
    }
    
}
