package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.contracttender.Contract;
import applusvelosi.projects.android.salt2.views.fragments.contract.ContractApprovalDetailFragment;

/**
 * Created by Velosi on 24/05/2017.
 */

public class ContractApprovalDetailActivity extends LinearNavFragmentActivity {

    public static final String INTENTKEY_CONTRACTID = "contractkey";
    public int contractId;
    public Contract contractHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contractId = getIntent().getExtras().getInt(INTENTKEY_CONTRACTID);
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ContractApprovalDetailFragment()).commit();
    }
}
