package applusvelosi.projects.android.salt2.models;

import org.json.JSONObject;

import java.io.Serializable;

public class Category implements Serializable{
	public static final int TYPE_MILEAGE = 1;
	public static final int TYPE_BUSINESSADVANCE = 4;
	public static final int TYPE_ASSET = 7;
	
	private int attendeeTypeID;
	private int categoryTypeID, categoryID;
	private float spendlimit;
	private String desc;
	private int currencyID;
	private String currThree;
	private String sapCode;

	public Category(JSONObject jsonCategory) throws Exception{
		attendeeTypeID = jsonCategory.getInt("Attendee");
		categoryTypeID = jsonCategory.getInt("CategoryTypeID");
		categoryID = jsonCategory.getInt("CategoryID");
		spendlimit = (float)jsonCategory.getDouble("SpendLimit");
		desc = jsonCategory.getString("Description");
		currencyID = jsonCategory.getInt("Currency");
		currThree = jsonCategory.getString("CurrencyAbb");
		sapCode = jsonCategory.getString("SAPCode");
	}

	public Category(){
		attendeeTypeID = 0;
		categoryTypeID = 0;
		categoryID = 0;
		spendlimit = 0;
		desc = "";
		currencyID = 0;
		currThree = "";
		sapCode = "";
	}


	public int getAttendeeTypeID(){ return attendeeTypeID; }
	public int getCategoryTypeID(){
		return categoryTypeID;
	}
	public int getCategoryID(){ return categoryID;  }
	public float getSpendLimit(){ return spendlimit; }
	public String getName(){ return desc; }
	public int getCurrencyID(){
		return currencyID;
	}
	public String getCurrencyName(){ return currThree; }
	public int getSapCode() { return Integer.parseInt(sapCode); }
}
