package applusvelosi.projects.android.salt2.utils.interfaces;

import android.content.Intent;

/**
 * Created by Velosi on 11/17/15.
 */
public interface CameraCaptureInterface {
    void onCameraCaptureSuccess(Intent data, String filePath);
    void onCameraCaptureFailed();
}
