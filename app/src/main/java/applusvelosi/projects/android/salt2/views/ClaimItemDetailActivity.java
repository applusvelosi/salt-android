package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.ClaimItemCarousel;

/**
 * Created by Velosi on 11/18/15.
 */
public class ClaimItemDetailActivity extends LinearNavFragmentActivity{
    public static final String INTENTKEY_CLAIMHEADER = "claimheader";
    public static final String INTENTKEY_CLAIMITEM = "claimitemkey";
    public ClaimHeader claimHeader;
    public ClaimItem claimItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try{
            claimHeader = (ClaimHeader)getIntent().getSerializableExtra(INTENTKEY_CLAIMHEADER);
            claimItem = (ClaimItem)getIntent().getSerializableExtra(INTENTKEY_CLAIMITEM);
//            getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, (claimItem.getCategoryTypeID()== Category.TYPE_MILEAGE)?new ClaimItemDetailMileageFragment():new ClaimItemDetailGenericFragment()).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimItemCarousel()).commit();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

}
