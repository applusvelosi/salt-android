package applusvelosi.projects.android.salt2.views;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import applusvelosi.projects.android.salt2.R;
import applusvelosi.projects.android.salt2.models.Category;
import applusvelosi.projects.android.salt2.models.Currency;
import applusvelosi.projects.android.salt2.models.Office;
import applusvelosi.projects.android.salt2.models.SapTaxCodeSettings;
import applusvelosi.projects.android.salt2.models.claimheaders.ClaimHeader;
import applusvelosi.projects.android.salt2.models.claimitems.ClaimItem;
import applusvelosi.projects.android.salt2.models.claimitems.Project;
import applusvelosi.projects.android.salt2.utils.interfaces.LoaderStatusInterface;
import applusvelosi.projects.android.salt2.utils.threads.ProjectsLoader;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs.ClaimItemEditFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs.ClaimItemEditMileageFragment;
import applusvelosi.projects.android.salt2.views.fragments.claims.claimitems.claimiteminputs.ClaimItemNewCurrency;

/**
 * Created by Velosi on 11/13/15.
 */
public class ManageClaimItemActivity extends LinearNavFragmentActivity  implements LoaderStatusInterface {
    public static final String INTENTKEY_CLAIMHEADER = "claimheaderkey";
    public static final String INTENTKEY_CLAIMITEM = "claimitemkey";
    public static final String INTENTKEY_CLAIMITEMATTACHMENT = "claimitemattachment";

    private ArrayList<Project> projects;
    private ArrayList<Category> categories;
    private ArrayList<Office> offices;
    private List<SapTaxCodeSettings> sapTaxCodes;
    private File attachment;
    public ClaimHeader claimHeader;
    public ClaimItem claimItem;


   @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);

       if (getIntent().hasExtra(INTENTKEY_CLAIMITEMATTACHMENT))
           attachment = (File) getIntent().getExtras().get(INTENTKEY_CLAIMITEM);

       claimHeader = (ClaimHeader) getIntent().getExtras().getSerializable(INTENTKEY_CLAIMHEADER);
       offices = new ArrayList<Office>();
       startLoading();
       new GetAllOffices(null).start();
       if (getIntent().hasExtra(INTENTKEY_CLAIMITEM)) {
           claimItem = (ClaimItem) getIntent().getExtras().get(INTENTKEY_CLAIMITEM);
           if (claimItem.getCategoryTypeID() == Category.TYPE_MILEAGE)
               getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimItemEditMileageFragment()).commit();
           else
               getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimItemEditFragment()).commit();
       } else {
           claimItem = new ClaimItem(claimHeader);
           claimItem.setLocalCurrency(new Currency(app.getStaffOffice().getBaseCurrencyID(), app.getStaffOffice().getBaseCurrencyName(), app.getStaffOffice().getBaseCurrencyThree()));

           getSupportFragmentManager().beginTransaction().replace(R.id.activity_view, new ClaimItemNewCurrency()).commit();
       }
       if (getProjects() == null)
           new Thread(new ProjectsLoader(this, this)).start();
   }

    public void updateAttachmentUri(File attachmentUri) {
        this.attachment = attachmentUri;
    }

    public File getAttachment() {
        return attachment;
    }

    public void updateProjectList(ArrayList<Project> projects){
        this.projects = new ArrayList<Project>();
        this.projects.addAll(projects);
        Collections.sort(projects, new Comparator<Project>() {
            @Override
            public int compare(Project lhs, Project rhs) {
                return lhs.getProjectName().toLowerCase().compareTo(rhs.getProjectName().toLowerCase());
            }
        });
    }

    public void updateSapTaxCodeList(List<SapTaxCodeSettings> sapTaxCodes){
        this.sapTaxCodes = new ArrayList<SapTaxCodeSettings>();
        this.sapTaxCodes.addAll(sapTaxCodes);
        Collections.sort(sapTaxCodes, new Comparator<SapTaxCodeSettings>() {
            @Override
            public int compare(SapTaxCodeSettings lhs, SapTaxCodeSettings rhs) {
                return lhs.getSapCode().toLowerCase().compareTo(rhs.getSapCode().toLowerCase());
            }
        });
    }

    public void updateCategoryList(ManageClaimItemActivity key, ArrayList<Category> categories){
        this.categories = new ArrayList<Category>();
        this.categories.addAll(categories);
        Collections.sort(categories, new Comparator<Category>() {
            @Override
            public int compare(Category lhs, Category rhs) {
                return lhs.getName().compareToIgnoreCase(rhs.getName().toString());
            }
        });
    }


    public ArrayList<Project> getProjects(){ return projects; }
    public ArrayList<Category> getCategories(){ return categories; }
    public ArrayList<Office> getOffices(){ return offices; }
    public List<SapTaxCodeSettings> getSapTaxCodes() { return sapTaxCodes; }

    public void reloadOffices(ItemStatusReloader reloader){
        (new GetAllOffices(reloader)).start();
    }

    @Override
    public void onLoadSuccess(Object result) {
        finishLoading();
        updateProjectList((ArrayList<Project>)result);
    }

    @Override
    public void onLoadFailed(String failureMessage) {
        finishLoading();
        finishLoading(failureMessage);
    }

    private class GetAllOffices extends Thread{
        ItemStatusReloader reloader;
        private Object tempResult;

        public GetAllOffices(ItemStatusReloader reloader){
            this.reloader = reloader;
        }

        @Override
        public void run() {
            try {
                tempResult = app.onlineGateway.getAllOffices();
            }catch(Exception e){
                e.printStackTrace();
                tempResult = e.getMessage();
            }

            final Object result = tempResult;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if(result instanceof String) {
                        if(reloader!=null)
                            reloader.onFailed(result.toString());
                        else
                            Toast.makeText(ManageClaimItemActivity.this, "Unable to retrieve offices " + result.toString(), Toast.LENGTH_SHORT).show();
                    }else{
                        offices.clear();
                        offices.addAll((ArrayList<Office>) result);
                        Collections.sort(offices, new Comparator<Office>() {
                            @Override
                            public int compare(Office lhs, Office rhs) {
                                return lhs.getName().compareTo(rhs.getName());
                            }
                        });
                        if(reloader !=null)
                            reloader.onSuccess();
                    }
                }
            });
        }
    }

    public interface ItemStatusReloader {
        void onSuccess();
        void onFailed(String message);
    }

}
